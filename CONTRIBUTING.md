# Introduction

The LBRY-GTK project has a lot to offer for anyone wanting to contribute.
The client uses GTK as the GUI toolkit. Larger part is written in C with some
Python still in the codebase. The client is built with the use of `GtkBuilder`
and glade files. We also use scripts to do various tasks such as appimage
and Windows package build and these are currently standard shell scripts.
On top of this we use markdown for our help pages and currently those are
largely out-of-date or work-in-progress. All of this listed here requires
attention and we very much appreciate anyone wanting to take
their time to contribute to our project.

We hope you find the relevant information in this document. Any comments or
questions you may have can be directed to our
[matrix channel](https://matrix.to/#/#LBRY-GTK:matrix.org). Keep in mind, this
document is made for **anyone** wanting to contribute to our project and 
therefore some of the things said in here may seem self-explanatory or as
something you are personally well aware of.

## Project Structure

The project is divided into several different parts as follows:

```
LBRY-GTK
├── Makefile        # Root makefile
├── share
│   └── lbry-gtk
│       ├── Glade   # Glade files used to build the client
│       ├── Help    # Help files (markdown)
│       ├── Json    # Json data files
│       └── Source  # Python source code
├── Source          # C source code
│   ├── Header      # Header files
│   └── Makefile    # Makefile specific to C source file compilation
└── Utilities       # Shell scripts
```

The root of the repository has a makefile, used to manage the entire project's
source compilation and installation. Currently we only have one additional
makefile found in C source directory but this will soon change when the C
source code is divided into categorised directories. The makefile in C source
directory is especially useful for those wanting to make changes in the source
code.

## Setup

Most of the development happens on GNU/Linux based distributions.
Other operating systems may still be used and anyone keen on cross-compatibility
is very much welcome to contribute to the project.

### Windows

Install [MSYS2](https://www.msys2.org/) on your Windows PC.

All of the required packages can be installed with following command:
```
pacman -S mingw-w64-ucrt-x86_64-gtk3 mingw-w64-ucrt-x86_64-python-gobject \
        mingw-w64-ucrt-x86_64-toolchain mingw-w64-ucrt-x86_64-jansson \
        mingw-w64-ucrt-x86_64-curl mingw-w64-ucrt-x86_64-md4c \
        mingw-w64-ucrt-x86_64-uncrustify mingw-w64-ucrt-x86_64-python-black \
        mingw-w64-ucrt-x86_64-imagemagick base-devel git
```

**NOTE:** You may at an option use something different from `ucrt`. Simply
replace `ucrt` in the package name with something different.

You might need some flags for the embedded python to work. These
flags are `PYTHONLEGACYWINDOWSDLLLOADING=1` and `PYTHONHOME=/ucrt64`. If you
are using something different from `ucrt` then you need to change `PYTHONHOME`
to different path accordingly.

### Debian/Ubuntu

Most of the packages are found in the standard repositories. To install these:
```
sudo apt install git make gcc libgtk-3-dev libjansson-dev libsqlite3-dev \
        libcurl4-gnutls-dev libmd4c-dev libmd4c-html0-dev python3-dev \
        black uncrustify
```

ImageMagick-7 is not found in the standard repositories. Neither is MD4C in
some older distributions. You might be able to use the 
`Utilities/UbuntuInstall.sh` setup script but we will cover source installing
these packages in this document regardless.

**IMPORTANT**: there may be other avenues to acquire both MD4C and 
ImageMagick-7 besides source installing. This document doesn't go
into detail what these avenues might be (or if there are any) and you have to 
find those yourself.
By rule of thumb it is always best to use the package manager of your 
distribution to install the correct software packages.

If MD4C library/development packages are not found in the standard
repositories, see [Building MD4C](#building-md4c) for instructions.

You still need ImageMagick of version 7 besides these packages. See 
[Building ImageMagick](#building-imagemagick) for instructions.

### Arch Linux

Install required packages:
```
sudo pacman -S git make gcc gtk3 jansson sqlite curl imagemagick pcre2 md4c \
        python uncrustify python-black
```

### Other requirements

To make changes to the glade files, you must install 
[Glade](https://infrastructure.pages.gitlab.gnome.org/glade-web/).
These glade files are used to build the client and any changes to the
client will therefore require the use of Glade.

The two programs needed for formatting are
[Black](https://github.com/psf/black) and
[Uncrustify](https://github.com/uncrustify/uncrustify). These also need to be
run each time you make a [pull request](#making-pull-requests).

Other optional tools may include markdown editor, json editor/tooling, git
frontend of any kind, etc. We don't have a say in these and therefore give you
free hands to do as you see fit.

### Test build

After the dependencies are installed, attempt to run `make` in the root of the
repository to build the project. If `make` were to exit successfully, you 
should be able to find the program in `bin/lbry-gtk`.

## Building MD4C

MD4C is fairly straight forward to build and install. In order to build MD4C,
you need to install `cmake` for your system. After you've made sure the package
is installed, you can run these commands to build and install MD4C:

```
git clone https://github.com/mity/md4c.git
cd md4c
mkdir build
cd build
cmake ..
make
sudo make install
```

## Building ImageMagick

ImageMagick requires some dependencies to be built successfully. Run this to
install these packages:

```
sudo apt install libjpeg-dev libwebp-dev build-essential
```

After all required dependencies are installed, you can build and install
ImageMagick with these commands:

```
git clone https://github.com/ImageMagick/ImageMagick.git ImageMagick-7.1.1
cd ImageMagick-7.1.1
./configure --prefix=/usr --sysconfdir=/etc \
    --enable-shared \
    --disable-static \
    --enable-hdri \
    --with-jxl \
    --with-openjp2 \
    --with-rsvg \
    --with-webp \
    --with-xml \
    --without-autotrace \
    --without-djvu \
    --without-dps \
    --without-fftw \
    --without-fpx \
    --without-gcc-arch \
    --without-gslib \
    --without-gvc \
    --without-magick-plus-plus \
    --without-modules \
    --without-openexr \
    --without-perl \
    --without-tiff \
    --without-wmf
make
sudo make install
```

# Contributing

When contributing, remember these few guidelines:

- Use PascalCase for source code.
- Do not use shortened variable/function names.
- Use Glade for designing the GUI.
- When working on the GUI, use the Glade files as much as possible.
- When creating containers, use GtkBox instead of GtkGrid as much as possible.
- Use the listed [docs](#docs).

## C Specific

Rules for files:
- Global names have to start with the file name (variables, functions, structs, etc.).
- Use single line comments: `// The comment you made..`
- Header files go to [header directory](Source/Header).
- Each C source file should have a corresponding header file with same name.
- Line length at maximum is 80 characters, with tabs taking 4 spaces.
- Always format the code for pull request with the command specified below.

For instructions, see the [Makefile](Source/Makefile)

In short, all the commands you need/might need are:
- `make` to build the project,
- `make test` to test the functionality,
- `make format` to format the code before commit and
- `make clean` to clean up all files created by `make`.

## Docs

Collection of documentation for different dependencies:

- [GTK3](https://docs.gtk.org/gtk3/)
- [ImageMagick](https://imagemagick.org/script/magick-wand.php)
- [Jansson](https://jansson.readthedocs.io/)
- [LBRYNet](https://lbry.tech/api/sdk)
- [MD4C](https://github.com/mity/md4c/wiki)
- [PCRE2](https://pcre.org/current/doc/html/)
- [Pygobject](https://pygobject.readthedocs.io)
- [SQLite3](https://www.sqlite.org/docs.html)

For Python, you could see the [Official Python documentation](https://docs.python.org/3.11/) 
and specifically for C Python extensions the [C API](https://docs.python.org/3.11/c-api/).

# Using git

Fork the repository from 
[upstream repository](https://codeberg.org/MorsMortium/LBRY-GTK).
To do this, you need to have an existing codeberg.org account. After this you 
can clone your newly forked repository and add the upstream repository 
to your local PC with these commands:

```
git clone https://codeberg.org/your-name/LBRY-GTK
cd LBRY-GTK/
git remote add upstream https://codeberg.org/MorsMortium/LBRY-GTK
```

## Making changes

To start off, you should create a new branch for your commits. To do this,
you need to issue command

```
git branch your-branch
```

where you want to give your branch a meaningful name, such as 
*toppanel-button-fix*.

Then switch to this branch with:

```
git checkout your-branch
```

From here you can use the standard `git add` and `git commit` commands after
editing the source code files.
Please refer to [git documentation](https://git-scm.com/docs) for more
information on different commands you can issue.

## Making pull requests

Double check the client works, that you've built the client on most recent
code, and that the code changes and additions are all part of your commits.
Also make sure to format the code with required tools and make sure there
are no irregularities in the code. Occasionally the formatting tools may
mangle something and you need to intervene to make the code look readable 
and clean.

You can only make a pull request on codeberg.org. To do this, you can
go to your repository and click the "New Pull Request" icon or go to the 
[upstream repository](https://codeberg.org/MorsMortium/LBRY-GTK/pulls) and
click the "New Pull Request" button from there.

From there you should select the branch you want to make a pull request for
(i.e. MorsMortium:CRewrite or MorsMortium:main) and the branch you want to
merge.

Remember to add a description about your pull request and remember to prefix 
the title with "WIP:" if you are still working on your code.
