################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2023 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import gi, threading, queue, copy, datetime, copy, json, sys

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gdk, GLib

from Source import Settings
from Source import SelectUtil
from Source.Publication import Publication
from Source.NewPublication import NewPublication
from Source.Select import Select
from Source.PyGObjectCAPI import PyGObjectCAPI
from CRewrite import DateTime, Global, Icons, Language, List, Move, Order
from CRewrite import Places, Popup, Preferences, Replace, State, Status, Tabs
from CRewrite import Tag, Threads

CAPI = PyGObjectCAPI()


class FakeItem:
    def is_visible(self):
        return True

    def get_realized(self):
        return True

    def get_name(self):
        return "FakeItem"

    def set_active(self, Discard):
        return


class Page:
    MainSpaceHeight, MainSpaceWidth = 0, 0

    def __init__(self, *Args):
        (
            FirstPage,
            NoStartup,
            self.AddPage,
            self.Balance,
            self.Signing,
            self.CloseButton,
            Main,
            Default,
        ) = Args
        Builder = CAPI.ToObject(Global.Builder)
        Window = CAPI.ToObject(Global.Window)
        with open(Places.JsonDir + "Languages.json") as LanguageFile:
            self.Languages = json.load(LanguageFile)
        Builder.add_from_file(Places.GladeDir + "Page.glade")
        Builder.connect_signals(self)
        self.Page = Builder.get_object("Page")
        self.Title = Builder.get_object("Title")
        self.Title.set_text("")
        self.MainSpace = Builder.get_object("MainSpace")
        self.Spaces = {
            "Content": Builder.get_object("ContentSpace"),
            "Wallet": Builder.get_object("WalletSpace"),
            "File": Builder.get_object("FileSpace"),
            "Notification": Builder.get_object("NotificationSpace"),
        }

        self.NoContentNotice = Builder.get_object("NoContentNotice")
        self.DateTimeBox = Builder.get_object("DateTimeBox")
        self.OrderBox = Builder.get_object("OrderBox")
        self.Inequality = Builder.get_object("Inequality")
        self.Text = Builder.get_object("Text")
        self.Channel = Builder.get_object("Channel")
        self.ClaimType = Builder.get_object("ClaimType")
        self.StreamType = Builder.get_object("StreamType")
        self.DateType = Builder.get_object("DateType")
        self.DateLabel = Builder.get_object("DateLabel")
        self.DateInterval = Builder.get_object("DateInterval")
        self.DateValue = Builder.get_object("DateValue")
        self.Grid = Builder.get_object("Grid")
        self.ScrollDown = Builder.get_object("ScrollDown")
        self.ScrollUp = Builder.get_object("ScrollUp")
        self.MoveLeft = Builder.get_object("MoveLeft")
        self.MoveRight = Builder.get_object("MoveRight")
        self.MoveUp = Builder.get_object("MoveUp")
        self.MoveDown = Builder.get_object("MoveDown")
        self.ListOpenOnCurrent = Builder.get_object("ListOpenOnCurrent")
        self.ListOpenOnNew = Builder.get_object("ListOpenOnNew")
        self.Search = Builder.get_object("Search")
        self.SelectNext = Builder.get_object("SelectNext")
        self.SelectPrevious = Builder.get_object("SelectPrevious")
        self.Apply = Builder.get_object("Apply")
        self.DateTypeActive = Builder.get_object("DateTypeActive")
        self.DateIntervalActive = Builder.get_object("DateIntervalActive")
        self.DateValueActive = Builder.get_object("DateValueActive")
        self.InequalityActive = Builder.get_object("InequalityActive")
        self.ClaimTypeActive = Builder.get_object("ClaimTypeActive")
        self.StreamTypeActive = Builder.get_object("StreamTypeActive")
        self.Advanced = Builder.get_object("Advanced")
        self.TagSelect = Builder.get_object("TagSelect")
        self.SimpleDateBox = Builder.get_object("SimpleDateBox")
        self.WalletGrid = Builder.get_object("WalletGrid")

        # Allocates space for new tab
        TabPointer = Tabs.Create()
        self.Stater = State.Create(TabPointer)

        for Child in self.WalletGrid.get_children():
            if isinstance(Child, Gtk.Image):
                Child.set_from_pixbuf(CAPI.ToObject(Icons.Get()["LBCLabel"]))
        self.LanguagerList = ["any_languages", "all_languages", "not_languages"]
        self.TaggerList = ["any_tags", "all_tags", "not_tags", "claim_ids"]
        self.TaggerList.extend(["channel_ids", "not_channel_ids"])
        self.TaggerList.extend(["any_languages", "all_languages"])
        self.TaggerList.extend(["not_languages", "stream_types"])
        self.TaggersDict, self.Taggers, self.Languagers = {}, [], []
        self.FlowBoxes = []
        Tags = []
        for Index in self.TaggerList:
            self.TaggersDict[Index] = [Builder.get_object(Index)]
            if Index in self.LanguagerList:
                Object = Language.Create(TabPointer)
                NewTag = Object["TaggerPointer"]
                self.Languagers.append(Object)
                LanguageObject = CAPI.ToObject(Object["Language"])
                self.TaggersDict[Index][0].pack_start(
                    LanguageObject, True, True, 0
                )
            else:
                Object = Tag.Create(True, TabPointer, self.AddPage)
                NewTag = Object["Pointer"]
                self.Taggers.append(Object)
                TagObject = CAPI.ToObject(Object["Tag"])
                self.TaggersDict[Index][0].pack_start(TagObject, True, True, 0)
            self.FlowBoxes.append(Object)
            self.TaggersDict[Index].append(Object)
            Tags.append(NewTag)

        CAPI.ToObject(self.TaggersDict["stream_types"][1]["Entry"]).hide()
        self.on_StreamType_changed(self.StreamType)

        self.DateTimer = DateTime.Create()
        DateTimeObject = CAPI.ToObject(self.DateTimer["DateTime"])
        self.DateTimeBox.pack_start(DateTimeObject, True, True, 0)
        CAPI.ToObject(self.DateTimer["DateTime"]).set_no_show_all(True)
        self.DateUse = CAPI.ToObject(self.DateTimer["DateUse"])
        self.DateHour = CAPI.ToObject(self.DateTimer["Hour"])
        self.DateMinute = CAPI.ToObject(self.DateTimer["Minute"])
        self.DateSecond = CAPI.ToObject(self.DateTimer["Second"])
        DateReset = CAPI.ToObject(self.DateTimer["DateReset"])
        DateActive = CAPI.ToObject(self.DateTimer["DateActive"])
        DateUseActive = CAPI.ToObject(self.DateTimer["DateUseActive"])

        self.Orderer = Order.Create()
        OrderObject = CAPI.ToObject(self.Orderer["Order"])
        self.OrderBox.pack_start(OrderObject, True, True, 0)

        self.on_DateType_changed(self.DateType)

        self.Publicationer = Publication(
            self.Title,
            self.MainSpace,
            self.AddPage,
            TabPointer,
        )

        SpacesList = []
        for Item in self.Spaces.values():
            SpacesList.append(CAPI.AsVoidPointer(Item.__gpointer__))
        self.Lister = List.Create(
            CAPI.AsVoidPointer(self.NoContentNotice.__gpointer__),
            [
                CAPI.AsVoidPointer(Builder.get_object("Total").__gpointer__),
                CAPI.AsVoidPointer(
                    Builder.get_object("Available").__gpointer__
                ),
                CAPI.AsVoidPointer(Builder.get_object("Reserved").__gpointer__),
                CAPI.AsVoidPointer(Builder.get_object("Claims").__gpointer__),
                CAPI.AsVoidPointer(Builder.get_object("Supports").__gpointer__),
                CAPI.AsVoidPointer(Builder.get_object("Tips").__gpointer__),
            ],
            CAPI.AsVoidPointer(self.MainSpace.__gpointer__),
            CAPI.AsVoidPointer(self.Grid.__gpointer__),
            SpacesList,
            CAPI.AsVoidPointer(self.Text.__gpointer__),
            CAPI.AsVoidPointer(self.Title.__gpointer__),
            CAPI.AsVoidPointer(self.Channel.__gpointer__),
            CAPI.AsVoidPointer(self.ClaimType.__gpointer__),
            CAPI.AsVoidPointer(self.DateType.__gpointer__),
            CAPI.AsVoidPointer(self.Inequality.__gpointer__),
            CAPI.AsVoidPointer(self.Advanced.__gpointer__),
            Tags,
            self.DateTimer["Pointer"],
            self.Orderer["Pointer"],
            self.AddPage,
            TabPointer,
        )
        TitlePointer = CAPI.AsVoidPointer(self.Title.__gpointer__)
        self.Statuser = Status.Create(TitlePointer, TabPointer)

        self.Settingser = Settings.Settings(
            self.Title, self.Stater, Main, Default, TabPointer
        )

        self.NewPublicationer = NewPublication(self.Title, TabPointer)

        StatusObject = CAPI.ToObject(self.Statuser["Status"])
        self.Threader = Threads.Create(
            self.NewPublicationer,
            self.Publicationer,
            self.Settingser,
            TabPointer,
        )

        WidgetList = [self.Publicationer.Publication, self.Spaces["Content"]]
        WidgetList.extend([self.Spaces["Wallet"], self.Spaces["File"]])
        WidgetList.extend([self.Spaces["Notification"]])
        WidgetList.extend([self.NewPublicationer.NewPublication])
        WidgetList.extend([self.Settingser.Settings, StatusObject])
        for Index in range(len(WidgetList)):
            WidgetList[Index] = CAPI.AsVoidPointer(
                WidgetList[Index].__gpointer__
            )
        WidgetList.extend([self.Publicationer.Documenter.Document])
        NameList = ["Publication", "ContentList", "WalletList", "FileList"]
        NameList.extend(["NotificationList", "NewPublication", "Settings"])
        NameList.extend(["Status", "Document"])
        Replace.Init(
            WidgetList,
            NameList,
            CAPI.AsVoidPointer(self.MainSpace.__gpointer__),
            TabPointer,
        )

        Widgets, Fake = [], FakeItem()
        OrdererNames = ["UseOrdering", "UseOrderingActive", "OrderBy"]
        OrdererNames.extend(["OrderByActive", "Direction", "DirectionActive"])
        for OrdererName in OrdererNames:
            Widget = CAPI.ToObject(self.Orderer[OrdererName])
            Widgets.append(Widget)
        Widgets.extend([self.DateType, self.DateTypeActive, self.DateInterval])
        Widgets.extend([self.DateIntervalActive, self.DateValue])
        Widgets.extend([self.DateValueActive, self.Inequality])
        Widgets.extend([self.InequalityActive, self.DateTimeBox])
        Widgets.extend([DateActive, self.DateHour, self.DateMinute])
        Widgets.extend([self.DateSecond, DateReset])
        Widgets.extend([self.DateUse, DateUseActive, self.Text])
        Widgets.extend([self.Channel, self.ClaimType, self.ClaimTypeActive])
        for Index in self.TaggerList:
            if Index != "stream_types":
                Widgets.append(self.TaggersDict[Index][0])
        Widgets.extend([self.StreamType, self.StreamTypeActive, self.Search])
        Widgets.extend([self.Apply, Fake])

        Daters = [self.DateTimer]
        Items = SelectUtil.PrepareWidgets(
            Widgets, self.Taggers, self.Languagers, Daters
        )
        Items[4][-1] = self.StreamTyperHelper
        self.Selector = Select(*Items)

        self.TabPointer = TabPointer

        if NoStartup:
            LBRYSettings = json.loads(Preferences.Get())
            Server = LBRYSettings["Preferences"]["Session"]["Server"]
            Status.SetServer(self.Statuser["Pointer"], Server)
            Status.SetLBRYGTK(self.Statuser["Pointer"], True)

        Window.show_all()

        if not isinstance(FirstPage, str):
            Name, Data = FirstPage

            # If Data is int type, it means we have a pointer to arguments
            if not isinstance(Data, int):
                # Attempt to fix the type of Data[3] if it's wrong type
                if len(Data) == 4 and not isinstance(Data[3], str):
                    if not Data[3]:
                        del Data[3]
                    else:
                        Data[3] = json.dumps(Data[3])
            State.Import(Name, Data, TabPointer)

    def StopScroll(self, Widget, Event):
        Widget.emit_stop_by_name("scroll-event")

    def StreamTyperHelper(self, Button):
        if Button == 1:
            self.StreamType.popup()
        else:
            Tag.KeybindHelper(
                self.TaggersDict["stream_types"][1]["Pointer"], Button
            )

    def Scroll(self, Direction=1):
        Adjustment = self.MainSpace.get_vadjustment()
        if self.Settingser.Settings.get_realized():
            Children = self.Settingser.Settings.get_children()
            Child = Children[1]
            if isinstance(Children[0], Gtk.Notebook):
                Child = Children[0]
            Page = Child.get_nth_page(Child.get_current_page())
            Adjustment = Page.get_vadjustment()
        Adjustment.set_value(
            Adjustment.get_value() + Direction * Adjustment.get_step_increment()
        )

    def on_ScrollDown_activate(self, Widget):
        self.Scroll()

    def on_MainSpace_destroy(self, Widget):
        for Space in self.Spaces:
            self.Spaces[Space].destroy()
        self.NoContentNotice.destroy()

    def on_ScrollUp_activate(self, Widget):
        self.Scroll(-1)

    def on_TagSelect_activate(self, Widget):
        if self.Grid.get_realized() or self.NoContentNotice.get_realized():
            if 15 < self.Selector.CurrentItem < 26:
                Tag.Select(
                    self.FlowBoxes[self.Selector.CurrentItem - 16]["Pointer"]
                )

    def GetFlowbox(self):
        if self.Grid.get_realized() or self.NoContentNotice.get_realized():
            if 15 < self.Selector.CurrentItem < 26:
                return self.FlowBoxes[self.Selector.CurrentItem - 16]["FlowBox"]
            if self.Grid.get_realized():
                return CAPI.AsVoidPointer(self.Grid.__gpointer__)
        return False

    def on_MoveLeft_activate(self, Widget):
        FlowBoxPointer = self.GetFlowbox()
        if FlowBoxPointer:
            Move.FlowBoxLeftRight(FlowBoxPointer, -1)

    def on_MoveRight_activate(self, Widget):
        FlowBoxPointer = self.GetFlowbox()
        if FlowBoxPointer:
            Move.FlowBoxLeftRight(FlowBoxPointer, 1)

    def on_MoveDown_activate(self, Widget):
        FlowBoxPointer = self.GetFlowbox()
        if FlowBoxPointer:
            Move.FlowBoxUpDown(FlowBoxPointer, 1)

    def on_MoveUp_activate(self, Widget):
        FlowBoxPointer = self.GetFlowbox()
        if FlowBoxPointer:
            Move.FlowBoxUpDown(FlowBoxPointer, -1)

    def on_ListOpenOnCurrent_activate(self, Widget):
        if (
            self.Grid.get_realized() or self.NoContentNotice.get_realized()
        ) and -1 < self.Selector.CurrentItem < len(self.Selector.Widgets) - 1:
            self.Selector.Activate(0)
        elif self.Grid.get_realized():
            try:
                Child = (
                    self.Grid.get_selected_children()[0]
                    .get_children()[0]
                    .get_children()[0]
                    .get_children()[0]
                )
                Event = Gdk.Event.new(Gdk.EventType.BUTTON_PRESS)
                Event.button = Gdk.BUTTON_PRIMARY
                Event.window = Child.get_window()
                Child.event(Event)
            except:
                pass

    def on_ListOpenOnNew_activate(self, Widget):
        if (
            self.Grid.get_realized() or self.NoContentNotice.get_realized()
        ) and -1 < self.Selector.CurrentItem < len(self.Selector.Widgets) - 1:
            self.Selector.Activate(1)
        elif self.Grid.get_realized():
            try:
                Child = (
                    self.Grid.get_selected_children()[0]
                    .get_children()[0]
                    .get_children()[0]
                    .get_children()[0]
                )
                Event = Gdk.Event.new(Gdk.EventType.BUTTON_PRESS)
                Event.button = Gdk.BUTTON_MIDDLE
                Event.window = Child.get_window()
                Child.event(Event)
            except:
                pass

    def on_StreamType_changed(self, Widget):
        Entry = CAPI.ToObject(self.TaggersDict["stream_types"][1]["Entry"])
        Entry.set_text(Widget.get_active_text().lower())

    def on_SimpleDate_changed(self, Widget=""):
        self.Inequality.set_active(1)
        DateValue = self.DateValue.get_active()
        if self.DateInterval.get_active() == 0:
            DateTime.OnDateResetButtonPressEvent(self.DateTimer["Pointer"])
            self.DateSecond.set_value(0)
            if -1 < DateValue:
                self.DateMinute.set_value(0)
            if 0 < DateValue:
                self.DateHour.set_value(0)
            Date = datetime.date.today()
            if 1 < DateValue:
                Date -= datetime.timedelta(days=Date.weekday())
            if 2 < DateValue:
                Date -= datetime.timedelta(days=Date.day - 1)
            if 3 < DateValue:
                Date = datetime.date(Date.year, 1, 1)
            Calendar = CAPI.ToObject(self.DateTimer["Calendar"])
            Calendar.select_day(Date.day)
            Calendar.select_month(Date.month - 1, Date.year)
        else:
            DateTime.OnDateResetButtonPressEvent(self.DateTimer["Pointer"])
            if 0 == DateValue:
                self.DateHour.spin(Gtk.SpinType.STEP_BACKWARD, 1)
            Date = datetime.date.today()
            if 1 == DateValue:
                Date -= datetime.timedelta(days=1)
            if 2 == DateValue:
                Date -= datetime.timedelta(days=7)
            if 3 == DateValue:
                if Date.month == 1:
                    Date = datetime.date(Date.year, 12, Date.day)
                else:
                    Date = datetime.date(Date.year, Date.month - 1, Date.day)
            if 4 == DateValue:
                Date = datetime.date(Date.year - 1, Date.month, Date.day)
            Calendar.select_day(Date.day)
            Calendar.select_month(Date.month - 1, Date.year)

    def on_DateType_changed(self, Widget):
        Children = self.DateTimeBox.get_children()
        self.DateUse.set_active(Widget.get_active() == 1)
        self.DateLabel.set_visible(Widget.get_active() != 0)
        if Widget.get_active() == 0:
            self.DateTimeBox.hide()
            self.SimpleDateBox.hide()
        elif Widget.get_active() == 1:
            self.DateTimeBox.hide()
            self.SimpleDateBox.show()
            self.on_SimpleDate_changed()
        elif Widget.get_active() == 2:
            self.Inequality.set_active(0)
            self.DateTimeBox.show()
            self.SimpleDateBox.hide()
            DateTime.OnDateResetButtonPressEvent(self.DateTimer["Pointer"])

    def on_MainSpace_draw(self, Widget, event):
        OldHeight, OldWidth = self.MainSpaceHeight, self.MainSpaceWidth
        self.MainSpaceHeight = self.MainSpace.get_allocated_height()
        self.MainSpaceWidth = self.MainSpace.get_allocated_width()
        Space = Replace.GetSpace(self.TabPointer)
        if (
            OldHeight < self.MainSpaceHeight or OldWidth < self.MainSpaceWidth
        ) and Space.endswith("List"):
            # Started check moved to GenericLoaded
            self.Lister.GenericLoaded()

    def on_MainSpace_edge_overshot(self, Widget, Position):
        self.on_MainSpace_edge_reached(Widget, Position)

    def on_MainSpace_edge_reached(self, Widget, Position):
        Space = Replace.GetSpace(self.TabPointer)
        if Space.endswith("List") and Position == Gtk.PositionType.BOTTOM:
            self.Lister.UpdateMainSpace()

    def on_Search_button_press_event(self, Widget, Event):
        if Event.button == Gdk.BUTTON_PRIMARY:
            self.Threader.LibrarySearchThread()
        elif Event.button == Gdk.BUTTON_MIDDLE:
            self.AddPage(".", "", ["Library Search", []])

    def on_Apply_button_press_event(self, Widget, Event):
        LBRYSettings = json.loads(Preferences.Get())
        ShowMature = LBRYSettings["Preferences"]["ShowMature"]
        LBRYGTKSettings = LBRYSettings["Preferences"]
        RawTime = DateTime.GetTime(self.DateTimer["Pointer"], 0)
        Time, TimeText = str(RawTime), "equals "
        Texts = ["less than", "more than", "less or equals", "more or equals"]
        Times = ["<", ">", "<=", ">="]
        if Time != "-1":
            Inequality = self.Inequality.get_active()
            if Inequality != 2:
                if 2 < Inequality:
                    Inequality -= 1
                TimeText, Time = Texts[Inequality], Times[Inequality] + Time

        Data = {"text": self.Text.get_text(), "timestamp": Time}
        Data["channel"] = self.Channel.get_text()

        if not ShowMature:
            LBRYGTKSettings["TagsNot"].append("mature")

        for Index in self.TaggersDict:
            Language = False
            try:
                Tags = Tag.GetTags(self.TaggersDict[Index][1]["TaggerPointer"])
                Language = True
            except:
                Tags = Tag.GetTags(self.TaggersDict[Index][1]["Pointer"])

            Data[Index] = copy.deepcopy(Tags)
            if Index == "not_tags":
                for GotTag in Data[Index]:
                    if GotTag in LBRYGTKSettings["TagsNot"]:
                        Data[Index].remove(GotTag)
            if Index == "not_channel_ids":
                for GotTag in Data[Index]:
                    if GotTag in LBRYGTKSettings["ChannelsNot"]:
                        Data[Index].remove(GotTag)
            if Language:
                for SubIndex in range(len(Data[Index])):
                    Data[Index][SubIndex] = Data[Index][SubIndex].split("/")[0]

        OrderBy = Order.Get(self.Orderer["Pointer"])
        if OrderBy:
            Data["order_by"] = [OrderBy]
        else:
            Data["order_by"] = []

        ClaimType = self.ClaimType.get_active_text().lower()

        if ClaimType != "all":
            Data["claim_type"] = ClaimType

        Title = ""
        if Data["text"] != "":
            Title = "Text: " + Data["text"] + ", "
        if Data["timestamp"] != "-1":
            Time = str(datetime.datetime.fromtimestamp(RawTime))
            Title += "Time: " + TimeText + Time + ", "
        if "order_by" in Data:
            Title += Order.Print(self.Orderer["Pointer"])
        if "claim_type" in Data:
            Title += "Type: %s, " % Data["claim_type"]

        for Index in self.TaggersDict:
            if Data[Index] != []:
                NewIndex = Index.replace("_", " ").title()
                NewData = copy.deepcopy(Data[Index])
                if Index in self.LanguagerList:
                    for ItemIndex in range(len(NewData)):
                        Item = NewData[ItemIndex]
                        for Language in self.Languages:
                            if Language[0] == Item:
                                NewData[ItemIndex] = (
                                    Language[0] + "/" + Language[1]
                                )
                                break
                Title += NewIndex + ": " + ", ".join(NewData) + ", "

        if Data["channel"] != "":
            Title += "Channel: " + Data["channel"] + ", "

        Title = "Advanced Search - " + Title[:-2]
        Args = [Title, None, None, json.dumps(Data)]
        if Event.button == Gdk.BUTTON_PRIMARY:
            State.Import("Advanced Search", Args, self.TabPointer)
        elif Event.button == Gdk.BUTTON_MIDDLE:
            self.AddPage(".", "", State.Export("Advanced Search", Args))

    def on_SelectNext_activate(self, Widget):
        if self.Grid.get_realized() or self.NoContentNotice.get_realized():
            self.Selector.Forward()

    def on_SelectPrevious_activate(self, Widget):
        if self.Grid.get_realized() or self.NoContentNotice.get_realized():
            self.Selector.Backward()

    def NoToggleBefore(self, Widget):
        Widget.set_name(str(Widget.get_active()))

    def NoToggleAfter(self, Widget):
        Widget.set_active(Widget.get_name() == "True")

    def on_Advanced_activate(self, Widget):
        Widget.get_children()[0].set_visible(not Widget.get_expanded())
