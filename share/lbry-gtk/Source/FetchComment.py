################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2023 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import gi, json, queue, threading, concurrent.futures, time

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GLib

from Source.Comment import Comment
from CRewrite import Comments, Channel, Popup


class FetchComment:
    def __init__(self, *args):
        (
            self.TabPointer,
            self.AddPage,
            self.BottomStack,
        ) = args
        self.Queues = []
        self.KillingAll = False

    def SingleComment(self, *Args):
        Comment(
            *Args,
            self.SingleComment,
            self.TabPointer,
            self.AddPage,
            self.BottomStack,
        )

    def DisplayPrice(self, BottomStack, ChannelID, CommentServer):
        CommentSettings = Comments.SettingGet(ChannelID, CommentServer)
        CommentSettings = json.loads(CommentSettings)
        if "error" in CommentSettings:
            return
        Amount = CommentSettings["min_tip_amount_comment"]
        if Amount != None:
            GLib.idle_add(self.UpdateCommentName, BottomStack, Amount)

    def UpdateCommentName(self, BottomStack, Amount):
        Child = self.BottomStack.get_nth_page(1)
        self.BottomStack.set_tab_label_text(
            Child, "💬 \n(" + str(Amount) + " LBC)"
        )
        self.BottomStack.get_tab_label(Child).set_tooltip_text(
            "Comments - Tip " + str(Amount) + " LBC to comment"
        )

    def KillAll(self):
        while len(self.Queues) != 0:
            for Queue in self.Queues:
                Queue.put(True)
            time.sleep(0.01)

    def Fetch(self, *Args):
        self.Threads = concurrent.futures.ThreadPoolExecutor(max_workers=1)
        self.Threads.submit(self.FetchBranch, *Args)

    def FetchBranch(
        self,
        ClaimID,
        Box,
        ExitQueue,
        CommentServer,
        PChannel,
        LBRYGTKSettings,
        PChannelName,
        PChannelId,
        CommentID="",
    ):
        self.Queues.append(ExitQueue)
        Session = LBRYGTKSettings["Session"]
        GotChannel, PageIndex, RowQueue = (
            {"Name": "", "Claim": ""},
            0,
            queue.Queue(),
        )
        ChannelList, PageNumber = json.loads(Channel.Get(Session["Server"])), 1

        for ChannelItem in ChannelList:
            if ChannelItem["Name"] == LBRYGTKSettings["CommentChannel"]:
                GotChannel = ChannelItem
                break

        CommentPerLoading = int(LBRYGTKSettings["CommentPerLoading"])

        while PageIndex != PageNumber:
            try:
                ExitQueue.get(block=False)
                return self.Queues.remove(ExitQueue)
            except:
                pass

            PageIndex += 1

            Value = Comments.List(
                PChannelName,
                PChannelId,
                CommentID,
                ClaimID,
                CommentServer,
                CommentPerLoading,
                PageIndex,
                Session["Server"],
            )

            Value = json.loads(Value)
            if len(Value) != 2 or "Error" in Value or "error" in Value:
                return self.Queues.remove(ExitQueue)
            Rows, PageNumber, CommentIDs = *Value, ""
            for Row in Rows:
                CommentIDs += str(Row["Id"]) + ","

            Reactions = Comments.ReactionList(
                GotChannel["Name"],
                GotChannel["Claim"],
                CommentServer,
                CommentIDs,
                Session["Server"],
            )
            Reactions = json.loads(Reactions)

            for Row in Rows:
                try:
                    ExitQueue.get(block=False)
                    return self.Queues.remove(ExitQueue)
                except:
                    pass

                try:
                    OtherReaction = Reactions["others_reactions"][Row["Id"]]
                    Likes = OtherReaction["like"]
                    Dislikes = OtherReaction["dislike"]
                    CreatorLike = OtherReaction["creator_like"] == 1
                except:
                    Likes, Dislikes, CreatorLike = 0, 0, False

                try:
                    MyReaction = Reactions["my_reactions"][Row["Id"]]
                    CreatorLike = CreatorLike or MyReaction["creator_like"] == 1
                    Likes += MyReaction["like"]
                    Dislikes += MyReaction["dislike"]
                    Like = MyReaction["like"] == 1
                    Dislike = MyReaction["dislike"] == 1
                except:
                    Like, Dislike = False, False

                GLib.idle_add(
                    self.SingleComment,
                    ClaimID,
                    Box,
                    Row,
                    [Likes, Dislikes, Like, Dislike, CreatorLike],
                    CommentServer,
                    False,
                    LBRYGTKSettings,
                    ChannelList,
                    PChannel,
                    RowQueue,
                )

                if Row["Replies"] != 0:
                    NewCommentID = Row["Id"]
                    try:
                        NewBox = RowQueue.get(timeout=5)
                    except:
                        return self.Queues.remove(ExitQueue)

                    Args = [ClaimID, NewBox, queue.Queue(), CommentServer]
                    Args.extend([PChannel, LBRYGTKSettings, PChannelName])
                    Args.extend([PChannelId, NewCommentID])
                    self.Threads.submit(self.FetchBranch, *Args)
        self.Queues.remove(ExitQueue)
