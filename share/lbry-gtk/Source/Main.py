################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2023 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import gi, json, threading, sys

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GdkPixbuf, Gdk, GLib

from Source import SettingsUpdate, SelectUtil
from Source.Page import Page
from Source.TopPanel import TopPanel
from Source.Select import Select
from Source.PyGObjectCAPI import PyGObjectCAPI
from CRewrite import Connect, Global, Places, Popup, Preferences, Replace
from CRewrite import SidePanel, Startup, State, Tabs

if sys.platform == "win32":
    MouseBack = 4
    MouseForward = 5
else:
    MouseBack = 8
    MouseForward = 9

CAPI = PyGObjectCAPI()
Default = {}


class Main:
    def __init__(self):
        Global.SetBuilder()
        Builder = CAPI.ToObject(Global.Builder)
        Builder.add_from_file(Places.GladeDir + "LBRY-GTK.glade")
        Builder.connect_signals(self)
        self.Pages, self.Menu, self.Pager = [], "", Builder.get_object("Pager")
        Window = Builder.get_object("Window")
        self.Header = Builder.get_object("Header")
        self.PageGrid = Builder.get_object("PageGrid")
        self.MainBox = Builder.get_object("MainBox")
        self.TopPanelBox = Builder.get_object("TopPanelBox")
        self.Back = Builder.get_object("Back")
        self.Forward = Builder.get_object("Forward")
        self.Add = Builder.get_object("Add")
        self.Refresh = Builder.get_object("Refresh")
        self.Previous = Builder.get_object("Previous")
        self.Next = Builder.get_object("Next")
        self.OnCurrent = Builder.get_object("OnCurrent")
        self.OnNew = Builder.get_object("OnNew")
        self.SelectPrevious = Builder.get_object("SelectPrevious")
        self.SelectNext = Builder.get_object("SelectNext")
        self.BackImage = Builder.get_object("BackImage")

        self.BackHistory = Builder.get_object("BackHistory")
        self.ForwardHistory = Builder.get_object("ForwardHistory")

        Global.SetWindow(CAPI.AsVoidPointer(Window.__gpointer__))

        Window.drag_dest_set(
            Gtk.DestDefaults.ALL,
            [Gtk.TargetEntry.new("text/plain", Gtk.TargetFlags.OTHER_APP, 0)],
            Gdk.DragAction.COPY,
        )

        self.TopPaneler = TopPanel(
            self.BackImage,
            self.NewPage,
            self.SamePage,
            self.SetMenu,
        )
        self.TopPanelBox.pack_start(self.TopPaneler.TopPanel, True, True, 0)
        self.MenuSelector = self.TopPaneler.Selector

        self.SidePaneler = SidePanel.Create(self)

        SidePanelObject = CAPI.ToObject(self.SidePaneler["SidePanel"])
        SidePanelRevealerObject = CAPI.ToObject(self.SidePaneler["Revealer"])
        SidePanelToggleObject = CAPI.ToObject(
            self.SidePaneler["SidePanelToggle"]
        )

        self.PageGrid.attach(SidePanelToggleObject, 0, 0, 1, 1)
        self.MainBox.pack_start(SidePanelRevealerObject, False, False, 0)

        CssProvider = Gtk.CssProvider.new()
        CssProvider.load_from_data(b"* {padding: 0px;}")
        StyleContext = self.Header.get_style_context()
        StyleContext.add_provider(
            CssProvider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
        )

        with open(Places.JsonDir + "DefaultSettings.json", "r") as File:
            Default["Settings"] = json.load(File)
            Default["Session"] = Default["Settings"]["Session"]
            Default["Keys"] = Default["Settings"]["Keyboard"]

        self.on_Add_button_press_event()
        self.on_Header_switch_page(self.Header, self.Pages[-1].Page, 0)

        Startup.Create(
            CAPI.AsVoidPointer(self.TopPaneler.Balance.__gpointer__),
            CAPI.AsVoidPointer(self.TopPaneler.Signing.__gpointer__),
            self.Pages[0].Statuser["Pointer"],
        )

        Window.set_icon_from_file(Places.ImageDir + "lbry-gtk.svg")

        TopPaneler = self.TopPaneler
        Widgets = [self.Refresh, self.Add, self.Back, self.Forward]
        Widgets.extend([TopPaneler.LBRY, TopPaneler.Search, TopPaneler.Inbox])
        Widgets.extend(SidePanelObject.get_children())
        self.Selector = Select(*SelectUtil.PrepareWidgets(Widgets))
        SettingsUpdate.WindowAcceleratorCreate(self)
        self.BackAndForward = [self.Back, self.Forward]

    def on_Previous_activate(self, Widget):
        Current = self.Header.get_current_page()
        if Current != 0:
            self.Header.set_current_page(Current - 1)

    def on_Next_activate(self, Widget):
        Current = self.Header.get_current_page()
        if Current != self.Header.get_n_pages() - 1:
            self.Header.set_current_page(Current + 1)

    def NewPage(self, Function, Data):
        Page = self.Pages[self.Pager.get_current_page()]
        self.on_Add_button_press_event(".", "", [Function, Data])

    def SamePage(self, Function, Data):
        Page = self.Pages[self.Pager.get_current_page()]
        State.Import(Function, Data, Page.TabPointer)

    def on_MenuItem_button_press_event(self, Widget, Event, GotState):
        Page = self.Pages[self.Pager.get_current_page()]
        if Event.button == Gdk.BUTTON_MIDDLE:
            self.on_Add_button_press_event(
                ".", "", State.Export(GotState["Function"], GotState["Data"])
            )
        else:
            Page.Stater.Goto(GotState["Pointer"])

    def on_Window_button_press_event(self, Widget, Event):
        if (
            Event.button == MouseBack or Event.button == MouseForward
        ) and Global.GetStarted():
            LBRYSettings = json.loads(Preferences.Get())
            if LBRYSettings["Preferences"]["MouseBackAndForward"]:
                if Event.button == MouseBack:
                    self.Selector.Activate(0, 2)
                elif Event.button == MouseForward:
                    self.Selector.Activate(0, 3)

    def on_Window_drag_data_received(
        self, widget, Context, X, Y, Data, Info, Time
    ):
        if Global.GetStarted():
            Page = self.Pages[self.Pager.get_current_page()]
            State.Import("Publication", ["", Data.get_text()], Page.TabPointer)
        else:
            Popup.Message("LBRYNet is not running.")

    def on_StaterList_button_press_event(self, Widget, Event):
        Page = self.Pages[self.Pager.get_current_page()]
        IsBack = Widget.get_image() == self.BackImage

        if Event.button == Gdk.BUTTON_SECONDARY:
            if IsBack:
                List = Page.Stater.Before()
            else:
                List = Page.Stater.After()
            Length = len(List)
            if Length != 0:
                Menu, Activates = Gtk.Menu.new(), []
                Widgets, Exceptions, Enters, Leaves = [], [], [], []
                for Index in range(Length):
                    Item = List[Index]
                    MenuItem = Gtk.MenuItem.new_with_label(Item["Title"])
                    Widgets.append(MenuItem)
                    Exceptions.append(MenuItem)
                    Enters.append(MenuItem.select)
                    Leaves.append(MenuItem.deselect)
                    Activates.append(
                        SelectUtil.EventLambda(MenuItem, Menu.popdown)
                    )
                    MenuItem.connect(
                        "button-press-event",
                        self.on_MenuItem_button_press_event,
                        Item,
                    )
                    if IsBack:
                        Start, End = Length - Index - 1, Length - Index
                    else:
                        Start, End = Index, Index + 1
                    Menu.attach(MenuItem, 0, 1, Start, End)
                Menu.show_all()
                Menu.popup_at_widget(
                    Widget, Gdk.Gravity.SOUTH_WEST, Gdk.Gravity.NORTH_WEST
                )
                self.MenuSelector = Select(
                    Widgets, Exceptions, Enters, Leaves, Activates
                )
                SettingsUpdate.MenuAcceleratorCreate(Menu.get_parent(), self)
                self.Menu = Menu
        elif Event.button == Gdk.BUTTON_MIDDLE:
            if IsBack:
                GotState = Page.Stater.Previous()
            else:
                GotState = Page.Stater.Next()
            if GotState != []:
                self.on_Add_button_press_event(".", "", GotState)
        elif Event.button == Gdk.BUTTON_PRIMARY:
            if IsBack:
                Page.Stater.Backward()
            else:
                Page.Stater.Forward()

    def SetMenu(self, Remove=False):
        if Remove:
            self.Menu = ""
            return
        Windows = Gtk.Window.list_toplevels()
        for Window in Windows:
            if (
                Window.get_realized()
                and isinstance(Window.get_child(), Gtk.Menu)
                and Window.get_name() != "Set"
            ):
                Window.connect(
                    "focus-out-event",
                    self.TopPaneler.on_MenuBar_focus_out_event,
                )
                SettingsUpdate.MenuAcceleratorCreate(Window, self)
        self.Menu = self.TopPaneler.MenuBar

    def on_History_activate(self, Widget):
        Button = self.BackAndForward[int(Widget.get_label())]
        SelectUtil.Event(Button, Gdk.BUTTON_SECONDARY)

    def on_Add_button_press_event(self, Widget="", Event="", GotState=""):
        if Widget == "" or Global.GetStarted():
            FirstPage = GotState
            try:
                LBRYSettings = json.loads(Preferences.Get())
                LBRYGTKSettings = LBRYSettings["Preferences"]
                Session = LBRYGTKSettings["Session"]
                if Widget != "" and GotState == "":
                    FirstPage = [
                        LBRYGTKSettings["HomeFunction"],
                        json.loads(LBRYGTKSettings["HomeData"]),
                    ]
            except:
                pass
            NoStartup = Widget != ""

            CloseButton = Gtk.Button.new_from_icon_name(
                "window-close", Gtk.IconSize.BUTTON
            )

            NewPage = Page(
                FirstPage,
                NoStartup,
                self.on_Add_button_press_event,
                self.TopPaneler.Balance,
                self.TopPaneler.Signing,
                CloseButton,
                self,
                Default,
            )

            self.Pages.append(NewPage)

            TitleBox, Width = Gtk.Box.new(Gtk.Orientation.HORIZONTAL, 0), 128
            try:
                Width = Session["PageWidth"]
            except:
                pass
            NewPage.Title.set_size_request(Width, -1)
            TitleBox.pack_start(NewPage.Title, False, False, 0)
            NewPage.Title.connect("draw", self.on_Title_draw, NewPage)
            CloseButton.connect("clicked", self.on_CloseButton_clicked, NewPage)
            TitleBox.pack_start(CloseButton, True, True, 0)
            CloseButton.set_halign(Gtk.Align.END)
            try:
                Expand = Session["PageExpand"]
            except:
                Expand = True
            CloseButton.set_hexpand(Expand)
            TitleBox.show_all()
            self.Pager.append_page(NewPage.MainSpace)
            self.Header.append_page(
                Gtk.Box.new(Gtk.Orientation.VERTICAL, 0), TitleBox
            )
            self.Pager.set_current_page(-1)
            self.Header.show_all()
            self.Header.set_current_page(-1)

    def on_Refresh_button_press_event(self, Widget, Event):
        Page = self.Pages[self.Pager.get_current_page()]
        Page.Stater.Goto(0)

    def on_Title_draw(self, Widget, Discard, Page):
        if Widget.get_name() != Widget.get_text():
            Widget.set_name(Widget.get_text())
            self.on_Header_switch_page(
                self.Header, Page, self.Pages.index(Page)
            )

    def on_CloseButton_clicked(self, Widget, Page):
        if len(self.Pages) != 1:
            Page.Publicationer.Commenter.KillAll()
            Index = self.Pager.page_num(Page.MainSpace)
            GotHeader = self.Header.get_nth_page(Index)
            GotPage = self.Pager.get_nth_page(Index)
            self.Header.remove_page(Index)
            self.Pager.remove_page(Index)
            self.Pages.remove(Page)
            GotHeader.destroy()
            GotPage.destroy()
            Tabs.Free(Page.TabPointer)

    def on_Header_switch_page(self, Widget, Page, Index):
        Text = (
            Widget.get_tab_label(Widget.get_nth_page(Index))
            .get_children()[0]
            .get_text()
        )
        if Text == "":
            Text = "LBRY-GTK"
        else:
            Text += " - LBRY-GTK"
        Window = CAPI.ToObject(Global.Window)
        Window.set_title(Text)
        self.Pager.set_current_page(Index)
        SettingsUpdate.PageAcceleratorCreate(self.Pages[Index])

    def ShutdownThread(self):
        for Page in self.Pages:
            Page.Publicationer.Commenter.KillAll()
        Connect.CreateExit()
        LBRYSettings = json.loads(Preferences.Get())
        Session = LBRYSettings["Preferences"]["Session"]
        if Session["Stop"]:
            Connect.Destroy(Session["Server"])
        GLib.idle_add(self.ShutdownUpdate)

    def ShutdownUpdate(self):
        Gtk.main_quit()

    def on_Window_destroy(self, *args):
        threading.Thread(None, self.ShutdownThread).start()

    def on_OnCurrent_activate(self, Widget):
        if self.Menu != "" and self.Menu.get_realized():
            self.MenuSelector.Activate(0)
        else:
            self.Selector.Activate(0)

    def on_OnNew_activate(self, Widget):
        if self.Menu != "" and self.Menu.get_realized():
            self.MenuSelector.Activate(1)
        else:
            self.Selector.Activate(1)

    def on_SelectPrevious_activate(self, Widget):
        if self.Menu != "" and self.Menu.get_realized():
            self.MenuSelector.Backward()
        else:
            self.Selector.Backward()

    def on_SelectNext_activate(self, Widget):
        if self.Menu != "" and self.Menu.get_realized():
            self.MenuSelector.Forward()
        else:
            self.Selector.Forward()

    def NoToggleBefore(self, Widget):
        Widget.set_name(str(Widget.get_active()))

    def NoToggleAfter(self, Widget):
        Widget.set_active(Widget.get_name() == "True")
