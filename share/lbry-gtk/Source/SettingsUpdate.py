################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2023 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import gi, threading, json, queue

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gdk, GLib

from Source.PyGObjectCAPI import PyGObjectCAPI
from CRewrite import Global, KeyBind, Preferences, Thumbnail
from CRewrite import NotificationDB

CAPI = PyGObjectCAPI()


def GetTopPanel(Window):
    TopPanelParent = Window.get_children()[0].get_children()[1]
    Children = TopPanelParent.get_children()
    for Child in Children:
        if Child.get_name() == "TopPanelBox":
            return Child.get_children()[0]


def UpdateWidth(Session):
    # Get the pointer to window
    Window = CAPI.ToObject(Global.Window)

    Pager = Window.get_children()[0].get_children()[0]
    Length = Pager.get_n_pages()
    for Index in range(Length):
        Page = Pager.get_nth_page(Index)
        Label = Pager.get_tab_label(Page)
        Children = Label.get_children()
        Children[0].set_size_request(Session["PageWidth"], -1)
        Children[1].set_hexpand(Session["PageExpand"])


UpdateMetaQueue = queue.Queue()


def UpdateMeta(Inbox, Window):
    # Exit if user disabled Meta Service otherwise continue
    global UpdateMetaQueue
    threading.Thread(None, UpdateMetaThread, None, [Inbox, Window]).start()
    try:
        UpdateMetaQueue.get(block=False)
        return False
    except:
        return True


def UpdateMetaThread(Inbox, Window):
    # Check if user disabled Meta Service
    global UpdateMetaQueue
    LBRYSettings = json.loads(Preferences.Get())
    Session = LBRYSettings["Preferences"]["Session"]
    if not Session["EnableMetaService"]:
        UpdateMetaQueue.put(True)
        return
    if NotificationDB.Check() == 1:
        GLib.idle_add(UpdateMetaUpdate, Inbox)
    Args = [
        600,
        LBRYSettings["Preferences"]["CommentServer"],
        Session["Server"],
    ]
    threading.Thread(None, NotificationDB.Make, None, Args).start()


def UpdateMetaUpdate(Inbox):
    Inbox.set_active(True)


def ChangeMeta(EnableMetaService):
    # Get the pointer to window
    Window = CAPI.ToObject(Global.Window)

    # Hide or show Icon
    TopPanel = GetTopPanel(Window)
    Children = TopPanel.get_children()
    for Child in Children:
        try:
            if Child.get_children()[0].get_label() == "🔔":
                Inbox = Child
                Child.set_visible(EnableMetaService)
                break
        except:
            pass

    # Start notification service
    if EnableMetaService:
        GLib.timeout_add(60 * 1000, UpdateMeta, Inbox, Window)
        threading.Thread(None, UpdateMetaThread, None, [Inbox, Window]).start()


MinimumWidth = 0

MenuIcons = [
    "menu_new",
    "preferences-other",
    "image-loading",
    "document-properties",
    "applications-others",
]


def ChangeMenu(Type, MenuIcon="", Remove=True):
    # Get the pointer to window
    Window = CAPI.ToObject(Global.Window)

    # Set MinimumWidth with full menu only once
    global MinimumWidth, MenuIcons
    if MinimumWidth == 0:
        MinimumWidth = Window.get_preferred_size().minimum_size.width

    TopPanel = GetTopPanel(Window)
    Children, Menu = TopPanel.get_children(), ""

    # Get all needed components
    for Child in Children:
        if isinstance(Child, Gtk.Box):
            try:
                Menu = Child.get_children()[0]
            except:
                pass
            MenuBox = Child
        if isinstance(Child, Gtk.MenuButton):
            Button = Child
    Popover = Button.get_popover()
    if Menu == "":
        Menu = Popover.get_children()[0]

    # Set icon
    Image = Button.get_children()[0]
    if MenuIcon != "" and Image.get_icon_name() != MenuIcons[MenuIcon]:
        Image.set_from_icon_name(MenuIcons[MenuIcon], Gtk.IconSize.BUTTON)

    # Do not change type if it's already that
    if TopPanel.get_name() != str(Type):
        TopPanel.set_name(str(Type))

        # Initial settings needed for each type
        Menu.unparent()
        MenuBox.foreach(MenuBox.remove)
        Popover.foreach(Popover.remove)
        Button.set_visible(Type == 2)
        WindowName = Window.get_name()

        # Do not remove function, if original type is adaptive
        if WindowName != "GtkWindow" and Remove:
            Window.disconnect(int(WindowName))
            Window.set_name("GtkWindow")
        # Add menu to the correct place
        # Adaptive adds it to the button, as function assumes it is somewhere
        if Type == 0:
            Window.set_name(str(Window.connect("configure-event", UpdateMenu)))
            Menu.set_pack_direction(Gtk.PackDirection.TTB)
            Popover.add(Menu)
            UpdateMenu(Window, "", Window.get_size().width)
        elif Type == 1:
            Menu.set_pack_direction(Gtk.PackDirection.LTR)
            MenuBox.add(Menu)
        elif Type == 2:
            Menu.set_pack_direction(Gtk.PackDirection.TTB)
            Popover.add(Menu)


def UpdateMenu(Window, Event, Width=""):
    # Setting MenuType depending on Window size
    if Width == "":
        Width = Event.width
    global MinimumWidth
    if Width < MinimumWidth:
        ChangeMenu(2, "", False)
    else:
        ChangeMenu(1, "", False)


def GetWindowWidgets(Main):
    MainActivate = Main.Selector.Activate
    return {
        "Refresh": [lambda: MainActivate(0, 0)],
        "Add": [lambda: MainActivate(1, 0)],
        "BackOnCurrent": [lambda: MainActivate(2, 0)],
        "BackOnNew": [lambda: MainActivate(2, 1)],
        "ForwardOnCurrent": [lambda: MainActivate(3, 0)],
        "ForwardOnNew": [lambda: MainActivate(3, 1)],
        "LBRY-GTK": [lambda: MainActivate(4, 0)],
        "Search": [lambda: MainActivate(5, 0)],
        "InboxOnCurrent": [lambda: MainActivate(6, 0)],
        "InboxOnNew": [lambda: MainActivate(6, 1)],
        "HomeOnCurrent": [lambda: MainActivate(7, 0)],
        "HomeOnNew": [lambda: MainActivate(7, 1)],
        "FollowingOnCurrent": [lambda: MainActivate(8, 0)],
        "FollowingOnNew": [lambda: MainActivate(8, 1)],
        "YourTagsOnCurrent": [lambda: MainActivate(9, 0)],
        "YourTagsOnNew": [lambda: MainActivate(9, 1)],
        "DiscoverOnCurrent": [lambda: MainActivate(10, 0)],
        "DiscoverOnNew": [lambda: MainActivate(10, 1)],
        "LibraryOnCurrent": [lambda: MainActivate(11, 0)],
        "LibraryOnNew": [lambda: MainActivate(11, 1)],
        "CollectionsOnCurrent": [lambda: MainActivate(12, 0)],
        "CollectionsOnNew": [lambda: MainActivate(12, 1)],
        "FollowedOnCurrent": [lambda: MainActivate(13, 0)],
        "FollowedOnNew": [lambda: MainActivate(13, 1)],
        "UploadsOnCurrent": [lambda: MainActivate(14, 0)],
        "UploadsOnNew": [lambda: MainActivate(14, 1)],
        "ChannelsOnCurrent": [lambda: MainActivate(15, 0)],
        "ChannelsOnNew": [lambda: MainActivate(15, 1)],
        "NativeOnCurrent": [lambda: MainActivate(16, 0)],
        "NativeOnNew": [lambda: MainActivate(16, 1)],
        "PreviousPage": [Main.Previous.activate],
        "NextPage": [Main.Next.activate],
        "Menu": [Main.TopPaneler.ShowMenu.activate],
        "NewPublicationOnCurrent": [
            Main.TopPaneler.NewPublicationOnCurrent.activate
        ],
        "NewPublicationOnNew": [Main.TopPaneler.NewPublicationOnNew.activate],
        "SettingsOnCurrent": [Main.TopPaneler.SettingsOnCurrent.activate],
        "SettingsOnNew": [Main.TopPaneler.SettingsOnNew.activate],
        "HelpOnCurrent": [Main.TopPaneler.HelpOnCurrent.activate],
        "HelpOnNew": [Main.TopPaneler.HelpOnNew.activate],
        "StatusOnCurrent": [Main.TopPaneler.StatusOnCurrent.activate],
        "StatusOnNew": [Main.TopPaneler.StatusOnNew.activate],
        "About": [Main.TopPaneler.AboutAction.activate],
        "BalanceOnCurrent": [Main.TopPaneler.BalanceOnCurrent.activate],
        "BalanceOnNew": [Main.TopPaneler.BalanceOnNew.activate],
        "MainOnCurrent": [Main.OnCurrent.activate],
        "MainOnNew": [Main.OnNew.activate],
        "MainSelectPrevious": [Main.SelectPrevious.activate],
        "MainSelectNext": [Main.SelectNext.activate],
        "BackHistory": [Main.BackHistory.activate],
        "ForwardHistory": [Main.ForwardHistory.activate],
    }


def GetPageWidgets(Page):
    Publicationer = Page.Publicationer
    PublicationActivate = Publicationer.PublicationControler.Selector.Activate
    CommentAction = Publicationer.CommentAction
    return {
        "ScrollUp": [Page.ScrollUp.activate],
        "ScrollDown": [Page.ScrollDown.activate],
        "Close": [Page.CloseButton.activate],
        "MoveLeft": [
            Page.MoveLeft.activate,
            Publicationer.MoveLeft.activate,
            Page.NewPublicationer.MoveLeft.activate,
            Page.Settingser.MoveLeft.activate,
        ],
        "MoveRight": [
            Page.MoveRight.activate,
            Publicationer.MoveRight.activate,
            Page.NewPublicationer.MoveRight.activate,
            Page.Settingser.MoveRight.activate,
        ],
        "MoveUp": [
            Page.MoveUp.activate,
            Publicationer.MoveUp.activate,
            Page.NewPublicationer.MoveUp.activate,
            Page.Settingser.MoveUp.activate,
        ],
        "MoveDown": [
            Page.MoveDown.activate,
            Publicationer.MoveDown.activate,
            Page.NewPublicationer.MoveDown.activate,
            Page.Settingser.MoveDown.activate,
        ],
        "PageOnCurrent": [
            Page.ListOpenOnCurrent.activate,
            Publicationer.OpenOnCurrent.activate,
            Page.NewPublicationer.OpenOnCurrent.activate,
            Page.Settingser.OpenOnCurrent.activate,
        ],
        "PageOnNew": [
            Page.ListOpenOnNew.activate,
            Publicationer.OpenOnNew.activate,
            Page.NewPublicationer.OpenOnNew.activate,
            Page.Settingser.OpenOnNew.activate,
        ],
        "PageSelectNext": [
            Publicationer.SelectNext.activate,
            Page.SelectNext.activate,
            Page.NewPublicationer.SelectNext.activate,
            Page.Settingser.SelectNext.activate,
        ],
        "PageSelectPrevious": [
            Publicationer.SelectPrevious.activate,
            Page.SelectPrevious.activate,
            Page.NewPublicationer.SelectPrevious.activate,
            Page.Settingser.SelectPrevious.activate,
        ],
        "PlayOnCurrent": [lambda: PublicationActivate(0, 0)],
        "PlayOnNew": [lambda: PublicationActivate(1, 0)],
        "ContentOnCurrent": [lambda: PublicationActivate(0, 1)],
        "ContentOnNew": [lambda: PublicationActivate(2, 1)],
        "ChannelOnCurrent": [lambda: PublicationActivate(0, 2)],
        "ChannelOnNew": [lambda: PublicationActivate(2, 2)],
        "RSS": [lambda: PublicationActivate(0, 3)],
        "Follow": [lambda: PublicationActivate(0, 4)],
        "Download": [lambda: PublicationActivate(0, 5)],
        "Block": [lambda: PublicationActivate(0, 6)],
        "Link": [lambda: PublicationActivate(0, 7)],
        "Share": [lambda: PublicationActivate(0, 8)],
        "Repost": [lambda: PublicationActivate(0, 9)],
        "RelatedOnCurrent": [lambda: PublicationActivate(0, 10)],
        "RelatedOnNew": [lambda: PublicationActivate(2, 10)],
        "Boost": [lambda: PublicationActivate(0, 11)],
        "Tip": [lambda: PublicationActivate(0, 12)],
        "CommentProfile": [lambda: CommentAction(0, 0)],
        "CommentChannelOnNew": [lambda: CommentAction(1, 1)],
        "CommentChannelOnCurrent": [lambda: CommentAction(0, 1)],
        "CommentPublicationsOnNew": [lambda: CommentAction(1, 2)],
        "CommentPublicationsOnCurrent": [lambda: CommentAction(0, 2)],
        "CommentUnhide": [lambda: CommentAction(0, 3)],
        "CommentEdit": [lambda: CommentAction(0, 5)],
        "CommentDelete": [lambda: CommentAction(0, 6)],
        "CommentHeart": [lambda: CommentAction(0, 7)],
        "CommentLike": [lambda: CommentAction(0, 8)],
        "CommentDislike": [lambda: CommentAction(0, 9)],
        "CommentReply": [lambda: CommentAction(0, 10)],
        "CommentChannels": [lambda: CommentAction(0, 11)],
        "CommentReplyText": [lambda: CommentAction(0, 12)],
        "CommentTip": [lambda: CommentAction(0, 13)],
        "CommentPost": [lambda: CommentAction(0, 14)],
        "CommentSave": [lambda: CommentAction(0, 15)],
        "MarkdownDown": [lambda: CommentAction(0, 16)],
        "MarkdownUp": [lambda: CommentAction(0, 17)],
        "TagSelect": [
            Publicationer.TagSelect.activate,
            Page.TagSelect.activate,
            Page.NewPublicationer.TagSelect.activate,
            Page.Settingser.TagSelect.activate,
        ],
        "Thumbnail": [
            lambda: Thumbnail.PressCheck(Publicationer.Thumbnailer["Pointer"])
        ],
        "Data": [Publicationer.DataAction.activate],
        "Tags": [Publicationer.TagsAction.activate],
        "Description": [Publicationer.DescriptionAction.activate],
        "Links": [Publicationer.LinksAction.activate],
        "Comments": [Publicationer.CommentsAction.activate],
        "AdvancedSearch": [Page.Advanced.activate],
    }


def GetMenuWidgets(Main):
    return {
        "MainOnCurrent": [Main.OnCurrent.activate],
        "MainOnNew": [Main.OnNew.activate],
        "MainSelectPrevious": [Main.SelectPrevious.activate],
        "MainSelectNext": [Main.SelectNext.activate],
        "Menu": [Main.TopPaneler.ShowMenu.activate],
    }


def ShortCut(AccelGroup, Window, KeyVal, Modifier, Function):
    Function()


def CreateLambda(Function):
    return lambda A, B, C, D: ShortCut(A, B, C, D, Function)


def AcceleratorCreate(Window, Widgets, AccelGroup):
    Window.add_accel_group(AccelGroup)
    LBRYSettings = json.loads(Preferences.Get())
    Keys = LBRYSettings["Preferences"]["Keyboard"]
    for WidgetName in Widgets:
        try:
            for Widget in Widgets[WidgetName]:
                Converted = KeyBind.Convert(Keys[WidgetName])
                if Converted:
                    Value = Converted["Value"]
                    Modifier = Gdk.ModifierType(Converted["Modifier"])
                    AccelGroup.connect(Value, Modifier, 0, CreateLambda(Widget))
        except:
            pass


WindowAccelGroup = ""


def WindowAcceleratorCreate(Main):
    # Get the pointer to window
    Window = CAPI.ToObject(Global.Window)

    global WindowAccelGroup
    if WindowAccelGroup != "":
        Window.remove_accel_group(WindowAccelGroup)
    WindowAccelGroup = Gtk.AccelGroup.new()
    AcceleratorCreate(Window, GetWindowWidgets(Main), WindowAccelGroup)


PageAccelGroup = ""


def PageAcceleratorCreate(Page):
    # Get the pointer to window
    Window = CAPI.ToObject(Global.Window)

    global PageAccelGroup
    if PageAccelGroup != "":
        Window.remove_accel_group(PageAccelGroup)
    PageAccelGroup = Gtk.AccelGroup.new()
    AcceleratorCreate(Window, GetPageWidgets(Page), PageAccelGroup)


def MenuAcceleratorCreate(Window, Main):
    AcceleratorCreate(Window, GetMenuWidgets(Main), Gtk.AccelGroup.new())
    Window.set_name("Set")
