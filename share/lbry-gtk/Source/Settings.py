################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2023 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import gi, threading, json, copy, pathlib, queue, sys

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GLib, Gdk

from Source import SettingsUpdate, SelectUtil
from Source.Select import Select
from Source.PyGObjectCAPI import PyGObjectCAPI
from CRewrite import Channel, Global, Identity, Image, KeyBind, Language, Move
from CRewrite import Order, Places, Popup, Preferences, Replace, State, Tag

CAPI = PyGObjectCAPI()


class FakeOrder:
    def __init__(self, Orderer):
        self.Orderer = Orderer

    def get_value(self):
        return Order.Get(self.Orderer["Pointer"])

    def set_value(self, Value):
        Order.Set(self.Orderer["Pointer"], Value)


class Settings:
    def __init__(self, *args):
        self.Title, self.Stater, self.Main, self.Default, self.TabPointer = args
        Builder = CAPI.ToObject(Global.Builder)
        Builder.add_from_file(Places.GladeDir + "Settings.glade")
        Builder.connect_signals(self)
        self.Settings = Builder.get_object("Settings")
        self.LocalTagBox = Builder.get_object("LocalTagBox")
        self.SharedTagBox = Builder.get_object("SharedTagBox")
        self.NotTagBox = Builder.get_object("NotTagBox")
        self.NotChannelBox = Builder.get_object("NotChannelBox")
        self.OrderBox = Builder.get_object("OrderBox")
        self.CommentChannelBox = Builder.get_object("CommentChannelBox")
        self.PresetHome = Builder.get_object("PresetHome")
        self.KeyBindBox = Builder.get_object("KeyBindBox")
        self.SelectPrevious = Builder.get_object("SelectPrevious")
        self.SelectNext = Builder.get_object("SelectNext")
        self.OpenOnCurrent = Builder.get_object("OpenOnCurrent")
        self.OpenOnNew = Builder.get_object("OpenOnNew")
        self.Import = Builder.get_object("Import")
        self.Export = Builder.get_object("Export")
        self.Save = Builder.get_object("Save")
        self.MoveLeft = Builder.get_object("MoveLeft")
        self.MoveRight = Builder.get_object("MoveRight")
        self.MoveUp = Builder.get_object("MoveUp")
        self.MoveDown = Builder.get_object("MoveDown")
        self.TagSelect = Builder.get_object("TagSelect")
        self.LanguageBox = Builder.get_object("LanguageBox")

        self.SettingFields = {}
        Nots = ["Session", "Accounts", "Following", "TagsShared", "TagsLocal"]
        Nots.append("Keyboard")
        for Setting in self.Default["Settings"]:
            if Setting not in Nots:
                self.SettingFields[Setting] = Builder.get_object(Setting)
        GetObject = Builder.get_object
        self.LBRYFields = {"save_resolved_claims": GetObject("SaveResolved")}
        self.LBRYFields["save_files"] = GetObject("SaveFiles")
        self.LBRYFields["save_blobs"] = GetObject("SaveBlobs")
        self.LBRYFields["download_dir"] = GetObject("DownloadPath")
        self.LBRYFields["blob_download_timeout"] = GetObject("BlobTimeout")
        self.LBRYFields["download_timeout"] = GetObject("DownloadTimeout")
        self.LBRYFields["hub_timeout"] = GetObject("HubTimeout")
        self.LBRYFields["node_rpc_timeout"] = GetObject("NodeRPCTimeout")
        self.LBRYFields["peer_connect_timeout"] = GetObject("PeerTimeout")

        self.SessionFields = {}
        self.KeyFields = {}
        for Setting in self.Default["Session"]:
            self.SessionFields[Setting] = Builder.get_object(Setting)
        for Setting in self.Default["Keys"]:
            Title = ""
            for Index in range(len(Setting)):
                if Index != 0:
                    if (
                        Setting[Index].isupper()
                        and Index + 1 < len(Setting)
                        and Setting[Index + 1].islower()
                    ):
                        Title += " "
                Title += Setting[Index]
            Label = Gtk.Label.new(Title)
            Label.set_halign(Gtk.Align.START)
            self.KeyBindBox.pack_start(Label, True, True, 0)

            KeyBinder = KeyBind.Create()
            self.KeyBindBox.pack_start(
                CAPI.ToObject(KeyBinder["KeyBind"]), True, True, 0
            )
            self.KeyFields[Setting] = KeyBinder

        self.KeyBindBox.show_all()
        self.Taggers = [self.LocalTagBox, self.SharedTagBox, self.NotTagBox]
        self.Taggers.extend([self.NotChannelBox])

        for Index in range(len(self.Taggers)):
            TagPlace = self.Taggers[Index]
            Tagger = Tag.Create(True, self.TabPointer, None)
            TagPlace.pack_start(CAPI.ToObject(Tagger["Tag"]), True, True, 0)
            self.Taggers[Index] = Tagger

        self.Languager = Language.Create(self.TabPointer)
        LanguageObject = CAPI.ToObject(self.Languager["Language"])
        self.LanguageBox.pack_start(LanguageObject, True, True, 0)

        self.Orderer = Order.Create()
        OrderObject = CAPI.ToObject(self.Orderer["Order"])
        self.OrderBox.pack_start(OrderObject, True, True, 0)

        GetObject("DownloadPath").get_children()[1].connect(
            "scroll-event", self.StopScroll
        )
        self.SettingFields["Order"] = FakeOrder(self.Orderer)
        self.SettingFields["TagsNot"] = self.Taggers[2]["Pointer"]
        self.SettingFields["ChannelsNot"] = self.Taggers[3]["Pointer"]
        self.SettingFields["Languages"] = self.Languager["TaggerPointer"]
        self.Identifier = Identity.Create(True)
        self.CommentChannelBox.add(
            CAPI.ToObject(self.Identifier["IdentitiesBox"])
        )

        Items, AllChildren = [[], [], [], [], []], []
        self.Notebook = self.Settings.get_children()[0]
        ChildrenNumbers = []
        for Index in range(self.Notebook.get_n_pages() - 1):
            Box = self.Notebook.get_nth_page(Index).get_child().get_child()
            SelectUtil.PrepareNew(
                Items, Box.get_children(), self.Taggers, [self.Languager]
            )
            ChildrenNumbers.append(len(Items[0]))
        for Index in range(len(Items[2])):
            Enter, Number = Items[2][Index], 0
            for ChildrenNumber in ChildrenNumbers:
                if Index < ChildrenNumber:
                    break
                Number += 1
            Items[2][Index] = self.EnterLambda(Enter, Number)
        Items[0].extend([self.Import, self.Export, self.Save])
        self.Selector = Select(*Items, True)

    def on_TagSelect_activate(self, Widget):
        if self.Settings.get_realized():
            CurrentWidget = self.Selector.Widgets[self.Selector.CurrentItem]
            Name = CurrentWidget.get_parent().get_parent().get_name()
            if Name.startswith("Tag"):
                Index = int(Name.split("-")[1])
                Tag.Select(self.Taggers[Index]["Pointer"])
            elif Name.startswith("LanguageBox"):
                Tag.Select(self.Languager["TaggerPointer"])

    def on_MoveLeft_activate(self, Widget):
        if self.Settings.get_realized():
            CurrentWidget = self.Selector.Widgets[self.Selector.CurrentItem]
            Name = CurrentWidget.get_parent().get_parent().get_name()
            if Name.startswith("Tag"):
                Index = int(Name.split("-")[1])
                Move.FlowBoxLeftRight(self.Tagger["FlowBox"], -1)
            elif Name.startswith("LanguageBox"):
                Move.FlowBoxLeftRight(self.Languager["FlowBox"], -1)

    def on_MoveRight_activate(self, Widget):
        if self.Settings.get_realized():
            CurrentWidget = self.Selector.Widgets[self.Selector.CurrentItem]
            Name = CurrentWidget.get_parent().get_parent().get_name()
            if Name.startswith("Tag"):
                Index = int(Name.split("-")[1])
                Move.FlowBoxLeftRight(self.Tagger["FlowBox"], 1)
            elif Name.startswith("LanguageBox"):
                Move.FlowBoxLeftRight(self.Languager["FlowBox"], 1)

    def on_MoveDown_activate(self, Widget):
        if self.Settings.get_realized():
            CurrentWidget = self.Selector.Widgets[self.Selector.CurrentItem]
            Name = CurrentWidget.get_parent().get_parent().get_name()
            if Name.startswith("Tag"):
                Index = int(Name.split("-")[1])
                Move.FlowBoxUpDown(self.Tagger["FlowBox"], 1)
            elif Name.startswith("LanguageBox"):
                Move.FlowBoxUpDown(self.Languager["FlowBox"], 1)

    def on_MoveUp_activate(self, Widget):
        if self.Settings.get_realized():
            CurrentWidget = self.Selector.Widgets[self.Selector.CurrentItem]
            Name = CurrentWidget.get_parent().get_parent().get_name()
            if Name.startswith("Tag"):
                Index = int(Name.split("-")[1])
                Move.FlowBoxUpDown(self.Tagger["FlowBox"], -1)
            elif Name.startswith("LanguageBox"):
                Move.FlowBoxUpDown(self.Languager["FlowBox"], -1)

    def on_OpenOnCurrent_activate(self, Widget):
        if self.Settings.get_realized():
            self.Selector.Activate(0)

    def on_OpenOnNew_activate(self, Widget):
        if self.Settings.get_realized():
            self.Selector.Activate(1)

    def EnterLambda(self, Function, Index):
        return lambda: self.Enter(Function, Index)

    def Enter(self, Function, Index):
        if Index != self.Notebook.get_current_page():
            self.Notebook.set_current_page(Index)
        Function()

    def on_SelectPrevious_activate(self, Widget):
        if self.Settings.get_realized():
            self.Selector.Backward()

    def on_SelectNext_activate(self, Widget):
        if self.Settings.get_realized():
            self.Selector.Forward()

    def StopScroll(self, Widget, Event):
        Widget.emit_stop_by_name("scroll-event")

    def GetValue(self, Widget):
        try:
            return Widget.get_value()
        except:
            try:
                return Widget.get_active()
            except:
                try:
                    return Widget.get_text()
                except:
                    try:
                        return Widget.get_filename()
                    except:
                        try:
                            return Tag.GetTags(Widget)
                        except:
                            pass
        try:
            if "Pointer" in Widget:
                return KeyBind.Get(Widget["Pointer"])
        except:
            pass

    def SetValue(self, Widget, Value):
        try:
            Widget.set_value(Value)
        except:
            try:
                Widget.set_active(Value)
            except:
                try:
                    Widget.set_text(Value)
                except:
                    try:
                        Widget.set_filename(Value)
                    except:
                        try:
                            Tag.RemoveAll(Widget)
                            Tag.Append(Value, Widget)
                        except:
                            pass
        try:
            if "Pointer" in Widget:
                KeyBind.Set(Value, Widget["Pointer"])
        except:
            pass

    def ShowSettings(self, OverWrite=True):
        self.Loaded = False
        if Global.GetStarted():
            if OverWrite:
                self.LBRYSetting = json.loads(Preferences.Get())
                self.Session = self.LBRYSetting["Preferences"]["Session"]
                self.Keys = self.LBRYSetting["Preferences"]["Keyboard"]
            Settings = self.LBRYSetting["settings"]
            LBRYGTKSettings = self.LBRYSetting["Preferences"]
            Session = LBRYGTKSettings["Session"]
            for Setting in self.LBRYFields:
                self.SetValue(self.LBRYFields[Setting], Settings[Setting])

            for Setting in self.SettingFields:
                self.SetValue(
                    self.SettingFields[Setting], LBRYGTKSettings[Setting]
                )

            Tag.Append(LBRYGTKSettings["TagsLocal"], self.Taggers[0]["Pointer"])
            Tag.Append(
                LBRYGTKSettings["TagsShared"], self.Taggers[1]["Pointer"]
            )

            Identity.Update(self.Identifier["Pointer"])
            self.on_HomeType_changed(self.SettingFields["HomeType"])
            self.Loaded = True

            for Setting in self.SessionFields:
                self.SetValue(
                    self.SessionFields[Setting], self.Session[Setting]
                )

            for Setting in self.KeyFields:
                self.SetValue(self.KeyFields[Setting], self.Keys[Setting])

        Replace.Replace(self.TabPointer, "Settings")
        self.Title.set_text("Settings")

        self.SettingFields["AuthToken"].set_visibility(False)

    def on_Edit_button_press_event(self, Widget, Event):
        self.SettingFields["AuthToken"].set_visibility(
            not self.SettingFields["AuthToken"].get_visibility()
        )

    States = [
        ["Following", "[]"],
        ["YourTags", "[]"],
        ["Discover", "[]"],
        ["Library", "[]"],
        ["Library Search", "[]"],
        ["Collections", "[]"],
        ["Followed", "[]"],
        ["Uploads", "[]"],
        ["Channels", "[]"],
        ["NewPublication", "[]"],
        ["Settings", "[]"],
        ["Status", "[]"],
        ["Help", "[]"],
        ["Inbox", "[]"],
    ]

    def on_HomeType_changed(self, Widget):
        Children = Widget.get_parent().get_children()
        if Widget.get_active() == 0:
            for Index in range(len(self.States)):
                State = self.States[Index]
                if (
                    self.SettingFields["HomeFunction"].get_text() == State[0]
                    and self.SettingFields["HomeData"].get_text() == State[1]
                ):
                    self.PresetHome.set_active(Index)
                    break
            self.on_PresetHome_changed(self.PresetHome)
        else:
            for Index in range(2, len(Children)):
                Children[Index].set_visible(3 < Index)

    def on_MenuItem_activate(self, Widget, GotState):
        self.SettingFields["HomeFunction"].set_text(GotState[0])
        # TODO: For the one rewriting: consider a new way to do this
        if isinstance(GotState[1], int):
            GotState[1] = json.dumps(State.AsList(GotState[1]))
        self.SettingFields["HomeData"].set_text(GotState[1])

    def on_HomeFromPageHistory_button_press_event(self, Widget, Event):
        List = self.Stater.Before()
        Length = len(List)
        if Length != 0:
            Menu = Gtk.Menu.new()
            for Index in range(Length):
                Item = List[Index]
                MenuItem = Gtk.MenuItem.new_with_label(Item["Title"])
                MenuItem.connect(
                    "activate",
                    self.on_MenuItem_activate,
                    State.Export(
                        Item["Function"],
                        Item["Data"],
                    ),
                )
                Menu.attach(MenuItem, 0, 1, Length - Index - 1, Length - Index)
            Menu.show_all()
            Menu.popup_at_widget(
                Widget, Gdk.Gravity.SOUTH_WEST, Gdk.Gravity.NORTH_WEST
            )

    def on_PresetHome_changed(self, Widget):
        self.on_MenuItem_activate("", self.States[Widget.get_active()])

    def on_Export_button_press_event(self, Widget, Event):
        Dialog = Gtk.FileChooserDialog(
            "Export settings - LBRY-GTK",
            CAPI.ToObject(Global.Window),
            Gtk.FileChooserAction.SAVE,
            (
                Gtk.STOCK_CANCEL,
                Gtk.ResponseType.CANCEL,
                Gtk.STOCK_SAVE,
                Gtk.ResponseType.ACCEPT,
            ),
        )
        Dialog.set_do_overwrite_confirmation(True)
        Filter = Gtk.FileFilter.new()
        Filter.set_name("Json files")
        Filter.add_pattern("*.json")
        Dialog.add_filter(Filter)
        Dialog.set_current_name("LBRY-GTK-Settings.json")
        Dialog.set_current_folder(str(pathlib.Path.home()))
        Ran = Dialog.run()

        while (
            Ran == Gtk.ResponseType.ACCEPT
            and not Dialog.get_filename().endswith(".json")
        ):
            Dialog.set_current_name(Dialog.get_current_name() + ".json")
            Ran = Dialog.run()

        if Ran == Gtk.ResponseType.ACCEPT and Dialog.get_filename().endswith(
            ".json"
        ):
            Args = [Dialog.get_filename()]
            threading.Thread(None, self.ExportThread, None, Args).start()
        Dialog.destroy()
        return True

    def ExportThread(self, Path):
        # This will be done much differently but I won't do it
        self.LBRYSetting = json.loads(Preferences.Get())
        Table = copy.deepcopy(self.LBRYSetting)
        Json = json.dumps(Table, indent=4)
        with open(Path, "w") as File:
            File.write(Json)
        GLib.idle_add(self.ExportUpdate, True)

    def ExportUpdate(self, Success):
        Message = "The export was not successful."
        if Success:
            Message = "The export was successful."
        Popup.Message(Message)

    def on_Import_button_press_event(self, Widget, Event):
        Dialog = Gtk.FileChooserDialog(
            "Import settings - LBRY-GTK",
            CAPI.ToObject(Global.Window),
            Gtk.FileChooserAction.OPEN,
            (
                Gtk.STOCK_CANCEL,
                Gtk.ResponseType.CANCEL,
                Gtk.STOCK_OPEN,
                Gtk.ResponseType.ACCEPT,
            ),
        )
        Filter = Gtk.FileFilter.new()
        Filter.set_name("Json files")
        Filter.add_pattern("*.json")
        Dialog.add_filter(Filter)
        Dialog.set_current_folder(str(pathlib.Path.home()))
        if Dialog.run() == Gtk.ResponseType.ACCEPT:
            Args = [Dialog.get_filename()]
            threading.Thread(None, self.ImportThread, None, Args).start()
        Dialog.destroy()
        return True

    def ImportThread(self, Path):
        with open(Path, "r") as File:
            Table = json.load(File)
        self.LBRYSetting = copy.deepcopy(Table)
        self.Session = copy.deepcopy(Table["Preferences"]["Session"])
        self.Keys = copy.deepcopy(Table["Preferences"]["Keyboard"])
        GLib.idle_add(self.ImportUpdate, True)

    def ImportUpdate(self, Success):
        Message = "The export was not successful."
        if Success:
            Message = (
                "The export was successful. Click Save to save the settings."
            )
            self.ShowSettings(False)
        Popup.Message(Message)

    def on_Save_button_press_event(self, Widget, Event):
        # We grab the focus since some widgets may print out old values
        Widget.grab_focus()
        self.LBRYSetting = json.loads(Preferences.Get())
        LBRYGTKSettings = self.LBRYSetting["Preferences"]
        self.Session = LBRYGTKSettings["Session"]
        self.Keys = LBRYGTKSettings["Keyboard"]
        if self.Loaded:
            Settings = self.LBRYSetting["settings"]
            for Setting in self.LBRYFields:
                Settings[Setting] = self.GetValue(self.LBRYFields[Setting])

            for Setting in self.SettingFields:
                LBRYGTKSettings[Setting] = self.GetValue(
                    self.SettingFields[Setting]
                )

            LBRYGTKSettings["TagsLocal"] = Tag.GetTags(
                self.Taggers[0]["Pointer"]
            )
            LBRYGTKSettings["TagsShared"] = Tag.GetTags(
                self.Taggers[1]["Pointer"]
            )

            try:
                Name = json.loads(Identity.Get(self.Identifier["Pointer"]))[
                    "Name"
                ]
                LBRYGTKSettings["CommentChannel"] = Name
            except:
                pass

        for Setting in self.SessionFields:
            if Setting != "Server" and Setting != "Binary":
                self.Session[Setting] = self.GetValue(
                    self.SessionFields[Setting]
                )
        for Setting in self.KeyFields:
            self.Keys[Setting] = self.GetValue(self.KeyFields[Setting])

        self.Session["NewServer"] = self.GetValue(self.SessionFields["Server"])
        self.Session["NewBinary"] = self.GetValue(self.SessionFields["Binary"])

        KeyBinds = {}
        self.Duplicates = {}
        self.SameBindings = False
        for Setting in self.Keys:
            KeyText = self.Keys[Setting]
            if KeyText != "":
                if KeyText in KeyBinds:
                    self.SameBindings = True
                    if not KeyBinds[KeyText] in self.Duplicates:
                        self.Duplicates[KeyBinds[KeyText]] = []
                    self.Duplicates[KeyBinds[KeyText]].append(Setting)
                else:
                    KeyBinds[KeyText] = Setting

        GLib.idle_add(self.SaveHelper)

    def PrintDuplicates(self):
        Text = ""
        for Item in self.Duplicates:
            Text += Item + ": "
            for SubItem in self.Duplicates[Item]:
                Text += SubItem + ", "
            Text = Text[0:-2] + "\n"
        return Text[0:-1]

    def SaveHelper(self):
        Response = Gtk.ResponseType.OK
        if (
            self.Session["Server"] != self.Session["NewServer"]
            or self.Session["Binary"] != self.Session["NewBinary"]
        ):
            Dialog = Gtk.MessageDialog(
                CAPI.ToObject(Global.Window), buttons=Gtk.ButtonsType.OK_CANCEL
            )
            Dialog.props.text = "Warning: changing Binary or Server only activates after a restart."
            Response = Dialog.run()
            Dialog.destroy()
        if self.SameBindings:
            Popup.Message(
                "There are duplicate key bindings. Change them before the settings can be saved:\n"
                + self.PrintDuplicates()
            )
        elif Response == Gtk.ResponseType.OK:
            threading.Thread(None, self.SaveThread).start()

    def SaveThread(self):
        if self.Loaded:
            JsonData = json.loads(
                Preferences.Set(
                    Places.ConfigDir,
                    json.dumps(self.LBRYSetting),
                )
            )
            if "error" in JsonData:
                Popup.Error(JsonData["error"])

        GLib.idle_add(SettingsUpdate.UpdateWidth, self.Session)
        GLib.idle_add(
            SettingsUpdate.ChangeMenu,
            self.Session["MenuType"],
            self.Session["MenuIcon"],
        )
        GLib.idle_add(
            SettingsUpdate.ChangeMeta,
            self.Session["EnableMetaService"],
        )
        GLib.idle_add(SettingsUpdate.WindowAcceleratorCreate, self.Main)
        Image.UpdateMax()

    def NoToggleBefore(self, Widget):
        Widget.set_name(str(Widget.get_active()))

    def NoToggleAfter(self, Widget):
        Widget.set_active(Widget.get_name() == "True")
