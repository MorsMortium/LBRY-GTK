# TODO:

## Frontend
- Comment scrolling from notifications
- Debug window
- Display time in publications
- Display version number
- Fallback font for emojis
- First result in search is resolve
- First startup page
- List/Publication skeleton view
- Login improvements (thread, password shower)
- Native notifications
- Odysee emojis/stickers
- Optional GTK CSD
- Organize keybinds, with tooltips
- Scroll list with keybind selection
- Set publication page's TopBox to homogeneous and off on user action
- Tooltips everywhere
- Primary button press to clicked
- 3way comment layout (horizontal, vertical, adaptive)
- No comments/publication notice
- Keybind/selector for Box

## Backend
- Check keybinds on every startup
- Do not use GTK get_name/set_name
- Follow sync
- Global downloader class
- Split out Advanced Search
- Every lbry web instance, with librarian instances functional/link only
- Use Error.c, everywhere
- Get publication comments and creators hearts (NotificationDB)
- Resync CommentDB (edits/removals of comments)
- Runtime pixbuf/animation cache

## Features
- Ask for download location
- Cache management
- Comment ordering
- Delete button
- Delete notifications
- Downloads management
- Edit text in external editor
- Edit/delete own publications
- Error log to file
- Extra buttons
- Filter out shorts option
- Livestream play/search
- Multi modifier keybinds
- Not publication (settings, advanced search)
- Optional repost following
- Page up/down keybinds
- Read more for comments
- Set channel notifications
- Sidebar collapse option
- Simple upload
- Statistics page
- Tab history
- Upload settings
- Work without internet
- Youtube links converted to Invidious
- Watch later
- Global instance with multiple windows
- Open lbry:// links (MimeType=x-scheme-handler/mailto;)
- Built in video/audio/image player
- Middle click close/open new tab
- Markdown chapter links
- Resolve missing CDN images

## Misc.
- Alternative hub server(s)
- Documentation
- Group source files inside Source (Widget/Frontend/Backend)
- Missing help articles
- Organization repo
