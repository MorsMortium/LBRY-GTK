USR=/usr
appdir=$(USR)/share/applications
licensedir=$(USR)/share/licenses
icondir=$(USR)/share/icons/hicolor/scalable/apps
bindir=$(USR)/bin
sharedir=$(USR)/share/lbry-gtk

.PHONY : build
build :
	$(MAKE) -C ./Source

.PHONY : clean
clean :
	$(MAKE) -C ./Source clean

.PHONY : install
install : build

	for dir in "$(bindir)" "$(licensedir)/lbry-gtk" "$(appdir)" "$(icondir)"; do \
		mkdir -p "$(DESTDIR)$$dir";\
	done

	for dir in Glade Help Json Source; do \
		mkdir -p "$(DESTDIR)$(sharedir)/$$dir"; \
	done

	install -m 755 bin/* $(DESTDIR)$(bindir)
	install -m 644 share/icons/hicolor/scalable/apps/* $(DESTDIR)$(icondir)
	install -m 644 share/lbry-gtk/Glade/* $(DESTDIR)$(sharedir)/Glade/
	install -m 644 share/lbry-gtk/Help/* $(DESTDIR)$(sharedir)/Help/
	install -m 644 share/lbry-gtk/Json/* $(DESTDIR)$(sharedir)/Json/
	install -m 644 share/lbry-gtk/Source/*.py $(DESTDIR)$(sharedir)/Source/
	install -m 644 LICENSE $(DESTDIR)$(licensedir)/lbry-gtk/LICENSE
	install -m 644 lbry-gtk.desktop $(DESTDIR)$(appdir)/lbry-gtk.desktop

.PHONY : uninstall
uninstall :
	-rm $(DESTDIR)$(bindir)/lbry-gtk
	-rm $(DESTDIR)$(bindir)/lbry-gtk-convert
	-rm $(DESTDIR)$(icondir)/lbry-gtk.svg
	-rm $(DESTDIR)$(icondir)/lbry-gtk-lbc.svg
	-rm $(DESTDIR)$(appdir)/lbry-gtk.desktop
	-rm -r $(DESTDIR)$(licensedir)/lbry-gtk
	-rm -r $(DESTDIR)$(sharedir)
