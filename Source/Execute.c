/******************************************************************************\
* LBRY-GTK                                                                     *
* Copyright (C) 2021-2023 MorsMortium and Other Contributors                   *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU General Public License as published by         *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                *
* GNU General Public License for more details.                                 *
*                                                                              *
* You should have received a copy of the GNU General Public License            *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
\******************************************************************************/

// This file is responsible for opening a file with a program or a default
// opener in a cross platform manner

#define PY_SSIZE_T_CLEAN
#include <Python.h>
#include <string.h>
#include <Python.h>
#include <stdlib.h>
#include <stdio.h>
#include <gtk/gtk.h>

#include "Header/OS.h"
#include "Header/Execute.h"

void ExecuteFile(char *Uri, char *Application) {
	// This function is responsible for creating a full command from a Uri and
	// an Application, if any, then running it with GLib

	// Opener application
	char Opener[Max(strlen(Application), 8) + 1];

	// If Application is set, put that into Opener
	if (strlen(Application) != 0) {
		sprintf(Opener, "%s", Application);
	} else {
		sprintf(Opener, OSOpener);
	}

	// Parse Opener
	int ArgumentNumber;
	char **ParsedArguments;
	g_shell_parse_argv(Opener, &ArgumentNumber, &ParsedArguments, NULL);

	// Create argument list from parsed Opener and Uri
	char *Arguments[ArgumentNumber + 2];
	for (int Index = 0; Index < ArgumentNumber; ++Index) {
		Arguments[Index] = ParsedArguments[Index];
	}
	Arguments[ArgumentNumber] = Uri;
	Arguments[ArgumentNumber + 1] = NULL;

	// Open with GLib
	g_spawn_async(NULL, Arguments, NULL,
		G_SPAWN_SEARCH_PATH | G_SPAWN_STDOUT_TO_DEV_NULL |
		G_SPAWN_STDERR_TO_DEV_NULL, NULL, NULL, NULL, NULL);

	// Free data
	for (int Index = 0; Index < ArgumentNumber; ++Index) {
		free(ParsedArguments[Index]);
	}
	free(ParsedArguments);
}

// Everything under this line is removable after full C conversion
static PyObject *ExecuteFilePython(PyObject *Py_UNUSED(Self), PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Get arguments and pass it to the pythonless function
	PyObject *UnicodePointerUri, *UnicodePointerApp;
	if (!PyArg_ParseTuple(Args, "UU", &UnicodePointerUri, &UnicodePointerApp)) {
		return NULL;
	}

	char *Uri = (char *) PyUnicode_AsUTF8(UnicodePointerUri);
	char *Application = (char *) PyUnicode_AsUTF8(UnicodePointerApp);

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	ExecuteFile(Uri, Application);

	// Acquire lock
	PyEval_RestoreThread(Save);

	return PyLong_FromLong(0);
}

// Function names and other python data
static PyMethodDef ExecuteMethods[] = {
	{"Open", ExecuteFilePython, METH_VARARGS, "Execute"},
	{NULL, NULL, 0, NULL}
};

static struct PyModuleDef ExecutesModule = {
	PyModuleDef_HEAD_INIT, "Execute", "Execute module", -1, ExecuteMethods, 0,
	0, 0, 0
};

// The function name has to end with the module name
PyMODINIT_FUNC PyInit_Execute(void) {
	return PyModule_Create(&ExecutesModule);
}
