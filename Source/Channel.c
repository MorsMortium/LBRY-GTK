/******************************************************************************\
* LBRY-GTK                                                                     *
* Copyright (C) 2021-2023 MorsMortium and Other Contributors                   *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU General Public License as published by         *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                *
* GNU General Public License for more details.                                 *
*                                                                              *
* You should have received a copy of the GNU General Public License            *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
\******************************************************************************/

// This file deals with channel related tasks

#include <Python.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <jansson.h>

#include "Header/GTKExtra.h"
#include "Header/Request.h"
#include "Header/Filter.h"
#include "Header/Url.h"
#include "Header/Json.h"
#include "Header/Channel.h"

json_t *ChannelList(int PageSize, int Page, char *Server) {
	// This function lists the channels of the user

	json_t *Params = json_object();

	// Set all the values that will always be present
	json_object_set_new(Params, "page_size", json_integer(PageSize));
	json_object_set_new(Params, "page", json_integer(Page));
	json_object_set_new(Params, "no_totals", json_true());

	// Dump resulting parameters object
	char *ParamStr = json_dumps(Params, 0);
	json_decref(Params);

	// Make the request
	char Request[strlen(ParamStr) + 55];
	sprintf(Request, "{\"method\": \"channel_list\", \"params\": %s }",
		ParamStr);

	// Free dumped string
	free(ParamStr);

	// Get result
	json_t *Result = RequestJson(Server, Request);
	if (json_object_get(Result, "error") != NULL) {
		return Result;
	}

	// Get items
	json_t *Value, *Items = json_object_get(Result, "items");
	size_t Index, Length = json_array_size(Items);
	char *Urls[Length];
	json_array_foreach(Items, Index, Value) {
		// Get claim id
		Urls[Index] =
			JsonStringObject(Value, GTKExtraClaimId, GTKExtraMinusOne);
	}

	// Resolve urls, add them to items
	json_t *Resolved = UrlGet(Urls, Length, Server);
	json_array_foreach(Items, Index, Value) {
		json_object_set(Value, "resolved", json_array_get(Resolved, Index));
	}
	json_decref(Resolved);

	// Get parse stuff
	return FilterChannels(Result);
}

json_t *ChannelGet(char *Server) {
	// This function is responsible for getting every single user owned channel

	json_t *Channels = json_array();
	int Length, Page = 1;
	do {
		// Get single page of 5 channels, get actual number of channels in page
		json_t *NewChannels = ChannelList(5, Page, Server);
		++Page;
		Length = json_array_size(NewChannels);

		// Put every new channel in channels, destroy NewChannels
		size_t Index;
		json_t *Value;
		json_array_foreach(NewChannels, Index, Value) {
			json_array_append(Channels, Value);
		}
		json_decref(NewChannels);

		// While we got as many channels as asked
	} while (Length == 5);

	return Channels;
}

// Everything under this line is removable after full C conversion
static PyObject *ChannelGetPython(PyObject *Py_UNUSED(Self), PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Get arguments and pass it to the pythonless function
	char *Server;
	if (!PyArg_ParseTuple(Args, "s", &Server)) {
		return NULL;
	}

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	json_t *JsonData = ChannelGet(Server);

	// Acquire lock
	PyEval_RestoreThread(Save);

	return JsonToString(JsonData);
}

// Function names and other python data
static PyMethodDef ChannelMethods[] = {
	{"Get", ChannelGetPython, METH_VARARGS, "Get every channel"},
	{NULL, NULL, 0, NULL}
};

static struct PyModuleDef ChannelModule = {
	PyModuleDef_HEAD_INIT, "Channel", "Channel module", -1, ChannelMethods, 0,
	0, 0, 0
};

// The function name has to end with the module name
PyMODINIT_FUNC PyInit_Channel(void) {
	return PyModule_Create(&ChannelModule);
}
