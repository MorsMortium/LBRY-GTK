/******************************************************************************\
* LBRY-GTK                                                                     *
* Copyright (C) 2021-2023 MorsMortium and Other Contributors                   *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU General Public License as published by         *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                *
* GNU General Public License for more details.                                 *
*                                                                              *
* You should have received a copy of the GNU General Public License            *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
\******************************************************************************/

// This file is responsible for converting markdown text into text
// understandable by the client

#define PCRE2_CODE_UNIT_WIDTH 8

#include <Python.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <md4c-html.h>
#include <pcre2.h>

#include "Header/Parse.h"

// Regex pointers

pcre2_code *ParseUrl, *ParseMDUrl, *ParseProperty, *ParseNoP1, *ParseNoP2;
pcre2_code *ParseNoP3, *ParseRemove;

char *ParseReplace(char *String, const char *Find, const char *New, int Index,
	int ToReplace) {
	// This function replaces Find with New, in string, from Index
	// ToReplace times

	// Set number of Replaced to zero
	int Replaced = 0;

	// Location is where Find is in the string, from Index
	char *Location = NULL;
	if (Index < (int) strlen(String)) {
		Location = strstr(String + Index, Find);
	}

	// While Find is found and Index is not the end of String
	while (Location != NULL && Index != (int) strlen(String)) {
		// If the function replaced enough, exit loop
		if (ToReplace == Replaced++) {
			break;
		}

		// Get length of String from start to Find
		int StartLength = Location - String;

		// Create Start and End, long enough to contain strings before and
		// after Find
		char Start[StartLength + 1], End[strlen(Location) - strlen(Find) + 1];

		//Copy start of String into Start, end of String into End
		strncpy(Start, String, StartLength);
		Start[StartLength] = '\0';
		sprintf(End, "%s", Location + strlen(Find));

		// Get size difference between New and Find
		int Difference = -strlen(Find);
		if (New != NULL) {
			Difference += strlen(New);
		}

		if (Difference != 0) {
			// If it is not zero, grow or shrink String
			char *Temporary = realloc(String, strlen(String) + Difference + 1);

			// If realloc could not find a long enough memory, exit
			if (Temporary == NULL) {
				break;
			}

			// Overwrite String
			String = Temporary;
		}

		// Copy everything into String
		if (New != NULL) {
			sprintf(String, "%s%s%s", Start, New, End);
		} else {
			sprintf(String, "%s%s", Start, End);
		}

		// Set Index after replaced string, look for new occurence of Find
		Index = StartLength;
		if (New != NULL) {
			Index += strlen(New);
		}
		Location = strstr(String + Index, Find);
	}

	// Return fully replaced string
	return String;
}

int ParseMatchNumber(pcre2_code *Regex, char *String) {
	// This function is responsible for counting number of occurences of Regex
	// in String

	// Set Index, MatchNumber to 0, create match data holder
	int Index = 0, MatchNumber = 0;
	pcre2_match_data *Match = pcre2_match_data_create_from_pattern(Regex, NULL);

	// Check if Regex matched, if it did, fill Match
	int Matched =
		pcre2_match(Regex, (PCRE2_SPTR) String + Index, strlen(String + Index),
			0, 0, Match, NULL);

	// If matched
	while (!(Matched < 0)) {
		// Increase MatchNumber
		MatchNumber++;

		// Get end index of Match, set Index to that
		PCRE2_SIZE *OVector = pcre2_get_ovector_pointer(Match);
		Index += OVector[1];

		// Get new match
		Matched =
			pcre2_match(Regex, (PCRE2_SPTR) String + Index,
				strlen(String + Index), 0, 0, Match, NULL);
	}

	pcre2_match_data_free(Match);

	// Return MatchNumber
	return MatchNumber;
}

void ParseMatches(pcre2_code *Regex, char *String, int *Matches) {
	// This function is responsible for filling getting every match of Regex
	// Into a integer array

	// Set Index, MatchIndex to -1, create match data holder
	int Index = 0, MatchIndex = -1;
	pcre2_match_data *Match = pcre2_match_data_create_from_pattern(Regex, NULL);

	// Check if Regex matched, if it did, fill Match
	int Matched =
		pcre2_match(Regex, (PCRE2_SPTR) String + Index, strlen(String + Index),
			0, 0, Match, NULL);

	// If matched
	while (!(Matched < 0)) {
		// Get end and start of Match
		PCRE2_SIZE *OVector = pcre2_get_ovector_pointer(Match);

		// Add them to Matches
		Matches[++MatchIndex] = OVector[0] + Index;
		Matches[++MatchIndex] = OVector[1] + Index;

		// Set Index to end of Match
		Index += OVector[1];

		// Get new match
		Matched =
			pcre2_match(Regex, (PCRE2_SPTR) String + Index,
				strlen(String + Index), 0, 0, Match, NULL);
	}
	pcre2_match_data_free(Match);
}

char *ParseSubstitute(pcre2_code *Regex, char *New, char *String, int Matches,
	int Index, int ToReplace) {
	// This function is responsible for replacing Regex with New, in String
	// from Index, ToReplace times

	char *NewCopy = NULL;

	if (New != NULL) {
		// Create NewCopy as long as New
		NewCopy = malloc(strlen(New) + 1);

		// If malloc could not find a long enough memory, exit
		if (NewCopy == NULL) {
			return String;
		}
	}

	// Set Replaced to 0, create match data holder
	int Replaced = 0;
	pcre2_match_data *Match = pcre2_match_data_create_from_pattern(Regex, NULL);

	// Check if Regex matched, if it did, fill Match
	int Matched =
		pcre2_match(Regex, (PCRE2_SPTR) String + Index, strlen(String + Index),
			0, 0, Match, NULL);

	// If matched
	while (!(Matched < 0)) {
		// If the function replaced enough, exit loop
		if (ToReplace == Replaced++) {
			break;
		}

		// Get ends and starts of Match and groups
		PCRE2_SIZE *OVector = pcre2_get_ovector_pointer(Match);

		if (New != NULL) {
			// Copy New into NewCopy
			sprintf(NewCopy, "%s", New);

			// Go through as many groups as function caller wanted
			for (int Groups = 0; Groups < Matches * 2; Groups += 2) {
				// Create and fill variable containing group
				int GroupLength = OVector[Groups + 1] - OVector[Groups];
				char Group[GroupLength + 1];
				strncpy(Group, String + OVector[Groups] + Index, GroupLength);
				Group[GroupLength] = '\0';

				// Create a fill variable containg string to be replaced
				// \Number
				// Length because of possible length of Groups
				char Name[13];
				sprintf(Name, "\\%d", Groups / 2);

				// Replace \Number with Group
				NewCopy = ParseReplace(NewCopy, Name, Group, 0, -1);
			}
		}

		// Create a fill variable containg string to be replaced
		int ToReplaceLength = OVector[1] - OVector[0];
		char ToReplace[ToReplaceLength + 1];
		strncpy(ToReplace, String + OVector[0] + Index, ToReplaceLength);
		ToReplace[ToReplaceLength] = '\0';

		if (New != NULL) {
			// Replace ToReplace with NewCopy
			String = ParseReplace(String, ToReplace, NewCopy, 0, -1);
		} else {
			String = ParseReplace(String, ToReplace, NULL, 0, -1);
		}

		// Set Index to end of NewCopy in String
		Index += OVector[0];

		if (New != NULL) {
			Index += strlen(NewCopy);
		}

		// Get new match
		Matched =
			pcre2_match(Regex, (PCRE2_SPTR) String + Index,
				strlen(String + Index), 0, 0, Match, NULL);
	}

	// Free memory of NewCopy, Match
	if (New != NULL) {
		free(NewCopy);
	}
	pcre2_match_data_free(Match);

	// Return fully replaced string
	return String;
}

char *ParseUrlReplace(char *String, int MatchNumber, int *Matches) {
	// This function is responsible for converting every raw Url into a markdown
	// Url (Url -> [Url](Url))

	// Set Index, Shift to 0, create match data holder
	int Index = 0, Shift = 0;
	pcre2_match_data *Match =
		pcre2_match_data_create_from_pattern(ParseUrl, NULL);

	// Check if Regex matched, if it did, fill Match
	int Matched = pcre2_match(ParseUrl, (PCRE2_SPTR) String + Index,
			strlen(String + Index), 0,
			0, Match, NULL);

	// If matched
	while (!(Matched < 0)) {
		// Get end and start of Match
		PCRE2_SIZE *OVector = pcre2_get_ovector_pointer(Match);

		// Get url start and end, et NewIndex to end, InMarkdown to false
		int MatchEnd = OVector[1] + Index, MatchStart = OVector[0] + Index;
		int NewIndex = MatchEnd;
		bool InMarkdown = false;

		// Go through ever existing markdown url
		for (int MatchIndex = 0; MatchIndex < MatchNumber * 2;
			MatchIndex += 2) {
			// Get start and end of markdown url
			int NewMatchStart = Matches[MatchIndex] + Shift;
			int NewMatchEnd = Matches[MatchIndex + 1] + Shift;

			// Check for start and end overlaps
			bool Start = NewMatchStart < MatchStart && MatchStart < NewMatchEnd;
			bool End = NewMatchStart < MatchEnd && MatchEnd < NewMatchEnd;

			// If they are overlapping
			if (Start || End) {
				// If index of end of the markdown url is smaller than the
				// end of raw url, that will be NewIndex
				if (MatchStart < NewMatchEnd && NewMatchEnd < MatchEnd) {
					NewIndex = NewMatchEnd;
				}

				// Set InMarkdown to true and exit loop
				InMarkdown = true;
				break;
			}
		}

		// If url is in a markdown url
		if (InMarkdown) {
			// Set Index to NewIndex, get new match, and skip to end of loop
			Index = NewIndex;
			Matched =
				pcre2_match(ParseUrl, (PCRE2_SPTR) String + Index,
					strlen(String + Index), 0, 0, Match, NULL);
			continue;
		}

		// Get length of url
		int MatchLength = MatchEnd - MatchStart;

		// If url is at the end of string, increase length
		if ((int) strlen(String) + 1 == MatchEnd) {
			if ((int) strlen(String) == MatchEnd) {
				MatchLength++;
			}
		}

		// Create and fill variable holdin url
		char MatchString[MatchLength + 1];
		strncpy(MatchString, String + MatchStart, MatchLength);
		MatchString[MatchLength] = '\0';

		// If url ends with dot, remove it
		if (MatchString[MatchLength - 1] == '.') {
			MatchString[strlen(MatchString) - 1] = '\0';
		}

		// If url is only the protocol ("https://")
		if (strlen(strstr(MatchString, "://")) == 3) {
			// Set Index to MatchEnd, get new match, and skip to end of loop
			Index = MatchEnd;
			Matched =
				pcre2_match(ParseUrl, (PCRE2_SPTR) String + Index,
					strlen(String + Index), 0, 0, Match, NULL);
			continue;
		}

		// Format string in variable for gcc to not complain
		char ToInsert[] = "[%s]\(%s)";

		// Create and fill variable keeping url converted to markdown url
		char New[strlen(ToInsert) + strlen(MatchString) * 2];
		sprintf(New, ToInsert, MatchString, MatchString);

		// Get ammount of characters the new url is longer
		Shift += strlen(New) - (strlen(MatchString) - 1);

		// Replace url with markdown url in string
		String = ParseReplace(String, MatchString, New, Index, 1);

		// Set Index to end of markdown url, get new match
		Index = MatchStart + strlen(New);
		Matched =
			pcre2_match(ParseUrl, (PCRE2_SPTR) String + Index,
				strlen(String + Index), 0, 0, Match, NULL);
	}

	pcre2_match_data_free(Match);

	// Return fully converted String
	return String;
}

void ParseInit(void) {
	// This function is responsible for compiling every regex used in the file

	// Variables keeping errors
	int Number;
	PCRE2_SIZE Offset;

	// Regex for catching raw urls
	PCRE2_SPTR Regex =
		(PCRE2_SPTR) "(?<!\\]\\()(sftp|ftps|lbry)://[^\\s]+";
	ParseUrl =
		pcre2_compile(Regex, PCRE2_ZERO_TERMINATED, 0, &Number, &Offset, NULL);

	// Regex for catching markdown urls
	Regex = (PCRE2_SPTR) "\\[(.*?)\\]\\(([^)\\s]+?)(| [\"']([^\\s]*)[\"'])\\)";
	ParseMDUrl =
		pcre2_compile(Regex, PCRE2_ZERO_TERMINATED, 0, &Number, &Offset, NULL);

	// Regex for catching properties that should be removed
	Regex = (PCRE2_SPTR) " (start|alt|class|width|height)=\"[^\"]*\"";
	ParseProperty =
		pcre2_compile(Regex, PCRE2_ZERO_TERMINATED, 0, &Number, &Offset, NULL);

	// Regex for catching paragraphs in list items
	Regex = (PCRE2_SPTR) "<li>\n?<p>((.|\n)*?)</p>";
	ParseNoP1 =
		pcre2_compile(Regex, PCRE2_ZERO_TERMINATED, 0, &Number, &Offset, NULL);

	// Regex for catching paragraphs in quotes
	Regex = (PCRE2_SPTR) "<blockquote>\n<p>((.|\n)*?)</p>";
	ParseNoP2 =
		pcre2_compile(Regex, PCRE2_ZERO_TERMINATED, 0, &Number, &Offset, NULL);

	// Regex for catching paragraphs in quotes, when going any levels up
	Regex = (PCRE2_SPTR) "</blockquote>\n<p>((.|\n)*?)</p></blockquote>";
	ParseNoP3 =
		pcre2_compile(Regex, PCRE2_ZERO_TERMINATED, 0, &Number, &Offset, NULL);

	// Regex for removing not implemented tags
	Regex = (PCRE2_SPTR) "</?(module|click|stkr|center|pre|font)>";
	ParseRemove =
		pcre2_compile(Regex, PCRE2_ZERO_TERMINATED, 0, &Number, &Offset, NULL);
}

void ParseHtml(const MD_CHAR *Text, MD_SIZE Size, void *FullText) {
	// This function is responsible for copying html buffer text into the full
	// version. Helper function for md4c

	// Cast sent void data into string
	char **FullChar = (char **) FullText;

	// Get full size
	int NewSize = strlen(FullChar[0]) + Size + 1;

	// Resize string to full size
	char *Temporary = realloc(FullChar[0], NewSize);

	// If realloc could not find a long enough memory, exit
	if (Temporary == NULL) {
		return;
	}

	FullChar[0] = Temporary;

	// Copy buffer text into full version
	strncat(FullChar[0], Text, Size);
}

char *ParseNewLine(char *Line, bool *Types, int TypeLevel, int *Counters,
	int CounterLevel, char *NewLine) {
	// This function is responsible for creating a list item line, with correct
	// Tabbing and numbering/sign

	// Create variables full start, sign and one tab per level
	char ListAdd[13 + TypeLevel], Sign[] = "• ", Tabs[TypeLevel + 1];

	// If list has not ended
	if (TypeLevel != -1) {
		// Fill Tabs with one tab per level
		for (int Level = 0; Level < TypeLevel; ++Level) {
			Tabs[Level] = '\t';
		}
		Tabs[TypeLevel] = '\0';

		// Add Tabs to ListAdd if list is ordered, add counter and increase it
		// otherwise add Sign
		if (Types[TypeLevel]) {
			sprintf(ListAdd, "%s%d. ", Tabs, Counters[CounterLevel]++);
		} else {
			sprintf(ListAdd, "%s%s", Tabs, Sign);
		}
	} else {
		// If list ended set ListAdd to empty
		ListAdd[0] = '\0';
	}

	// Resize NewLine
	char *Temporary = realloc(NewLine, strlen(Line) + strlen(ListAdd) + 1);

	// If realloc could not find a long enough memory, exit
	if (Temporary == NULL) {
		return NewLine;
	}

	// Fill NewLine with Line
	NewLine = Temporary;
	sprintf(NewLine, "%s", Line);

	// Replace <li> with ListAdd, remove <ul>/<ol>
	NewLine = ParseReplace(NewLine, "<li>", ListAdd, 0, -1);
	NewLine = ParseReplace(NewLine, "<ul>", NULL, 0, -1);
	NewLine = ParseReplace(NewLine, "<ol>", NULL, 0, -1);
	return NewLine;
}

char *ParseMarkdown(char *Markdown) {
	// This function is responsible for the full conversion of a markdown text
	// into a version understandable by the client

	// Create string long enough to hold Markdown
	char *Text = malloc(strlen(Markdown) + 1);

	// If malloc could not find a long enough memory, exit
	if (Text == NULL) {
		return NULL;
	}

	//Fill Text
	sprintf(Text, "%s", Markdown);
	if (Text[0] == '\0') {
		return Text;
	}

	// Remove markdown url titles
	Text = ParseSubstitute(ParseMDUrl, "[\\1]\(\\2)", Text, 3, 0, -1);

	// Count and get markdown urls
	int MatchNumber = ParseMatchNumber(ParseMDUrl, Text);
	int Matches[MatchNumber * 2];
	ParseMatches(ParseMDUrl, Text, Matches);

	// Convert raw urls into markdown urls
	Text = ParseUrlReplace(Text, MatchNumber, Matches);

	// Allocate memory for markdown converted into html
	char *HtmlText[] = { malloc(1) };

	// If malloc could not find a long enough memory, exit
	if (HtmlText[0] == NULL) {
		return Markdown;
	}

	// Empty HtmlText, convert markdown into html
	HtmlText[0][0] = '\0';
	md_html(Text, strlen(
			Text), ParseHtml, HtmlText,
		MD_FLAG_STRIKETHROUGH | MD_FLAG_PERMISSIVEURLAUTOLINKS,
		0);

	// Free original string, set Text to HtmlText
	free(Text);
	Text = HtmlText[0];

	// Remove properties, not implemented tags
	Text = ParseSubstitute(ParseProperty, NULL, Text, 1, 0, -1);
	Text = ParseSubstitute(ParseRemove, NULL, Text, 1, 0, -1);

	// Remove paragraphs in list items and after opening quotes
	Text = ParseSubstitute(ParseNoP1, "<li>\\1", Text, 2, 0, -1);
	Text = ParseSubstitute(ParseNoP2, "<blockquote>\\1", Text, 2, 0, -1);

	// Remove new lines before closing quotes
	Text = ParseReplace(Text, "\n</blockquote>", "</blockquote>", 0, -1);

	// Remove paragraphs in quotes, when going any levels up
	pcre2_match_data *Match =
		pcre2_match_data_create_from_pattern(ParseUrl, NULL);
	while (!(pcre2_match(ParseNoP3, (PCRE2_SPTR) Text, strlen(Text), 0, 0,
		Match, NULL) < 0)) {
		Text =
			ParseSubstitute(ParseNoP3, "</blockquote>\n\\1</blockquote>",
				Text, 2, 0, -1);
	}

	pcre2_match_data_free(Match);

	// Convert html headers into pango markup sizes/line breaks
	Text =
		ParseReplace(Text, "<h1>", "<br><big><big><big><big><big><big>", 0, -1);
	Text = ParseReplace(Text, "<h2>", "<br><big><big><big><big><big>", 0, -1);
	Text = ParseReplace(Text, "<h3>", "<br><big><big><big><big>", 0, -1);
	Text = ParseReplace(Text, "<h4>", "<br><big><big><big>", 0, -1);
	Text = ParseReplace(Text, "<h5>", "<br><big><big>", 0, -1);
	Text = ParseReplace(Text, "<h6>", "<br><big>", 0, -1);
	Text =
		ParseReplace(Text, "</h1>",
			"</big></big></big></big></big></big><br>", 0, -1);
	Text =
		ParseReplace(Text, "</h2>", "</big></big></big></big></big><br>", 0,
			-1);
	Text = ParseReplace(Text, "</h3>", "</big></big></big></big><br>", 0, -1);
	Text = ParseReplace(Text, "</h4>", "</big></big></big><br>", 0, -1);
	Text = ParseReplace(Text, "</h5>", "</big></big><br>", 0, -1);
	Text = ParseReplace(Text, "</h6>", "</big><br>", 0, -1);

	// Remove paragraph closings, make paragraph openings line breaks
	Text = ParseReplace(Text, "</p>", NULL, 0, -1);
	Text = ParseReplace(Text, "<p>", "<br>", 0, -1);

	// Remove NULL-LEXENV tags, convert strikethrough
	Text = ParseReplace(Text, "<NULL-LEXENV>", NULL, 0, -1);
	Text = ParseReplace(Text, "<del>", "<s>", 0, -1);
	Text = ParseReplace(Text, "</del>", "</s>", 0, -1);

	// Convert bold, italic, code html tags into pango markup tags
	Text = ParseReplace(Text, "</strong>", "</b>", 0, -1);
	Text = ParseReplace(Text, "<strong>", "<b>", 0, -1);
	Text = ParseReplace(Text, "<em>", "<i>", 0, -1);
	Text = ParseReplace(Text, "</em>", "</i>", 0, -1);
	Text = ParseReplace(Text, "</code>", "</tt>", 0, -1);
	Text = ParseReplace(Text, "<code>", "<tt>", 0, -1);

	// Remove list item ends, add space before image tags
	Text = ParseReplace(Text, "</li>", NULL, 0, -1);
	Text = ParseReplace(Text, "<img", " <img", 0, -1);

	// Set TypeLevel and CounterLevel to -1, meaning not in a list/ordered list
	int TypeLevel = -1, CounterLevel = -1;

	char *Location = Text;

	// Types list containing every list. True is ordered list, false is
	// unordered. Counters keeps indexes of ordered lists
	bool *Types = malloc(sizeof(bool));
	int *Counters = malloc(sizeof(int));
	do {
		// If Location is on the end of Text, exit
		if (strlen(Location) == 0) {
			break;
		}

		// Get Length of line
		int LineEnd = strcspn(Location, "\n");

		// Get index of Location in Text
		int Index = strlen(Text) - strlen(Location);

		// If Length is zero, continue to the next line
		if (LineEnd == 0) {
			Location = strstr(Text + Index, "\n") + 1;
			continue;
		}

		// Create and fill Line with current line
		char Line[LineEnd + 1];
		strncpy(Line, Location, LineEnd);
		Line[LineEnd] = '\0';

		// Check if Line has ul, ol, /ul, /ol, li tags
		char *UL = strstr(Line, "<ul>"), *OL = strstr(Line, "<ol>");
		char *ULEnd = strstr(Line, "</ul>"), *OLEnd = strstr(Line, "</ol>");
		char *LI = strstr(Line, "<li>");

		if (UL != NULL) {
			// If Line has ul in it

			// If Line has li in it, and it is before ul
			if (LI != NULL && LI < UL) {
				// Create NewLine
				char *NewLine = malloc(1);

				// If malloc could not find a long enough memory, exit
				if (NewLine == NULL) {
					break;
				}

				// Fill NewLine, replace Line wih NewLine in Text, free NewLine
				NewLine =
					ParseNewLine(Line, Types, TypeLevel, Counters,
						CounterLevel, NewLine);
				Text = ParseReplace(Text, Line, NewLine, Index, 1);
				free(NewLine);
			} else {
				// Remove ul tag
				Text = ParseReplace(Text, "<ul>", NULL, Index, 1);
			}

			// Add unordered list to Types, increase TypeLevel
			Types = realloc(Types, sizeof(bool) * (++TypeLevel + 1));
			Types[TypeLevel] = false;
		} else if (OL != NULL) {
			// If Line has ol in it

			// If Line has li in it, and it is before ol
			if (LI != NULL && LI < OL) {
				// Create NewLine
				char *NewLine = malloc(1);

				// If malloc could not find a long enough memory, exit
				if (NewLine == NULL) {
					break;
				}

				// Fill NewLine, replace Line wih NewLine in Text, free NewLine
				NewLine =
					ParseNewLine(Line, Types, TypeLevel, Counters,
						CounterLevel, NewLine);
				Text = ParseReplace(Text, Line, NewLine, Index, 1);
				free(NewLine);
			} else {
				// Remove ol tag
				Text = ParseReplace(Text, "<ol>", NULL, Index, 1);
			}

			// Add ordered list to Types, increase TypeLevel
			Types = realloc(Types, sizeof(bool *) * (++TypeLevel + 1));
			Types[TypeLevel] = true;

			// Add 1 to Counters, increase CounterLevel
			Counters = realloc(Counters, sizeof(int) * (++CounterLevel + 1));
			Counters[CounterLevel] = 1;
		} else if (ULEnd != NULL || OLEnd != NULL) {
			// If one list ends

			// If an ordered list ends
			if (OLEnd != NULL) {
				// Remove last item from Counters, decrease CounterLevel
				// TODO: Investigate why CounterLevel might be 0 here already
				if (CounterLevel > 0) {
					Counters = realloc(Counters, sizeof(int) * CounterLevel--);
				}

				// Remove /ol tag
				Text = ParseReplace(Text, "</ol>", NULL, Index, 1);
			} else {
				//Remove /ul tag
				Text = ParseReplace(Text, "</ul>", NULL, Index, 1);
			}

			// Remove last item from Types, decrease TypeLevel
			// Check TypeLevel is not 0 for realloc
			if (TypeLevel) {
				Types = realloc(Types, sizeof(bool) * TypeLevel);
			}
			TypeLevel--;
		} else if (TypeLevel != -1 && LI != NULL) {
			// If a list started and there is a list item

			// Create NewLine
			char *NewLine = malloc(1);

			// If malloc could not find a long enough memory, exit
			if (NewLine == NULL) {
				break;
			}

			// Fill NewLine, replace Line wih NewLine in Text, free NewLine
			NewLine =
				ParseNewLine(Line, Types, TypeLevel, Counters,
					CounterLevel, NewLine);
			Text = ParseReplace(Text, Line, NewLine, Index, 1);
			free(NewLine);
		}

		// Get new line
		Location = strstr(Text + Index, "\n");

		// Move location one character forward, if found
		if (Location != NULL) {
			++Location;
		}

		// While line is found
	} while (Location != NULL);

	// Free every malloc variable
	free(Types);
	free(Counters);

	// Remove every type of double newline
	while (strstr(Text, "\n\n") != NULL) {
		Text = ParseReplace(Text, "\n\n", "\n", 0, -1);
	}
	while (strstr(Text, "<br>\n<br>") != NULL) {
		Text = ParseReplace(Text, "<br>\n<br>", "\n<br>", 0, -1);
	}
	while (strstr(Text, "\n<br>\n") != NULL) {
		Text = ParseReplace(Text, "\n<br>\n", "\n<br>", 0, -1);
	}

	// Replace html tags with newlines
	Text = ParseReplace(Text, "<br>", "\n", 0, -1);

	// Remove start and stop whitespace
	if (strlen(Text) != 0) {
		while (isspace(Text[strlen(Text) - 1])) {
			Text[strlen(Text) - 1] = '\0';
		}

		while (isspace(Text[0])) {
			memmove(Text, Text + 1, strlen(Text));
		}
	}

	return Text;
}
