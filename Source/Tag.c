/******************************************************************************\
* LBRY-GTK                                                                     *
* Copyright (C) 2021-2023 MorsMortium and Other Contributors                   *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU General Public License as published by         *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                *
* GNU General Public License for more details.                                 *
*                                                                              *
* You should have received a copy of the GNU General Public License            *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
\******************************************************************************/

// This file is responsible for managing the Tag widget

#include <Python.h>
#include <gtk/gtk.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "Header/Global.h"
#include "Header/Places.h"
#include "Header/GTKExtra.h"
#include "Header/Tabs.h"
#include "Header/Tag.h"

void TagChange(gpointer Widget, bool *State) {
	// This function is responsible for selecting or unselecting a tag

	// Get togglebutton of tag, set it to State
	GtkWidget *Child = gtk_bin_get_child((GtkBin *) Widget);
	gtk_toggle_button_set_active((GtkToggleButton *) Child, State[0]);
}

void TagChangeAll(GtkWidget *FlowBox, bool State) {
	// This function is responsible for selecting or unselecting every tags

	// Get list of tags
	GList *Children = gtk_container_get_children((GtkContainer *) FlowBox);

	// Call TagChange on all of them, free list
	g_list_foreach(Children, (GFunc) TagChange, &State);
	g_list_free(Children);
}

gboolean TagOnFlowBoxChildActivated(GtkWidget *Child) {
	// This function is responsible for selecting clicked tag
	gtk_button_clicked((GtkButton *) gtk_bin_get_child((GtkBin *) Child));
	return FALSE;
}

void TagSelect(TagData *Data) {
	// This function is responsible for selecting first tag

	// Get list of tags
	GList *Children =
		gtk_flow_box_get_selected_children((GtkFlowBox *) Data->FlowBox);

	// If there are tags, select first, free list
	if (g_list_length(Children) != 0) {
		TagOnFlowBoxChildActivated(g_list_nth_data(Children, 0));
	}
	g_list_free(Children);
}

void TagAdd(const char *Text, TagData *Data) {
	// This function is responsible for adding a single tag

	// If tag is empty, return
	if (strlen(Text) == 0) {
		return;
	}

	// If tag already exists in widget, return
	int Index = 0;
	while (Data->Tags[Index] != NULL) {
		if (strcmp(Text, Data->Tags[Index++]) == 0) {
			return;
		}
	}

	// Grow size of list of tags, if cannot, return
	char **Temporary = realloc(Data->Tags, sizeof(char *) * (Index + 2));
	if (Temporary == NULL) {
		return;
	}
	Data->Tags = Temporary;

	// Get memory for extra tag, if cannot, return
	Data->Tags[Index] = malloc(strlen(Text) + 1);
	if (Data->Tags[Index] == NULL) {
		Data->Tags = realloc(Data->Tags, sizeof(char *) * (Index + 1));
		return;
	}

	// Write tag into array of tags, close array with NULL
	sprintf(Data->Tags[Index], "%s", Text);
	Data->Tags[Index + 1] = NULL;

	// Create tag widget, get its label
	GtkWidget *NewTag = gtk_check_button_new_with_label(Text);
	GtkWidget *Label = gtk_bin_get_child((GtkBin *) NewTag);

	// Set label to wrap
	gtk_label_set_line_wrap((GtkLabel *) Label, true);
	gtk_label_set_line_wrap_mode((GtkLabel *) Label, PANGO_WRAP_WORD_CHAR);

	// Connect signals for hover highlighting
	g_object_connect(G_OBJECT(NewTag), "signal::enter-notify-event",
		GTKExtraOnEnterNotifyEvent, NULL, "signal::leave-notify-event",
		GTKExtraOnLeaveNotifyEvent, NULL, NULL);

	// Add to flowbox
	gtk_container_add((GtkContainer *) Data->FlowBox, NewTag);

	// Get actual child of flowbox, remove its padding
	GtkWidget *FlowChild = gtk_widget_get_parent(NewTag);
	GtkStyleContext *StyleContext = gtk_widget_get_style_context(FlowChild);
	gtk_style_context_add_provider(StyleContext,
		(GtkStyleProvider *) GTKExtraCss,
		GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);

	// Empty entry, display everything
	gtk_entry_set_text((GtkEntry *) Data->Entry, "");
	gtk_widget_show_all(Data->FlowBox);

	// If tag widget is part of a language widget, empty its entry too
	if (Data->LanguageEntry != NULL) {
		gtk_entry_set_text((GtkEntry *) Data->LanguageEntry, "");
	}
}

void TagAppend(char **List, TagData *Data) {
	// This function is responsible for adding an array of tags

	// Go through array until NULL, add tags one by one
	int Index = 0;
	while (List[Index] != NULL) {
		TagAdd(List[Index++], Data);
	}
}

gboolean TagOnAddClicked(G_GNUC_UNUSED GtkWidget *Widget, TagData *Data) {
	// This function is responsible for adding tag, when add is clicked
	TagAdd(gtk_entry_get_text((GtkEntry *) Data->Entry), Data);
	return FALSE;
}

void TagRemove(gpointer Widget, TagData *Data) {
	// This function is responsible for removing a single tag, if it is selected

	// Get child of widget, containing text
	GtkWidget *Child = gtk_bin_get_child((GtkBin *) Widget);

	// If tag is selected
	if (gtk_toggle_button_get_active((GtkToggleButton *) Child)) {
		// Get text of tag
		GtkWidget *GrandChild = gtk_bin_get_child((GtkBin *) Widget);
		const char *Label = gtk_button_get_label((GtkButton *) GrandChild);

		// Go through every tag
		int Index = 0;
		bool Remove = true;
		while (Data->Tags[Index] != NULL) {
			// Search for tag, if found, free it, set remove to false
			if (Remove && strcmp(Data->Tags[Index], Label) == 0) {
				free(Data->Tags[Index]);
				Remove = false;
			}

			// If tag was freed, move next tag to its place
			if (!Remove) {
				Data->Tags[Index] = Data->Tags[Index + 1];
			}
			++Index;
		}

		// Reduce size of array
		Data->Tags = realloc(Data->Tags, sizeof(char *) * (Index));

		// Close array with NULL, remove tag widget
		Data->Tags[Index - 1] = NULL;
		gtk_widget_destroy(Widget);
	}
}

gboolean TagOnRemoveClicked(G_GNUC_UNUSED GtkWidget *Widget, TagData *Data) {
	// This function is responsible for removing every tag, that was selected

	// Get list of every tag
	GList *Children =
		gtk_container_get_children((GtkContainer *) Data->FlowBox);

	// Remove them, if selected, by TagRemove, free list
	g_list_foreach(Children, (GFunc) TagRemove, Data);
	g_list_free(Children);
	return FALSE;
}

gboolean TagOnEntryKeyPressEvent(G_GNUC_UNUSED GtkWidget *Widget,
	GdkEventKey *Event, TagData *Data) {
	// This function is responsible for clicking Add, when return is pressed
	if (strcmp(gdk_keyval_name(Event->keyval), "Return") == 0) {
		TagOnAddClicked(NULL, Data);
	}

	// For allowing handling of other key presses
	return FALSE;
}

gboolean TagOnSelectAllClicked(G_GNUC_UNUSED GtkWidget *Widget, TagData *Data) {
	// This function is responsible for selecting every tag
	TagChangeAll(Data->FlowBox, true);
	return FALSE;
}

gboolean TagOnUnselectAllClicked(G_GNUC_UNUSED GtkWidget *Widget,
	TagData *Data) {
	// This function is responsible for unselecting every tag
	TagChangeAll(Data->FlowBox, false);
	return FALSE;
}

gboolean TagOnFlowBoxKeyPressEvent(G_GNUC_UNUSED GtkWidget *Widget,
	GdkEventKey *Event, TagData *Data) {
	// This function is responsible for clicking remove, when delete or
	// backspace is pressed
	char *Key = gdk_keyval_name(Event->keyval);
	if (Data->Edit && (strcmp(Key, "Delete") == 0
		|| strcmp(Key, "BackSpace") == 0)) {
		TagOnRemoveClicked(NULL, Data);
	}

	// For allowing handling of other key presses
	return FALSE;
}

void TagRemoveAll(TagData *Data) {
	// This function is responsible for selecting and removing every single tag
	TagOnSelectAllClicked(NULL, Data);
	TagOnRemoveClicked(NULL, Data);
}

void TagActive(gpointer Widget, GtkWidget **TagLabels) {
	// This function is responsible for adding a tag to an array, if it is
	// selected

	// Get child, check if it is selected
	GtkWidget *Child = gtk_bin_get_child((GtkBin *) Widget);
	if (gtk_toggle_button_get_active((GtkToggleButton *) Child)) {
		// Find last added tag
		int Index = 0;
		while (TagLabels[Index] != NULL) {
			++Index;
		}

		// Add tag after last
		TagLabels[Index] = Child;
		TagLabels[Index + 1] = NULL;
	}
}

gboolean TagOnTagDestroy(G_GNUC_UNUSED GtkWidget *Widget, TagData *Data) {
	// This function is responsible for freeing all data, after widget is
	// destroyed

	// Free tags
	int Index = 0;
	while (Data->Tags[Index] != NULL) {
		free(Data->Tags[Index++]);
	}

	// Free list of tags, struct
	free(Data->Tags);
	free(Data);
	return FALSE;
}

PyObject *TagBaseArguments(int TagIndex, const char **Tags, char *NewTitle) {
	// This function is responsible for creating arguments for python search
	// functions
	// TODO: Remove this

	// Create list of tags, add tags one by one
	PyObject *PyTags = PyList_New(TagIndex);
	for (int Index = 0; Index < TagIndex; ++Index) {
		PyObject *Tag = PyUnicode_FromString(Tags[Index]);
		PyList_SetItem(PyTags, Index, Tag);
	}

	// Create search dict, add tags to it
	PyObject *Dict = PyDict_New();
	PyDict_SetItemString(Dict, "any_tags", PyTags);
	Py_DECREF(PyTags);

	// Create list
	PyObject *BaseArguments = PyList_New(4);

	// Add new title
	PyObject *Object = PyUnicode_FromString(NewTitle);
	PyList_SetItem(BaseArguments, 0, Object);

	// Add None x2
	Object = Py_None;
	Py_INCREF(Object);
	PyList_SetItem(BaseArguments, 1, Object);
	Object = Py_None;
	Py_INCREF(Object);
	PyList_SetItem(BaseArguments, 2, Object);

	// Add dict containing tags
	PyList_SetItem(BaseArguments, 3, Dict);

	return BaseArguments;
}

gboolean TagOnSearchButtonPressEvent(G_GNUC_UNUSED GtkWidget *Widget,
	GdkEventButton *Event, TagData *Data) {
	// This function is responsible for starting a search

	// Get every tag widget
	GList *Children =
		gtk_container_get_children((GtkContainer *) Data->FlowBox);

	// Create widget and string array of max size
	GtkWidget *TagLabels[g_list_length(Children) + 1];
	const char *Tags[g_list_length(Children) + 1];

	// Close widget array, get selected tags, free list
	TagLabels[0] = NULL;
	g_list_foreach(Children, (GFunc) TagActive, TagLabels);
	g_list_free(Children);

	// If there are no selected tags, return
	if (TagLabels[0] == NULL) {
		return FALSE;
	}

	// Go through every tag widget
	int Index = 0, TagIndex = 0, TitleLength = 11;
	while (TagLabels[Index] != NULL) {
		// Get labels of tag widgets, calculate final length of title
		Tags[TagIndex] = gtk_button_get_label((GtkButton *) TagLabels[Index++]);
		TitleLength += strlen(Tags[TagIndex++]) + 1;
	}

	// Create title
	char NewTitle[TitleLength];
	sprintf(NewTitle, "%s", "Tagsearch:");
	int CurrentTitle = 10;

	// Add every tag to title, close it
	for (Index = 0; Index < TagIndex; ++Index) {
		NewTitle[CurrentTitle++] = ' ';
		strcpy(NewTitle + CurrentTitle, Tags[Index]);
		CurrentTitle += strlen(Tags[Index]);
	}
	NewTitle[TitleLength - 1] = '\0';

	// Get the mousekey if not already set
	if (Event != NULL) {
		Data->MouseKey = Event->button;
	}

	// Primary mouse button is left, current page, middle is middle, new page
	if (Data->MouseKey == GDK_BUTTON_PRIMARY) {
		json_t *TagsList = json_array();
		for (Index = 0; Index < TagIndex; ++Index) {
			json_array_append(TagsList, json_string(Tags[Index]));
		}

		ThreadsMakeExecute(Data->TabData, ThreadsButtonHelper, NewTitle, NULL,
			"any_tags", TagsList);
	} else if (Data->MouseKey == GDK_BUTTON_MIDDLE) {
		// TODO: Remove this and rewrite with c functions
		// Get GIL state, create arguments
		PyGILState_STATE State = PyGILState_Ensure();
		PyObject *BaseArguments =
			TagBaseArguments(TagIndex, Tags, NewTitle);

		// Create list for function and data, fill it
		PyObject *FinalList = PyList_New(2);
		PyList_SetItem(FinalList, 0, PyUnicode_FromString("Advanced Search"));
		PyList_SetItem(FinalList, 1, BaseArguments);

		// Create arguments with dummy GTK values, call function
		PyObject *Arguments = Py_BuildValue("ssO", ".", "", FinalList);
		Py_DECREF(FinalList);
		Py_DECREF(PyObject_Call(Data->AddPage, Arguments, NULL));

		// Free arguments, release GIL
		Py_DECREF(Arguments);
		PyGILState_Release(State);
	}

	return FALSE;
}

void TagKeybindHelper(TagData *Data, int Button) {
	// This function is responsible for clicking add on keybind left clicks
	if (Button == 0) {
		TagOnAddClicked(NULL, Data);
	}
}

TagData *TagCreate(bool Edit, TabsData *TabData, PyObject *AddPage) {
	// This function is responsible for creating the Tag widget

	// Allocate memory for data and tags, abort if NULL
	TagData *Data = malloc(sizeof(TagData));
	if (Data == NULL) {
		abort();
	}
	Data->Tags = malloc(sizeof(char *));
	if (Data->Tags == NULL) {
		abort();
	}

	// Get glade file name, load it in builder
	char WidgetFile[strlen(PlacesGlade) + 12];
	sprintf(WidgetFile, "%sTag.glade", PlacesGlade);
	gtk_builder_add_from_file(GlobalBuilder, WidgetFile, NULL);

	// Get widgets used
	Data->FlowBox = (GtkWidget *) gtk_builder_get_object(GlobalBuilder,
			"FlowBox");
	Data->Entry = (GtkWidget *) gtk_builder_get_object(GlobalBuilder, "Entry");
	Data->Tag = (GtkWidget *) gtk_builder_get_object(GlobalBuilder, "Tag");

	// Set editability
	Data->Edit = Edit;

	// Set tags to empty, set AddPage function, set LanguageEntry to NULL
	Data->Tags[0] = NULL;
	Data->AddPage = AddPage;
	Data->LanguageEntry = NULL;

	// TabData pointer
	Data->TabData = TabData;

	if (!Edit) {
		// If not editable, hide entry, add and remove buttons
		gtk_widget_hide(Data->Entry);
		gtk_widget_hide((GtkWidget *) gtk_builder_get_object(GlobalBuilder,
			"Add"));
		gtk_widget_hide((GtkWidget *) gtk_builder_get_object(GlobalBuilder,
			"Remove"));
	} else {
		// If editable, hide search button
		gtk_widget_hide((GtkWidget *) gtk_builder_get_object(GlobalBuilder,
			"Search"));
	}

	// Connect signals with data
	gtk_builder_connect_signals_full(GlobalBuilder, GTKExtraConnect, Data);

	return Data;
}

// Everything under this line is removable after full C conversion
static PyObject *TagSelectPython(PyObject *Py_UNUSED(Self), PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Fill DataPointer with sent data
	uintptr_t DataPointer;
	if (!PyArg_ParseTuple(Args, "L", &DataPointer)) {
		return NULL;
	}

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	TagSelect((TagData *) DataPointer);

	// Acquire lock
	PyEval_RestoreThread(Save);

	return PyLong_FromLong(0);
}

static PyObject *TagRemoveAllPython(PyObject *Py_UNUSED(Self), PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Fill DataPointer with sent data
	uintptr_t DataPointer;
	if (!PyArg_ParseTuple(Args, "L", &DataPointer)) {
		return NULL;
	}

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	TagRemoveAll((TagData *) DataPointer);

	// Acquire lock
	PyEval_RestoreThread(Save);

	return PyLong_FromLong(0);
}

static PyObject *TagAppendPython(PyObject *Py_UNUSED(Self), PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Fill ListObject, DataPointer with sent data
	PyObject *ListObject;
	uintptr_t DataPointer;
	if (!PyArg_ParseTuple(Args, "OL", &ListObject, &DataPointer)) {
		return NULL;
	}

	// Get length of python list, create array of same length
	int Length = PyObject_Length(ListObject);
	char *List[Length + 1];

	// Get python object per index, convert and put it into c array
	for (int Index = 0; Index < Length; ++Index) {
		PyObject *Name = PyList_GetItem(ListObject, Index);
		List[Index] = (char *) PyUnicode_AsUTF8(Name);
	}

	List[Length] = NULL;

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	TagAppend(List, (TagData *) DataPointer);

	// Acquire lock
	PyEval_RestoreThread(Save);

	return PyLong_FromLong(0);
}

static PyObject *TagCreatePython(PyObject *Py_UNUSED(Self), PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Fill Edit, AddPage with sent data
	uintptr_t TabPointer;
	long Edit;
	PyObject *AddPage;
	if (!PyArg_ParseTuple(Args, "lLO", &Edit, &TabPointer, &AddPage)) {
		return NULL;
	}

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	TagData *Data = TagCreate((bool) Edit, (TabsData *) TabPointer, AddPage);

	// Acquire lock
	PyEval_RestoreThread(Save);

	PyObject *Dict = PyDict_New();

	PyObject *Object = PyLong_FromVoidPtr((void *) Data->Tag);
	PyDict_SetItemString(Dict, "Tag", Object);
	Py_DECREF(Object);

	Object = PyLong_FromVoidPtr((void *) Data->FlowBox);
	PyDict_SetItemString(Dict, "FlowBox", Object);
	Py_DECREF(Object);

	Object = PyLong_FromVoidPtr((void *) Data->Entry);
	PyDict_SetItemString(Dict, "Entry", Object);
	Py_DECREF(Object);

	Object = PyLong_FromLong((long) Data->Edit);
	PyDict_SetItemString(Dict, "Edit", Object);
	Py_DECREF(Object);

	Object = PyLong_FromVoidPtr((void *) Data);
	PyDict_SetItemString(Dict, "Pointer", Object);
	Py_DECREF(Object);

	return Dict;
}

static PyObject *TagKeybindHelperPython(PyObject *Py_UNUSED(
		Self), PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Fill DataPointer, Button with sent data
	uintptr_t DataPointer;
	long Button;
	if (!PyArg_ParseTuple(Args, "Ll", &DataPointer, &Button)) {
		return NULL;
	}

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	TagKeybindHelper((TagData *) DataPointer, (int) Button);

	// Acquire lock
	PyEval_RestoreThread(Save);

	return PyLong_FromLong(0);
}

static PyObject *TagGetTagsPython(PyObject *Py_UNUSED(Self), PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Fill DataPointer with sent data
	uintptr_t DataPointer;
	if (!PyArg_ParseTuple(Args, "L", &DataPointer)) {
		return NULL;
	}

	TagData *Data = (TagData *) DataPointer;

	// Get number of tags
	int Length = 0;
	while (Data->Tags[Length] != NULL) {
		++Length;
	}

	// Create list, fill it
	PyObject *List = PyList_New(Length);
	for (int Index = 0; Index < Length; ++Index) {
		PyList_SetItem(List, Index, PyUnicode_FromString(Data->Tags[Index]));
	}

	return List;
}

static PyObject *TagOnSearchButtonPressEventPython(PyObject *Py_UNUSED(
		Self), PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Fill DataPointer with sent data
	uintptr_t DataPointer;
	int Button;
	if (!PyArg_ParseTuple(Args, "Li", &DataPointer, &Button)) {
		return NULL;
	}

	// Set the data ready
	TagData *Data = (TagData *) DataPointer;
	Data->MouseKey = Button;

	TagOnSearchButtonPressEvent(NULL, NULL, Data);

	return PyLong_FromLong(0);
}

// Function names and other python data
static PyMethodDef TagMethods[] = {
	{"Select", TagSelectPython, METH_VARARGS, ""},
	{"Create", TagCreatePython, METH_VARARGS, ""},
	{"RemoveAll", TagRemoveAllPython, METH_VARARGS, ""},
	{"Append", TagAppendPython, METH_VARARGS, ""},
	{"KeybindHelper", TagKeybindHelperPython, METH_VARARGS, ""},
	{"GetTags", TagGetTagsPython, METH_VARARGS, ""},
	{"OnSearchButtonPressEvent", TagOnSearchButtonPressEventPython,
	 METH_VARARGS, ""},
	{NULL, NULL, 0, NULL}
};

static struct PyModuleDef TagModule = {
	PyModuleDef_HEAD_INIT, "Tag", "Tag module", -1, TagMethods, 0, 0, 0, 0
};

// The function name has to end with the module name
PyMODINIT_FUNC PyInit_Tag(void) {
	return PyModule_Create(&TagModule);
}
