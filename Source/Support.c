/******************************************************************************\
* LBRY-GTK                                                                     *
* Copyright (C) 2021-2023 MorsMortium and Other Contributors                   *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU General Public License as published by         *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                *
* GNU General Public License for more details.                                 *
*                                                                              *
* You should have received a copy of the GNU General Public License            *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
\******************************************************************************/

// This file deals with sending tips and boosts

#include <Python.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <jansson.h>
#include <sys/time.h>

#include "Header/Request.h"
#include "Header/Json.h"
#include "Header/Support.h"

json_t *SupportCreate(char *ClaimId, double Amount, bool Tip, char *ChannelName,
	char *ChannelId, char *Server) {
	// This function sends a tip or a boost to a claim

	json_t *Params = json_object();

	// Set all the values that will always be present
	json_object_set_new(Params, "claim_id", json_string(ClaimId));
	char AmountString[50];
	sprintf(AmountString, "%lf", Amount);
	json_object_set_new(Params, "amount", json_string(AmountString));

	// Set optional values
	if (Tip) {
		json_object_set_new(Params, "tip", json_true());
	}

	if (ChannelName != NULL) {
		json_object_set_new(Params, "channel_name", json_string(ChannelName));
	}

	if (ChannelId != NULL) {
		json_object_set_new(Params, "channel_id", json_string(ChannelId));
	}

	// Dump resulting parameters object
	char *ParamStr = json_dumps(Params, 0);
	json_decref(Params);

	// Make the request
	char Request[strlen(ParamStr) + 55];
	sprintf(Request, "{\"method\": \"support_create\", \"params\": %s }",
		ParamStr);

	// Free dumped string
	free(ParamStr);

	// Get result
	return RequestJson(Server, Request);
}

// Everything under this line is removable after full C conversion
static PyObject *SupportCreatePython(PyObject *Py_UNUSED(Self),
	PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Get arguments and pass it to the pythonless function
	char *ClaimId, *ChannelName, *ChannelId, *Server;
	double Amount;
	long Tip;
	if (!PyArg_ParseTuple(Args, "sdlsss", &ClaimId, &Amount, &Tip, &ChannelName,
		&ChannelId, &Server)) {
		return NULL;
	}

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	if (strlen(ChannelName) == 0) {
		ChannelName = NULL;
	}
	if (strlen(ChannelId) == 0) {
		ChannelId = NULL;
	}

	json_t *JsonData =
		SupportCreate(ClaimId, Amount, (bool) Tip, ChannelName, ChannelId,
			Server);

	// Acquire lock
	PyEval_RestoreThread(Save);

	return JsonToString(JsonData);
}

// Function names and other python data
static PyMethodDef SupportMethods[] = {
	{"Create", SupportCreatePython, METH_VARARGS, "Create tip or boost"},
	{NULL, NULL, 0, NULL}
};

static struct PyModuleDef SupportModule = {
	PyModuleDef_HEAD_INIT, "Support", "Support module", -1, SupportMethods, 0,
	0, 0, 0
};

// The function name has to end with the module name
PyMODINIT_FUNC PyInit_Support(void) {
	return PyModule_Create(&SupportModule);
}
