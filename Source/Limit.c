/******************************************************************************\
* LBRY-GTK                                                                     *
* Copyright (C) 2021-2023 MorsMortium and Other Contributors                   *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU General Public License as published by         *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                *
* GNU General Public License for more details.                                 *
*                                                                              *
* You should have received a copy of the GNU General Public License            *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
\******************************************************************************/

// Counting currently running instances of functions with optional max number
// and shutdown notice

#include <time.h>
#include <stdbool.h>
#include <stdlib.h>
#include <pthread.h>

#include "Header/Limit.h"

LimitData *LimitCreate(int Max) {
	// Create a limit instance

	// Allocate memory, abort on failure
	LimitData *Data = malloc(sizeof(LimitData));
	if (Data == NULL) {
		abort();
	}

	// Init lock, set got/default values
	pthread_mutex_init(&Data->Lock, NULL);
	Data->Max = Max;
	Data->Current = 0;
	Data->Stop = false;

	return Data;
}

void LimitDestroy(LimitData *Data) {
	// Signal shutdown of app, wait until every instance stopped, then destroy
	// the limit instance

	// Lock, signal, check if there are instances running, unlock
	pthread_mutex_lock(&Data->Lock);
	Data->Stop = true;
	bool Recheck = Data->Current != 0;
	pthread_mutex_unlock(&Data->Lock);

	while (Recheck) {
		// While there are instances running

		// Sleep for 0.01 second
		struct timespec Remaining, Request = { 0, 10000000 };
		nanosleep(&Request, &Remaining);

		// Recheck if there are instances running
		pthread_mutex_lock(&Data->Lock);
		Recheck = Data->Current != 0;
		pthread_mutex_unlock(&Data->Lock);
	}

	// Destroy lock, free data
	pthread_mutex_destroy(&Data->Lock);
	free(Data);
}

bool LimitIncrease(LimitData *Data) {
	// Checks if app is not shutting down, if so, waits until it can give an
	// instance, increase counter and reports success/failure

	while (true) {
		// Until it could give an instance, or the app is shutting down

		// Lock for cross thread usage (always unlocked before sleep)
		pthread_mutex_lock(&Data->Lock);

		// Check if app is already shutting down, return true in that case
		if (Data->Stop) {
			pthread_mutex_unlock(&Data->Lock);
			return true;
		}

		// If Max is -1 (unlimited) or more than current increase Current and
		// return false
		if (Data->Current < Data->Max || Data->Max == -1) {
			Data->Current++;
			pthread_mutex_unlock(&Data->Lock);
			return false;
		}
		pthread_mutex_unlock(&Data->Lock);

		// Sleep for 0.01 second
		struct timespec Remaining, Request = { 0, 10000000 };
		nanosleep(&Request, &Remaining);
	}
}

void LimitDecrease(LimitData *Data) {
	// Decrease counter by one

	// Lock, decrease, unlock
	pthread_mutex_lock(&Data->Lock);
	Data->Current--;
	pthread_mutex_unlock(&Data->Lock);
}

void LimitMax(LimitData *Data, int Max) {
	// Set max to new value

	// Lock, set max, unlock
	pthread_mutex_lock(&Data->Lock);
	Data->Max = Max;
	pthread_mutex_unlock(&Data->Lock);
}
