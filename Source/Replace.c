/******************************************************************************\
* LBRY-GTK                                                                     *
* Copyright (C) 2021-2023 MorsMortium and Other Contributors                   *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU General Public License as published by         *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                *
* GNU General Public License for more details.                                 *
*                                                                              *
* You should have received a copy of the GNU General Public License            *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
\******************************************************************************/

// This file is responsible for switching the current view (list, publications)
// and for getting what the current view is

#define PY_SSIZE_T_CLEAN
#include <Python.h>
#include <stdbool.h>
#include <gtk/gtk.h>
#include <stdio.h>

#include "Header/Tabs.h"
#include "Header/Replace.h"

void ReplaceInit(GtkWidget **GotWidgets, char **GotNames, GtkWidget *GotSpace,
	int GotLength, TabsData *TabData) {
	// This function is responsible for creating main data structure containing
	// Everything needed by functions

	// Get data struct from tab data and set arrays to it
	ReplaceData *Data;
	TabsGet(TabData, Replacer, Data);

	Data->Widgets = malloc(sizeof(GtkWidget *) * GotLength);
	Data->Names = malloc(sizeof(char *) * GotLength);

	// Copy GotWidgets into Widgets
	memcpy(Data->Widgets, GotWidgets, sizeof(GtkWidget *) * GotLength);

	int MaxNameLength = 0;
	for (int Index = 0; Index < GotLength; ++Index) {
		// Get length of current name, calculate max length from it
		int NameLength = strlen(GotNames[Index]) + 1;
		MaxNameLength = Max(MaxNameLength, NameLength);

		// Allocate memory for string, copy name into it
		Data->Names[Index] = malloc(NameLength);
		sprintf(Data->Names[Index], "%s", GotNames[Index]);
	}

	// Set Space, and number of items in arrays
	Data->Space = GotSpace;
	Data->Length = GotLength;

	// Allocate memory for keeping current widget in Space, and fiil it with " "
	Data->Current = malloc(MaxNameLength);
	sprintf(Data->Current, " ");
}

void ReplaceRemove(GtkWidget *Widget, gpointer Space) {
	// This function is responsible for removing Widget from Space

	// Remove Widget from Space
	gtk_container_remove((GtkContainer *) Space, Widget);
}

void ReplaceSet(TabsData *TabData, char *Name) {
	// This function is responsible for replacing current widget with one called
	// by Name

	ReplaceData *Data;
	TabsGet(TabData, Replacer, Data);

	// If current widget is requested, do nothing
	if (strcmp(Data->Current, Name) == 0) {
		return;
	}

	// Set current widget to requested, empty Space
	sprintf(Data->Current, "%s", Name);
	gtk_container_foreach((GtkContainer *) Data->Space, ReplaceRemove,
		Data->Space);

	for (int Index = 0; Index < Data->Length; ++Index) {
		// If Name is found
		if (strcmp(Data->Names[Index], Name) == 0) {
			// Get widget and its parent
			GtkWidget *Widget = Data->Widgets[Index];
			GtkWidget *Parent = gtk_widget_get_parent(Widget);

			// If it has a parent, remove from it
			if (Parent != NULL) {
				gtk_container_remove((GtkContainer *) Parent, Widget);
			}

			// Add it to Space
			gtk_container_add((GtkContainer *) Data->Space, Widget);
			break;
		}
	}
}

char *ReplaceGet(ReplaceData *Data) {
	// This function is responsible for getting the name of the current widget

	// Get current widget in Space
	return Data->Current;
}

void ReplaceFree(ReplaceData *Data) {
	// This function is responsible for freeing up memory allocated by Init

	// Free up every alloced memory
	free(Data->Widgets);
	for (int Index = 0; Index < Data->Length; ++Index) {
		free(Data->Names[Index]);
	}

	free(Data->Names);
}

// Everything under this line is removable after full C conversion
static PyObject *ReplaceInitPython(PyObject *Py_UNUSED(Self), PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Fill Object1, Object2 and LongPointer with sent data
	PyObject *Object1, *Object2;
	uintptr_t LongPointer, TabPointer;
	if (!PyArg_ParseTuple(Args, "OOLL", &Object1, &Object2, &LongPointer,
		&TabPointer)) {
		return NULL;
	}

	// Get length of python list, create arrays of same length
	int ListLength = PyObject_Length(Object1);
	GtkWidget *WidgetList[ListLength];
	char *NameList[ListLength];

	// Get python objects per index, convert and put them into c arrays
	for (int Index = 0; Index < ListLength; ++Index) {
		PyObject *Widget, *Name;
		Widget = PyList_GetItem(Object1, Index);
		Name = PyList_GetItem(Object2, Index);
		WidgetList[Index] = (GtkWidget *) PyLong_AsLongLong(Widget);
		NameList[Index] = (char *) PyUnicode_AsUTF8(Name);
	}

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	// Get Space
	GtkWidget *Space = (GtkWidget *) LongPointer;

	// Pass arguments to the pythonless function
	ReplaceInit(WidgetList, (char **) NameList, Space, ListLength,
		(TabsData *) TabPointer);

	// Acquire lock
	PyEval_RestoreThread(Save);

	// Return what it returns
	Py_RETURN_NONE;
}

static PyObject *ReplaceSetPython(PyObject *Py_UNUSED(Self), PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Fill LongPointer, Name with sent data
	uintptr_t TabPointer;
	char *Name;
	if (!PyArg_ParseTuple(Args, "Ls", &TabPointer, &Name)) {
		return NULL;
	}

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	// Pass arguments to the pythonless function
	ReplaceSet((TabsData *) TabPointer, Name);

	// Acquire lock
	PyEval_RestoreThread(Save);

	return PyLong_FromLong(0);
}

static PyObject *ReplaceGetPython(PyObject *Py_UNUSED(Self), PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Fill LongPointer with sent data
	uintptr_t TabPointer;
	if (!PyArg_ParseTuple(Args, "L", &TabPointer)) {
		return NULL;
	}

	ReplaceData *Replacer;
	TabsGet(((TabsData *) TabPointer), Replacer, Replacer);

	// Pass arguments to the pythonless function, return what it returns
	PyObject *Return = PyUnicode_FromString(ReplaceGet(Replacer));

	return Return;
}

// Function names and other python data
static PyMethodDef ReplaceMethods[] = {
	{"Init", ReplaceInitPython, METH_VARARGS, "Init"},
	{"Replace", ReplaceSetPython, METH_VARARGS, "Replace"},
	{"GetSpace", ReplaceGetPython, METH_VARARGS, "Get Space"},
	{NULL, NULL, 0, NULL}
};

static struct PyModuleDef ReplaceModule = {
	PyModuleDef_HEAD_INIT, "Replace", "Replace module", -1, ReplaceMethods, 0,
	0, 0, 0
};

// The function name has to end with the module name
PyMODINIT_FUNC PyInit_Replace(void) {
	return PyModule_Create(&ReplaceModule);
}
