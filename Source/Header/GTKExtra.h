#ifndef GTKEXTRA_H_
#define GTKEXTRA_H_

#include <gtk/gtk.h>

// Symbols from library/executable
extern GModule *GTKExtraSymbols;

extern GtkCssProvider *GTKExtraCss;

extern char *GTKExtraClaimId[];
extern char *GTKExtraMinusOne;

// This function is responsible for connecting functions to GTK signals,
// with extra passed data
void GTKExtraConnect(GtkBuilder *Builder, GObject *SignalObject,
	const gchar *SignalName, const gchar *HandlerName,
	GObject *ConnectObject, GConnectFlags Flags,
	gpointer Data);

// Function to return some nice string balance
void GTKExtraNumberLabel(GtkWidget *Label, double Value);

// This function is responsible for destroying a widget as a GTK signal handler
void GTKExtraDestroy(GtkWidget *Widget, gpointer Unused);

// This function is responsible for setting the name of a toggle
// button from its active status
gboolean GTKExtraNoToggleBefore(GtkToggleButton *Widget);

// This function is responsible for setting the active status of a toggle
// button from its name
gboolean GTKExtraNoToggleAfter(GtkToggleButton *Widget);

// This function is responsible for not letting the widget scroll
gboolean GTKExtraStopScroll(GtkWidget *Widget);

// This function is responsible for selecting a widget on mouse hover
gboolean GTKExtraOnEnterNotifyEvent(GtkWidget *Widget);

// This function is responsible for unselecting a widget when mouse hover stops
gboolean GTKExtraOnLeaveNotifyEvent(GtkWidget *Widget);

// This function is responsible for creating the 0 padding css style
void GTKExtraStart(void);

// This function is responsible for destroying the 0 padding css style
void GTKExtraStop(void);

#endif
