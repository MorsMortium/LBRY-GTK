#ifndef LIST_H_
#define LIST_H_

#include <gtk/gtk.h>
#include <jansson.h>
#include <pthread.h>

#include "Order.h"
#include "Box.h"
#include "DateTime.h"
#include "Replace.h"
#include "Tag.h"

// ListFunction types
// There are 4 different types of list views
enum ListButtons {
	ListSearchButton,
	ListWalletButton,
	ListFileButton,
	ListInboxButton,
};

// ListFunction names similar to types above
enum ListNames {
	ListContentName,
	ListWalletName,
	ListFileName,
	ListNotificationName,
};

// Simple wrapper for ListPlaceData
struct ListWrapper {
	bool NoErrorHad;
	json_t *RowData;
	TabsData *TabData;
};

// List data struct
typedef struct ListData {
	// AddPage python callobject/function
	PyObject *AddPage;

	// Tells List.c to load more of the same content if user wants
	bool Started;

	// KillThread boolean to easily exit other List threads in progress
	bool KillThread;

	// The count of threads still in progress
	int ThreadCount;

	// Count of patch of loaded items
	int LoadCount;

	// Count of claims in cached Claims
	int ClaimCount;

	// How manyeth page is loaded so far
	int ListPage;

	// Sets whether to display grid or list of items
	int ListDisplay;

	// ListButton/Name for ListFunctions defining how and what data is shown
	int ListButton, ListName;

	// Temporarily tells the running thread to wait for boxes to be placed in
	// to the list view
	int LastPlaced;

	// Title of the page
	char *Title;

	// Array of claims cached to check for duplicates
	char **Claims;

	// Json data passed to List.c
	json_t *Json;

	// Data for all 4 functions used by List.c
	void *FunctionData[4];

	// The data of a box in list (only one of them)
	BoxData *Box;

	// OrderData from Order.c
	OrderData *Order;

	// DateTimeData from DateTime.c
	DateTimeData *DateTime;

	// ReplaceData from Replace.c
	ReplaceData *Replace;

	// All of the tags in Advanced
	TagData **TagList;

	// Lock for threads
	pthread_mutex_t Lock;

	// Spaces that the List.c changes to
	GtkContainer **Spaces;

	// Widgets used by List.c
	// TODO: Make these global in something like Client.c
	GtkWidget *MainSpace, *NoContentNotice, *Grid, *Advanced;
	GtkLabel *TitleLabel;
	GtkEntry *TextLabel, *ChannelLabel;
	GtkComboBox *ClaimType, *Inequality, *DateType;
} ListData;

// Creates list data
void ListCreate(GtkWidget *NoContentNotice, GtkWidget **WalletSpaceParts,
	GtkWidget *MainSpace, GtkWidget *Grid, GtkWidget *Advanced,
	GtkContainer *Spaces[], int SpacesSize, GtkEntry *Text, GtkEntry *Channel,
	GtkLabel *Title, GtkComboBox *ClaimType,
	GtkComboBox *DateType, GtkComboBox *Inequality,
	TagData *TagList[], int TagsSize, DateTimeData *DateTimer,
	OrderData *Orderer,
	PyObject *AddPage, TabsData *TabData);

// Free all of data allocated by ListCreate
void ListFree(ListData *Data);

// Updates all the widgets in the page
gboolean ListButtonUpdate(gpointer Args);

// List button function
void ListButton(TabsData *TabData);

#endif
