#ifndef THREADS_H_
#define THREADS_H_

#include <Python.h>
#include <stdbool.h>
#include <jansson.h>

#include "List.h"

// TODO: Once rewrite is done, we can probably ditch this struct entirely
// NOTE: Remember to remove from Tabs.c as well
typedef struct ThreadsData {
	// Python callobjects/functions
	PyObject *KillAll, *ShowPublication, *ShowNewPublication;
	PyObject *ShowSettings;

	// Temporarily hold json item
	json_t *Json;
} ThreadsData;

// Arguments used in threads
typedef struct ThreadsArguments {
	// Tells if loaded from states or new call to function
	bool IsState;

	// Name used by some threads
	char *Name;

	// Path used by some threads
	char *Path;

	// Json data associated with the thread
	json_t *Json;

	// Pointer to tab that this thread belongs to
	TabsData *TabData;
} ThreadsArguments;

// Initiates threads adding functions Threads to State
void ThreadsStart(void);

// Free all of the allocated arguments
void ThreadsFreeArguments(ThreadsArguments *Data);

// Makes the argument struct used by threads functions
ThreadsArguments *ThreadsMakeArguments(char *Name, char *Path, char *Key,
	json_t *Json, TabsData *TabData);

// Executes given function with arguments
bool ThreadsExecute(void *(*Function)(void *), void *Args);

// Makes arguments for Thread and executes
bool ThreadsMakeExecute(TabsData *TabData, void *(*Function)(void *),
	char *Name, char *Path, char *Key, json_t *Json);

// Get publication thread
void *ThreadsGetPublication(void *Args);

// Status thread
void *ThreadsStatus(void *Args);

// Button thread
void *ThreadsButtonHelper(void *Args);

// Place the data to list
void ThreadsToLister(ListData *Lister, int ListName, int ButtonName,
	char *Title, const char *Key, json_t *Items);

#endif
