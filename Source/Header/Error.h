#ifndef ERROR_H_
#define ERROR_H_

// Warnings that should not be converted to full errors
extern char ErrorWarnings[1][100];

// This function is responsible for generating the error message
char *ErrorGet(int Code, const char *Message);

#endif
