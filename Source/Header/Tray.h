#ifndef TRAY_H_
#define TRAY_H_

#include <gtk/gtk.h>
#include <stdbool.h>

// Struct for widget data
typedef struct TrayData {
	// Subwidgets of Tray used in and outside of this file
	GtkWidget *Tray, *Menu, *ShowHide;
} TrayData;

// This function is responsible for creating the Tray widget
TrayData *TrayCreate(bool TrayMinimized);

#endif
