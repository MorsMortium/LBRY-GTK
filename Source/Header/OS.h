#ifndef OS_H_
#define OS_H_

#ifdef _WIN32

// Type of OS
#define OSType 0

// Default opener application
#define OSOpener "start"

// Directory separator
#define OSSeparator "\\"
#else
#define OSSeparator "/"
#endif

#ifdef __APPLE__
#define OSType 1
#define OSOpener "open"
#endif

#ifdef __unix__
#define OSType 2
#define OSOpener "xdg-open"
#endif

// Takes care of creating a directory
void OSMakeDirectory(const char *Name, unsigned int Mode);

// Signal handler to print stacktrace before exit(1)
void OSSIGHandler(int Signal);

// This function trims the "fat" off of the free'd memory
void OSMemoryTrimmer(void);

// This function is responsible for getting the absolute path to binary
// For uniformity
char *OSAbsolutePath(char *Arg0);

// Returns a stacktrace
char *OSBBacktrace(void);

#endif
