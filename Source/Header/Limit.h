#ifndef LIMIT_H_
#define LIMIT_H_

#include <stdbool.h>
#include <pthread.h>

typedef struct LimitData {
	// Lock for multi thread checking
	pthread_mutex_t Lock;

	// Max and current number of instances
	int Max, Current;

	// Whether the app is shutting down and does not accept new instances
	bool Stop;
} LimitData;

// Create a limit instance
LimitData *LimitCreate(int Max);

// Signal shutdown of app, wait until every instance stopped, then destroy
// the limit instance
void LimitDestroy(LimitData *Data);

// Check if app is not shutting down, if so, wait until it can give an instance,
// increase counter and report success/failure
bool LimitIncrease(LimitData *Data);

// Decrease counter by one
void LimitDecrease(LimitData *Data);

// Set max to new value
void LimitMax(LimitData *Data, int Max);

#endif
