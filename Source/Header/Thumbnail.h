#ifndef THUMBNAIL_H_
#define THUMBNAIL_H_

#include <stdbool.h>
#include <gtk/gtk.h>

// Struct for widget data
typedef struct ThumbnailData {
	// Widgets of Thumbnail used in and outside of this file
	GtkWidget *Thumbnail, *Frame, *EventBox;

	// Url and previous url of image
	char *Url, *OldUrl;

	// Size of thumbnail, calculation modes
	int Width, Height, Calculate;

	// Cursors for hovering
	GdkCursor *DefaultCursor, *PointerCursor;

	// Whether the thumbnail displays a channel
	bool Channel;

	// Display mode (0: regular; 1: circled; 2: rounded)
	char Mode;
} ThumbnailData;

// This function is responsible for creating the Thumbnail widget
ThumbnailData *ThumbnailCreate(int Width, int Height, char Mode);

// This function is responsible for setting thumbnail url and channel
void ThumbnailSetExtra(ThumbnailData *Data, bool Channel, char *Url);

#endif
