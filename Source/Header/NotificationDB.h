#ifndef NOTIFICATIONDB_H_
#define NOTIFICATIONDB_H_

#include <jansson.h>
#include <sqlite3.h>
#include <stdbool.h>

// This is used for checking if we are busy
extern bool NotificationDBBusy;

// This here holds row info used in comments thread
struct NotificationDBRows {
	char *ClaimID;
	char *ParentID;
	char *UserID;
};

// Save notifications to DB
int NotificationDBSave(sqlite3 *DB, char *ImageUrl, char *Title,
	char *Channel, char *ContentType, char *Url, char *Claim,
	unsigned int Timestamp);

// Finish the check for notifications
void NotificationDBFinish(char *ImageUrl, char *Title, char *Channel,
	char *ContentType, char *Url, char *Claim, unsigned int Timestamp);

// Check for notifications from belled channels
void *NotificationDBChannels(void);

// Check for notifications from comment-replies
void *NotificationDBComments(void);

// Check if there are new notifications
int NotificationDBCheck(void);

// List all notifications in DB
json_t *NotificationDBList(char *ContentType, int Page, int PageSize,
	char *Server);

#endif
