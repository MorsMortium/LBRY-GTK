#ifndef IDENTITY_H_
#define IDENTITY_H_

#include <gtk/gtk.h>
#include <jansson.h>
#include <stdbool.h>

// List of available identities
json_t *IdentityList = NULL;

// Struct for widget data
typedef struct IdentityData {
	// Subwidgets of Identity used in and outside of this file
	GtkWidget *Identities, *Active, *IdentitiesBox;

	// Wether the widget is displayed under a comment
	bool Comment;
} IdentityData;

// Creates the Identity widget
IdentityData *IdentityCreate(bool Comment);

// Fills widget with list of channels, selects main channel, if needed
void IdentityUpdate(IdentityData *Data);

// Gets currently selected channel, or empty channel
json_t *IdentityGet(IdentityData *Data);

#endif
