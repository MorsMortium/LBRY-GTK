#ifndef KEYBIND_H_
#define KEYBIND_H_

#include <gtk/gtk.h>

// Struct for widget data
typedef struct KeyBindData {
	// Subwidgets of keybind used in and outside of this file
	GtkWidget *KeyBind, *Modifier, *Character, *Use;
} KeyBindData;

static const unsigned int KeybindMasks[] =
{ GDK_MOD1_MASK, GDK_CONTROL_MASK, GDK_SUPER_MASK, GDK_HYPER_MASK,
  GDK_META_MASK };

// Creates a KeyBind widget
KeyBindData *KeyBindCreate(void);

// Convert string to key symbol and mask
unsigned int *KeyBindConvert(char *Text);

// Get the active keybind in a widget
void KeyBindGet(char *Out, KeyBindData *Data);

// Set keybind to a widget
void KeyBindSet(char *Text, KeyBindData *Data);

#endif
