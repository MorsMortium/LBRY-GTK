#ifndef CONNECT_H_
#define CONNECT_H_

#include <stdbool.h>
#include <pthread.h>
#include <jansson.h>

// Variables for async stopping of Start, in case of early exit
extern pthread_mutex_t ConnectLock;
extern bool ConnectExitStart;

// This function is responsible for getting the status of lbrynet and
// and returning it as a json object
json_t *ConnectStatus(char *Server);

// This function is responsible for stopping Start in an async manner
void ConnectCreateExit(void);

// This function is responsible for initiating everything used by the file
bool ConnectStart(void);

// This function is responsible for deleting everything used by the file
void ConnectStop(void);

// This function is responsible for checking the status of lbrynet
bool ConnectCheck(char *Server);

// This function is responsible for starting lbrynet and returning whether
// it started
void ConnectCreate(char *Binary, char *Server);

// This function is responsible for stopping lbrynet, and returning whether
// it ran prior to stopping it
bool ConnectDestroy(char *Server);

#endif
