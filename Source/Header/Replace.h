#ifndef REPLACE_H_
#define REPLACE_H_

#include <gtk/gtk.h>

// Forward declare TabData
typedef struct TabsData TabsData;

// Max macro
#define Max(i, j) (((i) > (j)) ? (i) : (j))

// Struct with every data needed for operation
typedef struct ReplaceData {
	char *Current;
	char **Names;
	GtkWidget **Widgets, *Space;
	int Length;
} ReplaceData;

// This function is responsible for creating main data structure containing
// everything needed by functions
void ReplaceInit(GtkWidget **GotWidgets, char **GotNames,
	GtkWidget *GotSpace, int GotLength, TabsData *TabData);

// This function is responsible for replacing current widget with one called
// by Name
void ReplaceSet(TabsData *TabData, char *Name);

// This function is responsible for getting the name of the current widget
char *ReplaceGet(ReplaceData *Data);

// This function is responsible for freeing up memory allocated by Init
void ReplaceFree(ReplaceData *Data);

#endif
