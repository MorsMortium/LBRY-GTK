#ifndef STATE_H_
#define STATE_H_

// Function type for state
typedef struct StateFunction {
	void *(*Function)(void *);
	char Name[20];
} StateFunction;

// Struct containing the history of pages (functions / arguments)
// TODO: May get reference to statefunction struct
typedef struct StateData {
	// Title of the state
	char *Title;

	// Function used to load the state
	void *(*Function)(void *);

	// Arguments used by the function
	void *Args;

	// Linked list navigation
	struct StateData *Previous;
	struct StateData *Next;
} StateData;

// All known state functions listed here
#define StateKnownFunctions 20
extern StateFunction StateFunctions[StateKnownFunctions];

// Free everything allocated by state
void StateFree(StateData *Data);

// Save state to history
void StateSave(char *Title, StateData **GotData, void *(*Function)(
		void *), void *Args);

// Check what state we talking about and execute that with given data
void StateImport(char *FunctionName, void *Args);

#endif
