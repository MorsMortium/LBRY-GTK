#ifndef BOX_H_
#define BOX_H_

#include <Python.h>
#include <gtk/gtk.h>
#include <jansson.h>

#include "Profile.h"

// Forward declared tab data
typedef struct TabsData TabsData;

// Struct for data provided by Filter
// TODO: Move into filter
typedef struct BoxPublicationData {
	// Type, title of publication, url of thumbnail, publication, channel
	char *Type, *Title, *Thumbnail, *Url, *Creator;

	// Url of thumbnail of channel, name of channel, wallet/notification action
	char *Profile, *Name, *Action;

	// Number of blocks since wallet action, time of post/action, number of
	// publications in channel
	long Confirmations, TimeStamp, Number;

	// Value of channel, value of post
	double Value, Support;
} BoxPublicationData;

// Struct for widget data
typedef struct BoxData {
	// Subwidgets of Box used in and outside of this file
	GtkWidget *Box, *Type, *Thumbnail;

	// Last rendered width of title, number of rows in title
	int TextWidth, TitleRows;

	// If the widget is displayed in a grid or list
	bool Grid;

	// Profile widget data
	ProfileData *Profiler;

	// Publication title and url from Row
	char *Title, *Url;

	// Tab data
	TabsData *TabData;

	// Functions for opening on current/new page
	// TODO: do this with C functions
	PyObject *AddPage;
} BoxData;

// This function is responsible for creating the Box widget
BoxData *BoxCreate(bool Grid, json_t *GotRow,
	int Type, PyObject *AddPage, GtkWidget *FlowBox, int Width, int TitleRows,
	int Padding, bool ThumbnailRounding, bool ProfileCircling,
	int ListChannelWidth, bool PublicationProfile, int PublicationProfileSize,
	TabsData *TabData);

#endif
