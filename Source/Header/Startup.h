#ifndef STARTUP_H_
#define STARTUP_H_

#include <Python.h>
#include <gtk/gtk.h>

#include "Status.h"

// Startup data containing all needed info for StartupHelper
typedef struct {
	// Stage tells where in the execution we are
	int Stage;

	// Cycle count is for stage 2 when ConnectCheck is repeated
	unsigned long CycleCount;

	// Started tells if client was to start lbrynet
	bool Started;

	// Widgets used in startup
	GtkWidget *Balance;
	GtkWidget *Signing;

	// Other data used by startup helper
	StatusData *Statuser;
	TabsData *TabData;

	// TODO: Remove when SettingsUpdate is rewritten
	PyObject *SettingsUpdate;
} StartupData;

void StartupInit(GtkWidget *GotBalance, GtkWidget *GotSigning,
	StatusData *Statuser, TabsData *TabData, PyObject *SettingsUpdate);

#endif
