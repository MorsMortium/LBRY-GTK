#ifndef SIDEPANEL_H_
#define SIDEPANEL_H_

#include <Python.h>
#include <gtk/gtk.h>

// Struct for widget data
typedef struct SidePanelData {
	// Gtk items
	GtkWidget *SidePanel, *ToggleButton, *Revealer;

	// TODO: Change to C functions SameTab and NewTab
	// NOTE: SameTab and NewTab are new names for SamePage and NewPage
	PyObject *Mainer;
} SidePanelData;

// Creates sidepanel widget
SidePanelData *SidePanelCreate(PyObject *Mainer);

// When button is pressed, we take the widget text and send to check
gboolean SidePanelOnButtonPressEvent(GtkWidget *Widget,
	GdkEventButton *Event, SidePanelData *Data);

// When panel gets activated, we take the widget text and send to check
gboolean SidePanelOnActivate(GtkWidget *Widget, SidePanelData *Data);

#endif
