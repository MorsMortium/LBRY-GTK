#ifndef ORDER_H_
#define ORDER_H_

#include <gtk/gtk.h>

// From Glade file and the format strings
#define OrderMaxPrint 33
#define OrderMaxGet 17

// Types of orderings
extern char *OrderTypes[];

// Struct for widget data
typedef struct OrderData {
	// Subwidgets of order used in and outside of this file
	GtkWidget *Order, *UseOrdering, *UseOrderingActive, *OrderBy;
	GtkWidget *OrderByActive, *Direction, *DirectionActive;
} OrderData;

// This function is responsible for getting currently set value in a string
// format
void OrderGet(OrderData *Data, char **Returned);

// This function is responsible for setting value from a string
void OrderSet(OrderData *Data, char *Ordering);

// This function is responsible for getting currently set value in a human
// readable format, for titles
void OrderPrint(OrderData *Data, char *Returned);

// This function is responsible for creating the Order widget and putting it
// into Box
OrderData *OrderCreate(void);

#endif
