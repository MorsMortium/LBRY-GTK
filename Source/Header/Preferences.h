#ifndef PREFERENCES_H_
#define PREFERENCES_H_

#include <jansson.h>
#include <pthread.h>

// Lock so we won't accidentally import settings
extern pthread_mutex_t PreferencesLock;

// This function will update any information in given json object
// from given default file
json_t *PreferencesUpdate(char *DefaultPath, json_t *OldObject);

// This function will be used to import old settings
json_t *PreferencesImport(char *ConfigPath, char *DefaultPath);

// This function gets the settings/preferences
json_t *PreferencesGet(void);

// This function sets the settings for client
int PreferencesSetClient(char *ConfigPath, json_t *Preferences);

// This function sets the settings for lbrynet
int PreferencesSetLBRY(char *Server, json_t *Settings);

// This function sets the preferences/settings
int PreferencesSet(char *ConfigPath, json_t *Settings);

// This function syncs the settings/preferences to global variables
int PreferencesSync(char *ConfigPath, char *DefaultPath);

// Inits mutex
void PreferencesStart(char *FilePath, char *DefaultPath);

// Destroy mutex
void PreferencesStop(void);

#endif
