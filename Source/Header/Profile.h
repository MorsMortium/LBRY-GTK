#ifndef PROFILE_H_
#define PROFILE_H_

#include <Python.h>
#include <gtk/gtk.h>
#include <stdbool.h>

#include "Thumbnail.h"

typedef struct TabsData TabsData;

// Struct for widget data
typedef struct ProfileData {
	// Widgets of Profile used in and outside of this file
	GtkWidget *Image, *Profile, *Channel, *Publications, *ImageActive;

	// Urls for LBRY channel and thumbnail file, name for channel
	char *ChannelUrl, *Title;

	ThumbnailData *Thumbnail;

	TabsData *TabData;

	// Function for opening on new page
	// TODO: replace with C functions
	PyObject *AddPage;
} ProfileData;

// This function is responsible for creating the Profile widget
ProfileData *ProfileCreate(char *ChannelUrl, char *Thumbnail, float ChannelLBC,
	float PostLBC, int UploadNumber, unsigned int Stamp,
	int Size, bool ProfileImage, PyObject *AddPage, char *Title, bool Circled,
	int MaxWidth, TabsData *TabData);

#endif
