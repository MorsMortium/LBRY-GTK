#ifndef MARKDOWN_H_
#define MARKDOWN_H_

#include <Python.h>
#include <gtk/gtk.h>
#include <stdbool.h>

// Forward-declare FillData to avoid recursive including
typedef struct FillData FillData;

// Forward declare TabsData
typedef struct TabsData TabsData;

extern GtkCssProvider *MarkdownCssProvider;
extern GtkStyleContext *Context;
extern GdkRGBA MarkdownColor;

typedef struct MarkdowData {
	// Gtk widgets in the markdown widget
	GtkWidget *Document, *TextBox, *Scrolled;

	// Cursors for text buffer
	GdkCursor *TextCursor, *PointerCursor;

	// Adjustment from MainSpace (for Documents)
	GtkAdjustment *Adjustment;

	// Scrolling place of adjustment stored for workaround
	double ScrollPlace;

	// CurrentTagIndex to tell what link is focused in text buffer
	int CurrentTagIndex;

	// Add is used for passing arguments to functions
	int Add;

	// Holds the text for the text buffer
	char *Text;

	// Booleans used in checking some functionality
	bool IfMarkdown, IfUpdateHeight, FirstRun;

	// Pointer to child struct(s)
	FillData *TextData;

	// Pointer to tab data
	TabsData *TabData;
} MarkdownData;

// Release memory used by text fields
gboolean MarkdownOnTextBoxDestroy(GtkWidget *TextBox, gpointer Args);

// This function is responsible for creating the Markdown widget
MarkdownData *MarkdownCreate(char *Text, char *Path, bool IfComment,
	bool IfMarkdown, bool IfResolve, TabsData *TabData, PyObject *AddPage,
	GtkAdjustment *Adjustment);

// Wraps forward action for tag selection
void MarkdownSelectNext(MarkdownData *Data);

// Wraps backward action for tag selection
void MarkdownSelectPrevious(MarkdownData *Data);

// Fill the textview widget
gboolean MarkdownFill(gpointer Args);

// Start function for Markdown
void MarkdownStart(void);

// Stop function for Markdown
void MarkdownStop(void);

#endif
