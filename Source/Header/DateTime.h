#ifndef DATETIME_H_
#define DATETIME_H_

#include <gtk/gtk.h>

// Struct for widget data
typedef struct DateTimeData {
	// Calendar widget with it's own type
	GtkCalendar *Calendar;

	// Subwidgets of datetime used in and outside of this file
	GtkWidget *DateTime, *Hour, *Minute, *Second, *DateUse,
		*DateReset, *DateActive, *DateUseActive;

	// Some integer values to store time values per the names
	// Used for spin actions and wraps
	int OldHour, OldMinute, OldSecond, OlderHour, OlderMinute, OlderSecond;
} DateTimeData;

// This function is responsible for creating the DateTime widget
DateTimeData *DateTimeCreate(void);

// This funtion will set time to calendar
void DateTimeSet(DateTimeData *Data, struct tm *Time);

// This gets the time
time_t DateTimeGet(DateTimeData *Data, int Added);

// This function changes the date by a day
gboolean DateTimeDateChange(int Button, DateTimeData *Data);

// This function is responsible for resetting the calendar date
gboolean DateTimeOnDateResetButtonPressEvent(GtkWidget *Widget,
	GdkEventButton *Event, DateTimeData *Data);

#endif
