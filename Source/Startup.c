/******************************************************************************\
* LBRY-GTK                                                                     *
* Copyright (C) 2021-2023 MorsMortium and Other Contributors                   *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU General Public License as published by         *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                *
* GNU General Public License for more details.                                 *
*                                                                              *
* You should have received a copy of the GNU General Public License            *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
\******************************************************************************/

// This file deals with startup of LBRY-GTK

#define _XOPEN_SOURCE 700
#include <Python.h>
#include <gtk/gtk.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <ftw.h>
#include <unistd.h>

#include "Header/Connect.h"
#include "Header/Global.h"
#include "Header/Status.h"
#include "Header/State.h"
#include "Header/Json.h"
#include "Header/Places.h"
#include "Header/Popup.h"
#include "Header/Threads.h"
#include "Header/Tray.h"
#include "Header/Tabs.h"
#include "Header/Preferences.h"
#include "Header/Wallet.h"
#include "Header/GTKExtra.h"
#include "Header/OS.h"
#include "Header/Startup.h"

int StartupRemoveDirectory(const char *FilePath,
	const struct stat *UNUSED(Stat), int UNUSED(TypeFlag),
	struct FTW *UNUSED(FTWBuffer)) {
	// Callback for nftw to remove files in directory

	if (strcmp(FilePath, PlacesTmp) == 0) {
		return -1;
	}

	return remove(FilePath);
}

void StartupWindowSetting(StartupData *Data, json_t *Session) {
	// Set window correctly

	const char *Server = JsonString(json_object_get(Session, "Server"), "");

	Data->Statuser->Server = malloc(strlen(Server) + 1);
	if (Data->Statuser->Server != NULL) {
		strcpy(Data->Statuser->Server, Server);
	}

	// Minimized window setting
	if (json_is_true(json_object_get(Session, "Minimized"))) {
		gtk_window_iconify(GlobalWindow);
	} else {
		gtk_widget_show_all(GTK_WIDGET(GlobalWindow));
	}

	// If tray should be created and if the app should be minimized to tray
	if (json_is_true(json_object_get(Session, "Tray"))) {
		TrayCreate(json_is_true(json_object_get(Session, "TrayMinimized")));
	}

	// Ugly python crap
	PyGILState_STATE State = PyGILState_Ensure();

	// Get callable function
	PyObject *Function =
		PyObject_GetAttrString(Data->SettingsUpdate, "ChangeMenu");

	PyObject *Arguments = Py_BuildValue("ii",
			json_integer_value(json_object_get(Session, "MenuType")),
			json_integer_value(json_object_get(Session, "MenuIcon")));

	PyObject *Call = PyObject_CallObject(Function, Arguments);
	if (Call) {
		Py_DECREF(Call);
	} else {
		PyErr_Print();
	}

	// Free arguments, release GIL
	Py_DECREF(Arguments);
	PyGILState_Release(State);

	// Scale the window to user configured size
	if (json_is_true(json_object_get(Session, "WindowSize"))) {
		gtk_window_resize(GlobalWindow,
			json_number_value(json_object_get(Session, "WindowWidth")),
			json_number_value(json_object_get(Session, "WindowHeight")));
	}
}

gboolean StartupIsDisplayed(gpointer Args) {
	// Display helper which checks if we are properly displayed and then
	// executes the home function

	StartupData *Data = (StartupData *) Args;
	gboolean Continue = G_SOURCE_CONTINUE;

	// When we are appropriately "displayed" call the home function
	if (strcmp(gtk_widget_get_name(Data->Statuser->Status), "Displayed") == 0) {
		// Get user configured homedata
		json_t *HomeData =
			json_loads(json_string_value(json_object_get(GlobalPreferences,
				"HomeData")), 0, NULL);

		// Note: if the homedata is empty, all of these arguments are left NULL
		ThreadsArguments *Arguments = ThreadsMakeArguments(
			(char *) json_string_value(json_array_get(HomeData, 0)),
			(char *) json_string_value(json_array_get(HomeData, 1)),
			(char *) json_string_value(json_array_get(HomeData, 2)),
			json_incref(json_array_get(HomeData, 3)), Data->TabData);

		// Finally, import function with name and call it with arguments
		StateImport((char *) json_string_value(
				json_object_get(GlobalPreferences, "HomeFunction")), Arguments);

		// Free data here since it is no longer used anywhere
		json_decref(HomeData);
		free(Data);

		// No longer have this helper be called
		Continue = G_SOURCE_REMOVE;
	}

	return Continue;
}

gboolean StartupBalancer(gpointer Args) {
	// Balance helper which simply populates the balance label with user's
	// wallet balance (if there is any)

	GtkWidget *BalanceLabel = (GtkWidget *) Args;

	const char *Server =
		JsonString(json_object_get(json_object_get(GlobalPreferences,
			"Session"), "Server"), "http://localhost:5279");
	json_t *Balance = WalletBalance((char *) Server);

	// Get total balance from returned balance data
	double Total = strtod(json_string_value(json_object_get(Balance, "total")),
			NULL);

	// Set current balance to balance label
	GTKExtraNumberLabel(BalanceLabel, Total);
	gtk_widget_show_all(BalanceLabel);

	return G_SOURCE_CONTINUE;
}

gboolean StartupHelper(gpointer Args) {
	// Startup helper function that deals with all the stages that need to
	// be had before the client is properly running

	StartupData *Data = (StartupData *) Args;
	gboolean Continue = G_SOURCE_CONTINUE;

	json_t *Session = json_object_get(GlobalPreferences, "Session");
	const char *Binary = JsonString(json_object_get(Session, "Binary"), "");
	const char *Server = JsonString(json_object_get(Session, "Server"),
			"http://localhost:5279");

	if (Data->Stage < 1) {
		ThreadsMakeExecute(Data->TabData, ThreadsStatus, NULL, NULL, NULL,
			NULL);

		if (Session) {
			json_t *NewBinary = json_object_get(Session, "NewBinary");
			json_t *NewServer = json_object_get(Session, "NewServer");

			// Set new binary and server value to server and binary
			if (json_string_length(NewBinary)) {
				json_object_set(Session, "Binary", NewBinary);
			}

			if (json_string_length(NewServer)) {
				json_object_set(Session, "Server", NewServer);
			}

			StartupWindowSetting(Data, Session);
		}

		Data->Stage = 1;
	} else if (Data->Stage == 1) {
		// Only if Start is actually set, will we start lbrynet
		if (json_is_true(json_object_get(Session, "Start"))) {
			ConnectCreate((char *) Binary, (char *) Server);
			Data->Started = true;
		}
		Data->Stage = 2;
	} else if (Data->Stage == 2) {
		int Timeout = JsonInt(json_object_get(Session, "Timeout"), 30);

		// CycleCount makes sure we only do connection check every second or so
		if ((Data->CycleCount % 10) == 0) {
			if (ConnectCheck(JsonString(json_object_get(Session, "Server"),
				"http://localhost:5279"))) {
				Data->Stage = 3;
			}
		}
		Data->CycleCount++;

		// If cyclecount gets larger than timeout implies, we let user know
		if (Timeout && (unsigned long) (Timeout * 10) == Data->CycleCount) {
			if (Data->Started) {
				PopupMessage("Lbrynet could not be started!\n\n"
					"Attempting again..");
			} else {
				PopupMessage("Lbrynet is not running! "
					"Did you remember to do manual start?");
			}
		}
	} else if (Data->Stage == 3) {
		char DefaultSettings[strlen(PlacesJson) + 25];
		sprintf(DefaultSettings, "%sDefaultSettings.json", PlacesJson);
		GlobalPreferences = PreferencesUpdate(DefaultSettings,
				GlobalPreferences);

		// Set client settings from here and do sync again to get lbry settings
		PreferencesSetClient(PlacesConfig, GlobalPreferences);
		PreferencesSync(PlacesConfig, DefaultSettings);

		// Check if user has signed in account
		if (json_string_length(json_object_get(GlobalPreferences,
			"AuthToken")) == 0) {
			gtk_label_set_label(GTK_LABEL(Data->Signing), "Sign In");
		}

		// Ugly python crap
		PyGILState_STATE State = PyGILState_Ensure();

		// Get callable function
		PyObject *Function =
			PyObject_GetAttrString(Data->SettingsUpdate, "ChangeMeta");

		PyObject *EnableMetaService = PyLong_FromLong(json_boolean_value(
					json_object_get(Session, "EnableMetaService")));
		PyObject *Arguments = PyTuple_New(1);
		PyTuple_SetItem(Arguments, 0, EnableMetaService);

		PyObject *Call = PyObject_CallObject(Function, Arguments);
		if (Call) {
			Py_DECREF(Call);
		} else {
			PyErr_Print();
		}

		// Free arguments, release GIL
		Py_DECREF(Arguments);
		PyGILState_Release(State);

		// Rest of the changes to startup
		Data->Statuser->LBRYGTK = true;
		GlobalStarted = true;
		Data->Stage = 4;

		// Finals calls and timeouts (balancer is left running)
		StartupBalancer(Data->Balance);
		g_timeout_add(100, StartupIsDisplayed, Data);
		g_timeout_add(60000, StartupBalancer, Data->Balance);

		Continue = G_SOURCE_REMOVE;
	}

	return Continue;
}

void StartupInit(GtkWidget *GotBalance, GtkWidget *GotSigning,
	StatusData *Statuser, TabsData *TabData, PyObject *SettingsUpdate) {
	// Creates directories and starts all services required by client

	struct stat Stat = {0};

	// All items in TMP directory will be destroyed if the directory exists
	if (stat(PlacesTmp, &Stat) == -1) {
		OSMakeDirectory(PlacesTmp, 0700);
	} else {
		nftw(PlacesTmp, StartupRemoveDirectory, 64, FTW_DEPTH | FTW_PHYS);
	}

	if (stat(PlacesCache, &Stat) == -1) {
		OSMakeDirectory(PlacesCache, 0700);
	}

	if (stat(PlacesConfig, &Stat) == -1) {
		OSMakeDirectory(PlacesConfig, 0700);
	}

	// Allocate memory for temporary startup struct
	StartupData *Data = malloc(sizeof(StartupData));
	if (Data == NULL) {
		abort();
	}

	Data->Stage = 0;
	Data->CycleCount = 0;
	Data->Started = false;
	Data->Balance = GotBalance;
	Data->Signing = GotSigning;
	Data->TabData = TabData;
	Data->Statuser = Statuser;
	Data->SettingsUpdate = SettingsUpdate;

	g_timeout_add(100, StartupHelper, Data);
}

// Everything under this line is removable after full C conversion
static PyObject *StartupCreatePython(PyObject *Py_UNUSED(Self),
	PyObject *Args) {
	// Fill DataPointer with sent data
	uintptr_t BalancePointer, SigningPointer, StatuserPointer;
	PyObject *SettingsUpdate;
	if (!PyArg_ParseTuple(Args, "LLL", &BalancePointer, &SigningPointer,
		&StatuserPointer)) {
		return NULL;
	}

	SettingsUpdate = PyImport_ImportModule("Source.SettingsUpdate");
	Py_INCREF(SettingsUpdate);

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	StartupInit((GtkWidget *) BalancePointer, (GtkWidget *) SigningPointer,
		(StatusData *) StatuserPointer, TabsList, SettingsUpdate);

	// Acquire lock
	PyEval_RestoreThread(Save);

	Py_RETURN_NONE;
}

// Function names and other python data
static PyMethodDef StartupMethods[] = {
	{"Create", StartupCreatePython, METH_VARARGS, ""},
	{NULL, NULL, 0, NULL}
};

static struct PyModuleDef StartupModule = {
	PyModuleDef_HEAD_INIT, "Startup", "Startup module", -1, StartupMethods, 0,
	0, 0, 0
};

// The function name has to end with the module name
PyMODINIT_FUNC PyInit_Startup(void) {
	return PyModule_Create(&StartupModule);
}
