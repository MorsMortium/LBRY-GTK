/******************************************************************************\
* LBRY-GTK                                                                     *
* Copyright (C) 2021-2023 MorsMortium and Other Contributors                   *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU General Public License as published by         *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                *
* GNU General Public License for more details.                                 *
*                                                                              *
* You should have received a copy of the GNU General Public License            *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
\******************************************************************************/

// State action file

#include <Python.h>
#include <jansson.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#include "Header/Profile.h"
#include "Header/Tabs.h"
#include "Header/Json.h"
#include "Header/Threads.h"
#include "Header/State.h"

// All known state functions listed here
StateFunction StateFunctions[StateKnownFunctions];

static void StateFreeData(StateData *Data) {
	// Frees data allocated to struct

	// Free title if present
	if (Data->Title != NULL) {
		free(Data->Title);
	}

	// Free the given data
	if (Data->Args != NULL) {
		ThreadsFreeArguments(Data->Args);
	}
}

static void *StateFreeForward(StateData *Data) {
	// Frees all the memory used by statedata structs going forward

	StateData *Temp;
	while (Data != NULL) {
		// Free items inside struct
		StateFreeData(Data);

		// We need temp here for when pointer to Next is NULL
		Temp = Data;
		Data = Data->Next;
		free(Temp);
	}

	return NULL;
}

static void *StateFreeBackward(StateData *Data) {
	// Frees all the memory used by statedata structs going backward

	StateData *Temp;
	while (Data != NULL) {
		// Free items inside struct
		StateFreeData(Data);

		// We need temp here for when pointer to Next is NULL
		Temp = Data;
		Data = Data->Previous;
		free(Temp);
	}

	return NULL;
}

void StateFree(StateData *Data) {
	// Free everything allocated by state

	StateFreeForward(Data->Next);
	StateFreeBackward(Data);
}

static int StateDataNew(char *Title, void *(*Function)(
		void *), void *Args, StateData **Data) {
	// Allocates more memory for StateData

	StateData *NewData = calloc(1, sizeof *NewData);
	if (NewData == NULL) {
		fprintf(stderr, "ERR: %s:%d: could not calloc\n", __func__, __LINE__);
		abort();
	}

	// Allocate memory for title
	NewData->Title = malloc(strlen(Title) + 1);
	if (NewData->Title == NULL) {
		fprintf(stderr, "ERR: %s:%d: could not malloc\n", __func__, __LINE__);
		abort();
	}
	strcpy(NewData->Title, Title);

	NewData->Function = Function;
	NewData->Args = Args;
	NewData->Previous = *Data;
	NewData->Next = NULL;
	if (*Data != NULL) {
		(*Data)->Next = NewData;
	}

	// Point to our new top most state
	*Data = NewData;

	return 0;
}

void StateImport(char *FunctionName, void *Args) {
	// Check what state we talking about and execute that with given data

	if (FunctionName == NULL || FunctionName[0] == '\0') {
		fprintf(stderr, "ERR: %s:%d: No FunctionName specified!\n", __func__,
			__LINE__);
		return;
	}

	// Find the function corresponding to the name given
	void *(*Function)(void *) = NULL;
	for (int Index = 0; Index < StateKnownFunctions; Index++) {
		if (strcmp(StateFunctions[Index].Name, FunctionName) == 0) {
			Function = StateFunctions[Index].Function;
		}
	}

	// If for whatever reason the function name didn't match, do nothing
	if (Function == NULL) {
		fprintf(stderr, "ERR: %s:%d: Function NULL!\n", __func__, __LINE__);
		return;
	}

	ThreadsExecute(Function, Args);
}

const char *StateExport(void *(*Function)(void *)) {
	// Export function information if function is also known
	// TODO: Add args check here

	if (Function == NULL) {
		fprintf(stderr, "ERR: %s:%d: Function NULL!\n", __func__, __LINE__);
		return NULL;
	}

	// Find the function name related to function given
	const char *FunctionName = NULL;
	for (int Index = 0; Index < StateKnownFunctions; Index++) {
		if (Function == StateFunctions[Index].Function) {
			FunctionName = StateFunctions[Index].Name;
		}
	}

	// NOTE: Remember not to free this (owned by State.c)
	return FunctionName;
}

void StateSave(char *Title, StateData **GotData, void *(*Function)(
		void *), void *Args) {
	// Save state to memory

	StateData *Data = *GotData;

	// If there is a next state, free it for new one
	if (Data != NULL && Data->Next != NULL) {
		StateFreeForward(Data->Next);
		Data->Next = NULL;
	}

	StateDataNew(Title, Function, Args, &Data);

	*GotData = Data;
}

static void StateExecute(TabsData *TabData, StateData *Data) {
	// Go to the specified state

	if (ThreadsExecute(Data->Function, Data->Args) == false) {
		TabData->Stater = Data;
	}
}

void StateBackward(TabsData *TabData) {
	// Go back to previous state

	StateData *Data = TabData->Stater;

	if (Data->Previous != NULL) {
		StateExecute(TabData, Data->Previous);
	}
}

void StateForward(TabsData *TabData) {
	// Go forward to newer state

	StateData *Data = TabData->Stater;

	if (Data->Next != NULL) {
		StateExecute(TabData, Data->Next);
	}
}

// NOTE: Python has Next and Previous functions but in C we can instead
// take copy of certain structures and pass those around. That is the reason
// why we don't have StateNext and StatePrevious functions

// Everything under this line is removable after full C conversion
static PyObject *StateMakeListFromArgumentsPython(ThreadsArguments *Data) {
	// Make a list of arguments

	PyObject *List = PyList_New(0);
	PyObject *NoneObject;
	if (Data->Name) {
		PyList_Append(List, PyUnicode_FromString(Data->Name));
	}

	if (Data->Path) {
		if (!Data->Name) {
			NoneObject = Py_None;
			Py_INCREF(NoneObject);
			PyList_Append(List, NoneObject);
		}
		PyList_Append(List, PyUnicode_FromString(Data->Path));
	}

	if (Data->Json) {
		if (!Data->Path) {
			if (!Data->Name) {
				NoneObject = Py_None;
				Py_INCREF(NoneObject);
				PyList_Append(List, NoneObject);
			}
			NoneObject = Py_None;
			Py_INCREF(NoneObject);
			PyList_Append(List, NoneObject);
		}

		// This adds the key place as well
		// TODO: We could drop this None if StateImportPython
		// doesn't need nor take key as argument
		NoneObject = Py_None;
		Py_INCREF(NoneObject);
		PyList_Append(List, NoneObject);
		PyList_Append(List, JsonParseToPython(Data->Json));
	}

	return List;
}

static TabsData *StateGetTabPointerPython(PyObject *Module) {
	// Returns the tab data pointer

	PyObject *Dict = PyModule_GetDict(Module);
	PyObject *TabPointerObject = PyDict_GetItemString(Dict, "TabPointer");

	return (TabsData *) PyLong_AsLongLong(TabPointerObject);
}

static PyObject *StateBackwardPython(PyObject *Self,
	PyObject *Py_UNUSED(Args)) {
	// Go back in states

	TabsData *TabData = StateGetTabPointerPython(Self);

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	StateBackward(TabData);

	// Acquire lock
	PyEval_RestoreThread(Save);

	Py_RETURN_NONE;
}

static PyObject *StateForwardPython(PyObject *Self, PyObject *Py_UNUSED(Args)) {
	// Go forward in states

	TabsData *TabData = StateGetTabPointerPython(Self);

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	StateForward(TabData);

	// Acquire lock
	PyEval_RestoreThread(Save);

	Py_RETURN_NONE;
}

static PyObject *StatePreviousPython(PyObject *Self,
	PyObject *Py_UNUSED(Args)) {
	// In aid of Export to get the previous state data

	TabsData *TabData = StateGetTabPointerPython(Self);

	PyObject *ReturnList = PyList_New(0);
	const char *Value = NULL;

	if (TabData->Stater->Previous != NULL) {
		// Thread safe operation begins (release GIL)
		PyThreadState *Save = PyEval_SaveThread();

		Value = StateExport(TabData->Stater->Previous->Function);

		// Acquire lock
		PyEval_RestoreThread(Save);

		PyList_Append(ReturnList, PyUnicode_FromString(Value));
		PyList_Append(ReturnList,
			StateMakeListFromArgumentsPython(TabData->Stater->Previous->Args));
	}

	return ReturnList;
}

static PyObject *StateNextPython(PyObject *Self, PyObject *Py_UNUSED(Args)) {
	// In aid of Export to get the next state data

	TabsData *TabData = StateGetTabPointerPython(Self);

	PyObject *ReturnList = PyList_New(0);
	const char *Value = NULL;

	if (TabData->Stater->Next != NULL) {
		// Thread safe operation begins (release GIL)
		PyThreadState *Save = PyEval_SaveThread();

		Value = StateExport(TabData->Stater->Next->Function);

		// Acquire lock
		PyEval_RestoreThread(Save);

		PyList_Append(ReturnList, PyUnicode_FromString(Value));
		PyList_Append(ReturnList,
			StateMakeListFromArgumentsPython(TabData->Stater->Next->Args));
	}

	return ReturnList;
}

static PyObject *StateGotoPython(PyObject *Self, PyObject *Args) {
	// Make the specific given struct new struct and execute it

	uintptr_t LongPointer = 0;
	if (!PyArg_ParseTuple(Args, "L", &LongPointer)) {
		return NULL;
	}

	TabsData *TabData = StateGetTabPointerPython(Self);

	StateData *Info =
		(!LongPointer) ? TabData->Stater : (StateData *) LongPointer;

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	StateExecute(TabData, Info);

	// Acquire lock
	PyEval_RestoreThread(Save);

	Py_RETURN_NONE;
}

static PyObject *StateBeforePython(PyObject *Self, PyObject *Py_UNUSED(Args)) {
	// Gets a list of states before current state

	PyObject *List = PyList_New(0);

	TabsData *TabData = StateGetTabPointerPython(Self);
	StateData *Info = TabData->Stater;
	Info = Info->Previous;

	PyObject *Dict;
	PyObject *PointerObject = PyUnicode_FromString("Pointer");
	PyObject *TitleObject = PyUnicode_FromString("Title");
	PyObject *FunctionObject = PyUnicode_FromString("Function");
	PyObject *DataObject = PyUnicode_FromString("Data");
	while (Info != NULL) {
		Dict = PyDict_New();
		PyDict_SetItem(Dict, PointerObject, PyLong_FromVoidPtr(Info));
		PyDict_SetItem(Dict, TitleObject, PyUnicode_FromString(Info->Title));
		PyDict_SetItem(Dict, DataObject, PyLong_FromVoidPtr(Info->Args));
		PyDict_SetItem(Dict, FunctionObject,
			PyLong_FromVoidPtr(Info->Function));
		PyList_Append(List, Dict);
		Info = Info->Previous;
	}
	Py_DECREF(PointerObject);
	Py_DECREF(TitleObject);
	Py_DECREF(FunctionObject);
	Py_DECREF(DataObject);

	return List;
}

static PyObject *StateAfterPython(PyObject *Self, PyObject *Py_UNUSED(Args)) {
	// Gets a list of states after current state

	PyObject *List = PyList_New(0);

	TabsData *TabData = StateGetTabPointerPython(Self);
	StateData *Info = TabData->Stater;
	Info = Info->Next;

	PyObject *Dict;
	PyObject *PointerObject = PyUnicode_FromString("Pointer");
	PyObject *TitleObject = PyUnicode_FromString("Title");
	PyObject *FunctionObject = PyUnicode_FromString("Function");
	PyObject *DataObject = PyUnicode_FromString("Data");
	while (Info != NULL) {
		Dict = PyDict_New();
		PyDict_SetItem(Dict, PointerObject, PyLong_FromVoidPtr(Info));
		PyDict_SetItem(Dict, TitleObject, PyUnicode_FromString(Info->Title));
		PyDict_SetItem(Dict, DataObject, PyLong_FromVoidPtr(Info->Args));
		PyDict_SetItem(Dict, FunctionObject,
			PyLong_FromVoidPtr(Info->Function));
		PyList_Append(List, Dict);
		Info = Info->Next;
	}
	Py_DECREF(PointerObject);
	Py_DECREF(TitleObject);
	Py_DECREF(FunctionObject);
	Py_DECREF(DataObject);

	return List;
}

// Methods specific to returned object
static PyMethodDef StateModMethods[] = {
	{"Backward", (PyCFunction) StateBackwardPython, METH_NOARGS, "Backward"},
	{"Forward", (PyCFunction) StateForwardPython, METH_NOARGS, "Forward"},
	{"Previous", (PyCFunction) StatePreviousPython, METH_NOARGS, "Previous"},
	{"Next", (PyCFunction) StateNextPython, METH_NOARGS, "Next"},
	{"Goto", StateGotoPython, METH_VARARGS, "Goto"},
	{"Before", (PyCFunction) StateBeforePython, METH_NOARGS, "Before"},
	{"After", (PyCFunction) StateAfterPython, METH_NOARGS, "After"},
	{NULL, NULL, 0, NULL}
};

static struct PyModuleDef StateModModule = {
	PyModuleDef_HEAD_INIT, "State", "State module", -1, StateModMethods, 0, 0,
	0, 0
};

static PyObject *StateCreatePython(PyObject *Py_UNUSED(Self), PyObject *Args) {
	// Create module and add tab pointer to it

	PyObject *TabPointerObject = NULL;
	if (!PyArg_ParseTuple(Args, "O", &TabPointerObject)) {
		return NULL;
	}

	PyObject *Module = PyModule_Create(&StateModModule);
	PyModule_AddObject(Module, "TabPointer", TabPointerObject);

	return Module;
}

static PyObject *StateImportPython(PyObject *Py_UNUSED(Self), PyObject *Args) {
	// Saves calls to memory

	char *Name;
	PyObject *Arguments;
	uintptr_t TabPointer;
	if (!PyArg_ParseTuple(Args, "sOL", &Name, &Arguments, &TabPointer)) {
		return NULL;
	}

	TabsData *TabData;
	if (!TabPointer) {
		TabData = TabsList;
	} else {
		TabData = (TabsData *) TabPointer;
	}

	char *ThreadName = NULL, *Path = NULL, *Key = NULL;
	json_t *Json = NULL;
	ThreadsArguments *Data = NULL;

	// Return if the given argument is NOT of type PyList or PyLong
	if (!PyList_Check(Arguments)) {
		if (!PyLong_Check(Arguments)) {
			Py_RETURN_NONE;
		}

		// NOTE: This onward effectively copies the data, unless
		// TabsIsExecuting is still returning trues
		Data = (ThreadsArguments *) PyLong_AsLongLong(Arguments);
		ThreadName = Data->Name;
		Path = Data->Path;
		Json = Data->Json;
	} else {
		PyObject *Object;
		json_error_t Error;
		for (int Index = 0; Index < PyList_GET_SIZE(Arguments); Index++) {
			Object = PyList_GetItem(Arguments, Index);
			if (Object == NULL) {
				break;
			}
			switch (Index) {
				case 0:
					if (Object != &_Py_NoneStruct) {
						ThreadName = (char *) PyUnicode_AsUTF8(Object);
					}
					break;
				case 1:
					if (Object != &_Py_NoneStruct) {
						Path = (char *) PyUnicode_AsUTF8(Object);
					}
					break;
				case 2:
					if (Object != &_Py_NoneStruct) {
						Key = (char *) PyUnicode_AsUTF8(Object);
					}
					break;
				case 3:
					Json = json_loads(PyUnicode_AsUTF8(Object), 0, &Error);
					break;
			}
		}
	}

	// TODO: This if statement here is just a workaround. Figure out what to do
	// with newly allocated arguments, and when those don't get passed onto
	// StateSave due to there being something still under execution.
	if (!TabsIsExecuting(TabData)) {
		// Exception to allocation is when the arguments are already allocated
		Data = ThreadsMakeArguments(ThreadName, Path, Key, Json, TabData);

		// Thread safe operation begins (release GIL)
		PyThreadState *Save = PyEval_SaveThread();

		StateImport(Name, Data);

		// Acquire lock
		PyEval_RestoreThread(Save);
	}

	Py_RETURN_NONE;
}

static PyObject *StateExportPython(PyObject *Py_UNUSED(Self), PyObject *Args) {
	// Saves calls to memory

	uintptr_t LongPointer;
	PyObject *Function, *Arguments;
	if (!PyArg_ParseTuple(Args, "OO", &Function, &Arguments)) {
		return NULL;
	}

	// If function isn't long pointer, we just return the same stuff back
	PyObject *List = PyList_New(2);
	if (!PyLong_Check(Function)) {
		PyList_SET_ITEM(List, 0, Function);
		PyList_SET_ITEM(List, 1, Arguments);
	} else {
		// Get the function name and return it along the arguments
		const char *FunctionName = NULL;
		LongPointer = PyLong_AsLongLong(Function);

		// Thread safe operation begins (release GIL)
		PyThreadState *Save = PyEval_SaveThread();

		FunctionName = StateExport((void *(*)(void *)) LongPointer);

		// Acquire lock
		PyEval_RestoreThread(Save);

		PyList_SET_ITEM(List, 0, PyUnicode_FromString(FunctionName));
		PyList_SET_ITEM(List, 1, Arguments);
	}

	return List;
}

static PyObject *StateAsListPython(PyObject *Py_UNUSED(Self), PyObject *Args) {
	// ThreadsArguments to python list

	uintptr_t LongPointer;
	if (!PyArg_ParseTuple(Args, "L", &LongPointer)) {
		return NULL;
	}

	return StateMakeListFromArgumentsPython((ThreadsArguments *) LongPointer);
}

// General type of methods
static PyMethodDef StateMethods[] = {
	{"Create", StateCreatePython, METH_VARARGS, "Create"},
	{"Import", StateImportPython, METH_VARARGS, "Import"},
	{"Export", StateExportPython, METH_VARARGS, "Export"},
	{"AsList", StateAsListPython, METH_VARARGS, "AsList"},
	{NULL, NULL, 0, NULL}
};

static struct PyModuleDef StateModule = {
	PyModuleDef_HEAD_INIT, "State", "State module", -1, StateMethods, 0, 0, 0, 0
};

// The function name has to end with the module name
PyMODINIT_FUNC PyInit_State(void) {
	return PyModule_Create(&StateModule);
}
