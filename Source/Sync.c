/******************************************************************************\
* LBRY-GTK                                                                     *
* Copyright (C) 2021-2023 MorsMortium and Other Contributors                   *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU General Public License as published by         *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                *
* GNU General Public License for more details.                                 *
*                                                                              *
* You should have received a copy of the GNU General Public License            *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
\******************************************************************************/

#include <Python.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <curl/curl.h>
#include <jansson.h>

#include "Header/Request.h"

int SyncDisconnect(json_t *AccountIDs, char *Server) {
	// This function will unsync the account

	// Create a request block
	json_error_t Error;
	json_t *Request = json_loads("{\"method\": \"account_remove\", "
			"\"params\": {}}", 0, &Error);
	json_t *Params = json_object_get(Request, "params");

	// Loop over each element in AccountIDs
	json_t *Value;
	size_t Index;
	json_array_foreach(AccountIDs, Index, Value) {
		// Add account_id to params with the value
		json_object_set(Params, "account_id", Value);

		// Dump request to string
		char *RequestStr = json_dumps(Request, 0);

		// Make the request
		json_t *Result = RequestJson(Server, RequestStr);

		// Free request string
		free(RequestStr);

		// Check for errors
		if (json_object_get(Result, "error") != NULL) {
			json_decref(Result);
			json_decref(Request);
			return (int) (Index + 1);
		}
		json_decref(Result);
	}
	json_decref(Request);

	// Return zero to signal succesful unsync
	return 0;
}

json_t *SyncConnect(char *Email, char *Password, char *Server) {
	// Syncs the account with lbrynet

	// Create json blocks to hold the data
	json_t *ReturnData = json_object();
	json_t *AccountIDs = json_array();
	json_object_set(ReturnData, "AccountIDs", AccountIDs);

	// Get account list for reference
	json_t *AccountList = RequestJson(Server,
			"{\"method\": \"account_list\"}");
	if (json_object_get(AccountList, "error") != NULL) {
		json_decref(ReturnData);
		return AccountList;
	}

	// Check each item in the array
	json_t *Value;
	size_t Index;
	json_array_foreach(json_object_get(AccountList, "items"), Index, Value) {
		json_array_append(AccountIDs, json_object_get(Value, "id"));
	}
	json_decref(AccountList);

	// Get an authentication token from odysee (to get account from there)
	json_t *AuthData = RequestMake("https://api.odysee.com/user/new", NULL,
			NULL);
	if (json_object_get(AuthData, "error") != NULL) {
		json_decref(ReturnData);
		return AuthData;
	}

	// Get the authtoken
	const char *AuthToken =
		json_string_value(json_object_get(AuthData, "auth_token"));
	json_object_set(ReturnData, "AuthToken", json_object_get(AuthData,
		"auth_token"));

	// Init curl for url encoding strings
	CURL *Curl = curl_easy_init();
	if (Curl == NULL) {
		return NULL;
	}

	// Strings to be url econded
	char *EncodedEmail = curl_easy_escape(Curl, Email, 0);
	char *EncodedPassword = curl_easy_escape(Curl, Password, 0);
	char *EncodedAuthToken = curl_easy_escape(Curl, AuthToken, 0);

	// Make the parameters string
	char Params[strlen(EncodedEmail) + strlen(EncodedPassword)
		+ strlen(EncodedAuthToken) + 35];
	sprintf(Params, "email=%s&password=%s&auth_token=%s", EncodedEmail,
		EncodedPassword, EncodedAuthToken);

	// Curl free strings
	curl_free(EncodedEmail);
	curl_free(EncodedPassword);

	// Send request to sign in i.e. validate authtoken
	json_t *ResponseData = RequestMake("https://api.odysee.com/user/signin",
			NULL, Params);
	if (json_object_get(ResponseData, "error") != NULL) {
		json_decref(ReturnData);
		json_decref(AuthData);
		curl_free(EncodedAuthToken);
		curl_easy_cleanup(Curl);
		return ResponseData;
	}
	json_decref(ResponseData);

	// Get wallet hash
	json_t *WalletHash = RequestJson(Server, "{\"method\": \"sync_hash\"}");
	if (json_object_get(WalletHash, "error") != NULL) {
		json_decref(ReturnData);
		json_decref(AuthData);
		curl_free(EncodedAuthToken);
		curl_easy_cleanup(Curl);
		return WalletHash;
	}

	// Get the hash value for wallet
	const char *HashValue = json_string_value(WalletHash);

	// Strings to be url econded
	char *EncodedHashValue = curl_easy_escape(Curl, HashValue, 0);

	// Make the hash parameters string
	char HashParams[strlen(EncodedHashValue) + strlen(EncodedAuthToken) + 25];
	sprintf(HashParams, "hash=%s&auth_token=%s", EncodedHashValue,
		EncodedAuthToken);

	// Curl free strings
	curl_free(EncodedHashValue);
	curl_free(EncodedAuthToken);

	// Easy cleanup
	curl_easy_cleanup(Curl);

	// Finally retrieve wallet
	json_t *AccountData = RequestMake("https://api.lbry.com/sync/get",
			NULL, HashParams);

	// Decrease reference to both unusued objects at this point
	json_decref(AuthData);
	json_decref(WalletHash);

	if (json_object_get(AccountData, "error") != NULL) {
		json_decref(ReturnData);
		return AccountData;
	}

	// Get data for applying the sync
	const char *SyncValue =
		json_string_value(json_object_get(AccountData, "data"));

	// Make the sync apply method string
	char SyncParams[strlen(SyncValue) + 40];
	sprintf(SyncParams, "{\"method\": \"sync_apply\", \"params\": "
		"{ \"password\": \"\", \"blocking\": true, "
		"\"data\": \"%s\" }}", SyncValue);

	// Apply sync to lbrynet
	json_t *Sync = RequestJson(Server, SyncParams);
	if (json_object_get(Sync, "error") != NULL) {
		json_decref(ReturnData);
		return Sync;
	}
	json_decref(Sync);

	// Get account list for last checkup
	AccountList = RequestJson(Server, "{\"method\": \"account_list\"}");
	if (json_object_get(AccountList, "error") != NULL) {
		json_decref(ReturnData);
		return AccountList;
	}

	// Check each item in the array
	json_array_foreach(json_object_get(AccountList, "items"), Index, Value) {
		size_t IndexID;
		json_t *ValueID;
		bool Broken = false;
		json_array_foreach(AccountIDs, IndexID, ValueID) {
			// If the values are equal we will break and delete it
			if (json_equal(json_object_get(Value, "id"), ValueID)) {
				json_array_remove(AccountIDs, IndexID);
				Broken = true;
			}
		}

		// Only add new if it didn't exist
		if (Broken != true) {
			json_array_append(AccountIDs, json_object_get(Value, "id"));
		}
	}
	json_decref(AccountList);

	// ReturnData consists of auth_token and account ids
	return ReturnData;
}

// Everything under this line can be removed after not used in python code
static PyObject *SyncDisconnectPython(PyObject *Py_UNUSED(Self),
	PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Get arguments and pass it to the pythonless function
	PyObject *Unicode;
	char *Server;
	if (!PyArg_ParseTuple(Args, "Us", &Unicode, &Server)) {
		return NULL;
	}

	char *Text = (char *) PyUnicode_AsUTF8(Unicode);

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	// Convert Text into json
	json_error_t Error;
	json_t *AccountIDs = json_loads(Text, 0, &Error);

	int Disconnected = SyncDisconnect(AccountIDs, Server);

	// Acquire lock
	PyEval_RestoreThread(Save);

	return PyBool_FromLong(Disconnected);
}

static PyObject *SyncConnectPython(PyObject *Py_UNUSED(Self), PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Get arguments and pass it to the pythonless function
	char *Email, *Password, *Server;
	if (!PyArg_ParseTuple(Args, "sss", &Email, &Password, &Server)) {
		return NULL;
	}

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	json_t *JsonData = SyncConnect(Email, Password, Server);

	char *ResultString = json_dumps(JsonData, 0);
	json_decref(JsonData);

	// Acquire lock
	PyEval_RestoreThread(Save);

	PyObject *ResultPython = PyUnicode_FromString(ResultString);
	free(ResultString);

	return ResultPython;
}

// Function names and other python data
static PyMethodDef SyncMethods[] = {
	{"Disconnect", SyncDisconnectPython, METH_VARARGS, "Unsync account"},
	{"Connect", SyncConnectPython, METH_VARARGS, "Sync account"},
	{NULL, NULL, 0, NULL}
};

static struct PyModuleDef SyncModule = {
	PyModuleDef_HEAD_INIT, "Sync", "Sync module", -1, SyncMethods, 0, 0, 0, 0
};

// The function name has to end with the module name
PyMODINIT_FUNC PyInit_Sync(void) {
	return PyModule_Create(&SyncModule);
}
