/******************************************************************************\
* LBRY-GTK                                                                     *
* Copyright (C) 2021-2023 MorsMortium and Other Contributors                   *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU General Public License as published by         *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                *
* GNU General Public License for more details.                                 *
*                                                                              *
* You should have received a copy of the GNU General Public License            *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
\******************************************************************************/

// This file is responsible for loading in icons at specific sizes

#include <Python.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <gtk/gtk.h>

#include "Header/Places.h"
#include "Header/Icons.h"

GdkPixbuf *IconsLBCLabel, *IconsLogoBig;

void IconsFill(char *FilePath, GdkPixbufLoader *Loader) {
	// This function is responsible for filling a loader with an image

	//  Open image
	FILE *File = fopen(FilePath, "rb");
	if (File != NULL) {
		unsigned char Buffer[1024];
		size_t Read = 0;

		// Write image into loader, close file and loader
		while ((Read = fread(Buffer, 1, sizeof(Buffer), File)) > 0) {
			if (!gdk_pixbuf_loader_write(Loader, Buffer, Read, NULL)) {
				break;
			}
		}
		fclose(File);
	}
	gdk_pixbuf_loader_close(Loader, NULL);
}

void IconsStart(void) {
	// This function is responsible for creating every icon

	// Create a label, display it, get its height then destroy it
	GtkWidget *Label = gtk_label_new("Label");
	gtk_widget_show(Label);
	int Height;
	gtk_widget_get_preferred_height(Label, &Height, NULL);
	gtk_widget_destroy(Label);

	// Create loader, set resolution
	GdkPixbufLoader *Loader = gdk_pixbuf_loader_new();
	gdk_pixbuf_loader_set_size(Loader, Height, Height);

	// Get icon path, fill loader
	char IconPath[strlen(PlacesImage) + 17];
	sprintf(IconPath, "%slbry-gtk-lbc.svg", PlacesImage);
	IconsFill(IconPath, Loader);

	// Get icon pixbuf, make it standalone
	IconsLBCLabel = gdk_pixbuf_loader_get_pixbuf(Loader);
	g_object_ref(IconsLBCLabel);
	g_object_unref(Loader);

	// Create new loader, get icon path, fill loader
	Loader = gdk_pixbuf_loader_new();
	sprintf(IconPath, "%slbry-gtk.svg", PlacesImage);
	IconsFill(IconPath, Loader);

	// Get icon pixbuf, make it standalone
	IconsLogoBig = gdk_pixbuf_loader_get_pixbuf(Loader);
	g_object_ref(IconsLogoBig);
	g_object_unref(Loader);
}

void IconsStop(void) {
	// This function is responsible for freeing every icon

	// Free icon pixbufs
	g_object_unref(IconsLBCLabel);
	g_object_unref(IconsLogoBig);
}

// Everything under this line is removable after full C conversion
static PyObject *IconsGetPython(PyObject *Py_UNUSED(Self), PyObject *Py_UNUSED(
		Args)) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	PyObject *Dict = PyDict_New();

	PyObject *Object = PyLong_FromVoidPtr(IconsLBCLabel);
	PyDict_SetItemString(Dict, "LBCLabel", Object);
	Py_DECREF(Object);

	Object = PyLong_FromVoidPtr(IconsLogoBig);
	PyDict_SetItemString(Dict, "LogoBig", Object);
	Py_DECREF(Object);

	return Dict;
}

// Function names and other python data
static PyMethodDef IconsMethods[] = {
	{"Get", IconsGetPython, METH_VARARGS, "Get"},
	{NULL, NULL, 0, NULL}
};

static struct PyModuleDef IconsModule = {
	PyModuleDef_HEAD_INIT, "Icons", "Icons module", -1, IconsMethods, 0, 0, 0, 0
};

// The function name has to end with the module name
PyMODINIT_FUNC PyInit_Icons(void) {
	return PyModule_Create(&IconsModule);
}
