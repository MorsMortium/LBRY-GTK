/******************************************************************************\
* LBRY-GTK                                                                     *
* Copyright (C) 2021-2023 MorsMortium and Other Contributors                   *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU General Public License as published by         *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                *
* GNU General Public License for more details.                                 *
*                                                                              *
* You should have received a copy of the GNU General Public License            *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
\******************************************************************************/

// This file is responsible for managing the Status widget/page

#include <Python.h>
#include <ctype.h>
#include <gtk/gtk.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <jansson.h>

#include "Header/GTKExtra.h"
#include "Header/Global.h"
#include "Header/Places.h"
#include "Header/Connect.h"
#include "Header/Replace.h"
#include "Header/Tabs.h"
#include "Header/Status.h"

gboolean StatusSet(gpointer GotData) {
	// This function is responsible for displaying got status, or freeing data
	// if exit bool is set

	StatusData *Data = (StatusData *) GotData;

	// If exit bool is set, free data used by widget, exit
	if (Data->Exit) {
		if (Data->Server != NULL) {
			free(Data->Server);
		}
		json_decref(Data->GotStatus);
		pthread_mutex_destroy(&Data->Lock);
		return FALSE;
	}

	// Remove previously displayed status items
	gtk_container_foreach((GtkContainer *) Data->FlowBox, GTKExtraDestroy,
		NULL);

	const char *Key;
	json_t *Value;

	// Counter for already started components
	float ActiveNumber = 0;

	// Go through every component
	json_object_foreach(Data->GotStatus, Key, Value) {
		// Copy Key into local variable
		char Label[strlen(Key) + 1];
		sprintf(Label, "%s", Key);

		if (strlen(Label) < 5) {
			// If name is less than 5 characters long, it is an abbreviation,
			// capitalize every character
			for (long unsigned int Index = 0; Index < strlen(Label); ++Index) {
				Label[Index] = toupper(Label[Index]);
			}
		} else {
			// The name is a fully written name with '_' instead of ' ',
			// capitalize first character
			Label[0] = toupper(Label[0]);
			for (long unsigned int Index = 1; Index < strlen(Label); ++Index) {
				// If character is '_' make it space and capitalize the next
				if (Label[Index] == '_') {
					Label[Index] = ' ';
					Label[Index + 1] = toupper(Label[Index + 1]);
				}
			}
		}

		// Create checkbutton with Label, disable toggling
		GtkWidget *Button = gtk_check_button_new_with_label(Label);
		g_object_connect(G_OBJECT(Button), "signal::pressed",
			GTKExtraNoToggleBefore, NULL, "signal::released",
			GTKExtraNoToggleAfter, NULL, NULL);

		// Get activity, count it
		bool Active = json_is_true(Value);
		if (Active) {
			++ActiveNumber;
		}

		// Set activity, add to FlowBox
		gtk_toggle_button_set_active((GtkToggleButton *) Button, Active);
		gtk_container_add((GtkContainer *) Data->FlowBox, Button);
	}

	// Calculate how much of every component started, display it
	float Percent = ActiveNumber / (float) json_object_size(Data->GotStatus);
	gtk_progress_bar_set_fraction((GtkProgressBar *) Data->Progress, Percent);

	// Display everything, set name to signal finishing of display, free json,
	// release lock
	gtk_widget_show_all(Data->Status);
	if (Percent == 1) {
		gtk_widget_set_name(Data->Status, "Displayed");
	}
	json_decref(Data->GotStatus);
	pthread_mutex_unlock(&Data->Lock);

	return FALSE;
}

void *StatusGet(void *GotData) {
	// This function is responsible for getting status in a thread then starting
	// display in the main thread
	StatusData *Data = (StatusData *) GotData;

	// Get lock
	pthread_mutex_lock(&Data->Lock);

	// Get status, set LBRY-GTK as what we have
	Data->GotStatus = ConnectStatus(Data->Server);
	json_object_set_new(Data->GotStatus, "LBRY-GTK",
		json_boolean(Data->LBRYGTK));

	// Start display
	g_idle_add(StatusSet, Data);

	return NULL;
}

static gboolean StatusUpdate(gpointer GotData) {
	// This function is responsible for repeatedly starting status getting if
	// the exit bool is not set and the widget is realized

	StatusData *Data = (StatusData *) GotData;

	// Exit if widget is not realized
	if (!gtk_widget_get_realized(Data->Status)) {
		Data->Running = false;
		return FALSE;
	}

	// If exit bool is not set and server is null return with later execution
	if (!Data->Exit && (Data->Server == NULL)) {
		return TRUE;
	}

	// Start getting status in thread
	pthread_t Thread;
	pthread_create(&Thread, NULL, StatusGet, Data);
	pthread_detach(Thread);

	// If exit bool is set, return with no later execution
	if (Data->Exit) {
		return FALSE;
	}

	// Return with later execution
	return TRUE;
}

gboolean StatusDisplay(gpointer GotData) {
	// This function is responsible for displaying the widget on the page

	TabsData *TabData = (TabsData *) GotData;
	StatusData *Statuser;
	TabsGet(TabData, Statuser, Statuser);

	// Replace current widget with status, set page title to status
	ReplaceSet(TabData, "Status");
	gtk_label_set_text((GtkLabel *) Statuser->Title, "Status");

	// Start repeated getting of status, if needed
	if (!Statuser->Running) {
		g_timeout_add(1000, StatusUpdate, Statuser);
		Statuser->Running = true;
	}

	return FALSE;
}

StatusData *StatusCreate(GtkWidget *Title, TabsData *TabData) {
	// This function is responsible for creating the Status widget

	// Add data to StatusData
	StatusData *Data;
	TabsGet(TabData, Statuser, Data);

	// Get glade file name, load it in builder
	char WidgetFile[strlen(PlacesGlade) + 15];
	sprintf(WidgetFile, "%sStatus.glade", PlacesGlade);
	gtk_builder_add_from_file(GlobalBuilder, WidgetFile, NULL);

	// Set got/default values
	Data->Exit = false;
	Data->LBRYGTK = false;
	Data->Server = NULL;
	Data->Title = Title;
	Data->Running = false;

	// Get widgets used
	Data->Status = (GtkWidget *) gtk_builder_get_object(GlobalBuilder,
			"Status");
	Data->Progress = (GtkWidget *) gtk_builder_get_object(GlobalBuilder,
			"Progress");
	Data->FlowBox = (GtkWidget *) gtk_builder_get_object(GlobalBuilder,
			"FlowBox");

	// Add reference for Replace not to destroy the widget
	g_object_ref((GObject *) Data->Status);

	// Initiating mutex
	pthread_mutex_init(&Data->Lock, NULL);

	// Connect signals with data
	gtk_builder_connect_signals_full(GlobalBuilder, GTKExtraConnect,
		&TabData->Statuser);

	return &TabData->Statuser;
}

gboolean StatusOnStatusDestroy(G_GNUC_UNUSED GtkWidget *Widget,
	StatusData *Data) {
	// This function is responsible for signaling when the widget is destroyed
	// and repeated getting of status should stop and data should be freed

	// Set exit bool, send data for removal
	Data->Exit = true;
	g_idle_add(StatusUpdate, Data);

	return FALSE;
}

// Everything under this line is removable after full C conversion
static PyObject *StatusSetLBRYGTKPython(PyObject *Py_UNUSED(
		Self), PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Fill DataPointer, LBRYGTK with sent data
	uintptr_t DataPointer;
	long LBRYGTK;
	if (!PyArg_ParseTuple(Args, "Ll", &DataPointer, &LBRYGTK)) {
		return NULL;
	}

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	StatusData *Data = (StatusData *) DataPointer;
	Data->LBRYGTK = (bool) LBRYGTK;

	// Acquire lock
	PyEval_RestoreThread(Save);

	return PyLong_FromLong(0);
}

static PyObject *StatusSetServerPython(PyObject *Py_UNUSED(
		Self), PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Fill DataPointer, Server with sent data
	uintptr_t DataPointer;
	char *Server;
	if (!PyArg_ParseTuple(Args, "Ls", &DataPointer, &Server)) {
		return NULL;
	}

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	StatusData *Data = (StatusData *) DataPointer;
	Data->Server = malloc(strlen(Server) + 1);
	if (Data->Server != NULL) {
		sprintf(Data->Server, "%s", Server);
	}

	// Acquire lock
	PyEval_RestoreThread(Save);

	return PyLong_FromLong(0);
}

static PyObject *StatusDisplayPython(PyObject *Py_UNUSED(Self),
	PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Fill DataPointer with sent data
	uintptr_t DataPointer;
	if (!PyArg_ParseTuple(Args, "L", &DataPointer)) {
		return NULL;
	}

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	StatusDisplay((StatusData *) DataPointer);

	// Acquire lock
	PyEval_RestoreThread(Save);

	return PyLong_FromLong(0);
}

static PyObject *StatusCreatePython(PyObject *Py_UNUSED(Self), PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Fill TitlePointer with sent data
	uintptr_t TitlePointer, TabPointer;
	if (!PyArg_ParseTuple(Args, "LL", &TitlePointer, &TabPointer)) {
		return NULL;
	}

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	StatusData *Data = StatusCreate((GtkWidget *) TitlePointer,
			(TabsData *) TabPointer);

	// Acquire lock
	PyEval_RestoreThread(Save);

	PyObject *Dict = PyDict_New();

	PyObject *Object = PyLong_FromVoidPtr((void *) Data->Status);
	PyDict_SetItemString(Dict, "Status", Object);
	Py_DECREF(Object);

	Object = PyLong_FromVoidPtr((void *) Data);
	PyDict_SetItemString(Dict, "Pointer", Object);
	Py_DECREF(Object);

	return Dict;
}

// Function names and other python data
static PyMethodDef StatusMethods[] = {
	{"Create", StatusCreatePython, METH_VARARGS, ""},
	{"SetLBRYGTK", StatusSetLBRYGTKPython, METH_VARARGS, ""},
	{"SetServer", StatusSetServerPython, METH_VARARGS, ""},
	{"Display", StatusDisplayPython, METH_VARARGS, ""},
	{NULL, NULL, 0, NULL}
};

static struct PyModuleDef StatusModule = {
	PyModuleDef_HEAD_INIT, "Status", "Status module", -1, StatusMethods, 0, 0,
	0, 0
};

// The function name has to end with the module name
PyMODINIT_FUNC PyInit_Status(void) {
	return PyModule_Create(&StatusModule);
}
