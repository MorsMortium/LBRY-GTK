/******************************************************************************\
* LBRY-GTK                                                                     *
* Copyright (C) 2021-2023 MorsMortium and Other Contributors                   *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU General Public License as published by         *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                *
* GNU General Public License for more details.                                 *
*                                                                              *
* You should have received a copy of the GNU General Public License            *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
\******************************************************************************/

// This file is responsible for managing the Thumbnail widget

// POSIX.1-2008 compatibility
#define _XOPEN_SOURCE 700

#include <Python.h>
#include <gtk/gtk.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "Header/OS.h"
#include "Header/Global.h"
#include "Header/Places.h"
#include "Header/Image.h"
#include "Header/Popup.h"
#include "Header/Json.h"
#include "Header/Execute.h"
#include "Header/GTKExtra.h"
#include "Header/Thumbnail.h"

ThumbnailData *ThumbnailCreate(int Width, int Height, char Mode) {
	// This function is responsible for creating the Thumbnail widget

	// Allocate memory for data, abort if NULL
	ThumbnailData *Data = malloc(sizeof(ThumbnailData));
	if (Data == NULL) {
		abort();
	}

	// Get glade file name, load it in builder
	char WidgetFile[strlen(PlacesGlade) + 20];
	sprintf(WidgetFile, "%sThumbnail.glade", PlacesGlade);
	gtk_builder_add_from_file(GlobalBuilder, WidgetFile, NULL);

	// Set got/default values
	Data->Width = Width;
	Data->Height = Height;
	Data->Mode = Mode;
	Data->Url = NULL;
	Data->OldUrl = NULL;

	// Get widgets used
	Data->Thumbnail =
		(GtkWidget *) gtk_builder_get_object(GlobalBuilder, "Thumbnail");
	Data->Frame = (GtkWidget *) gtk_builder_get_object(GlobalBuilder, "Frame");
	Data->EventBox = (GtkWidget *) gtk_builder_get_object(GlobalBuilder,
			"EventBox");
	Data->Calculate = 2;

	// Set calculation mode and frame size based on got data (for comments)
	if (Width) {
		Data->Calculate = 1;
		gtk_widget_set_vexpand(Data->Frame, false);
		gtk_widget_set_size_request(Data->Frame, Width + 5, Height + 5);
	}

	// Get cursors for hovering
	GdkDisplay *Display = gtk_widget_get_display(Data->Thumbnail);
	Data->DefaultCursor = gdk_cursor_new_from_name(Display, "default");
	Data->PointerCursor = gdk_cursor_new_from_name(Display, "pointer");

	// Connect signals with data
	gtk_builder_connect_signals_full(GlobalBuilder, GTKExtraConnect, Data);

	return Data;
}

gboolean ThumbnailOnFrameDraw(GtkWidget *Widget, G_GNUC_UNUSED cairo_t *Cairo,
	ThumbnailData *Data) {
	// This function is responsible for redrawing the thumbnail, if needed

	bool SizeChanged = false;
	if (Data->Calculate == 2) {
		GtkAllocation Size;

		// Check if the widget changed size, set it if true
		gtk_widget_get_allocated_size(Widget, &Size, NULL);
		if (Data->Width != Size.width - 5 || Data->Height != Size.height - 5) {
			Data->Width = Size.width - 5;
			Data->Height = Size.height - 5;
			SizeChanged = true;
		}
	}

	// Calculate == 1 means it has to be drawn once
	if (Data->Calculate == 1) {
		SizeChanged = true;
	}

	if (Data->Url != Data->OldUrl) {
		// If url changed
		// Free old url if needed
		if (Data->OldUrl != NULL) {
			free(Data->OldUrl);
		}

		// Set new url, display it
		Data->OldUrl = Data->Url;
		ImageUrl(Data->Url, (GtkImage *) Data->Thumbnail, Data->Width,
			Data->Height, Data->Channel, Data->Mode);
	} else if (Data->Calculate != 0 && SizeChanged) {
		// If size changed or it needs to be drawn once

		// Disable drawing once
		if (Data->Calculate == 1) {
			Data->Calculate = 0;
		}

		// If url is empty, display it that way
		if (Data->Url == NULL) {
			ImageUrl(Data->Url, (GtkImage *) Data->Thumbnail, Data->Width,
				Data->Height, Data->Channel, false);
		} else {
			// Create and send data for displaying image file, instead of url
			ImageData *NewData = malloc(sizeof(ImageData));
			if (NewData == NULL) {
				return FALSE;
			}
			NewData->Path = ImagePath(Data->Url);
			NewData->Url = NULL;
			NewData->Image = (GtkImage *) Data->Thumbnail;
			NewData->Width = Data->Width;
			NewData->Height = Data->Height;
			NewData->Channel = Data->Channel;
			NewData->Transform = (bool) Data->Mode;
			NewData->Retry = false;
			pthread_mutex_lock(&ImageListLock);
			NewData->ListCount = ImageListCount;
			pthread_mutex_unlock(&ImageListLock);

			pthread_t Thread;
			pthread_create(&Thread, NULL, ImageFile, NewData);
			pthread_detach(Thread);
		}
	}
	return FALSE;
}

// For getting the command set for images
char *ThumbnailCommand[] = { "ImageCommand", NULL };

void *ThumbnailPress(void *GotData) {
	// This function is responsible for opening the thumbnail in a different
	// program

	ThumbnailData *Data = (ThumbnailData *) GotData;

	// If url is empty, display message
	if (Data->Url == NULL) {
		PopupMessage("This publication does not have a thumbnail.");
		return NULL;
	}

	// Get command for images, path of url
	char *ImageCommand = JsonStringObject(GlobalPreferences, ThumbnailCommand,
			"");
	char *FilePath = ImagePath(Data->Url);

	// Open image, free leftover data
	ExecuteFile(FilePath, ImageCommand);
	free(FilePath);

	return NULL;
}

void ThumbnailPressCheck(ThumbnailData *Data) {
	// This function is responsible for checking if the thumbnail is displayed
	// before opening it

	// Exit if not displayed
	if (!gtk_widget_get_realized(Data->Frame)) {
		return;
	}

	// Open thumbnail
	pthread_t Thread;
	pthread_create(&Thread, NULL, ThumbnailPress, Data);
	pthread_detach(Thread);
}

gboolean ThumbnailOnEventBoxLeaveNotifyEvent(G_GNUC_UNUSED GtkWidget *Widget,
	G_GNUC_UNUSED GdkEventCrossing *Event, ThumbnailData *Data) {
	// This function is responsible for setting cursor to default when leaving
	// thumbnail

	// Check if thumbnail is displayed, set cursor
	GdkWindow *Window = gtk_widget_get_window(Data->Thumbnail);
	if (Window != NULL) {
		gdk_window_set_cursor(Window, Data->DefaultCursor);
	}
	return FALSE;
}

gboolean ThumbnailOnEventBoxEnterNotifyEvent(G_GNUC_UNUSED GtkWidget *Widget,
	G_GNUC_UNUSED GdkEventCrossing *Event, ThumbnailData *Data) {
	// This function is responsible for setting cursor to pointer when entering
	// thumbnail

	// Check if thumbnail is displayed, set cursor
	GdkWindow *Window = gtk_widget_get_window(Data->Thumbnail);
	if (Window != NULL) {
		gdk_window_set_cursor(Window, Data->PointerCursor);
	}
	return FALSE;
}

gboolean ThumbnailOnEventBoxButtonPressEvent(G_GNUC_UNUSED GtkWidget *Widget,
	GdkEventButton *Event, ThumbnailData *Data) {
	// This function is responsible for opening the thumbnail when left clicked

	// If not left clicked, exit
	if (Event->button != GDK_BUTTON_PRIMARY) {
		return FALSE;
	}

	// Open thumbnail
	pthread_t Thread;
	pthread_create(&Thread, NULL, ThumbnailPress, Data);
	pthread_detach(Thread);
	return FALSE;
}

void ThumbnailSetExtra(ThumbnailData *Data, bool Channel, char *Url) {
	// This function is responsible for setting thumbnail url and channel

	// Set new channel
	Data->Channel = Channel;

	// If Url is empty, set that and at least single redraw
	if (Url == NULL) {
		Data->Url = NULL;
		if (Data->Calculate == 0) {
			Data->Calculate = 1;
		}
		return;
	}

	// Allocate memory, if not able set default value and at least single redraw
	Data->Url = malloc(strlen(Url) + 1);
	if (Data->Url == NULL) {
		if (Data->Calculate == 0) {
			Data->Calculate = 1;
		}
		return;
	}

	// Set new url
	sprintf(Data->Url, "%s", Url);
}

static void *ThumbnailFreeData(void *Args) {
	// Free thumbnail data

	ThumbnailData *Data = (ThumbnailData *) Args;

	// Only free value, if needed
	if (Data->Url != NULL) {
		free(Data->Url);
	}
	if (Data->Url != Data->OldUrl && Data->OldUrl != NULL) {
		free(Data->OldUrl);
	}

	free(Data);
	OSMemoryTrimmer();

	return NULL;
}

gboolean ThumbnailOnFrameDestroy(G_GNUC_UNUSED GtkWidget *Widget,
	ThumbnailData *Data) {
	// This function is responsible for freeing everything used by the widget

	pthread_t Thread;
	pthread_create(&Thread, NULL, ThumbnailFreeData, Data);
	pthread_detach(Thread);

	return FALSE;
}

// Everything under this line is removable after full C conversion
static PyObject *ThumbnailCreatePython(PyObject *Py_UNUSED(
		Self), PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Fill Width, Height with sent data
	long Width, Height, Mode;
	if (!PyArg_ParseTuple(Args, "lll", &Width, &Height, &Mode)) {
		return NULL;
	}

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	ThumbnailData *Data = ThumbnailCreate((int) Width, (int) Height,
			(char) Mode);

	// Acquire lock
	PyEval_RestoreThread(Save);

	PyObject *Dict = PyDict_New();

	PyObject *Object = PyLong_FromVoidPtr((void *) Data->Frame);
	PyDict_SetItemString(Dict, "Frame", Object);
	Py_DECREF(Object);

	Object = PyLong_FromVoidPtr((void *) Data);
	PyDict_SetItemString(Dict, "Pointer", Object);
	Py_DECREF(Object);

	return Dict;
}

static PyObject *ThumbnailSetExtraPython(PyObject *Py_UNUSED(
		Self), PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Fill DataPointer, Channel, Url with sent data
	uintptr_t DataPointer;
	long Channel;
	char *Url;
	if (!PyArg_ParseTuple(Args, "Lls", &DataPointer, &Channel, &Url)) {
		return NULL;
	}

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	if (strlen(Url) == 0) {
		Url = NULL;
	}

	ThumbnailSetExtra((ThumbnailData *) DataPointer, (bool) Channel, Url);

	// Acquire lock
	PyEval_RestoreThread(Save);

	return PyLong_FromLong(0);
}

static PyObject *ThumbnailPressCheckPython(PyObject *Py_UNUSED(
		Self), PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Fill DataPointer with sent data
	uintptr_t DataPointer;
	if (!PyArg_ParseTuple(Args, "L", &DataPointer)) {
		return NULL;
	}

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	ThumbnailPressCheck((ThumbnailData *) DataPointer);

	// Acquire lock
	PyEval_RestoreThread(Save);

	return PyLong_FromLong(0);
}

// Function names and other python data
static PyMethodDef ThumbnailMethods[] = {
	{"Create", ThumbnailCreatePython, METH_VARARGS, ""},
	{"SetExtra", ThumbnailSetExtraPython, METH_VARARGS, ""},
	{"PressCheck", ThumbnailPressCheckPython, METH_VARARGS, ""},
	{NULL, NULL, 0, NULL}
};

static struct PyModuleDef ThumbnailModule = {
	PyModuleDef_HEAD_INIT, "Thumbnail", "Thumbnail module", -1,
	ThumbnailMethods, 0, 0, 0, 0
};

// The function name has to end with the module name
PyMODINIT_FUNC PyInit_Thumbnail(void) {
	return PyModule_Create(&ThumbnailModule);
}
