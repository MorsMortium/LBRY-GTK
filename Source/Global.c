/******************************************************************************\
* LBRY-GTK                                                                     *
* Copyright (C) 2021-2023 MorsMortium and Other Contributors                   *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU General Public License as published by         *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                *
* GNU General Public License for more details.                                 *
*                                                                              *
* You should have received a copy of the GNU General Public License            *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
\******************************************************************************/

// Temporary file to set and return global variables
// TODO: This can be removed once main is rewritten

#include <Python.h>
#include <gtk/gtk.h>
#include <stdio.h>
#include <stdbool.h>
#include <jansson.h>

#include "Header/Global.h"

GtkBuilder *GlobalBuilder = NULL;
GtkWindow *GlobalWindow = NULL;
bool GlobalStarted = false;
json_t *GlobalPreferences = NULL, *GlobalSettings = NULL;

// Everything in this file is removable, not just these under this line
static PyObject *GlobalSetBuilderPython(PyObject *Self, PyObject *Py_UNUSED(
		Args)) {
	// Sets the
	GlobalBuilder = gtk_builder_new();
	PyModule_AddObject(Self, "Builder",
		PyLong_FromVoidPtr((void *) GlobalBuilder));

	return PyLong_FromLong(0);
}

static PyObject *GlobalSetWindowPython(PyObject *Self, PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0
	uintptr_t WindowPointer;
	if (!PyArg_ParseTuple(Args, "L", &WindowPointer)) {
		return NULL;
	}

	GlobalWindow = (GtkWindow *) WindowPointer;
	PyModule_AddObject(Self, "Window",
		PyLong_FromVoidPtr((void *) GlobalWindow));

	return PyLong_FromLong(0);
}

static PyObject *GlobalGetStartPython(PyObject *Py_UNUSED(Self),
	PyObject *Py_UNUSED(Args)) {
	// Gets started flag

	return PyLong_FromLong(GlobalStarted);
}

// Function names and other python data
static PyMethodDef GlobalMethods[] = {
	{"SetBuilder", (PyCFunction) GlobalSetBuilderPython, METH_NOARGS, ""},
	{"SetWindow", GlobalSetWindowPython, METH_VARARGS, ""},
	{"GetStarted", (PyCFunction) GlobalGetStartPython, METH_NOARGS, ""},
	{NULL, NULL, 0, NULL}
};

static struct PyModuleDef GlobalModule = {
	PyModuleDef_HEAD_INIT, "Global", "Global module", -1, GlobalMethods, 0, 0,
	0, 0
};

// The function name has to end with the module name
PyMODINIT_FUNC PyInit_Global(void) {
	return PyModule_Create(&GlobalModule);
}
