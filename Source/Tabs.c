/******************************************************************************\
* LBRY-GTK                                                                     *
* Copyright (C) 2021-2023 MorsMortium and Other Contributors                   *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU General Public License as published by         *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                *
* GNU General Public License for more details.                                 *
*                                                                              *
* You should have received a copy of the GNU General Public License            *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
\******************************************************************************/

// Creates and returns data used by individual tabs

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

#include "Header/List.h"
#include "Header/Replace.h"
#include "Header/State.h"
#include "Header/Threads.h"
#include "Header/Tabs.h"

// Global linked list for tab data
TabsData *TabsList = NULL;

// TODO: Remove this once Threads.c no longer needs this
void ThreadsFreePython(ThreadsData *Data);

TabsData *TabsNew(void) {
	// Make new item to linked list of tabs

	// TODO: Figure out if we create everything here or just allocate memory
	// Creates new tab
	TabsData *NewList = malloc(sizeof(TabsData));
	if (NewList == NULL) {
		fprintf(stderr, "Could not allocate memory for new tab!\n");
		abort();
	}

	// Set GettingThread to false as no threads are yet running
	NewList->GettingThread = false;

	// Init mutex lock
	pthread_mutex_init(&NewList->Lock, NULL);

	// New item to linked list
	if (TabsList != NULL) {
		TabsList->Next = NewList;
	}
	NewList->Previous = TabsList;
	NewList->Next = NULL;
	TabsList = NewList;
	TabsList->Stater = NULL;

	return TabsList;
}

void TabsFree(TabsData *Data) {
	// Free one tab of data

	// Next and previous should point to each other
	if (Data->Next) {
		Data->Next->Previous = Data->Previous;
	}

	if (Data->Previous) {
		Data->Previous->Next = Data->Next;
	}

	// All free functions here
	ReplaceFree(&Data->Replacer);
	ListFree(&Data->Lister);
	ThreadsFreePython(&Data->Threader);
	StateFree(Data->Stater);

	pthread_mutex_destroy(&TabsList->Lock);

	free(Data);
}

bool TabsIsExecuting(TabsData *TabData) {
	// Check if some thread is still executing

	pthread_mutex_lock(&TabData->Lock);
	bool IsExecuting = TabData->GettingThread;
	pthread_mutex_unlock(&TabData->Lock);

	return IsExecuting;
}

// Everything under this line is removable after full C conversion
static PyObject *TabsNewPython(PyObject *Py_UNUSED(Self), PyObject *Py_UNUSED(
		Args)) {
	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	TabsData *TabData = TabsNew();

	// Acquire lock
	PyEval_RestoreThread(Save);

	return PyLong_FromVoidPtr(TabData);
}

static PyObject *TabsFreePython(PyObject *Py_UNUSED(Self), PyObject *Args) {
	uintptr_t LongPointer;
	if (!PyArg_ParseTuple(Args, "L", &LongPointer)) {
		return NULL;
	}

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	TabsFree((TabsData *) LongPointer);

	// Acquire lock
	PyEval_RestoreThread(Save);

	Py_RETURN_NONE;
}

// Function names and other python data
static PyMethodDef TabsMethods[] = {
	{"Free", TabsFreePython, METH_VARARGS, "Free tab"},
	{"Create", TabsNewPython, METH_NOARGS, "Creates new tab"},
	{NULL, NULL, 0, NULL}
};

static struct PyModuleDef TabsModule = {
	PyModuleDef_HEAD_INIT, "Tabs", "Tabs module", -1, TabsMethods, 0, 0, 0, 0
};

// The function name has to end with the module name
PyMODINIT_FUNC PyInit_Tabs(void) {
	return PyModule_Create(&TabsModule);
}
