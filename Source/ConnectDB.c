/******************************************************************************\
* LBRY-GTK                                                                     *
* Copyright (C) 2021-2022 MorsMortium and Other Contributors                   *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU General Public License as published by         *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                *
* GNU General Public License for more details.                                 *
*                                                                              *
* You should have received a copy of the GNU General Public License            *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
\******************************************************************************/

// This file is in aid of all database calls: use this to retrieve handle and
// when doing something in the DB remember to use ConnectDBLock

#include <Python.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <pthread.h>
#include <sqlite3.h>

#include "Header/Places.h"
#include "Header/ConnectDB.h"

// Lock for all database actions
pthread_mutex_t ConnectDBLock;

// Holds the information of us running or not
bool ConnectDBNoDB = true;

sqlite3 *ConnectDBOpen(void) {
	// This makes the connection to DB

	// The handle
	sqlite3 *DB;

	// Get the path
	char DBPath[strlen(PlacesConfig) + 15];
	sprintf(DBPath, "%sLBRY-GTK.db", PlacesConfig);

	// If we are not running anymore, return NULL
	if (ConnectDBNoDB) {
		return NULL;
	}

	// Open the database
	int RC = sqlite3_open_v2(DBPath, &DB,
			SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE | SQLITE_OPEN_NOMUTEX,
			NULL);

	// Make sure connection holds
	if (RC != SQLITE_OK) {
		sqlite3_close_v2(DB);
		return NULL;
	}

	// Settings table setup
	char *Query = "CREATE TABLE IF NOT EXISTS SettingTable "
		"(SettingName TEXT, SettingValue INTEGER)";
	RC = sqlite3_exec(DB, Query, 0, 0, NULL);

	Query = "CREATE UNIQUE INDEX IF NOT EXISTS idx_SettingTable_SettingName "
		"ON SettingTable (SettingName)";
	RC = sqlite3_exec(DB, Query, 0, 0, NULL);

	// Notifications table setup
	Query = "CREATE TABLE IF NOT EXISTS Notifications"
		"(ImageUrl TEXT, Title TEXT, Channel TEXT, "
		"ContentType TEXT, Url TEXT, Claim TEXT, Timestamp INTEGER)";
	RC = sqlite3_exec(DB, Query, 0, 0, NULL);

	// Comments table setup
	Query = "CREATE TABLE IF NOT EXISTS Comments"
		"(Comment TEXT, ClaimID TEXT, CommentID TEXT, ChannelID TEXT,"
		" Timestamp INTEGER)";
	RC = sqlite3_exec(DB, Query, 0, 0, NULL);

	Query = "CREATE UNIQUE INDEX IF NOT EXISTS idx_Comments_CommentID "
		"ON Comments (CommentID)";
	RC = sqlite3_exec(DB, Query, 0, 0, NULL);

	// Check for errors
	if (RC != SQLITE_OK) {
		fprintf(stderr, "Something went wrong creating defaults: %s\n",
			sqlite3_errmsg(DB));
	}

	// Return the wanted handle
	return DB;
}

void ConnectDBJoin(int ThreadNumber, ConnectDBArgs *CInfo) {
	// Wait for threads to stop doing their thing

	// Loop over each thread
	int Index = 0;
	while (Index < ThreadNumber) {
		// Join waits for thread to end
		int RC = pthread_join(CInfo[Index].Thread, NULL);

		// Increment by one
		Index++;

		// Checks
		if (RC != 0) {
			fprintf(stderr, "Could not join thread %d\n", Index - 1);
		}
	}
}

void ConnectDBStart(void) {
	// DB write/read possible
	ConnectDBNoDB = false;

	// Init lock for DB
	pthread_mutex_init(&ConnectDBLock, NULL);
}

void ConnectDBStop(void) {
	// No DB write/read anymore
	ConnectDBNoDB = true;

	// Destroy lock for DB
	pthread_mutex_destroy(&ConnectDBLock);
}
