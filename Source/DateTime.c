/******************************************************************************\
* LBRY-GTK                                                                     *
* Copyright (C) 2021-2023 MorsMortium and Other Contributors                   *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU General Public License as published by         *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                *
* GNU General Public License for more details.                                 *
*                                                                              *
* You should have received a copy of the GNU General Public License            *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
\******************************************************************************/

// This is the DateTime widget file responsible for creating a calendar and
// date manipulation widget

#include <Python.h>
#include <gtk/gtk.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "Header/Global.h"
#include "Header/Places.h"
#include "Header/GTKExtra.h"
#include "Header/Timestamp.h"
#include "Header/DateTime.h"

DateTimeData *DateTimeCreate(void) {
	// This function is responsible for creating the DateTime widget

	// Allocate memory for data, abort if NULL
	DateTimeData *Data = malloc(sizeof(DateTimeData));
	if (Data == NULL) {
		abort();
	}

	// Get glade file name, load it in builder
	char WidgetFile[strlen(PlacesGlade) + 20];
	sprintf(WidgetFile, "%sDateTime.glade", PlacesGlade);
	gtk_builder_add_from_file(GlobalBuilder, WidgetFile, NULL);

	// Set data fields
	Data->Calendar =
		(GtkCalendar *) gtk_builder_get_object(GlobalBuilder, "Calendar");
	Data->DateTime =
		(GtkWidget *) gtk_builder_get_object(GlobalBuilder, "DateTime");
	Data->Hour =
		(GtkWidget *) gtk_builder_get_object(GlobalBuilder, "Hour");
	Data->Minute =
		(GtkWidget *) gtk_builder_get_object(GlobalBuilder, "Minute");
	Data->Second =
		(GtkWidget *) gtk_builder_get_object(GlobalBuilder, "Second");
	Data->DateUse =
		(GtkWidget *) gtk_builder_get_object(GlobalBuilder, "DateUse");
	Data->DateReset =
		(GtkWidget *) gtk_builder_get_object(GlobalBuilder, "DateReset");
	Data->DateActive =
		(GtkWidget *) gtk_builder_get_object(GlobalBuilder, "DateActive");
	Data->DateUseActive =
		(GtkWidget *) gtk_builder_get_object(GlobalBuilder, "DateUseActive");

	// Connect signals with data
	gtk_builder_connect_signals_full(GlobalBuilder, GTKExtraConnect, Data);

	return Data;
}

void DateTimeSet(DateTimeData *Data, struct tm *Time) {
	// This funtion will set time to calendar

	// Place day to the first day
	gtk_calendar_select_day(Data->Calendar, 1);

	// Set the date
	gtk_calendar_select_month(Data->Calendar, (Time->tm_mon),
		1900 + Time->tm_year);
	gtk_calendar_select_day(Data->Calendar, Time->tm_mday);

	// Set spinbutton values
	gtk_spin_button_set_value((GtkSpinButton *) Data->Hour,
		(double) Time->tm_hour);
	gtk_spin_button_set_value((GtkSpinButton *) Data->Minute,
		(double) Time->tm_min);
	gtk_spin_button_set_value((GtkSpinButton *) Data->Second,
		(double) Time->tm_sec);

	// Set old time values to data
	Data->OldHour = Time->tm_hour;
	Data->OldMinute = Time->tm_min;
	Data->OldSecond = Time->tm_sec;
}

time_t DateTimeGet(DateTimeData *Data, int Added) {
	// This gets the time

	// This will hold the returned value
	time_t Timestamp = -1;

	// Here we will get the wanted timestamp or set date to calendar
	if (gtk_toggle_button_get_active((GtkToggleButton *) Data->DateUse)
		|| Added) {
		// Hold calendar values in these
		int Year, Month, Day;

		// Get calendar date
		gtk_calendar_get_date(Data->Calendar,
			(unsigned *) &Year, (unsigned *) &Month, (unsigned *) &Day);

		// Make tm struct to hold our date
		struct tm CalendarTime;

		// So many things to get here
		CalendarTime.tm_year = Year - 1900;
		CalendarTime.tm_mon = Month;
		CalendarTime.tm_mday = Day + Added;
		CalendarTime.tm_hour =
			(int) gtk_spin_button_get_value((GtkSpinButton *) Data->Hour);
		CalendarTime.tm_min =
			(int) gtk_spin_button_get_value((GtkSpinButton *) Data->Minute);
		CalendarTime.tm_sec =
			(int) gtk_spin_button_get_value((GtkSpinButton *) Data->Second);
		CalendarTime.tm_isdst = -1;

		// Make into a timestamp
		Timestamp = mktime(&CalendarTime);

		// If we added a day (or minus day), we change it to calendar
		if (Added) {
			DateTimeSet(Data, &CalendarTime);
		}
	}

	return Timestamp;
}

gboolean DateTimeOnDateResetButtonPressEvent(G_GNUC_UNUSED GtkWidget *Widget,
	G_GNUC_UNUSED GdkEventButton *Event, DateTimeData *Data) {
	// This function is responsible for resetting the calendar date

	// Get the local time
	time_t RawTime = time(NULL);
	struct tm *TimeNow = localtime(&RawTime);

	// Now actually set the time
	DateTimeSet(Data, TimeNow);

	return FALSE;
}

gboolean DateTimeOnSecondValueChanged(GtkWidget *Widget, DateTimeData *Data) {
	// This function resets second values in data

	Data->OlderSecond = Data->OldSecond;
	Data->OldSecond = (int) gtk_spin_button_get_value((GtkSpinButton *) Widget);

	return FALSE;
}

gboolean DateTimeOnMinuteValueChanged(GtkWidget *Widget, DateTimeData *Data) {
	// This function resets minute values in data

	Data->OlderMinute = Data->OldMinute;
	Data->OldMinute = (int) gtk_spin_button_get_value((GtkSpinButton *) Widget);

	return FALSE;
}

gboolean DateTimeOnHourValueChanged(GtkWidget *Widget, DateTimeData *Data) {
	// This function resets hour values in data

	Data->OlderHour = Data->OldHour;
	Data->OldHour = (int) gtk_spin_button_get_value((GtkSpinButton *) Widget);

	return FALSE;
}

gboolean DateTimeOnSecondWrapped(G_GNUC_UNUSED GtkWidget *Widget,
	DateTimeData *Data) {
	// This function spins the way we want for seconds

	// Check if step forward or backward
	int Step =
		(Data->OldSecond <
		Data->OlderSecond) ? GTK_SPIN_STEP_FORWARD : GTK_SPIN_STEP_BACKWARD;

	// Do the spin
	gtk_spin_button_spin((GtkSpinButton *) Data->Minute, Step, (double) 1);

	return FALSE;
}

gboolean DateTimeOnMinuteWrapped(G_GNUC_UNUSED GtkWidget *Widget,
	DateTimeData *Data) {
	// This function spins the way we want for minutes

	// Check if step forward or backward
	int Step =
		(Data->OldMinute <
		Data->OlderMinute) ? GTK_SPIN_STEP_FORWARD : GTK_SPIN_STEP_BACKWARD;

	// Do the spin
	gtk_spin_button_spin((GtkSpinButton *) Data->Hour, Step, (double) 1);

	return FALSE;
}

gboolean DateTimeOnHourWrapped(G_GNUC_UNUSED GtkWidget *Widget,
	DateTimeData *Data) {
	// This function changes the calendar as hours wrap

	// Hold calendar values in these
	int Year, Month, Day;

	// Get calendar date
	gtk_calendar_get_date(Data->Calendar, (unsigned *) &Year,
		(unsigned *) &Month, (unsigned *) &Day);

	// Place day to the first day
	gtk_calendar_select_day(Data->Calendar, 1);

	// Some flags and such
	int Add = -1;
	bool DaysWrap;
	bool MonthsWrap;

	// Some basic checks for setting few flags
	if (Data->OldHour < Data->OlderHour) {
		// Get days in a month
		int Days = TimestampMonthDays(Year, Month + 1);
		DaysWrap = Days < Day + 1;
		MonthsWrap = 11 < Month + 1;
		Add = 1;
	} else {
		DaysWrap = Day - 1 < 1;
		MonthsWrap = Month - 1 < 0;
	}

	// This gets decided on if days have wrapped
	if (DaysWrap) {
		// If months have wrapped then change year and month
		if (MonthsWrap) {
			Month = (Month) ? -1 : 12;
			Year = Year + Add;
		}

		// Change the day to either of its max
		Day = (Add != 1) ? TimestampMonthDays(Year, Month) + 1 : 0;

		// This changes the year and month
		gtk_calendar_select_month(Data->Calendar, Month + Add, Year);
	}

	// Finally select the day
	gtk_calendar_select_day(Data->Calendar, Day + Add);

	return FALSE;
}

gboolean DateTimeDateChange(int Button, DateTimeData *Data) {
	// This function changes the date by a day

	// Make the change: if button 0 minus one else plus one
	DateTimeGet(Data, (Button == 0) ? -1 : 1);

	return FALSE;
}

// Everything under this line is removable after full C conversion
static PyObject *DateTimeCreatePython(PyObject *Py_UNUSED(
		Self), PyObject *Py_UNUSED(Args)) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	// Pass object to pythonless function
	DateTimeData *Data = DateTimeCreate();

	// Acquire lock
	PyEval_RestoreThread(Save);

	PyObject *Dict = PyDict_New();

	PyObject *Object = PyLong_FromVoidPtr((void *) Data->Calendar);
	PyDict_SetItemString(Dict, "Calendar", Object);
	Py_DECREF(Object);

	Object = PyLong_FromVoidPtr((void *) Data->DateTime);
	PyDict_SetItemString(Dict, "DateTime", Object);
	Py_DECREF(Object);

	Object = PyLong_FromVoidPtr((void *) Data->DateActive);
	PyDict_SetItemString(Dict, "DateActive", Object);
	Py_DECREF(Object);

	Object = PyLong_FromVoidPtr((void *) Data->DateUseActive);
	PyDict_SetItemString(Dict, "DateUseActive", Object);
	Py_DECREF(Object);

	Object = PyLong_FromVoidPtr((void *) Data->DateUse);
	PyDict_SetItemString(Dict, "DateUse", Object);
	Py_DECREF(Object);

	Object = PyLong_FromVoidPtr((void *) Data->DateReset);
	PyDict_SetItemString(Dict, "DateReset", Object);
	Py_DECREF(Object);

	Object = PyLong_FromVoidPtr((void *) Data->Hour);
	PyDict_SetItemString(Dict, "Hour", Object);
	Py_DECREF(Object);

	Object = PyLong_FromVoidPtr((void *) Data->Minute);
	PyDict_SetItemString(Dict, "Minute", Object);
	Py_DECREF(Object);

	Object = PyLong_FromVoidPtr((void *) Data->Second);
	PyDict_SetItemString(Dict, "Second", Object);
	Py_DECREF(Object);

	Object = PyLong_FromVoidPtr((void *) Data);
	PyDict_SetItemString(Dict, "Pointer", Object);
	Py_DECREF(Object);

	return Dict;
}

static PyObject *DateTimeSetPython(PyObject *Py_UNUSED(Self), PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0
	uintptr_t LongPointer;
	time_t Timestamp = 0;
	if (!PyArg_ParseTuple(Args, "LL", &LongPointer, &Timestamp)) {
		return NULL;
	}

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	struct tm *Time = localtime(&Timestamp);

	// Pass object to pythonless function with time struct
	DateTimeSet((DateTimeData *) LongPointer, Time);

	// Acquire lock
	PyEval_RestoreThread(Save);

	return PyLong_FromLong(0);
}

static PyObject *DateTimeGetPython(PyObject *Py_UNUSED(Self), PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0
	uintptr_t LongPointer;
	int Added = 0;
	if (!PyArg_ParseTuple(Args, "Li", &LongPointer, &Added)) {
		return NULL;
	}

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	// Pass object to pythonless function with time struct
	time_t Rawtime = DateTimeGet((DateTimeData *) LongPointer, Added);

	// Acquire lock
	PyEval_RestoreThread(Save);

	return PyLong_FromLongLong(Rawtime);
}

static PyObject *DateTimeDateChangePython(PyObject *Py_UNUSED(
		Self), PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0
	uintptr_t LongPointer;
	int Button = 0;
	if (!PyArg_ParseTuple(Args, "Li", &LongPointer, &Button)) {
		return NULL;
	}

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	// Pass object to pythonless function with time struct
	DateTimeDateChange(Button, (DateTimeData *) LongPointer);

	// Acquire lock
	PyEval_RestoreThread(Save);

	return PyLong_FromLong(0);
}

static PyObject *DateTimeOnDateResetButtonPressEventPython(PyObject *Py_UNUSED(
		Self), PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0
	uintptr_t LongPointer;
	if (!PyArg_ParseTuple(Args, "L", &LongPointer)) {
		return NULL;
	}

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	// Pass object to pythonless function with time struct
	DateTimeOnDateResetButtonPressEvent(NULL, NULL,
		(DateTimeData *) LongPointer);

	// Acquire lock
	PyEval_RestoreThread(Save);

	return PyLong_FromLong(0);
}

// Function names and other python data
static PyMethodDef DateTimeMethods[] = {
	{"Create", DateTimeCreatePython, METH_VARARGS, "DateTime init"},
	{"SetTime", DateTimeSetPython, METH_VARARGS, "Set time"},
	{"GetTime", DateTimeGetPython, METH_VARARGS, "Get time"},
	{"DateChange", DateTimeDateChangePython, METH_VARARGS, "DateChange"},
	{"OnDateResetButtonPressEvent", DateTimeOnDateResetButtonPressEventPython,
	 METH_VARARGS, "DateChange"},
	{NULL, NULL, 0, NULL}
};

static struct PyModuleDef DateTimeModule = {
	PyModuleDef_HEAD_INIT, "DateTime", "DateTime module", -1, DateTimeMethods,
	0, 0, 0, 0
};

// The function name has to end with the module name
PyMODINIT_FUNC PyInit_DateTime(void) {
	return PyModule_Create(&DateTimeModule);
}
