/******************************************************************************\
* LBRY-GTK                                                                     *
* Copyright (C) 2021-2023 MorsMortium and Other Contributors                   *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU General Public License as published by         *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                *
* GNU General Public License for more details.                                 *
*                                                                              *
* You should have received a copy of the GNU General Public License            *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
\******************************************************************************/

// This file is responsible for getting data about (a) publication(s), for
// resolving repost thumbnails and for getting web instance links

#include <Python.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <jansson.h>

#include "Header/Request.h"
#include "Header/Error.h"
#include "Header/Json.h"
#include "Header/Url.h"

// Data for getting json paths
char *UrlThumbnailPath1[] = { "value", "thumbnail", "url", NULL };
char *UrlThumbnailPath2[] =
{ "reposted_claim", "value", "thumbnail", "url", NULL };
char *UrlTypePath1[] = { "value", "stream_type", NULL };
char *UrlTypePath2[] = { "value_type", NULL };
char *UrlRepostPath1[] = { "reposted_claim", "canonical_url", NULL };
char *UrlRepostPath2[] = { "value", "claim_id", NULL };

// List of web instances of LBRY
char *UrlInstances[6][3] = {
	// Name, Url, Content available ("1" == true)
	{"Odysee", "https://odysee.com/", "1"},
	{"Madiator", "https://madiator.com/", "1"},
	{"Librarian", "https://librarian.bcow.xyz/", "1"},
	{"LBRY", "lbry://", "1"},
	{"LBRYOpen", "https://open.lbry.com/", "0"},
	{"LBRYTv", "https://lbry.tv/", "0"}
};

// Number of items in Instances
int UrlSize = 6;

struct UrlInstance **UrlWeb(char *GotUrl) {
	// This function is responsible for getting web instance links

	// Copy url into local variable for usage
	char Url[strlen(GotUrl) + 1];
	sprintf(Url, "%s", GotUrl);

	// Some web clients do not work with '#' in the URL
	for (int Index = 0; Index < (int) strlen(Url); ++Index) {
		if (Url[Index] == '#') {
			Url[Index] = ':';
		}
	}

	// Create and allocate list containg every url of every instance
	struct UrlInstance **List;
	List = malloc(sizeof(struct UrlInstance **) * UrlSize);

	for (int Index = 0; Index < UrlSize; ++Index) {
		// Get instance data and url length
		char **Instance = UrlInstances[Index];

		// If instance can be used for getting content (Link/Share/Links)
		if (Instance[2][0] == '1') {
			int Length = strlen(Url) - 6 + strlen(Instance[1]);

			// Create and allocate instance/url pair
			struct UrlInstance *ListItem;
			ListItem = malloc(sizeof(struct UrlInstance));
			ListItem->Provider = malloc(strlen(Instance[0]) + 1);
			ListItem->Url = malloc(Length);

			// Fill instance/url pair and add it to list
			sprintf(ListItem->Provider, "%s", Instance[0]);
			sprintf(ListItem->Url, "%s%s", Instance[1], Url + 7);
			List[Index] = ListItem;
		} else {
			// Close List with null
			List[Index] = NULL;
		}
	}

	return List;
}

json_t *UrlRequest(json_t *Data, bool IsClaim, char *Server) {
	// This function is responsible for sending search and resolve requests

	//create data json object
	json_t *RequestData = json_object();

	// Specifiy request kind
	if (IsClaim) {
		json_object_set_new(RequestData, "method", json_string("claim_search"));
	} else {
		json_object_set_new(RequestData, "method", json_string("resolve"));
	}

	// Add data to request
	json_t *RequestParams = json_object();
	if (IsClaim) {
		json_object_set(RequestParams, "claim_ids", Data);
	} else {
		json_object_set(RequestParams, "urls", Data);
	}
	json_object_set_new(RequestData, "params", RequestParams);

	// Get json string, free json object
	char *RequestString = json_dumps(RequestData, 0);
	json_decref(RequestData);

	// Get response json, free json string
	json_t *ReturnedData = RequestJson(Server, RequestString);
	free(RequestString);

	return ReturnedData;
}

int UrlReposted(json_t *Items, char **UrlArray, int *UrlIndices, bool Content) {
	// This function is responsible for filling a list containing every
	// reposted claim, from a list of claims

	int UrlNumber = 0;

	// Go through every claim
	size_t Index;
	json_t *Value;
	json_array_foreach(Items, Index, Value) {
		// Get type, with fallback
		char *TypeString = JsonStringObject(Value, UrlTypePath1, "");
		if (strlen(TypeString) == 0) {
			TypeString = JsonStringObject(Value, UrlTypePath2, "");
		}

		// If type is repost
		if (strcmp(TypeString, "repost") == 0) {
			char *Url;
			if (Content) {
				// If reposted claim is needed
				Url = JsonStringObject(Value, UrlRepostPath1, NULL);
				if (Url == NULL) {
					Url = JsonStringObject(Value, UrlRepostPath2, NULL);
				}
			} else {
				// If reposter claim is needed
				json_t *UrlObject = json_object_get(Value, "permanent_url");
				if (UrlObject == NULL) {
					UrlObject = json_object_get(Value, "canonical_url");
				}
				Url = JsonString(UrlObject, NULL);
			}

			// Put url into list
			UrlArray[UrlNumber] = Url;
			UrlIndices[UrlNumber++] = Index;
		}
	}
	return UrlNumber;
}

json_t *UrlGet(char **Querries, int QuerryNumber, char *Server) {
	// This function is responsible for getting data about (a) publication(s)

	// List for separating querry types and for keeping their order
	json_t *Claims = NULL, *Urls = NULL;
	int JsonQuerries[QuerryNumber];

	// Go through every querry
	for (int Index = 0; Index < QuerryNumber; ++Index) {
		// Get querry into local variable, extra length for protocol and \0
		int Length = strlen(Querries[Index]) + 8;
		char Querry[Length];
		sprintf(Querry, "%s", Querries[Index]);

		// Go through every web instance
		for (int InstanceIndex = 0; InstanceIndex < UrlSize; ++InstanceIndex) {
			// If querry begins with instance
			if (strstr(Querry, UrlInstances[InstanceIndex][1]) == Querry) {
				char *UrlBuffer = Querry;
				if (strstr(Querry, "@") != NULL) {
					// If querry is a publication with a channel, remove
					// everything before it
					UrlBuffer = strstr(Querry, "@");
				} else {
					// If querry does not have a channel, remove everything
					// until the last slash (slash included)
					while (strstr(UrlBuffer, "/") != NULL) {
						UrlBuffer = strstr(UrlBuffer, "/") + 1;
					}
				}

				// If querry has '?' in it, remove everything after it, with it
				if (strstr(Querry, "?") != NULL) {
					Querry[strlen(Querry) - strlen(strstr(Querry, "?"))] = '\0';
				}

				// Add protocol to querry
				memmove(Querry + 7, UrlBuffer, strlen(UrlBuffer) + 1);
				memmove(Querry, "lbry://", 7);
				break;
			}
		}

		// If querry is 40 chars long and does not begin with protocol, then it
		// is a claim, otherwise a url
		if (strlen(Querry) == 40 && strstr(Querry, "lbry://") != Querry) {
			if (Claims == NULL) {
				Claims = json_array();
			}
			json_array_append_new(Claims, json_string(Querry));

			// Signify querry as claim for later retrieval
			JsonQuerries[Index] = 0;
		} else {
			if (Urls == NULL) {
				Urls = json_array();
			}
			json_array_append_new(Urls, json_string(Querry));

			// Signify querry as url for later retrieval
			JsonQuerries[Index] = 1;
		}
	}

	// Json objects keeping request results
	json_t *ClaimsReturn = NULL, *UrlsReturn = NULL;

	// If there are claims
	if (Claims != NULL) {
		// Get request data
		ClaimsReturn = UrlRequest(Claims, true, Server);

		// If request returned an error, return it
		if (json_object_get(ClaimsReturn, "error") != NULL) {
			// Free list of urls
			if (Urls != NULL) {
				json_decref(Urls);
			}
			return ClaimsReturn;
		} else {
			// Get items key of json, set that as list
			json_t *Buffer = json_object_get(ClaimsReturn, "items");

			size_t Index;
			json_t *Value, *FinalClaims = json_array();
			bool Found = false;

			// Go through every claim
			json_array_foreach(Claims, Index, Value) {
				size_t BufferIndex;
				json_t *BufferValue;

				// Go through every claim returned by lbrynet
				json_array_foreach(Buffer, BufferIndex, BufferValue) {
					// If returned claim is requested, add it to list, set
					// variable for finding it and exit
					if (strcmp(json_string_value(json_object_get(BufferValue,
						"claim_id")),
						json_string_value(Value)) == 0) {
						json_array_append(FinalClaims, BufferValue);
						Found = true;
						break;
					}
				}
			}

			// If claim was not found, add empty object
			if (!Found) {
				json_array_append_new(FinalClaims, json_object());
			}

			// Free leftover json objects, set list of claim data
			json_decref(Claims);
			json_decref(ClaimsReturn);
			ClaimsReturn = FinalClaims;
		}
	}

	// If there are urls
	if (Urls != NULL) {
		// Get request data
		UrlsReturn = UrlRequest(Urls, false, Server);

		// If request returned an error, return it
		if (json_object_get(UrlsReturn, "error") != NULL) {
			// Free list of returned claims
			if (ClaimsReturn != NULL) {
				json_decref(ClaimsReturn);
			}
			return UrlsReturn;
		} else {
			size_t Index;
			json_t *Value, *FinalUrl = json_array();

			// Go through every url
			json_array_foreach(Urls, Index, Value) {
				// Get returned url
				const char *Url = json_string_value(Value);
				json_t *Add = json_object_get(UrlsReturn, Url);

				// Get error data, set default error
				json_t *Error = json_object_get(Add, "error");
				char *ErrorText;

				// If there was an error
				if (Error != NULL) {
					// If Error is not string get text sub part
					if (!json_is_string(Error)) {
						Error = json_object_get(Error, "text");
					}

					// Get error string and create json object
					ErrorText = (char *) json_string_value(Error);
					Add = json_string(ErrorText);
				} else {
					//If there was no error, make url data self sustainable
					json_incref(Add);
				}

				// Add url data or error to list of url data
				json_array_append_new(FinalUrl, Add);
			}

			// Free leftover json objects, set list of url data
			json_decref(Urls);
			json_decref(UrlsReturn);
			UrlsReturn = FinalUrl;
		}
	}

	// List for keeping final results of every querry
	json_t *FinalReturn = json_array();
	json_t *Returns[2] = { ClaimsReturn, UrlsReturn };

	// Go through every querry
	for (int Index = 0; Index < QuerryNumber; ++Index) {
		// Get claim or url data, depending on type
		int Type = JsonQuerries[Index];
		json_t *Item = json_array_get(Returns[Type], 0);

		// Add data to final list, remove it from original
		json_array_append(FinalReturn, Item);
		json_array_remove(Returns[Type], 0);
	}

	char *RepostArray[QuerryNumber];
	int Indices[QuerryNumber];

	// Get reposted claims
	int RepostNumber = UrlReposted(FinalReturn, RepostArray, Indices, true);

	if (RepostNumber != 0) {
		// Resolve every url
		json_t *Resolved = UrlGet(RepostArray, RepostNumber, Server);

		// If resolve did not succeed, finish
		if (json_object_get(Resolved, "error") == NULL) {
			// Replace repost data with resolved
			for (int Index = 0; Index < RepostNumber; ++Index) {
				json_t *Value = json_array_get(Resolved, 0);

				// Error message for missing claim
				if (json_object_size(Value) == 0) {
					char *Error =
						ErrorGet(-1, "Reposted claim could not be found.");
					json_object_set_new(Value, "error", json_string(Error));
					free(Error);
				}
				json_array_set(FinalReturn, Indices[Index], Value);
				json_array_remove(Resolved, 0);
			}
		}
		json_decref(Resolved);
	}

	// Free leftover json objects
	if (ClaimsReturn != NULL) {
		json_decref(ClaimsReturn);
	}
	if (UrlsReturn != NULL) {
		json_decref(UrlsReturn);
	}
	return FinalReturn;
}

void UrlThumbnail(json_t *Data, char *Server) {
	// This function is responsible for getting thumbnails of reposted claims
	// from original publication

	json_t *Items = json_object_get(Data, "items");

	int Length = json_array_size(Items);
	char *RepostArray[Length];
	int Indices[Length];

	// Get reposted claims
	int UrlNumber = UrlReposted(Items, RepostArray, Indices, false);

	if (UrlNumber != 0) {
		// Resolve every url
		json_t *Resolved = UrlGet(RepostArray, UrlNumber, Server);

		// If resolve did not succeed, finish
		if (json_object_get(Resolved, "error") != NULL) {
			return;
		}

		// Go through every resolved url
		size_t Index;
		json_t *Value;
		json_array_foreach(Resolved, Index, Value) {
			// Get url of thumbnail
			char *ThumbnailString =
				JsonStringObject(Value, UrlThumbnailPath1, NULL);
			if (ThumbnailString == NULL) {
				ThumbnailString =
					JsonStringObject(Value, UrlThumbnailPath2, NULL);
			}

			// If url of thumbnail is not empty
			if (ThumbnailString != NULL) {
				// Create thumbnail url object
				json_t *NewThumbnail = json_object();
				json_object_set_new(NewThumbnail, "url",
					json_string(ThumbnailString));

				// Get data of current url
				json_t *ValueObject = json_array_get(Items, Indices[Index]);
				ValueObject = json_object_get(ValueObject, "value");

				// Set thumbnail url
				json_object_set_new(ValueObject, "thumbnail", NewThumbnail);
			}
		}
		json_decref(Resolved);
	}
}

// Everything under this line is removable after full C conversion
static PyObject *UrlWebPython(PyObject *Py_UNUSED(Self), PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	PyObject *UnicodePointerUrl;
	if (!PyArg_ParseTuple(Args, "U", &UnicodePointerUrl)) {
		return NULL;
	}
	char *Url = (char *) PyUnicode_AsUTF8(UnicodePointerUrl);

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	struct UrlInstance **List = UrlWeb(Url);
	json_t *JsonList = json_array();

	for (int Index = 0; Index < UrlSize; ++Index) {
		json_t *JsonListItem = json_array();
		if (List[Index] == NULL) {
			break;
		}
		json_array_append_new(JsonListItem, json_string(List[Index]->Provider));
		json_array_append_new(JsonListItem, json_string(List[Index]->Url));
		json_array_append_new(JsonList, JsonListItem);
		free(List[Index]->Provider);
		free(List[Index]->Url);
		free(List[Index]);
	}
	free(List);
	char *JsonString = json_dumps(JsonList, 0);
	json_decref(JsonList);

	// Acquire lock
	PyEval_RestoreThread(Save);

	PyObject *PythonJsonString = PyUnicode_FromString(JsonString);
	free(JsonString);

	return PythonJsonString;
}

static PyObject *UrlGetPython(PyObject *Py_UNUSED(Self), PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	PyObject *UrlList, *UnicodePointerServer;
	if (!PyArg_ParseTuple(Args, "OU", &UrlList, &UnicodePointerServer)) {
		return NULL;
	}

	char *Server = (char *) PyUnicode_AsUTF8(UnicodePointerServer);

	int Length = PyList_Size(UrlList);
	char *Urls[Length];

	for (int Index = 0; Index < Length; ++Index) {
		Urls[Index] = (char *) PyUnicode_AsUTF8(PyList_GetItem(UrlList, Index));
	}

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	json_t *JsonList = UrlGet(Urls, Length, Server);
	char *JsonString = json_dumps(JsonList, 0);
	json_decref(JsonList);

	// Acquire lock
	PyEval_RestoreThread(Save);

	PyObject *PythonJsonString = PyUnicode_FromString(JsonString);
	free(JsonString);

	return PythonJsonString;
}

// Function names and other python data
static PyMethodDef UrlMethods[] = {
	{"Web", UrlWebPython, METH_VARARGS, "Web"},
	{"Get", UrlGetPython, METH_VARARGS, "Get"},
	{NULL, NULL, 0, NULL}
};

static struct PyModuleDef UrlModule = {
	PyModuleDef_HEAD_INIT, "Url", "Url module", -1, UrlMethods, 0, 0, 0, 0
};

// The function name has to end with the module name
PyMODINIT_FUNC PyInit_Url(void) {
	return PyModule_Create(&UrlModule);
}
