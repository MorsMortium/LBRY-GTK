/******************************************************************************\
* LBRY-GTK                                                                     *
* Copyright (C) 2021-2023 MorsMortium and Other Contributors                   *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU General Public License as published by         *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                *
* GNU General Public License for more details.                                 *
*                                                                              *
* You should have received a copy of the GNU General Public License            *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
\******************************************************************************/

// This file is responsible for filtering important parts of
// comments/publications and others for display

#include <jansson.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "Header/Error.h"
#include "Header/Json.h"
#include "Header/Filter.h"

// FallBack strings for missing values
char *FilterEmpty = "", *FilterClaimString = "claim", *FilterZero = "0";
char *FilterAction = "action";

// Keys for error data
char *FilterErrMes[] = { "error", "message", NULL };
char *FilterErrCod[] = { "error", "code", NULL };

// Keys for publication data
char *FilterPubTit1[] = { "value", "title", NULL };
char *FilterPubTit2[] = { "name", NULL };
char *FilterPubCla[] = { "claim_id", NULL };
char *FilterPubTim[] = { "timestamp", NULL };
char *FilterPubTyp1[] = { "value", "stream_type", NULL };
char *FilterPubTyp2[] = { "value_type", NULL };
char *FilterPubUrl1[] = { "permanent_url", NULL };
char *FilterPubUrl2[] = { "canonical_url", NULL };
char *FilterPubCha[] = { "signing_channel", NULL };
char *FilterPubThu[] = { "value", "thumbnail", "url", NULL };
char *FilterPubVal[] = { "meta", "effective_amount", NULL };
char *FilterPubNum[] = { "meta", "claims_in_channel", NULL };

char *FilterGetProfile(json_t *Channel, json_t *Item) {
	// This function is responsible for getting the profile of a channel, with a
	// fallback

	char *Profile = JsonStringObject(Channel, FilterPubThu, FilterEmpty);
	if (Profile == FilterEmpty &&
		json_object_get(Item, "signing_channel") == NULL) {
		Profile = JsonStringObject(Item, FilterPubThu, FilterEmpty);
	}

	return Profile;
}

double FilterGetValue(json_t *Channel, json_t *Item) {
	// This function is responsible for getting the value of a channel, or
	// publication, with a fallback

	char *ValueString = JsonStringObject(Channel, FilterPubVal, FilterZero);
	if (ValueString == FilterZero) {
		ValueString = JsonStringObject(Item, FilterPubVal, FilterZero);
	}

	return atof(ValueString);;
}

char *FilterGetType(json_t *Item) {
	// This function is responsible for getting the type of a publication, with
	// a fallback

	char *Type = JsonStringObject(Item, FilterPubTyp1, FilterClaimString);
	if (Type == FilterClaimString) {
		Type = JsonStringObject(Item, FilterPubTyp2, FilterClaimString);
	}

	return Type;
}

char *FilterGetName(json_t *Channel, json_t *Item) {
	// This function is responsible for getting the name of a channel,
	// with a fallback

	char *Name = JsonStringObject(Channel, FilterPubTit1, FilterEmpty);
	if (Name == FilterEmpty) {
		Name = JsonStringObject(Channel, FilterPubTit2, FilterEmpty);
		if (Name != FilterEmpty) {
			++Name;
		}
	}
	if (Name == FilterEmpty) {
		Name = JsonStringObject(Item, FilterPubTit1, FilterEmpty);
	}
	if (Name == FilterEmpty) {
		Name = JsonStringObject(Item, FilterPubTit2, FilterEmpty);
		if (Name != FilterEmpty) {
			++Name;
		}
	}

	return Name;
}

bool FilterNotification(json_t *Item, json_t *Notification) {
	// This function is responsible for parsing a single notification

	// Add everything already set
	json_object_update(Notification, Item);

	// Get resolved channel, publication
	json_t *Channel = json_object_get(Item, "Channel");
	json_t *Resolved = json_object_get(Item, "Resolved");

	char *Thumbnail = JsonStringObject(Resolved, FilterPubThu, FilterEmpty);
	double Value = FilterGetValue(Channel, Resolved);
	char *Type = FilterGetType(Resolved);
	char *Name = FilterGetName(Channel, Resolved);
	double Support = atof(JsonStringObject(Item, FilterPubVal, FilterZero));
	int Number = JsonIntObject(Channel, FilterPubNum, 0);
	if (Number == 0) {
		Number = JsonIntObject(Item, FilterPubNum, 0);
	}

	// Add everything to Notification json
	json_object_set_new(Notification, "Support", json_real(Support));
	json_object_set_new(Notification, "Value", json_real(Value));
	json_object_set_new(Notification, "Number", json_integer(Number));
	json_object_set_new(Notification, "Thumbnail", json_string(Thumbnail));
	json_object_set_new(Notification, "Name", json_string(Name));
	json_object_set_new(Notification, "Type", json_string(Type));
	json_object_set_new(Notification, "Confirmations", json_integer(0));

	// Remove resolved channel, publication
	json_object_del(Notification, "Channel");
	json_object_del(Notification, "Resolved");

	return true;
}

char *FilterGetChannel(json_t *Channel) {
	// This function is responsible for getting the url of a channel, with a
	// fallback

	char *ChannelUrl = JsonStringObject(Channel, FilterPubUrl2, FilterEmpty);
	if (ChannelUrl == FilterEmpty) {
		ChannelUrl = JsonStringObject(Channel, FilterPubTit2, FilterEmpty);
	} else {
		ChannelUrl += 7;
	}

	return ChannelUrl;
}

char *FilterGetUrl(json_t *Item) {
	// This function is responsible for getting the url of a publication, with a
	// fallback

	char *Url = JsonStringObject(Item, FilterPubUrl1, FilterEmpty);
	if (Url == FilterEmpty) {
		Url = JsonStringObject(Item, FilterPubUrl2, FilterEmpty);
	}

	return Url;
}

char *FilterGetTitle(json_t *Item) {
	// This function is responsible for getting the title of a publication, with
	// a fallback

	char *Title = JsonStringObject(Item, FilterPubTit1, FilterEmpty);
	if (Title == FilterEmpty) {
		Title = JsonStringObject(Item, FilterPubTit2, FilterEmpty);
	}

	return Title;
}

char *FilterGetCreator(json_t *Channel, json_t *Item) {
	// This function is responsible for getting the creator of a publication,
	// with a fallback

	char *Creator = JsonStringObject(Channel, FilterPubUrl2, FilterEmpty);
	if (Creator == FilterEmpty) {
		Creator = JsonStringObject(Item, FilterPubUrl2, FilterEmpty);
	}

	return Creator;
}

bool FilterPublication(json_t *Item, json_t *Publication) {
	// This function is responsible for parsing a single publication

	// Get thumbnail, claim, timestamp, type, url, title
	char *Thumbnail = JsonStringObject(Item, FilterPubThu, FilterEmpty);
	char *Claim = JsonStringObject(Item, FilterPubCla, FilterEmpty);
	int TimeStamp = JsonIntObject(Item, FilterPubTim, 0);
	char *Type = FilterGetType(Item);
	char *Url = FilterGetUrl(Item);
	char *Title = FilterGetTitle(Item);

	json_t *Channel = JsonObject(Item, FilterPubCha);

	// Get profile image, creator url, name
	char *Profile = FilterGetProfile(Channel, Item);
	char *Creator = FilterGetCreator(Channel, Item);
	char *Name = FilterGetName(Channel, Item);

	// Get channel value, publication support, channel publication number
	double Value = FilterGetValue(Channel, Item);
	double Support = atof(JsonStringObject(Item, FilterPubVal, FilterZero));
	int Number = JsonIntObject(Channel, FilterPubNum, 0);
	if (Number == 0) {
		Number = JsonIntObject(Item, FilterPubNum, 0);
	}

	// Add everything to Publication json
	json_object_set_new(Publication, "Confirmations", json_integer(0));
	json_object_set_new(Publication, "Title", json_string(Title));
	json_object_set_new(Publication, "TimeStamp", json_integer(TimeStamp));
	json_object_set_new(Publication, "Type", json_string(Type));
	json_object_set_new(Publication, "Url", json_string(Url));
	json_object_set_new(Publication, "Thumbnail", json_string(Thumbnail));
	json_object_set_new(Publication, "Claim", json_string(Claim));
	json_object_set_new(Publication, "Support", json_real(Support));
	json_object_set_new(Publication, "Value", json_real(Value));
	json_object_set_new(Publication, "Number", json_integer(Number));
	json_object_set_new(Publication, "Profile", json_string(Profile));
	json_object_set_new(Publication, "Creator", json_string(Creator));
	json_object_set_new(Publication, "Name", json_string(Name));
	json_object_set_new(Publication, "Action", json_string(FilterEmpty));

	return true;
}

// Keys for comment data
char *FilterComTex[] = { "comment", NULL };
char *FilterComSup[] = { "support_amount", NULL };
char *FilterComRep[] = { "replies", NULL };
char *FilterComId[] = { "comment_id", NULL };
char *FilterClaimId[] = { "claim_id", NULL };
char *FilterComUrl[] = { "channel_url", NULL };
char *FilterComRes[] = { "resolved", NULL };

bool FilterComment(json_t *Item, json_t *Comment) {
	// This function is responsible for parsing a single comment

	// Get text, support, replies, timestamp, url, id, thumbnail, channel lbc
	// amount, channel upload number
	char *Text = JsonStringObject(Item, FilterComTex, FilterEmpty);
	int Support = JsonIntObject(Item, FilterComSup, 0);
	int Replies = JsonIntObject(Item, FilterComRep, 0);
	int TimeStamp = JsonIntObject(Item, FilterPubTim, 0);
	char *Id = JsonStringObject(Item, FilterComId, FilterEmpty);
	char *ClaimId = JsonStringObject(Item, FilterClaimId, FilterEmpty);
	json_t *Resolved = JsonObject(Item, FilterComRes);
	char *Thumbnail = JsonStringObject(Resolved, FilterPubThu, FilterEmpty);
	char *Value = JsonStringObject(Resolved, FilterPubVal, FilterZero);
	int Number = JsonIntObject(Resolved, FilterPubNum, 0);
	char *Url = FilterGetCreator(Resolved, Resolved);
	int UrlLength = 1;

	// If url was not found, it could not be resolved, it is a deleted channel
	if (Url == FilterEmpty) {
		// Get original url and length for local editing
		Url = JsonStringObject(Item, FilterComUrl, FilterEmpty);
		UrlLength = strlen(Url) + 1;
	}

	// Create new url
	char NewUrl[UrlLength];
	if (UrlLength != 1) {
		// Write old into it, set local as url
		sprintf(NewUrl, "%s", Url);
		Url = NewUrl;

		// Remove everything after first character next to '#'
		Url[strlen(Url) - strlen(strchr(Url, '#')) + 2] = '\0';
	}

	char *Title = JsonStringObject(Resolved, FilterPubTit1, FilterEmpty);
	if (Title == FilterEmpty) {
		Title = JsonStringObject(Resolved, FilterPubTit2, FilterEmpty);
		if (Title != FilterEmpty) {
			++Title;
		}
	}

	// Add everything to Comment json
	json_object_set_new(Comment, "Replies", json_integer(Replies));
	json_object_set_new(Comment, "Support", json_integer(Support));
	json_object_set_new(Comment, "Text", json_string(Text));
	json_object_set_new(Comment, "Id", json_string(Id));
	json_object_set_new(Comment, "ClaimId", json_string(ClaimId));
	json_object_set_new(Comment, "Url", json_string(Url));
	json_object_set_new(Comment, "Thumbnail", json_string(Thumbnail));
	json_object_set_new(Comment, "TimeStamp", json_integer(TimeStamp));
	json_object_set_new(Comment, "Value", json_string(Value));
	json_object_set_new(Comment, "Number", json_integer(Number));
	json_object_set_new(Comment, "Title", json_string(Title));
	return true;
}

bool FilterChannel(json_t *Item, json_t *Channel) {
	// This function is responsible for parsing a single channel

	// Get name, url, thumbnail, title and claim
	char *Name = JsonStringObject(Item, FilterPubTit2, FilterEmpty);
	json_t *Resolved = JsonObject(Item, FilterComRes);
	char *Url = FilterGetCreator(Resolved, Resolved);
	char *Thumbnail = JsonStringObject(Item, FilterPubThu, FilterEmpty);
	char *Title = FilterGetTitle(Item);
	char *Claim = JsonStringObject(Item, FilterPubCla, FilterEmpty);

	// Add everything to Channel json
	json_object_set_new(Channel, "Name", json_string(Name));
	json_object_set_new(Channel, "Title", json_string(Title));
	json_object_set_new(Channel, "Url", json_string(Url));
	json_object_set_new(Channel, "Thumbnail", json_string(Thumbnail));
	json_object_set_new(Channel, "Claim", json_string(Claim));
	return true;
}

// Keys for balance data
char *FilterBalCon[] = { "confirmations", NULL };
char *FilterBalVal[] = { "value", NULL };
char *FilterBalInf[] = { "support", "update", "abandon", "claim", "purchase" };
char *FilterBalTit[] = { "claim_name", NULL };
char *FilterBalCla[] = { "claim", NULL };
char *FilterBalAmo[] = { "amount", NULL };
char *FilterBalTyp[] = { "value_type", NULL };

bool FilterBalance(json_t *Item, json_t *Balance) {
	// This function is responsible for parsing a single balance

	// Get confirmations, timestamp, amount
	int Confirmations = JsonIntObject(Item, FilterBalCon, 0);
	int TimeStamp = JsonIntObject(Item, FilterPubTim, -1);

	// Get transaction info
	json_t *Info;
	bool FoundInfo = false;
	double Support = 0;
	char *Action = FilterAction;
	for (int InfoLength = 0; InfoLength < 5; ++InfoLength) {
		// Go through info types

		//Get info object
		char InfoString[14];
		sprintf(InfoString, "%s_info", FilterBalInf[InfoLength]);
		char *BalInf[] = {InfoString, NULL};
		Info = JsonObject(Item, BalInf);

		if (json_array_size(Info) != 0) {
			// If info object array has items

			// Set type
			Action = FilterBalInf[InfoLength];

			// Get first item in info object array, set as info object
			Info = json_array_get(Info, 0);
			FoundInfo = true;

			// Get balance change, if it exists
			json_t *Delta = json_object_get(Info, "balance_delta");
			if (Delta != NULL) {
				sscanf(json_string_value(Delta), "%lf", &Support);
			}

			// If info object is the first type, check if transaction is a tip
			if (InfoLength == 0) {
				if (json_boolean_value(json_object_get(Info, "is_tip"))) {
					Action = "tip";
				} else {
					Action = "boost";
				}
			}
			break;
		}
	}
	if (Support == 0) {
		sscanf(JsonStringObject(Item, FilterBalVal,
			FilterEmpty), "%lf", &Support);
	}

	char *Title = FilterEmpty, *Claim = FilterEmpty, *Url = FilterEmpty;
	char *Creator = FilterEmpty, *Profile = FilterEmpty, *Name = FilterEmpty;
	char *Thumbnail = FilterEmpty, *Type = FilterClaimString;
	double Value = 0;
	int Number = 0;
	if (FoundInfo) {
		// If info is found

		// Get claim and channel object
		json_t *ClaimObject = JsonObject(Item, FilterBalCla);
		json_t *Channel = JsonObject(ClaimObject, FilterPubCha);

		// Get title, claim, thumbnail, type, url, channelurl
		Title = JsonStringObject(Info, FilterBalTit, FilterEmpty);
		Claim = JsonStringObject(Info, FilterPubCla, FilterEmpty);
		Thumbnail = JsonStringObject(ClaimObject, FilterPubThu, FilterEmpty);
		Type = JsonStringObject(ClaimObject, FilterBalTyp, FilterClaimString);
		Url = FilterGetUrl(ClaimObject);

		// Get channel value, publication support, channel publication number
		Value = FilterGetValue(Channel, ClaimObject);
		Number = JsonIntObject(Channel, FilterPubNum, 0);
		if (Number == 0) {
			Number = JsonIntObject(ClaimObject, FilterPubNum, 0);
		}

		// Get profile image, creator url, name
		Profile = FilterGetProfile(Channel, ClaimObject);
		Creator = FilterGetCreator(Channel, ClaimObject);
		Name = FilterGetName(Channel, ClaimObject);
	}

	// Add everything to Balance json
	json_object_set_new(Balance, "Confirmations", json_integer(Confirmations));
	json_object_set_new(Balance, "Title", json_string(Title));
	json_object_set_new(Balance, "TimeStamp", json_integer(TimeStamp));
	json_object_set_new(Balance, "Type", json_string(Type));
	json_object_set_new(Balance, "Url", json_string(Url));
	json_object_set_new(Balance, "Thumbnail", json_string(Thumbnail));
	json_object_set_new(Balance, "Claim", json_string(Claim));
	json_object_set_new(Balance, "Support", json_real(Support));
	json_object_set_new(Balance, "Value", json_real(Value));
	json_object_set_new(Balance, "Number", json_integer(Number));
	json_object_set_new(Balance, "Profile", json_string(Profile));
	json_object_set_new(Balance, "Creator", json_string(Creator));
	json_object_set_new(Balance, "Name", json_string(Name));
	json_object_set_new(Balance, "Action", json_string(Action));

	return true;
}

// Keys for file data
char *FilterFilTit[] = { "metadata", "title", NULL };
char *FilterFilTyp[] = { "metadata", "stream_type", NULL };
char *FilterFilThu[] = { "metadata", "thumbnail", "url", NULL };
char *FilterFilTim[] = { "added_on", NULL };

bool FilterFile(json_t *Item, json_t *File) {
	// This function is responsible for parsing a single file

	// Get claim name
	char *ClaimName = JsonStringObject(Item, FilterBalTit, FilterEmpty);

	// Get title with fallback
	char *Title = JsonStringObject(Item, FilterFilTit, FilterEmpty);
	if (Title == FilterEmpty) {
		Title = ClaimName;
	}

	json_t *Resolved = JsonObject(Item, FilterComRes);
	json_t *Channel = JsonObject(Resolved, FilterPubCha);

	// Get channel, type, thumbnail, timestamp, claim
	char *ChannelUrl = FilterGetChannel(Channel);
	char *Type = JsonStringObject(Item, FilterFilTyp, FilterClaimString);
	char *Thumbnail = JsonStringObject(Item, FilterFilThu, FilterEmpty);
	int TimeStamp = JsonIntObject(Item, FilterFilTim, -1);
	char *Claim = JsonStringObject(Item, FilterPubCla, FilterEmpty);

	// Get publication support, channel value, channel publication number,
	// channel thumbnail url, channel url, channel name
	double Support = atof(JsonStringObject(Resolved, FilterPubVal, FilterZero));
	double Value = FilterGetValue(Channel, Resolved);
	int Number = JsonIntObject(Resolved, FilterPubNum, 0);
	char *Profile = FilterGetProfile(Channel, Resolved);
	char *Creator = FilterGetCreator(Channel, Resolved);
	char *Name = FilterGetName(Channel, Resolved);

	// Get url from claim name and claim id
	char Url[strlen(ClaimName) + strlen(Claim) + 2];
	sprintf(Url, "%s#%s", ClaimName, Claim);

	// Add everything to File json
	json_object_set_new(File, "Confirmations", json_integer(0));
	json_object_set_new(File, "ChannelUrl", json_string(ChannelUrl));
	json_object_set_new(File, "Title", json_string(Title));
	json_object_set_new(File, "TimeStamp", json_integer(TimeStamp));
	json_object_set_new(File, "Type", json_string(Type));
	json_object_set_new(File, "Url", json_string(Url));
	json_object_set_new(File, "Thumbnail", json_string(Thumbnail));
	json_object_set_new(File, "Claim", json_string(Claim));
	json_object_set_new(File, "Support", json_real(Support));
	json_object_set_new(File, "Value", json_real(Value));
	json_object_set_new(File, "Number", json_integer(Number));
	json_object_set_new(File, "Profile", json_string(Profile));
	json_object_set_new(File, "Creator", json_string(Creator));
	json_object_set_new(File, "Name", json_string(Name));
	json_object_set_new(File, "Action", json_string(FilterEmpty));

	return true;
}

json_t *FilterErrors(json_t *Data) {
	// This function is responsible for getting a formatted error from a json
	// object

	// Get error message, code
	char *Message = JsonStringObject(Data, FilterErrMes, FilterEmpty);
	int Code = JsonIntObject(Data, FilterErrCod, -1);

	// Create error string
	char *ErrorString;
	if (Code == -1 && Message == FilterEmpty) {
		// No error found
		ErrorString = ErrorGet(-1, "Unknown Error.");
	} else {
		ErrorString = ErrorGet(Code, Message);
	}

	// Create error json, add string to it
	json_t *ErrorObject = json_object();
	json_object_set_new(ErrorObject, "Error", json_string(ErrorString));

	// Free error string
	free(ErrorString);

	// Delete original data, return error json object
	json_decref(Data);
	return ErrorObject;
}

json_t *FilterParse(json_t *Data, bool (*Parser)(json_t *,
	json_t *), bool GetItems) {
	// This function is responsible for parsing any kinds of data

	// If there are no items, return error
	if (Data == NULL) {
		return FilterErrors(Data);
	}

	// Get actual list of items into Items
	json_t *Items;
	if (GetItems) {
		Items = json_object_get(Data, "items");
	} else {
		Items = Data;
	}

	// If there are no items, return error
	if (Items == NULL) {
		return FilterErrors(Data);
	}

	// Create list of parsed items
	json_t *Parsed = json_array();

	// If Items is an array
	if (json_is_array(Items)) {
		size_t Index;
		json_t *Item;

		// Loop through every item
		json_array_foreach(Items, Index, Item) {
			// Create parsed array
			json_t *Value = json_object();

			// Parser returns true on success
			if (Parser(Item, Value)) {
				// Add to parsed items
				json_array_append_new(Parsed, Value);
			} else {
				// Delete
				json_decref(Value);
			}
		}
	}

	// Delete original data, return parsed data
	json_decref(Data);
	return Parsed;
}

// These functions are shortcuts to the generic Parse with the specific parser
// They might not be needed
json_t *FilterNotifications(json_t *Data) {
	return FilterParse(Data, FilterNotification, false);
}

json_t *FilterPublications(json_t *Data) {
	return FilterParse(Data, FilterPublication, true);
}

json_t *FilterComments(json_t *Data) {
	return FilterParse(Data, FilterComment, true);
}

json_t *FilterChannels(json_t *Data) {
	return FilterParse(Data, FilterChannel, true);
}

json_t *FilterBalances(json_t *Data) {
	return FilterParse(Data, FilterBalance, true);
}

json_t *FilterFiles(json_t *Data) {
	return FilterParse(Data, FilterFile, true);
}
