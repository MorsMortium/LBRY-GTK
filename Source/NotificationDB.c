/******************************************************************************\
* LBRY-GTK                                                                     *
* Copyright (C) 2021-2022 MorsMortium and Other Contributors                   *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU General Public License as published by         *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                *
* GNU General Public License for more details.                                 *
*                                                                              *
* You should have received a copy of the GNU General Public License            *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
\******************************************************************************/

// Here we make changes/query database for notifications

#include <Python.h>
#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <jansson.h>
#include <sqlite3.h>

#include "Header/Comments.h"
#include "Header/Content.h"
#include "Header/Places.h"
#include "Header/Filter.h"
#include "Header/Url.h"
#include "Header/Global.h"
#include "Header/ConnectDB.h"
#include "Header/CommentDB.h"
#include "Header/NotificationDB.h"

// This is used for checking if we are busy
bool NotificationDBBusy = false;

int NotificationDBSave(sqlite3 *DB, char *ImageUrl, char *Title, char *Channel,
	char *ContentType, char *Url, char *Claim, unsigned int Timestamp) {
	// Making necessary insertions into notifications

	// Statement holder
	sqlite3_stmt *Stmt;

	// Using prepare instead of exec, see next comment
	char *Query = "INSERT INTO Notifications(ImageUrl, Title, Channel, "
		"ContentType, Url, Claim, Timestamp) VALUES(?1, ?2, ?3, ?4, ?5,"
		" ?6, ?7)";
	int RC = sqlite3_prepare_v2(DB, Query, -1, &Stmt, 0);
	if (RC != SQLITE_OK) {
		fprintf(stderr, "Statement wrong (%d): %s\n", RC,
			sqlite3_errmsg(DB));
		sqlite3_finalize(Stmt);
		return RC;
	}

	// Since we are using prepare we need to bind text/int to placeholdes (?)
	sqlite3_bind_text(Stmt, 1, ImageUrl, strlen(ImageUrl), SQLITE_STATIC);
	sqlite3_bind_text(Stmt, 2, Title, strlen(Title), SQLITE_STATIC);
	sqlite3_bind_text(Stmt, 3, Channel, strlen(Channel), SQLITE_STATIC);
	sqlite3_bind_text(Stmt, 4, ContentType, strlen(ContentType), SQLITE_STATIC);
	sqlite3_bind_text(Stmt, 5, Url, strlen(Url), SQLITE_STATIC);
	sqlite3_bind_text(Stmt, 6, Claim, strlen(Claim), SQLITE_STATIC);
	sqlite3_bind_int(Stmt, 7, Timestamp);
	RC = sqlite3_step(Stmt);

	// Destroy Stmt
	sqlite3_finalize(Stmt);

	if (RC != SQLITE_DONE) {
		fprintf(stderr, "Couldn't save to DB (%d): %s\n", RC,
			sqlite3_errmsg(DB));
		return RC;
	}

	// This sets the NotifyFlag for notification checking
	Query = "REPLACE INTO SettingTable (SettingName, SettingValue) "
		"VALUES ('NotifyFlag', 1)";
	RC = sqlite3_exec(DB, Query, 0, 0, NULL);

	return RC;
}

void NotificationDBFinish(char *ImageUrl, char *Title, char *Channel,
	char *ContentType, char *Url, char *Claim, unsigned int Timestamp) {
	// Finishes the checking of content

	// Lock for operation/wait for lock
	pthread_mutex_lock(&ConnectDBLock);

	// Make new connection to DB
	sqlite3 *DB = ConnectDBOpen();
	if (DB == NULL) {
		pthread_mutex_unlock(&ConnectDBLock);
		return;
	}

	// Statement holder
	sqlite3_stmt *Stmt;

	// Queries the database for past Notifications
	char *Query = "SELECT rowid FROM Notifications WHERE Channel=?1 "
		"AND ContentType=?2 AND Timestamp=?3";
	int RC = sqlite3_prepare_v2(DB, Query, -1, &Stmt, 0);

	if (RC != SQLITE_OK) {
		fprintf(stderr, "Statement wrong (%d): %s\n", RC, sqlite3_errmsg(DB));
		sqlite3_finalize(Stmt);
		sqlite3_close_v2(DB);
		pthread_mutex_unlock(&ConnectDBLock);
		return;
	}

	// Since we are using prepare we need to bind text/int
	sqlite3_bind_text(Stmt, 1, Channel, strlen(Channel), SQLITE_TRANSIENT);
	sqlite3_bind_text(Stmt, 2, ContentType, strlen(ContentType),
		SQLITE_TRANSIENT);
	sqlite3_bind_int(Stmt, 3, Timestamp);

	// Step to find if there's a row
	RC = sqlite3_step(Stmt);

	// Destroy Stmt
	sqlite3_finalize(Stmt);

	// If found, exit
	if (RC == SQLITE_ROW) {
		sqlite3_close_v2(DB);
		pthread_mutex_unlock(&ConnectDBLock);
		return;
	}

	// Saving done here if Response is TRUE
	RC = NotificationDBSave(DB, ImageUrl, Title, Channel, ContentType, Url,
			Claim, Timestamp);

	// Finally actually close the current connection
	sqlite3_close_v2(DB);

	// Unlock
	pthread_mutex_unlock(&ConnectDBLock);
}

static void *NotificationDBChannelsThread(void *Args) {
	// Make search for latest publication of a channel

	// Get the given arguments into a struct
	ConnectDBArgs *CInfo = Args;

	// Create a parameters block
	json_t *Params = json_object();
	json_t *OrderBy = json_array();
	json_array_append_new(OrderBy, json_string("creation_height"));
	json_object_set_new(Params, "channel", json_string(CInfo->ChannelUrl));
	json_object_set_new(Params, "order_by", OrderBy);

	// Get the newest publication
	json_t *Result = ContentSearch(Params, 1, 1, CInfo->Server);
	json_decref(Params);

	// TODO: thread locks
	if (json_object_get(Result, "error") != NULL || ConnectDBNoDB) {
		json_decref(Result);
		return NULL;
	}

	// Get the item
	json_t *Item = json_array_get(Result, 0);

	// Get values to variables
	char *ImageUrl =
		(char *) json_string_value(json_object_get(Item, "Thumbnail"));
	char *Title = (char *) json_string_value(json_object_get(Item, "Title"));
	char *Channel =
		(char *) json_string_value(json_object_get(Item, "Creator"));
	char *ContentType = "new_content";
	char *Url = (char *) json_string_value(json_object_get(Item, "Url"));
	char *Claim = (char *) json_string_value(json_object_get(Item, "Claim"));
	unsigned int Timestamp =
		json_integer_value(json_object_get(Item, "TimeStamp"));

	// We finish checking content here and possibly save
	NotificationDBFinish(ImageUrl, Title, Channel, ContentType, Url, Claim,
		Timestamp);

	// Forget the object
	json_decref(Result);

	return NULL;
}

void *NotificationDBChannels(void) {
	// Checks each notified channel if those have content

	// Get the path to default path
	char DefaultPath[strlen(PlacesJson) + 21];
	sprintf(DefaultPath, "%sDefaultSettings.json", PlacesJson);

	// Get preferences
	json_t *Following = json_object_get(GlobalPreferences, "Following");
	char *Server =
		(char *) json_string_value(json_object_get(json_object_get(
				GlobalPreferences,
				"Session"), "Server"));

	// Some values to aid with checking of notified channels
	int ChannelsAmount = 0;
	char *Channels[json_array_size(Following) + 1];

	// Check for notified channels
	size_t Index;
	json_t *Value, *Notified, *Uri;
	json_array_foreach(Following, Index, Value) {
		// Get needed values
		Notified = json_object_get(Value, "notificationsDisabled");
		Uri = json_object_get(Value, "uri");

		// Check the value is real
		if (json_is_true(Notified)) {
			continue;
		}

		// Add the uri to channels array if notified
		Channels[ChannelsAmount++] = (char *) json_string_value(Uri);
	}

	// Add null to end for looping
	Channels[ChannelsAmount] = NULL;

	// Allocate memory for channel thread variables
	ConnectDBArgs *CInfo = calloc(ChannelsAmount, sizeof(*CInfo));
	if (CInfo == NULL) {
		return NULL;
	}

	// Here we create threads for each channel
	int RC;
	int ChannelNumber = 0;
	while (Channels[ChannelNumber] != NULL) {
		// All data to struct
		CInfo->Server = Server;
		CInfo[ChannelNumber].ChannelUrl = Channels[ChannelNumber];

		// Actual thread creation
		RC = pthread_create(&CInfo[ChannelNumber].Thread, NULL,
				&NotificationDBChannelsThread, &CInfo[ChannelNumber]);

		// Increment by one
		ChannelNumber++;

		// Check the thread actually got created
		if (RC != 0) {
			fprintf(stderr, "Could not create thread\n");
		}
	}

	// Wait for threads to do their work
	ConnectDBJoin(ChannelNumber, CInfo);

	// Free allocated memory
	free(CInfo);

	return NULL;
}

static void *NotificationDBCommentsThread(void *Args) {
	// Checks each comment for new replies

	// Get the given arguments into a struct
	ConnectDBArgs *CInfo = Args;

	// Lock for operation/wait for lock
	pthread_mutex_lock(&ConnectDBLock);

	// Make a connection to DB
	sqlite3 *DB = ConnectDBOpen();
	if (DB == NULL) {
		pthread_mutex_unlock(&ConnectDBLock);
		return NULL;
	}

	// Sqlite statement
	sqlite3_stmt *Stmt;

	// Get rows between the two timestamps
	char *Query = "SELECT rowid, ClaimID, CommentID, ChannelID "
		"FROM Comments WHERE Timestamp < ?1 AND Timestamp > ?2";
	sqlite3_prepare_v2(DB, Query, -1, &Stmt, 0);

	// Bind values to placeholder (?)
	sqlite3_bind_int(Stmt, 1, CInfo->Start);
	sqlite3_bind_int(Stmt, 2, CInfo->End);

	// Loop over each row in DB and save results to array
	struct NotificationDBRows Rows[5];
	int RowNumber = 0;
	while (sqlite3_step(Stmt) == SQLITE_ROW) {
		// Take different items used in comparing

		// Allocate memory for each needed string
		char *ClaimID = (char *) malloc(
			strlen((char *) sqlite3_column_text(Stmt, 1)) + 1);
		if (ClaimID == NULL) {
			continue;
		}
		char *ParentID = (char *) malloc(
			strlen((char *) sqlite3_column_text(Stmt, 2)) + 1);
		if (ParentID == NULL) {
			free(ClaimID);
			continue;
		}
		char *UserID = (char *) malloc(
			strlen((char *) sqlite3_column_text(Stmt, 3)) + 1);
		if (UserID == NULL) {
			free(ClaimID);
			free(ParentID);
			continue;
		}

		// Add everything to the newly allocated space
		strcpy(ClaimID, (char *) sqlite3_column_text(Stmt, 1));
		strcpy(ParentID, (char *) sqlite3_column_text(Stmt, 2));
		strcpy(UserID, (char *) sqlite3_column_text(Stmt, 3));

		// Now add them to struct
		struct NotificationDBRows Row = {
			.ClaimID = ClaimID,
			.ParentID = ParentID,
			.UserID = UserID
		};

		// Finally add to the created array
		Rows[RowNumber] = Row;

		// Increment only now
		RowNumber++;
	}

	// Destroy statement and close DB
	sqlite3_finalize(Stmt);
	sqlite3_close_v2(DB);

	// Unlock
	pthread_mutex_unlock(&ConnectDBLock);

	// Now after closing connection to DB we can loop over each row
	int AllRows = RowNumber;
	RowNumber = 0;
	while (RowNumber < AllRows) {
		// List all comments
		json_t *Value = CommentsList("", "", Rows[RowNumber].ParentID, "",
				Rows[RowNumber].ClaimID, CInfo->CommentServer, 3, 10, 1,
				CInfo->Server);

		// Erroring out means connection problems so we end here
		// Notification stop happens here if we closed the client
		// TODO: thread locks
		if (json_object_get(Value, "error") != NULL || ConnectDBNoDB) {
			json_decref(Value);
			break;
		}

		// First json value needs to be an array
		if (json_is_array(json_array_get(Value, 0)) == 0) {
			json_decref(Value);
			RowNumber++;
			continue;
		}

		// Create array for ClaimID
		json_t *ClaimIDs = json_array();
		json_array_append_new(ClaimIDs, json_string(Rows[RowNumber].ClaimID));

		// Create params object and add array to it
		json_t *Params = json_object();
		json_object_set_new(Params, "claim_ids", ClaimIDs);

		// Get the result of content search
		json_t *Result = ContentSearch(Params, 5, 1, CInfo->Server);
		json_decref(Params);
		if (json_object_get(Result, "error") != NULL) {
			json_decref(Value);
			json_decref(Result);
			break;
		}

		// Values that are always the same
		json_t *First = json_array_get(Result, 0);
		char *Title =
			(char *) json_string_value(json_object_get(First, "Title"));
		char *Url = (char *) json_string_value(json_object_get(First, "Url"));
		char *Claim =
			(char *) json_string_value(json_object_get(First, "Claim"));

		// Save the items to notifications
		size_t Index;
		json_t *Item;
		json_array_foreach(json_array_get(Value, 0), Index, Item) {
			// Compare IDs that they are not the same
			char *CompareID = (char *)
				json_string_value(json_object_get(Item, "Url"));

			CompareID = strstr(CompareID, "#");
			if (strcmp(CompareID + 1, Rows[RowNumber].UserID) == 0) {
				continue;
			}

			// Get values to variables
			char *ImageUrl =
				(char *) json_string_value(json_object_get(Item, "Thumbnail"));
			char *Channel =
				(char *) json_string_value(json_object_get(Item, "Url"));
			char *ContentType = "comment_reply";
			unsigned int Timestamp =
				json_integer_value(json_object_get(Item, "TimeStamp"));

			// We finish checking content here and possibly save
			NotificationDBFinish(ImageUrl, Title, Channel, ContentType, Url,
				Claim, Timestamp);
		}

		// Forget those json blocks
		json_decref(Value);
		json_decref(Result);

		// Finally increment row number
		RowNumber++;
	}

	// Now free all of allocated memory
	RowNumber = 0;
	while (RowNumber < AllRows) {
		free(Rows[RowNumber].ClaimID);
		free(Rows[RowNumber].ParentID);
		free(Rows[RowNumber].UserID);
		RowNumber++;
	}

	return NULL;
}

void *NotificationDBComments(void) {
	// Check commments for replies

	// Get the path to default path
	char DefaultPath[strlen(PlacesJson) + 21];
	sprintf(DefaultPath, "%sDefaultSettings.json", PlacesJson);

	// Stop execution if force stop used
	// TODO: thread locks
	if (ConnectDBNoDB) {
		return NULL;
	}

	// Get preferences
	char *Server =
		(char *) json_string_value(json_object_get(json_object_get(
				GlobalPreferences,
				"Session"), "Server"));
	char *CommentServer =
		(char *) json_string_value(json_object_get(GlobalPreferences,
			"CommentServer"));

	// Make a connection to DB
	sqlite3 *DB = ConnectDBOpen();
	if (DB == NULL) {
		return NULL;
	}

	sqlite3_stmt *Stmt;

	// This fetches first 20 rows
	char *Query =
		"SELECT Timestamp FROM Comments ORDER BY Timestamp DESC LIMIT 20";
	int RC = sqlite3_prepare_v2(DB, Query, -1, &Stmt, 0);
	if (RC != SQLITE_OK) {
		fprintf(stderr, "Couldn't prepare statement (%d): %s\n", RC,
			sqlite3_errmsg(DB));
		sqlite3_finalize(Stmt);
		sqlite3_close_v2(DB);
		return NULL;
	}

	// Getting the Timestamps in those rows
	int RowNumber = 0;
	unsigned int Timestamps[101];
	while (sqlite3_step(Stmt) == SQLITE_ROW) {
		Timestamps[RowNumber++] = sqlite3_column_int(Stmt, 0);
	}
	sqlite3_reset(Stmt);

	// Empty table exit
	if (RowNumber == 0) {
		sqlite3_finalize(Stmt);
		sqlite3_close_v2(DB);
		return NULL;
	}

	// Get last fetched row's timestamp
	Query = "SELECT SettingValue FROM SettingTable WHERE "
		"SettingName='LastTimestamp'";
	RC = sqlite3_prepare_v2(DB, Query, -1, &Stmt, 0);
	if (RC != SQLITE_OK) {
		fprintf(stderr, "Couldn't prepare statement (%d): %s\n", RC,
			sqlite3_errmsg(DB));
		sqlite3_finalize(Stmt);
		sqlite3_close_v2(DB);
		return NULL;
	}

	// If no Timestamp: we continue, else we add to timestamps
	unsigned int LastTimestamp;
	RC = sqlite3_step(Stmt);
	if (RC != SQLITE_ROW) {
		// This sets the last timestamp for the first time
		LastTimestamp = Timestamps[RowNumber - 1];
	} else {
		LastTimestamp = sqlite3_column_int(Stmt, 0);
		sqlite3_clear_bindings(Stmt);
		sqlite3_reset(Stmt);

		// Get 100 older entries for checking
		Query = "SELECT Timestamp FROM Comments WHERE "
			"Timestamp < ?1 ORDER BY Timestamp DESC LIMIT 80";

		sqlite3_prepare_v2(DB, Query, -1, &Stmt, 0);

		// Bind value to placeholder (?)
		sqlite3_bind_int(Stmt, 1, LastTimestamp);

		// Add the Timestamps to the array for further checking
		while (sqlite3_step(Stmt) == SQLITE_ROW) {
			Timestamps[RowNumber] = sqlite3_column_int(Stmt, 0);
			RowNumber++;
		}

		// This finally gets the last timestamp
		LastTimestamp = Timestamps[RowNumber - 1];

		// Clear the bindings since we are still using Stmt
		sqlite3_clear_bindings(Stmt);
	}
	sqlite3_reset(Stmt);

	// Add 0 to the end of array to signal last item
	Timestamps[RowNumber] = 0;

	// Lock for operation/wait for lock
	pthread_mutex_lock(&ConnectDBLock);

	// Add the new last timestamp to table
	Query = "REPLACE INTO SettingTable (SettingName, SettingValue) "
		"VALUES ('LastTimestamp', ?1)";
	RC = sqlite3_prepare_v2(DB, Query, -1, &Stmt, 0);

	// Bind value to placeholder (?)
	sqlite3_bind_int(Stmt, 1, LastTimestamp);

	RC = sqlite3_step(Stmt);
	if (RC != SQLITE_DONE) {
		fprintf(stderr, "Statement wrong (%d): %s\n", RC,
			sqlite3_errmsg(DB));
	}

	// Unlock
	pthread_mutex_unlock(&ConnectDBLock);

	// Finally destroy the statement
	sqlite3_finalize(Stmt);

	// Close DB
	sqlite3_close_v2(DB);

	// Allocate memory for comment thread variables
	const int Divider = 5;
	ConnectDBArgs *CInfo = calloc((RowNumber / Divider) + 1, sizeof(*CInfo));

	// Here we create threads for all fetched comments
	int CommentNumber = 0;
	int ThreadNumber = 0;
	while (CommentNumber < RowNumber) {
		// Take the next 10 comments to check (or less)
		unsigned int Start = Timestamps[CommentNumber];
		unsigned int End =
			Timestamps[((RowNumber - CommentNumber <
				Divider) ? (RowNumber - 1) : (CommentNumber + Divider - 1))];

		// Increment by the amount of divider
		CommentNumber = CommentNumber + Divider;

		// If for whatever reason the end is larger than start, we drop
		if (Start < End) {
			continue;
		}

		// All data to struct
		CInfo[ThreadNumber].Server = Server;
		CInfo[ThreadNumber].CommentServer = CommentServer;
		CInfo[ThreadNumber].Start = Start + 1;
		CInfo[ThreadNumber].End = End - 1;

		// Actual thread creation
		RC = pthread_create(&CInfo[ThreadNumber].Thread, NULL,
				&NotificationDBCommentsThread, &CInfo[ThreadNumber]);

		// Check the thread actually got created
		if (RC != 0) {
			fprintf(stderr, "Could not create thread\n");
			continue;
		}

		// Increment by one
		ThreadNumber++;
	}

	// Wait for threads to do their work
	ConnectDBJoin(ThreadNumber, CInfo);

	// Free allocated memory
	free(CInfo);

	return NULL;
}

json_t *NotificationDBList(char *UNUSED(
		ContentType), int PageSize, int Page, char *Server) {
	// List the notifications in the DB

	// Make a connection to DB
	sqlite3 *DB = ConnectDBOpen();
	if (DB == NULL) {
		return NULL;
	}

	// Statement holder
	sqlite3_stmt *Stmt;

	// The query
	char *Query = "SELECT rowid, Channel, Title, Timestamp, ContentType,"
		" Url, ImageUrl, Claim FROM Notifications ORDER BY Timestamp "
		" DESC LIMIT ?1 OFFSET ?2";

	// Prepare our statement
	int RC = sqlite3_prepare_v2(DB, Query, -1, &Stmt, 0);
	if (RC != SQLITE_OK) {
		fprintf(stderr, "Statement wrong (%d): %s\n", RC,
			sqlite3_errmsg(DB));
		sqlite3_finalize(Stmt);
		sqlite3_close_v2(DB);
		return NULL;
	}

	// Since we are using prepare we need to bind values to placeholders (?)
	sqlite3_bind_int(Stmt, 1, PageSize);
	sqlite3_bind_int(Stmt, 2, ((Page - 1) * PageSize));

	// Create json array holding our items
	json_t *Items = json_array();

	// Keeper for publication/channel urls
	char *Urls[PageSize];
	char *Channels[PageSize];
	size_t Index = 0;

	// Fetch each row into a json block
	while (sqlite3_step(Stmt) == SQLITE_ROW) {
		// New object to hold item
		json_t *Item = json_object();

		// Place values into an object
		json_object_set_new(Item, "Creator", json_string(
				(char *) sqlite3_column_text(Stmt, 1)));
		json_object_set_new(Item, "Title", json_string(
				(char *) sqlite3_column_text(Stmt, 2)));
		json_object_set_new(Item, "TimeStamp", json_integer(
				sqlite3_column_int(Stmt, 3)));
		json_object_set_new(Item, "Action", json_string(
				(char *) sqlite3_column_text(Stmt, 4)));
		json_object_set_new(Item, "Url", json_string(
				(char *) sqlite3_column_text(Stmt, 5)));
		json_object_set_new(Item, "Profile", json_string(
				(char *) sqlite3_column_text(Stmt, 6)));
		json_object_set_new(Item, "Claim", json_string(
				(char *) sqlite3_column_text(Stmt, 7)));

		// Get each channel and publication url, for resolving
		Channels[Index] =
			(char *) json_string_value(json_object_get(Item, "Creator"));
		Urls[Index] = (char *) json_string_value(json_object_get(Item, "Url"));
		++Index;

		// Append the newly made item into the items array
		json_array_append_new(Items, Item);
	}

	// Resolve publications, channels, add them to items
	json_t *Value, *Publications = UrlGet(Urls, Index, Server);
	json_t *ChannelObjects = UrlGet(Channels, Index, Server);
	json_array_foreach(Items, Index, Value) {
		json_object_set(Value, "Resolved", json_array_get(Publications, Index));
		json_object_set(Value, "Channel",
			json_array_get(ChannelObjects, Index));
	}
	json_decref(Publications);
	json_decref(ChannelObjects);

	// Destroy Stmt
	sqlite3_finalize(Stmt);

	// This resets the NotifyFlag untill next
	// time we find new notifications

	// Lock for operation/wait for lock
	pthread_mutex_lock(&ConnectDBLock);

	Query = "REPLACE INTO SettingTable (SettingName, SettingValue) "
		"VALUES ('NotifyFlag', 0)";
	RC = sqlite3_exec(DB, Query, 0, 0, NULL);

	// Close DB
	sqlite3_close_v2(DB);

	// Unlock
	pthread_mutex_unlock(&ConnectDBLock);

	// Return the json array
	return FilterNotifications(Items);
}

int NotificationDBCheck(void) {
	// Check if new notifications were made

	// Make a connection to DB
	sqlite3 *DB = ConnectDBOpen();
	if (DB == NULL) {
		return -1;
	}

	// Statement holder
	sqlite3_stmt *Stmt;

	// Create query
	char *Query = "SELECT SettingValue FROM SettingTable WHERE"
		" SettingName='NotifyFlag'";

	// Execute query
	int RC = sqlite3_prepare_v2(DB, Query, -1, &Stmt, 0);
	if (RC != SQLITE_OK) {
		fprintf(stderr, "Statement wrong (%d): %s\n", RC,
			sqlite3_errmsg(DB));
		sqlite3_finalize(Stmt);
		sqlite3_close_v2(DB);
		return -1;
	}
	sqlite3_step(Stmt);

	// Check if new notifications present
	int Notify = sqlite3_column_int(Stmt, 0);

	// Destroy Stmt
	sqlite3_finalize(Stmt);

	// Close DB
	sqlite3_close_v2(DB);

	return Notify;
}

// Everything under this line can be removed after not used in python code
static PyObject *NotificationDBBrewPython(PyObject *Py_UNUSED(
		Self), PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Get arguments and pass it to the pythonless function
	int MaxRecords;
	char *CommentServer, *Server;
	if (!PyArg_ParseTuple(Args, "iss", &MaxRecords, &CommentServer, &Server)) {
		return NULL;
	}

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	// Imports comments
	CommentDBImport(MaxRecords, CommentServer, Server);

	// Check first if last called operation is still busy
	// TODO: Collect this into a nice function once caller is C
	if (NotificationDBBusy == false) {
		NotificationDBBusy = true;
		NotificationDBChannels();
		NotificationDBComments();
		NotificationDBBusy = false;
	}

	// Acquire lock
	PyEval_RestoreThread(Save);

	return PyLong_FromLong(0);
}

static PyObject *NotificationDBCheckPython(PyObject *Py_UNUSED(
		Self), PyObject *Py_UNUSED(Args)) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	int Notify = NotificationDBCheck();

	// Acquire lock
	PyEval_RestoreThread(Save);

	return PyLong_FromLong(Notify);
}

// Function names and other python data
static PyMethodDef NotificationDBMethods[] = {
	{"Make", NotificationDBBrewPython, METH_VARARGS, "Brew notifications"},
	{"Check", NotificationDBCheckPython, METH_VARARGS, "Check to notify"},
	{NULL, NULL, 0, NULL}
};

static struct PyModuleDef NotificationDBModule = {
	PyModuleDef_HEAD_INIT, "NotificationDB", "Notifications module", -1,
	NotificationDBMethods, 0, 0, 0, 0
};

// The function name has to end with the module name
PyMODINIT_FUNC PyInit_NotificationDB(void) {
	return PyModule_Create(&NotificationDBModule);
}
