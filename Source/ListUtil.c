/******************************************************************************\
* LBRY-GTK                                                                     *
* Copyright (C) 2021-2023 MorsMortium and Other Contributors                   *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU General Public License as published by         *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                *
* GNU General Public License for more details.                                 *
*                                                                              *
* You should have received a copy of the GNU General Public License            *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
\******************************************************************************/

// Utility functions for List

#include <Python.h>
#include <gtk/gtk.h>
#include <jansson.h>
#include <stdio.h>

#include "Header/Json.h"
#include "Header/ListUtil.h"
#include "Header/Popup.h"
#include "Header/Wallet.h"

int ListUtilListContent(GtkWidget *MainSpace, GtkWidget *Box) {
	// Calculates number of items in list, to fill window

	// Default is 1, if the list item has no height
	gint MinimumHeight;
	gtk_widget_get_preferred_height(Box, &MinimumHeight, NULL);
	if (MinimumHeight == 0) {
		return 1;
	}

	// Available space divide by single item space
	return gtk_widget_get_allocated_height(MainSpace) / MinimumHeight;
}

int ListUtilGridContent(GtkWidget *MainSpace, GtkWidget *Box) {
	// Calculates number of items in grid, to fill window

	// Default is 1, if the list item has no height or width
	gint MinimumHeight, MinimumWidth;
	gtk_widget_get_preferred_height(Box, &MinimumHeight, NULL);
	gtk_widget_get_preferred_width(Box, &MinimumWidth, NULL);
	if (MinimumHeight == 0 || MinimumWidth == 0) {
		return 1;
	}

	// Get number of items horizontally and vertically, multiply them together
	int Horizontal = gtk_widget_get_allocated_width(MainSpace) / MinimumWidth;
	int Vertical = gtk_widget_get_allocated_height(MainSpace) / MinimumHeight;
	return Horizontal * Vertical;
}

json_t *ListUtilWallet(GtkWidget **WalletSpaceParts, int PageSize, int Page,
	char *Server) {
	// Gets transaction history, fills balance subpart labels

	// Get balance
	json_t *Balance = WalletBalance(Server);

	// In case of error, make popup, return transaction history
	json_t *Error = json_object_get(Balance, "error");
	if (Error != NULL) {
		PopupError(json_string_value(Error));
		json_decref(Balance);
		return WalletHistory(PageSize, Page, Server);
	}

	// Get balance subpart items
	const char *Values[6];
	Values[0] = json_string_value(json_object_get(Balance, "total"));
	Values[1] = json_string_value(json_object_get(Balance, "available"));
	Values[2] = json_string_value(json_object_get(Balance, "reserved"));
	json_t *ReservedSubtotals = json_object_get(Balance, "reserved_subtotals");
	Values[3] = json_string_value(json_object_get(ReservedSubtotals, "claims"));
	Values[4] =
		json_string_value(json_object_get(ReservedSubtotals, "supports"));
	Values[5] = json_string_value(json_object_get(ReservedSubtotals, "tips"));

	// Set labels, free Balance, return transaction history
	for (int Index = 0; Index < 6; ++Index) {
		gtk_label_set_label((GtkLabel *) WalletSpaceParts[Index],
			Values[Index]);
	}
	json_decref(Balance);
	return WalletHistory(PageSize, Page, Server);
}

// Everything under this line is removable after full C conversion
static PyObject *ListUtilListContentPython(PyObject *Py_UNUSED(
		Self), PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Fill MainSpacePointer, BoxPointer with sent data
	uintptr_t MainSpacePointer, BoxPointer;
	if (!PyArg_ParseTuple(Args, "LL", &MainSpacePointer, &BoxPointer)) {
		return NULL;
	}

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	int Content =
		ListUtilListContent((GtkWidget *) MainSpacePointer,
			(GtkWidget *) BoxPointer);

	// Acquire lock
	PyEval_RestoreThread(Save);

	return PyLong_FromLong(Content);
}

static PyObject *ListUtilGridContentPython(PyObject *Py_UNUSED(
		Self), PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Fill MainSpacePointer, BoxPointer with sent data
	uintptr_t MainSpacePointer, BoxPointer;
	if (!PyArg_ParseTuple(Args, "LL", &MainSpacePointer, &BoxPointer)) {
		return NULL;
	}

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	int Content =
		ListUtilGridContent((GtkWidget *) MainSpacePointer,
			(GtkWidget *) BoxPointer);

	// Acquire lock
	PyEval_RestoreThread(Save);

	return PyLong_FromLong(Content);
}

static PyObject *ListUtilWalletPython(PyObject *Py_UNUSED(Self),
	PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Get arguments and pass it to the pythonless function
	PyObject *WidgetList;
	int PageSize, Page;
	char *Server;
	if (!PyArg_ParseTuple(Args, "Oiis", &WidgetList, &PageSize, &Page,
		&Server)) {
		return NULL;
	}

	uintptr_t WidgetArray[6];
	for (int Index = 0; Index < 6; ++Index) {
		PyObject *Item = PyList_GetItem(WidgetList, Index);
		WidgetArray[Index] = PyLong_AsLongLong(Item);
	}

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	json_t *JsonData = ListUtilWallet((GtkWidget **) WidgetArray, PageSize,
			Page, Server);

	// Acquire lock
	PyEval_RestoreThread(Save);

	return JsonToString(JsonData);
}

// Function names and other python data
static PyMethodDef ListUtilMethods[] = {
	{"ListContent", ListUtilListContentPython, METH_VARARGS, ""},
	{"GridContent", ListUtilGridContentPython, METH_VARARGS, ""},
	{"Wallet", ListUtilWalletPython, METH_VARARGS, ""},
	{NULL, NULL, 0, NULL}
};

static struct PyModuleDef ListUtilModule = {
	PyModuleDef_HEAD_INIT, "ListUtil", "ListUtil module", -1, ListUtilMethods,
	0, 0, 0, 0
};

// The function name has to end with the module name
PyMODINIT_FUNC PyInit_ListUtil(void) {
	return PyModule_Create(&ListUtilModule);
}
