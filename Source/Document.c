/******************************************************************************\
* LBRY-GTK                                                                     *
* Copyright (C) 2021-2023 MorsMortium and Other Contributors                   *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU General Public License as published by         *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                *
* GNU General Public License for more details.                                 *
*                                                                              *
* You should have received a copy of the GNU General Public License            *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
\******************************************************************************/

// This file deals with Document widget

#include <Python.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <gtk/gtk.h>

#include "Header/Markdown.h"
#include "Header/Replace.h"
#include "Header/Tabs.h"
#include "Header/Document.h"

DocumentData *DocumentCreate(PyObject *AddPage, GtkLabel *Title,
	GtkScrolledWindow *MainSpace, TabsData *TabData) {
	// Sets the data to document data struct

	// Get the document data struct
	DocumentData *Data;
	TabsGet(TabData, Documenter, Data);

	// Get adjustment from mainspace
	GtkAdjustment *Adjustment = gtk_scrolled_window_get_vadjustment(MainSpace);

	// Create markdown data object and add it to document object
	Data->Markdown = MarkdownCreate("", "", false, true, false,
			TabData, AddPage, Adjustment);

	// Other items
	Data->Title = Title;

	return &TabData->Documenter;
}

gboolean DocumentDisplay(gpointer Args) {
	// Gtk function for document

	TabsData *TabData = (TabsData *) Args;
	DocumentData Data = TabData->Documenter;

	// Set made name to label and free memory
	gtk_label_set_text(Data.Title, Data.Name);
	free(Data.Name);

	// Fill markdown field
	MarkdownFill(Data.Markdown);

	// Finally replace the view with document
	ReplaceSet(TabData, "Document");

	return FALSE;
}

// Everything under this line is removable after full C conversion

// Just so we don't need to have the header file include this
PyObject *MarkdownModuleCreatePython(MarkdownData *Data);

static PyObject *DocumentCreatePython(PyObject *Py_UNUSED(Self),
	PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Get arguments and pass it to the pythonless function
	PyObject *AddPage;
	uintptr_t TitlePointer, MainSpacePointer, TabPointer;
	if (!PyArg_ParseTuple(Args, "OLLL", &AddPage, &TitlePointer,
		&MainSpacePointer, &TabPointer)) {
		return NULL;
	}

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	DocumentData *Data = DocumentCreate(AddPage, (GtkLabel *) TitlePointer,
			(GtkScrolledWindow *) MainSpacePointer, (TabsData *) TabPointer);

	// Acquire lock
	PyEval_RestoreThread(Save);

	PyObject *Module = MarkdownModuleCreatePython(Data->Markdown);

	return Module;
}

// Function names and other python data
static PyMethodDef DocumentMethods[] = {
	{"Create", DocumentCreatePython, METH_VARARGS, "Creates document"},
	{NULL, NULL, 0, NULL}
};

static struct PyModuleDef DocumentModule = {
	PyModuleDef_HEAD_INIT, "Document", "Document module", -1, DocumentMethods,
	0, 0, 0, 0
};

// The function name has to end with the module name
PyMODINIT_FUNC PyInit_Document(void) {
	return PyModule_Create(&DocumentModule);
}
