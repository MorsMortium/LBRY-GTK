/******************************************************************************\
* LBRY-GTK                                                                     *
* Copyright (C) 2021-2023 MorsMortium and Other Contributors                   *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU General Public License as published by         *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                *
* GNU General Public License for more details.                                 *
*                                                                              *
* You should have received a copy of the GNU General Public License            *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
\******************************************************************************/

// This file is responsible for generating a full error message from a simple
// message, an optional code and a stacktrace

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "Header/OS.h"
#include "Header/Error.h"

// Warnings that should not be converted to full errors
char ErrorWarnings[1][100] =
{ "has blocked you from replying to their comments" };

char *ErrorGet(int Code, const char *Message) {
	// This function is responsible for generating the error message

	// Go through every warning
	for (int Index = 0; Index < 1; ++Index) {
		// If the warning is found
		if (strstr(Message, ErrorWarnings[Index]) != NULL) {
			// Return malloced message string (for unified behavior)
			char *ErrorString = malloc(strlen(Message) + 1);
			if (ErrorString != NULL) {
				sprintf(ErrorString, "%s", Message);
			}
			return ErrorString;
		}
	}

	// Get sorted stacktrace
	char *FullTrace = OSBBacktrace();

	// Write error message into the full error
	char MessageFormat[] = "Error message: %s\n%s";
	char WithMessage[strlen(FullTrace) + strlen(Message) +
		strlen(MessageFormat)];
	sprintf(WithMessage, MessageFormat, Message, FullTrace);
	free(FullTrace);

	// If Code is -1 (no code), return malloced error
	if (Code == -1) {
		char *ErrorString = malloc(strlen(WithMessage) + 1);
		if (ErrorString != NULL) {
			sprintf(ErrorString, "%s", WithMessage);
		}
		return ErrorString;
	}

	// Write error code into the full error
	char CodeFormat[] = "Error code: %d\n%s";
	char WithCode[strlen(WithMessage) + 15 + strlen(CodeFormat)];
	sprintf(WithCode, CodeFormat, Code, WithMessage);

	// Return malloced error
	char *ErrorString = malloc(strlen(WithCode) + 1);
	if (ErrorString != NULL) {
		sprintf(ErrorString, "%s", WithCode);
	}
	return ErrorString;
}
