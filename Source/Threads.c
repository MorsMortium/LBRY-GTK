/******************************************************************************\
* LBRY-GTK                                                                     *
* Copyright (C) 2021-2023 MorsMortium and Other Contributors                   *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU General Public License as published by         *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                *
* GNU General Public License for more details.                                 *
*                                                                              *
* You should have received a copy of the GNU General Public License            *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
\******************************************************************************/

// Threads for the use of State.c

#include <Python.h>
#include <gtk/gtk.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <pthread.h>
#include <sys/stat.h>
#include <time.h>
#include <jansson.h>

#include "Header/Channel.h"
#include "Header/Document.h"
#include "Header/File.h"
#include "Header/Fill.h"
#include "Header/Global.h"
#include "Header/Json.h"
#include "Header/List.h"
#include "Header/Markdown.h"
#include "Header/OS.h"
#include "Header/Places.h"
#include "Header/Popup.h"
#include "Header/State.h"
#include "Header/Status.h"
#include "Header/Tabs.h"
#include "Header/Url.h"
#include "Header/Threads.h"

// Macro for Json wrapping
#define ThreadsWrapJson(Key, Json) \
		json_t *JsonObjectFor ## Key = json_object(); \
		json_object_set_new(JsonObjectFor ## Key, Key, Json); \
		Json = JsonObjectFor ## Key;

// TODO: Remove these when the parts that are in python are rewritten
#define ThreadsPythonCaller(Call) g_idle_add((GSourceFunc) ThreadsIdlePython, \
			TabData->Threader.Call)
static void ThreadsCallPython(PyObject *Callable, PyObject *Args);
static gboolean ThreadsIdlePython(gpointer Args);
static gboolean ThreadsShowPublicationPython(gpointer Args);

char *ThreadsServer[] = { "Session", "Server", NULL };

void ThreadsToLister(ListData *Lister, int ListName, int ButtonName,
	char *Title, const char *Key, json_t *Items) {
	// Place the data to list

	// Values to struct:
	Lister->Title = Title;
	Lister->ListName = ListName;
	Lister->ListButton = ButtonName;

	// Set json items to struct
	if (Key != NULL) {
		// Wrap the json item(s) inside json object
		ThreadsWrapJson(Key, Items);
		Lister->Json = Items;
	} else {
		// This allows ready set objects to be set
		// and takes care of NULL cases as well
		Lister->Json = Items;
	}
}

void ThreadsFreeArguments(ThreadsArguments *Data) {
	// Free all of the allocated arguments

	if (Data->Name != NULL) {
		free(Data->Name);
	}

	if (Data->Path != NULL) {
		free(Data->Path);
	}

	if (Data->Json != NULL) {
		json_decref(Data->Json);
	}

	free(Data);
}

ThreadsArguments *ThreadsMakeArguments(char *Name, char *Path, char *Key,
	json_t *Json, TabsData *TabData) {
	// Makes the required argument struct used by threads functions

	ThreadsArguments *Data = malloc(sizeof(ThreadsArguments));
	if (Data == NULL) {
		abort();
	}

	// Basic arguments
	Data->TabData = TabData;
	Data->IsState = false;
	Data->Name = NULL;
	Data->Path = NULL;

	// Key, if set, will wrap the Json item here
	if (Key && Json) {
		ThreadsWrapJson(Key, Json);
		Data->Json = Json;
	}

	Data->Json = (Json) ? Json : NULL;

	// Check which arguments are placed to data
	if (Name && Name[0] != '\0') {
		Data->Name = malloc(strlen(Name) + 1);
		strcpy(Data->Name, Name);
	}

	if (Path && Path[0] != '\0') {
		Data->Path = malloc(strlen(Path) + 1);
		strcpy(Data->Path, Path);
	}

	return Data;
}

static void ThreadsArrayHasItems(json_t *Array) {
	// Add default value to array if no items were found

	if (json_array_size(Array) == 0) {
		json_array_append_new(Array, json_string("-1"));
	}
}

static json_t *ThreadsGetChannels(void) {
	// Get channel claim IDs

	// Gets the channels
	json_t *Channels =
		ChannelGet(JsonStringObject(GlobalPreferences, ThreadsServer,
			"http://localhost:5279"));

	// Array to hold claim IDs
	json_t *ClaimIDs = json_array();

	json_t *Item;
	size_t Index;
	json_array_foreach(Channels, Index, Item) {
		json_array_append(ClaimIDs, json_object_get(Item, "Claim"));
	}

	// Check array and add default if empty
	ThreadsArrayHasItems(ClaimIDs);

	return ClaimIDs;
}

static void ThreadsKillAll(TabsData *TabData) {
	// Call the python KillAll method

	ThreadsData *Threader;
	TabsGet(TabData, Threader, Threader);

	ThreadsCallPython(Threader->KillAll, NULL);
}

void *ThreadsStatus(void *Args) {
	// Status thread

	ThreadsArguments *Data = (ThreadsArguments *) Args;
	TabsData *TabData = Data->TabData;

	// Kill all still being fetched data (comments)
	ThreadsKillAll(TabData);

	// Get if we want to save or not
	if (Data->IsState == false) {
		Data->IsState = true;
		StateSave("Status", &TabData->Stater, ThreadsStatus, Args);
	}

	TabsExecutingSet(TabData, false);

	g_idle_add((GSourceFunc) StatusDisplay, TabData);

	return NULL;
}

void *ThreadsDisplay(void *Args) {
	// Display thread

	ThreadsArguments *Data = (ThreadsArguments *) Args;
	TabsData *TabData = Data->TabData;

	// Kill all still being fetched data (comments)
	ThreadsKillAll(TabData);

	char Title[strlen(Data->Name) + 11];
	sprintf(Title, "Document: %s", Data->Name);

	// Get if we want to save or not
	if (Data->IsState == false) {
		Data->IsState = true;
		StateSave(Title, &TabData->Stater, ThreadsDisplay, Args);
	}

	// Check with 100ms delay if the file is still being downloaded
	time_t LastTime;
	struct stat FileStat;
	struct timespec Remaining, Request = { 0, 100000000 };
	do {
		nanosleep(&Request, &Remaining);

		if (stat(Data->Path, &FileStat) != 0) {
			return NULL;
		}

		LastTime = FileStat.st_mtime;
	} while (time(NULL) == LastTime);

	// Read the file to a buffer
	FILE *File = fopen(Data->Path, "r");
	fseek(File, 0, SEEK_END);
	int Length = ftell(File);
	rewind(File);

	// Allocate memory for buffer for persistency
	char *Text = malloc(Length + 1);
	if (Text == NULL) {
		fclose(File);
		TabsExecutingSet(TabData, false);
		return NULL;
	}

	// Read the file to buffer
	int Read = fread(Text, 1, Length, File);
	Text[Length] = '\0';
	fclose(File);
	if (Read != Length) {
		TabsExecutingSet(TabData, false);
		return NULL;
	}

	// Get the directory file is in
	int Place = (int) strlen(Data->Path) - 1;
	while (Data->Path[Place - 1] != OSSeparator[0]) {
		Place--;
	}

	char *Path = malloc(Place + 1);
	if (Path == NULL) {
		free(Text);
		TabsExecutingSet(TabData, false);
		return NULL;
	}
	strncpy(Path, Data->Path, Place);
	Path[Place] = '\0';

	// Add items to markdown data
	DocumentData *Documenter;
	TabsGet(TabData, Documenter, Documenter);
	MarkdownData *Markdowner = Documenter->Markdown;
	if (Markdowner->Text) {
		free(Markdowner->Text);
	}
	Markdowner->Text = Text;

	if (Markdowner->TextData->Path) {
		free(Markdowner->TextData->Path);
	}
	Markdowner->TextData->Path = Path;
	Documenter->Name = malloc(strlen(Title) + 1);
	if (Documenter->Name == NULL) {
		TabsExecutingSet(TabData, false);
		return NULL;
	}

	// Other values to documenter
	strcpy(Documenter->Name, Title);
	Documenter->Markdown = Markdowner;

	TabsExecutingSet(TabData, false);

	g_idle_add((GSourceFunc) DocumentDisplay, TabData);

	return NULL;
}

void *ThreadsNewPublication(void *Args) {
	// New publication thread

	ThreadsArguments *Data = (ThreadsArguments *) Args;
	TabsData *TabData = Data->TabData;

	// Kill all still being fetched data (comments)
	ThreadsKillAll(TabData);

	// Get if we want to save or not
	if (Data->IsState == false) {
		Data->IsState = true;
		StateSave("New Publication", &TabData->Stater, ThreadsNewPublication,
			Args);
	}

	TabsExecutingSet(TabData, false);

	ThreadsPythonCaller(ShowNewPublication);

	return NULL;
}

char *ThreadsTitle[] = { "signing_channel", "value", "title", NULL };
void *ThreadsGetPublication(void *Args) {
	// Get publication thread

	ThreadsArguments *Data = (ThreadsArguments *) Args;
	TabsData *TabData = Data->TabData;

	// Kill all still being fetched data (comments)
	ThreadsKillAll(TabData);

	char *Server = JsonStringObject(GlobalPreferences, ThreadsServer,
			"http://localhost:5279");

	// Get URL json data
	char *Querries[1];
	Querries[0] = Data->Path;
	json_t *UrlDataArray = UrlGet(Querries, 1, Server);
	json_t *UrlData = json_array_get(UrlDataArray, 0);
	json_incref(UrlData);
	json_decref(UrlDataArray);

	// UrlGet may return just a string, so we need to wrap it back to object
	if (json_is_string(UrlData)) {
		json_t *ErrorData = json_object();
		json_object_set_new(ErrorData, "error", UrlData);
		UrlData = ErrorData;
	}

	// Check for errors and stop moving to page if errors were encountered
	json_t *Error = json_object_get(UrlData, "error");
	if (Error) {
		char *ErrorString = (char *) json_string_value(Error);
		PopupError(ErrorString);

		// Print out parsed error message for user in case of repost
		if (strncmp(ErrorString,
			"Error Message: Reposted claim could not be found.",
			49) == 0 || strncmp(ErrorString,
			"Error Message: Could not find channel in", 40) == 0) {
			// 15 first bytes can be skipped
			size_t Length = strlen(ErrorString) - 15;

			char Temporary[Length + 1];
			strcpy(Temporary, 15 + ErrorString);

			// Find the first linebreak and end string there
			int Index = 0;
			while (Temporary[Index] != '\n' && Index < (int) Length - 1) {
				++Index;
			}
			Temporary[Index] = '\0';

			PopupMessage(Temporary);
		}

		json_decref(UrlData);
		ThreadsFreeArguments(Data);
		TabsExecutingSet(TabData, false);
		return NULL;
	}

	char *FirstPart = JsonStringObject(UrlData, ThreadsTitle, "");
	char *SecondPart = JsonStringObject(UrlData, ThreadsTitle + 1, NULL);
	if (SecondPart == NULL) {
		SecondPart = JsonString(json_object_get(UrlData, "name"), "");
	}

	// Create the title for state
	char Title[strlen(FirstPart) + strlen(SecondPart) + 2];
	sprintf(Title, "%s %s", FirstPart, SecondPart);

	// Get if we want to save or not
	if (Data->IsState == false) {
		Data->IsState = true;
		StateSave(Title, &TabData->Stater, ThreadsGetPublication, Args);
	}

	// Data to json so it gets passed around
	TabData->Threader.Json = UrlData;

	TabsExecutingSet(TabData, false);

	g_idle_add((GSourceFunc) ThreadsShowPublicationPython, &TabData->Threader);

	return NULL;
}

void *ThreadsSettings(void *Args) {
	// Settings thread

	ThreadsArguments *Data = (ThreadsArguments *) Args;
	TabsData *TabData = Data->TabData;

	// Kill all still being fetched data (comments)
	ThreadsKillAll(TabData);

	// Get if we want to save or not
	if (Data->IsState == false) {
		Data->IsState = true;
		StateSave("Settings", &TabData->Stater, ThreadsSettings, Args);
	}

	TabsExecutingSet(TabData, false);

	ThreadsPythonCaller(ShowSettings);

	return NULL;
}

void *ThreadsHelp(void *Args) {
	// Help thread

	ThreadsArguments *Data = (ThreadsArguments *) Args;
	TabsData *TabData = Data->TabData;

	// Kill all still being fetched data (comments)
	ThreadsKillAll(TabData);

	// Populate name with approriate name string
	// if not already there
	if (Data->Name == NULL) {
		Data->Name = malloc(8);
		if (Data->Name) {
			sprintf(Data->Name, "Help.md");
		}
	}

	// Populate path with approriate path string
	// if not already there
	if (Data->Path == NULL) {
		Data->Path = malloc(strlen(PlacesHelp) + 9);
		if (Data->Path) {
			sprintf(Data->Path, "%sIndex.md", PlacesHelp);
		}
	}

	// Get if we want to save or not
	if (Data->IsState == false) {
		Data->IsState = true;
		StateSave("Document: Help", &TabData->Stater, ThreadsHelp, Data);
	}

	TabsExecutingSet(TabData, false);

	ThreadsDisplay(Data);

	return NULL;
}

void *ThreadsButtonHelper(void *Args) {
	// Direct calling of button thread

	ThreadsArguments *Data = (ThreadsArguments *) Args;
	TabsData *TabData = Data->TabData;

	// Kill all still being fetched data (comments)
	ThreadsKillAll(TabData);

	// Get if we want to save or not
	if (Data->IsState == false) {
		Data->IsState = true;
		StateSave(Data->Name, &TabData->Stater, ThreadsButtonHelper,
			Args);
	}

	// NOTE: Key is not set, as Json is already wrapped with the key
	// See ThreadsMakeArguments
	ThreadsToLister(&TabData->Lister, ListContentName, ListSearchButton,
		Data->Name, NULL, Data->Json);

	TabsExecutingSet(TabData, false);

	ListButton(TabData);

	return NULL;
}

void *ThreadsWalletHelper(void *Args) {
	// Wallet thread

	ThreadsArguments *Data = (ThreadsArguments *) Args;
	TabsData *TabData = Data->TabData;

	// Kill all still being fetched data (comments)
	ThreadsKillAll(TabData);

	// Get if we want to save or not
	if (Data->IsState == false) {
		Data->IsState = true;
		StateSave("Wallet", &TabData->Stater, ThreadsWalletHelper,
			Args);
	}

	ThreadsToLister(&TabData->Lister, ListWalletName, ListWalletButton,
		"Wallet", NULL, NULL);

	TabsExecutingSet(TabData, false);

	ListButton(TabData);

	return NULL;
}

void *ThreadsDiscover(void *Args) {
	// Discover thread

	ThreadsArguments *Data = (ThreadsArguments *) Args;
	TabsData *TabData = Data->TabData;

	// Get if we want to save or not
	if (Data->IsState == false) {
		Data->IsState = true;
		StateSave("Discover", &TabData->Stater, ThreadsDiscover, Args);
	}

	json_t *JsonArray = json_array();
	json_array_append_new(JsonArray, json_string("trending_mixed"));

	ThreadsToLister(&TabData->Lister, ListSearchButton, ListContentName,
		"Discover", "order_by", JsonArray);

	TabsExecutingSet(TabData, false);

	ListButton(TabData);

	return NULL;
}

void *ThreadsUploads(void *Args) {
	// Uploads thread

	ThreadsArguments *Data = (ThreadsArguments *) Args;
	TabsData *TabData = Data->TabData;

	// Get if we want to save or not
	if (Data->IsState == false) {
		Data->IsState = true;
		StateSave("Uploads", &TabData->Stater, ThreadsUploads, Args);
	}

	json_t *Channels = ThreadsGetChannels();

	ThreadsToLister(&TabData->Lister, ListSearchButton, ListContentName,
		"Uploads", "channel_ids", Channels);

	TabsExecutingSet(TabData, false);

	ListButton(TabData);

	return NULL;
}

void *ThreadsLibrarySearch(void *Args) {
	// Library search thread

	ThreadsArguments *Data = (ThreadsArguments *) Args;
	TabsData *TabData = Data->TabData;

	// Get if we want to save or not
	if (Data->IsState == false) {
		Data->IsState = true;
		StateSave("Library Search", &TabData->Stater, ThreadsLibrarySearch,
			Args);
	}

	char *Server = JsonStringObject(GlobalPreferences, ThreadsServer,
			"http://localhost:5279");

	// Get claim IDs of downloaded content
	size_t Index = 100;
	json_t *Item, *Downloads = NULL;
	json_t *DownloadIDs = json_array();
	for (int Page = 1; Index >= 99; Page++) {
		Downloads = FileList(NULL, 100, Page, Server);

		json_array_foreach(Downloads, Index, Item) {
			json_array_append(DownloadIDs, json_array_get(Item, 4));
		}
		json_decref(Downloads);
	}

	// Check array and add default if empty
	ThreadsArrayHasItems(DownloadIDs);

	ThreadsToLister(&TabData->Lister, ListSearchButton, ListContentName,
		"Library Search", "claim_ids", DownloadIDs);

	TabsExecutingSet(TabData, false);

	ListButton(TabData);

	return NULL;
}

void *ThreadsLibrary(void *Args) {
	// Library thread

	ThreadsArguments *Data = (ThreadsArguments *) Args;
	TabsData *TabData = Data->TabData;

	// Get if we want to save or not
	if (Data->IsState == false) {
		Data->IsState = true;
		StateSave("Library", &TabData->Stater, ThreadsLibrary, Args);
	}

	json_t *EmptyObject = json_object();
	ThreadsToLister(&TabData->Lister, ListFileButton, ListFileName,
		"Library", NULL, EmptyObject);

	TabsExecutingSet(TabData, false);

	ListButton(TabData);

	return NULL;
}

void *ThreadsChannels(void *Args) {
	// Channels thread

	ThreadsArguments *Data = (ThreadsArguments *) Args;
	TabsData *TabData = Data->TabData;

	// Get if we want to save or not
	if (Data->IsState == false) {
		Data->IsState = true;
		StateSave("Channels", &TabData->Stater, ThreadsChannels, Args);
	}

	json_t *Channels = ThreadsGetChannels();

	ThreadsToLister(&TabData->Lister, ListSearchButton, ListContentName,
		"Channels", "claim_ids", Channels);

	TabsExecutingSet(TabData, false);

	ListButton(TabData);

	return NULL;
}

void ThreadsFollow(ThreadsArguments *Data, char *ThreadName, char *Key,
	void *(*Function)(
		void *)) {
	// Function for both Followed and Following since they
	// both use the same routine for the most part

	TabsData *TabData = Data->TabData;

	// Get if we want to save or not
	if (Data->IsState == false) {
		Data->IsState = true;
		StateSave(ThreadName, &TabData->Stater, Function, Data);
	}

	json_t *ChannelIDs = json_array();
	json_t *Item;
	size_t Index;
	json_array_foreach(json_object_get(GlobalPreferences,
		"Following"), Index, Item) {
		json_array_append(ChannelIDs, json_object_get(Item, "uri"));
	}

	// Check array and add default if empty
	ThreadsArrayHasItems(ChannelIDs);

	ThreadsToLister(&TabData->Lister, ListSearchButton, ListContentName,
		ThreadName, Key, ChannelIDs);

	TabsExecutingSet(TabData, false);

	ListButton(TabData);
}

void *ThreadsFollowed(void *Args) {
	// Followed thread

	ThreadsFollow((ThreadsArguments *) Args, "Followed", "claim_ids",
		ThreadsFollowed);

	return NULL;
}

void *ThreadsFollowing(void *Args) {
	// Following thread

	ThreadsFollow((ThreadsArguments *) Args, "Following", "channel_ids",
		ThreadsFollowing);

	return NULL;
}

void *ThreadsCollections(void *Args) {
	// Collections thread

	ThreadsArguments *Data = (ThreadsArguments *) Args;
	TabsData *TabData = Data->TabData;

	// Get if we want to save or not
	if (Data->IsState == false) {
		Data->IsState = true;
		StateSave("Collections", &TabData->Stater, ThreadsCollections, Args);
	}

	json_t *Channels = ThreadsGetChannels();

	ThreadsToLister(&TabData->Lister, ListSearchButton, ListContentName,
		"Collections", "channel_ids", Channels);

	// Add one more item to json object in data
	json_object_set_new(TabData->Lister.Json, "claim_type",
		json_string("collection"));

	TabsExecutingSet(TabData, false);

	ListButton(TabData);

	return NULL;
}

void *ThreadsYourTags(void *Args) {
	// YourTags thread

	ThreadsArguments *Data = (ThreadsArguments *) Args;
	TabsData *TabData = Data->TabData;

	// Get if we want to save or not
	if (Data->IsState == false) {
		Data->IsState = true;
		StateSave("YourTags", &TabData->Stater, ThreadsYourTags, Args);
	}

	// Get tags and copy the array to not change anything in preferences
	json_t *Tags = json_object_get(GlobalPreferences, "TagsShared");
	json_array_extend(Tags, json_object_get(GlobalPreferences, "TagsLocal"));
	Tags = json_copy(Tags);

	// Check array and add default if empty
	ThreadsArrayHasItems(Tags);

	ThreadsToLister(&TabData->Lister, ListSearchButton, ListContentName,
		"Your Tags", "any_tags", Tags);

	TabsExecutingSet(TabData, false);

	ListButton(TabData);

	return NULL;
}

void *ThreadsNative(void *Args) {
	// Native thread

	ThreadsArguments *Data = (ThreadsArguments *) Args;
	TabsData *TabData = Data->TabData;

	// Get if we want to save or not
	if (Data->IsState == false) {
		Data->IsState = true;
		StateSave("Native", &TabData->Stater, ThreadsNative, Args);
	}

	// Get unsorted list of languages
	json_t *LanguageUnsorted = json_object_get(GlobalPreferences, "Languages");

	// Sort all strings to only have the localisation e.g. "fi-FI"
	// Note that we neither need more than 11 places for char array
	// due to the fact our "Languages.json" contains 10 character
	// long string at best
	size_t Index;
	json_t *Value;
	char LanguageString[11];
	json_t *Languages = json_array();
	LanguageString[10] = '\0';
	json_array_foreach(LanguageUnsorted, Index, Value) {
		strncpy(LanguageString, json_string_value(Value), 10);

		// Check string until '/' and add NUL char. We can assume '/' not
		// to be in the first 2 character places. If '/' is not found before
		// NUL char, the loop exits normally and rest of the execution
		// happens as usual without any problems
		for (int Symbol = 1; LanguageString[Symbol] != '\0'; Symbol++) {
			if (LanguageString[Symbol + 1] == '/') {
				LanguageString[Symbol + 1] = '\0';
			}
		}

		json_array_append_new(Languages, json_string(LanguageString));
	}

	// Check array and add default if empty
	ThreadsArrayHasItems(Languages);

	ThreadsToLister(&TabData->Lister, ListSearchButton, ListContentName,
		"Native", "any_languages", Languages);

	TabsExecutingSet(TabData, false);

	ListButton(TabData);

	return NULL;
}

void *ThreadsHome(void *Args) {
	// Home/Default thread

	ThreadsArguments *Data = (ThreadsArguments *) Args;
	TabsData *TabData = Data->TabData;

	// Kill all still being fetched data (comments)
	ThreadsKillAll(TabData);

	char *HomeFunction =
		(char *) json_string_value(json_object_get(GlobalPreferences,
			"HomeFunction"));

	TabsExecutingSet(TabData, false);

	StateImport(HomeFunction, Args);

	return NULL;
}

void *ThreadsInbox(void *Args) {
	// Inbox thread

	ThreadsArguments *Data = (ThreadsArguments *) Args;
	TabsData *TabData = Data->TabData;

	// Get if we want to save or not
	if (Data->IsState == false) {
		Data->IsState = true;
		StateSave("Inbox", &TabData->Stater, ThreadsInbox, Args);
	}

	json_t *EmptyObject = json_object();
	ThreadsToLister(&TabData->Lister, ListInboxButton, ListNotificationName,
		"Inbox", NULL, EmptyObject);

	TabsExecutingSet(TabData, false);

	ListButton(TabData);

	return NULL;
}

bool ThreadsExecute(void *(*Function)(void *), void *Args) {
	// Executes given function with arguments

	ThreadsArguments *Data = (ThreadsArguments *) Args;

	// If one of the threds is still running, just return
	if (TabsIsExecuting(Data->TabData)) {
		return true;
	}

	// Set Tabs to execution state
	TabsExecutingSet(Data->TabData, true);

	pthread_t Thread;
	pthread_create(&Thread, NULL, Function, Args);
	pthread_detach(Thread);

	return false;
}

bool ThreadsMakeExecute(TabsData *TabData, void *(*Function)(
		void *), char *Name, char *Path, char *Key, json_t *Json) {
	// Makes arguments for Thread and executes

	// If one of the threds is still running, just return
	if (TabsIsExecuting(TabData)) {
		return true;
	}

	void *Arguments = ThreadsMakeArguments(Name, Path, Key, Json, TabData);
	ThreadsExecute(Function, Arguments);

	return false;
}

static void ThreadsStater(void *(*Function)(void *), char *Name, int Index) {
	// Places given function with name to statefunctions array

	StateFunctions[Index].Function = Function;
	strcpy(StateFunctions[Index].Name, Name);
}

void ThreadsStart(void) {
	// Add all thread functions to State functions list

	int Index = 0;

	// Place all items to statefunctions array
	ThreadsStater(ThreadsButtonHelper, "Advanced Search", Index++);
	ThreadsStater(ThreadsChannels, "Channels", Index++);
	ThreadsStater(ThreadsCollections, "Collections", Index++);
	ThreadsStater(ThreadsDiscover, "Discover", Index++);
	ThreadsStater(ThreadsDisplay, "Document", Index++);
	ThreadsStater(ThreadsFollowed, "Followed", Index++);
	ThreadsStater(ThreadsFollowing, "Following", Index++);
	ThreadsStater(ThreadsGetPublication, "Publication", Index++);
	ThreadsStater(ThreadsHelp, "Help", Index++);
	ThreadsStater(ThreadsHome, "Home", Index++);
	ThreadsStater(ThreadsInbox, "Inbox", Index++);
	ThreadsStater(ThreadsLibrary, "Library", Index++);
	ThreadsStater(ThreadsLibrarySearch, "Library Search", Index++);
	ThreadsStater(ThreadsNative, "Native", Index++);
	ThreadsStater(ThreadsNewPublication, "NewPublication", Index++);
	ThreadsStater(ThreadsSettings, "Settings", Index++);
	ThreadsStater(ThreadsStatus, "Status", Index++);
	ThreadsStater(ThreadsUploads, "Uploads", Index++);
	ThreadsStater(ThreadsWalletHelper, "Wallet", Index++);
	ThreadsStater(ThreadsYourTags, "YourTags", Index++);

	// Fill rest of the array with NULLs
	while (Index < StateKnownFunctions) {
		ThreadsStater(NULL, "", Index++);
	}
}

// Everything under this line is removable after full C conversion
// >> Python Wrappers >> //
static void ThreadsCallPython(PyObject *Callable, PyObject *Args) {
	// Wraps around python calls

	PyGILState_STATE State = PyGILState_Ensure();

	// Set args here if not set already and call callable
	if (Args == NULL) {
		Args = PyTuple_New(0);
	}

	// Error checking for the time being
	PyObject *Call = PyObject_Call(Callable, Args, NULL);
	if (Call) {
		Py_DECREF(Call);
	} else {
		PyErr_Print();
	}
	Py_DECREF(Args);

	PyGILState_Release(State);
}

static gboolean ThreadsIdlePython(gpointer Args) {
	// Wraps around GTK calls

	ThreadsCallPython(Args, NULL);

	return FALSE;
}

static gboolean ThreadsShowPublicationPython(gpointer Args) {
	// Wrapper for ShowPublication

	ThreadsData *Threader = (ThreadsData *) Args;

	PyGILState_STATE State = PyGILState_Ensure();

	PyObject *Tuple = PyTuple_New(1);
	PyTuple_SET_ITEM(Tuple, 0, JsonParseToPython(Threader->Json));

	PyGILState_Release(State);

	json_decref(Threader->Json);

	ThreadsCallPython(Threader->ShowPublication, Tuple);

	return FALSE;
}

// << Python Wrappers << //

static PyObject *ThreadsLibrarySearchPython(PyObject *Self, PyObject *Py_UNUSED(
		Args)) {
	PyObject *Dict = PyModule_GetDict(Self);
	PyObject *TabPointer = PyDict_GetItemString(Dict, "TabPointer");
	TabsData *TabData = (TabsData *) PyLong_AsLongLong(TabPointer);

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	ThreadsMakeExecute(TabData, ThreadsLibrarySearch, NULL, NULL, NULL, NULL);

	// Acquire lock
	PyEval_RestoreThread(Save);

	Py_RETURN_NONE;
}

static PyObject *ThreadsStatusPython(PyObject *Self,
	PyObject *Py_UNUSED(Args)) {
	PyObject *Dict = PyModule_GetDict(Self);
	PyObject *TabPointer = PyDict_GetItemString(Dict, "TabPointer");
	TabsData *TabData = (TabsData *) PyLong_AsLongLong(TabPointer);

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	ThreadsMakeExecute(TabData, ThreadsStatus, NULL, NULL, NULL, NULL);

	// Acquire lock
	PyEval_RestoreThread(Save);

	Py_RETURN_NONE;
}

// Function names and other python data
static PyMethodDef ThreadsModMethods[] = {
	{"LibrarySearch", (PyCFunction) ThreadsLibrarySearchPython, METH_NOARGS,
	 "Search"},
	{"Status", (PyCFunction) ThreadsStatusPython, METH_NOARGS, "Status"},
	{NULL, NULL, 0, NULL}
};

static struct PyModuleDef ThreadsModModule = {
	PyModuleDef_HEAD_INIT, "ThreadsMod", "Threads moded module", -1,
	ThreadsModMethods, 0, 0, 0, 0
};

static PyObject *ThreadsCreatePython(PyObject *Py_UNUSED(Self),
	PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0
	PyObject *NewPublicationer, *Publicationer, *Settingser;
	uintptr_t TabPointer;
	if (!PyArg_ParseTuple(Args, "OOOL", &NewPublicationer, &Publicationer,
		&Settingser, &TabPointer)) {
		return NULL;
	}

	// Get threads data struct
	TabsData *TabData = (TabsData *) TabPointer;
	ThreadsData *Data;
	TabsGet(TabData, Threader, Data);

	// Set up all callable python objects
	PyObject *Temp;

	Temp = PyObject_GetAttrString(Publicationer, "Commenter");
	Data->KillAll = PyObject_GetAttrString(Temp, "KillAll");
	Data->ShowPublication = PyObject_GetAttrString(Publicationer,
			"ShowPublication");
	Data->ShowNewPublication = PyObject_GetAttrString(NewPublicationer,
			"ShowNewPublication");
	Data->ShowSettings = PyObject_GetAttrString(Settingser, "ShowSettings");

	Py_INCREF(Data->KillAll);
	Py_INCREF(Data->ShowPublication);
	Py_INCREF(Data->ShowNewPublication);
	Py_INCREF(Data->ShowSettings);

	// Create module and add Pointer to it
	PyObject *Module = PyModule_Create(&ThreadsModModule);
	PyModule_AddObject(Module, "TabPointer", PyLong_FromLongLong(TabPointer));

	return Module;
}

void ThreadsFreePython(ThreadsData *Data) {
	// Free allocated memory

	Py_DECREF(Data->KillAll);
	Py_DECREF(Data->ShowPublication);
	Py_DECREF(Data->ShowNewPublication);
	Py_DECREF(Data->ShowSettings);
}

// Function names and other python data
static PyMethodDef ThreadsMethods[] = {
	{"Create", ThreadsCreatePython, METH_VARARGS, "Create"},
	{NULL, NULL, 0, NULL}
};

static struct PyModuleDef ThreadsModule = {
	PyModuleDef_HEAD_INIT, "Threads", "Threads module", -1, ThreadsMethods, 0,
	0, 0, 0
};

// The function name has to end with the module name
PyMODINIT_FUNC PyInit_Threads(void) {
	return PyModule_Create(&ThreadsModule);
}
