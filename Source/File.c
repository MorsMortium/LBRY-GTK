/******************************************************************************\
* LBRY-GTK                                                                     *
* Copyright (C) 2021-2023 MorsMortium and Other Contributors                   *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU General Public License as published by         *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                *
* GNU General Public License for more details.                                 *
*                                                                              *
* You should have received a copy of the GNU General Public License            *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
\******************************************************************************/

// This file deals with file related tasks

#include <Python.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <jansson.h>

#include "Header/GTKExtra.h"
#include "Header/Global.h"
#include "Header/Request.h"
#include "Header/Filter.h"
#include "Header/Url.h"
#include "Header/Json.h"
#include "Header/File.h"

json_t *FileList(void *UNUSED(Unused), int PageSize, int Page, char *Server) {
	// This function lists downloaded files

	json_t *Params = json_object();

	// Set all the values that will always be present
	json_object_set_new(Params, "page_size", json_integer(PageSize));
	json_object_set_new(Params, "page", json_integer(Page));
	json_object_set_new(Params, "completed", json_true());
	json_object_set_new(Params, "sort", json_string("added_on"));

	// Dump resulting parameters object
	char *ParamStr = json_dumps(Params, 0);
	json_decref(Params);

	// Make the request
	char Request[strlen(ParamStr) + 55];
	sprintf(Request, "{\"method\": \"file_list\", \"params\": %s }", ParamStr);

	// Free dumped string
	free(ParamStr);

	// Get result
	json_t *Result = RequestJson(Server, Request);
	if (json_object_get(Result, "error") != NULL) {
		return Result;
	}

	// Get items
	json_t *Value, *Items = json_object_get(Result, "items");
	size_t Index, Length = json_array_size(Items);
	char *Urls[Length];
	json_array_foreach(Items, Index, Value) {
		// Get claim id
		Urls[Index] =
			JsonStringObject(Value, GTKExtraClaimId, GTKExtraMinusOne);
	}

	// Resolve urls, add them to items
	json_t *Resolved = UrlGet(Urls, Length, Server);
	json_array_foreach(Items, Index, Value) {
		json_object_set(Value, "resolved", json_array_get(Resolved, Index));
	}

	json_decref(Resolved);

	// Get parse stuff
	return FilterFiles(Result);
}
