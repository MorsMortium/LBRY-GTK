/******************************************************************************\
* LBRY-GTK                                                                     *
* Copyright (C) 2021-2023 MorsMortium and Other Contributors                   *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU General Public License as published by         *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                *
* GNU General Public License for more details.                                 *
*                                                                              *
* You should have received a copy of the GNU General Public License            *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
\******************************************************************************/

// This file is responsible for creating a set sized Column of set number of
// lines

#include <stdbool.h>
#include <gtk/gtk.h>
#include <stdio.h>

#include "Header/Column.h"

int ColumnUTF8Length(char *String) {
	// This function is responsible for getting the length of an UTF8 string

	int Length = 0;
	while (*String) {
		if ((*String & 0xC0) != 0x80) {
			Length++;
		}
		String++;
	}
	return Length;
}

void ColumnCreate(char *GotText, GtkWidget *Box, int Width, int Lines,
	bool Dots, bool Dashes, bool Force, bool Middle) {
	// This function is responsible for creating a set sized Column of set
	// number of lines

	// Copy text into local variable
	char *Text;
	char TextArray[strlen(GotText) + 1];
	Text = TextArray;
	sprintf(Text, "%s", GotText);

	// Create Column and add it to Box
	GtkWidget *Column = (GtkWidget *) gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
	gtk_container_add((GtkContainer *) Box, Column);

	// Force size, if needed
	if (Force) {
		gtk_widget_set_size_request(Column, Width, -1);
	}

	// Repeat as many times as there are Lines in the Column
	for (int Index = 0; Index < Lines; ++Index) {
		// Get Length of full text, if it is only one, exit
		unsigned int Length = strlen(Text) + 1;
		if (Length == 1) {
			break;
		}

		// Create Label, create fill CurrentText with Text, create TestText
		GtkWidget *Label = gtk_label_new("");
		char CurrentText[Length];
		sprintf(CurrentText, "%s", Text);
		char TestText[Length + 4];

		// Variable keeping whether the dash was used
		bool Dashed = false;

		// Repeat until break
		while (true) {
			// Get Length of current text
			unsigned int CurrentLength = strlen(CurrentText) + 1;

			if (Dots && Index == Lines - 1 && CurrentLength < Length) {
				// If Dots are enabled, we are in the last line, and there is
				// still text, try text + ...

				if (Middle) {
					// If dots should appear in the middle of the text

					// Get half length and extra char in case of odd length
					int SecondHalf = (CurrentLength - 1) / 2;
					int Extra = (CurrentLength - 1) % 2;

					// Add first half
					sprintf(TestText, "%s", CurrentText);

					// Move backward until utf8 character ends
					int FirstHalf = SecondHalf;
					while ((TestText[FirstHalf] & 0xC0) == 0x80) {
						--FirstHalf;
					}

					// Add dots
					sprintf(TestText + FirstHalf, "...");

					// Move forward until utf8 character ends
					while ((CurrentText[SecondHalf + Extra] & 0xC0) == 0x80 ||
						(CurrentText[SecondHalf + Extra + 1] & 0xC0) == 0x80) {
						++SecondHalf;
					}

					// Add second half
					sprintf(TestText + FirstHalf + 3, "%s",
						&CurrentText[SecondHalf + 1]);
				} else {
					sprintf(TestText, "%s...", CurrentText);
				}
				Dashed = false;
			} else if (Dashes && CurrentLength < Length
				&& Text[CurrentLength - 1] != ' '
				&& Text[CurrentLength - 2] != ' ') {
				// If Dashes are enabled there is still text and both current
				// and next characters are not spaces, try text + -
				sprintf(TestText, "%s-", CurrentText);
				Dashed = true;
			} else {
				// Try text
				sprintf(TestText, "%s", CurrentText);
				Dashed = false;
			}

			// Create Layout from Label and TestText
			PangoLayout *Layout = gtk_widget_create_pango_layout(Label,
					TestText);

			// Get Label width
			int GotWidth = 0;
			pango_layout_get_pixel_size(Layout, &GotWidth, NULL);

			// Free layout, if Label width is less than Width, exit the loop
			g_object_unref(Layout);
			if (GotWidth < Width) {
				break;
			}

			// Get UTF8 length of string
			int OldLength = ColumnUTF8Length(CurrentText);

			// Shorten CurrentText with 1 character,
			// until UTF8 length will be less or length is 0
			int NewLength;
			do {
				if (Middle) {
					// From middle

					// Get half length and extra char in case of odd length
					int Half = (CurrentLength - 1) / 2;
					int Extra = (CurrentLength - 1) % 2;

					// Get pointer to middle, overwrite one character
					char *MiddlePoint = &CurrentText[Half + Extra];
					memmove(MiddlePoint, MiddlePoint + 1, strlen(MiddlePoint));
				} else {
					// From end
					CurrentText[strlen(CurrentText) - 1] = '\0';
				}
				NewLength = ColumnUTF8Length(CurrentText);
			} while (OldLength == NewLength && NewLength != 0);

			// If length is 0, exit
			if (NewLength == 0) {
				break;
			}
		}

		// Set Label text, alignment and add it to Column
		gtk_label_set_text((GtkLabel *) Label, TestText);
		gtk_widget_set_halign(Label, GTK_ALIGN_START);
		gtk_container_add((GtkContainer *) Column, (GtkWidget *) Label);

		// Cut off beginning of Text the length of the added text
		// Check length of remaining text
		if (0 < strlen(Text) - strlen(TestText) + 1) {
			// If length is not zero, set text to ending of TestText, minus
			// possible dash character
			Text = Text + strlen(TestText) - Dashed;

			// Move forward until utf8 character ends
			while ((Text[0] & 0xC0) == 0x80) {
				Text = Text + 1;
			}
		} else {
			// If length is zero, empty Text
			Text[0] = '\0';
		}

		// Remove starting space
		if (Text[0] == ' ') {
			Text = Text + 1;
		}
	}
}
