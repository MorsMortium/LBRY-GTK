/******************************************************************************\
* LBRY-GTK                                                                     *
* Copyright (C) 2021-2023 MorsMortium and Other Contributors                   *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU General Public License as published by         *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                *
* GNU General Public License for more details.                                 *
*                                                                              *
* You should have received a copy of the GNU General Public License            *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
\******************************************************************************/

// This file is responsible for dealing with the followed channels

#include <stdio.h>
#include <stdlib.h>
#include <jansson.h>
#include <Python.h>

#include "Header/Preferences.h"
#include "Header/Global.h"
#include "Header/Following.h"

int FollowingGet(char *Channel) {
	// This function returns if not followed (-1) or the index
	// of the followed channel

	// We take the following array
	json_t *Following = json_object_get(GlobalPreferences, "Following");
	if (Following == NULL) {
		return -1;
	}

	size_t ChannelIndex;
	json_t *Item, *Uri;
	const char *StrCompare;
	int Found = 0;

	// Here we check each object in array for same Channel uri
	json_array_foreach(Following, ChannelIndex, Item) {
		// Get the uri of json object block
		Uri = json_object_get(Item, "uri");

		// Make uri into a string so we can use strcmp to compare it
		StrCompare = json_string_value(Uri);

		// If found in the followed channels, we return index
		if (strcmp(StrCompare, Channel) == 0) {
			Found = 1;
			break;
		}
	}

	// Return the result or -1 on not found
	return (Found) ? (int) ChannelIndex : -1;
}

int FollowingSet(char *Channel, char *ConfigPath, int Option) {
	// This function toggles the following of a channel

	// We take the following array
	json_t *Following = json_object_get(GlobalPreferences, "Following");
	if (Following == NULL) {
		return 1;
	}

	// Get the index of the channel
	int ChannelIndex = FollowingGet(Channel);

	// Here we follow (1) or unfollow (0) the channel
	int RC;
	if (Option) {
		// If already in the followed channels, we stop execution
		if (ChannelIndex != -1) {
			return 1;
		}

		// Create new channel object
		json_t *NewFollowed = json_object();

		// Set values for new object
		json_object_set_new(NewFollowed, "notificationsDisabled",
			json_boolean(1));
		RC = json_object_set_new(NewFollowed, "uri",
				json_string(Channel));
		if (RC == -1) {
			return 1;
		}

		// Append to array
		RC = json_array_append_new(Following, NewFollowed);
		if (RC == -1) {
			return 1;
		}
	} else if (ChannelIndex != -1) {
		json_array_remove(Following, ChannelIndex);
	}

	// Now we want to save the new preferences to file
	RC = PreferencesSetClient(ConfigPath, GlobalPreferences);

	return RC;
}

// Everything under this line is removable after full C conversion
static PyObject *FollowingSetPython(PyObject *Py_UNUSED(Self), PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Get arguments and pass it to the pythonless function
	char *Channel = NULL;
	char *ConfigPath = NULL;
	int Option = 0;
	if (!PyArg_ParseTuple(Args, "ssi", &Channel, &ConfigPath, &Option)) {
		return NULL;
	}

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	int RC = FollowingSet(Channel, ConfigPath, Option);

	// Acquire lock
	PyEval_RestoreThread(Save);

	// Change to python compatible, i.e. true if all good, else false
	RC = (RC) ? 0 : 1;

	return PyLong_FromLong(RC);
}

static PyObject *FollowingGetPython(PyObject *Py_UNUSED(Self), PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Get arguments and pass it to the pythonless function
	char *Channel = NULL;
	char *ConfigPath = NULL;
	if (!PyArg_ParseTuple(Args, "ss", &Channel, &ConfigPath)) {
		return NULL;
	}

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	// We want to return a boolean which is also what we should check in
	// C version, in future.
	int ChannelIndex = FollowingGet(Channel);

	// Acquire lock
	PyEval_RestoreThread(Save);

	return PyBool_FromLong(ChannelIndex != -1);
}

// Function names and other python data
static PyMethodDef FollowingMethods[] = {
	{"Follow", FollowingSetPython, METH_VARARGS, "Set channel follow"},
	{"IsFollowed", FollowingGetPython, METH_VARARGS, "Get channel follow"},
	{NULL, NULL, 0, NULL}
};

static struct PyModuleDef FollowingModule = {
	PyModuleDef_HEAD_INIT, "Following", "Following module", -1,
	FollowingMethods, 0, 0, 0, 0
};

// The function name has to end with the module name
PyMODINIT_FUNC PyInit_Following(void) {
	return PyModule_Create(&FollowingModule);
}
