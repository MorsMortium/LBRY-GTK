/******************************************************************************\
* LBRY-GTK                                                                     *
* Copyright (C) 2021-2023 MorsMortium and Other Contributors                   *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU General Public License as published by         *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                *
* GNU General Public License for more details.                                 *
*                                                                              *
* You should have received a copy of the GNU General Public License            *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
\******************************************************************************/

// This file is responsible for getting every system folder used

#include <Python.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <glib-2.0/glib.h>

#include "Header/OS.h"
#include "Header/Places.h"

char *PlacesBin, *PlacesState;
char *PlacesRoot, *PlacesShare, *PlacesLBRYGTK, *PlacesImage, *PlacesTmp;
char *PlacesHelp, *PlacesGlade, *PlacesJson, *PlacesConfig, *PlacesCache;

void PlacesStart(char *GotCurrent) {
	// This function is responsible for filling every system folder used

	// Copy into local variable
	char Current[strlen(GotCurrent) + 1];
	sprintf(Current, "%s", GotCurrent);

	// Remove binary
	//TODO: Change this when crewrite is done, with OSAbsolutePath
	Current[strlen(Current) -
		strlen(strrchr(Current, OSSeparator[0])) + 1] = '\0';
	PlacesBin = malloc(strlen(Current) + 1);
	if (PlacesBin == NULL) {
		abort();
	}
	sprintf(PlacesBin, "%s", Current);

	// Remove bin folder
	Current[strlen(Current) - 1] = '\0';
	Current[strlen(Current) -
		strlen(strrchr(Current, OSSeparator[0])) + 1] = '\0';

	// Get root folder containing share
	PlacesRoot = malloc(strlen(Current) + 1);
	if (PlacesRoot == NULL) {
		abort();
	}
	sprintf(PlacesRoot, "%s", Current);

	// Get share folder
	PlacesShare = malloc(strlen(PlacesRoot) + 7);
	if (PlacesShare == NULL) {
		abort();
	}
	sprintf(PlacesShare, "%sshare" OSSeparator, PlacesRoot);

	// Get lbry-gtk folder
	PlacesLBRYGTK = malloc(strlen(PlacesShare) + 10);
	if (PlacesLBRYGTK == NULL) {
		abort();
	}
	sprintf(PlacesLBRYGTK, "%slbry-gtk" OSSeparator, PlacesShare);

	// Get folder for icons
	PlacesImage = malloc(strlen(PlacesShare) + 29);
	if (PlacesImage == NULL) {
		abort();
	}
	sprintf(PlacesImage, "%sicons" OSSeparator "hicolor" OSSeparator
		"scalable" OSSeparator "apps" OSSeparator, PlacesShare);

	// Get temporary folder
	const char *GotTmp = g_get_tmp_dir();
	PlacesTmp = malloc(strlen(GotTmp) + 11);
	if (PlacesTmp == NULL) {
		abort();
	}
	sprintf(PlacesTmp, "%s" OSSeparator "LBRY-GTK" OSSeparator, GotTmp);

	// Get help article folder
	PlacesHelp = malloc(strlen(PlacesLBRYGTK) + 6);
	if (PlacesHelp == NULL) {
		abort();
	}
	sprintf(PlacesHelp, "%sHelp" OSSeparator, PlacesLBRYGTK);

	// Get glade ui folder
	PlacesGlade = malloc(strlen(PlacesLBRYGTK) + 7);
	if (PlacesGlade == NULL) {
		abort();
	}
	sprintf(PlacesGlade, "%sGlade" OSSeparator, PlacesLBRYGTK);

	// Get json folder
	PlacesJson = malloc(strlen(PlacesLBRYGTK) + 6);
	if (PlacesJson == NULL) {
		abort();
	}
	sprintf(PlacesJson, "%sJson" OSSeparator, PlacesLBRYGTK);

	// Get configuration folder
	const char *GotConfig = g_get_user_config_dir();
	PlacesConfig = malloc(strlen(GotConfig) + 11);
	if (PlacesConfig == NULL) {
		abort();
	}
	sprintf(PlacesConfig, "%s" OSSeparator "LBRY-GTK" OSSeparator, GotConfig);

	// Get cache folder
	const char *GotCache = g_get_user_cache_dir();
	PlacesCache = malloc(strlen(GotCache) + 11);
	if (PlacesCache == NULL) {
		abort();
	}
	sprintf(PlacesCache, "%s" OSSeparator "LBRY-GTK" OSSeparator, GotCache);

	// Get cache folder
	const char *GotState = g_get_user_state_dir();
	PlacesState = malloc(strlen(GotState) + 11);
	if (PlacesState == NULL) {
		abort();
	}
	sprintf(PlacesState, "%s" OSSeparator "LBRY-GTK" OSSeparator, GotState);
}

void PlacesStop(void) {
	// This function is responsible for freeing every string created this file
	free(PlacesBin);
	free(PlacesRoot);
	free(PlacesShare);
	free(PlacesLBRYGTK);
	free(PlacesImage);
	free(PlacesHelp);
	free(PlacesGlade);
	free(PlacesJson);
	free(PlacesConfig);
	free(PlacesCache);
	free(PlacesTmp);
	free(PlacesState);
}

// Everything under this line is blah blah and you know it already
static struct PyModuleDef PlacesModule = {
	PyModuleDef_HEAD_INIT, "Places", "Places module", -1, NULL, 0, 0, 0, 0
};

// The function name has to end with the module name
PyMODINIT_FUNC PyInit_Places(void) {
	PyObject *Module = PyModule_Create(&PlacesModule);

	PyModule_AddObject(Module, "Root", PyUnicode_FromString(PlacesRoot));
	PyModule_AddObject(Module, "ShareDir", PyUnicode_FromString(PlacesShare));
	PyModule_AddObject(Module, "LBRYGTKDir",
		PyUnicode_FromString(PlacesLBRYGTK));
	PyModule_AddObject(Module, "ImageDir", PyUnicode_FromString(PlacesImage));
	PyModule_AddObject(Module, "TmpDir", PyUnicode_FromString(PlacesTmp));
	PyModule_AddObject(Module, "HelpDir", PyUnicode_FromString(PlacesHelp));
	PyModule_AddObject(Module, "GladeDir", PyUnicode_FromString(PlacesGlade));
	PyModule_AddObject(Module, "JsonDir", PyUnicode_FromString(PlacesJson));
	PyModule_AddObject(Module, "ConfigDir", PyUnicode_FromString(PlacesConfig));
	PyModule_AddObject(Module, "CacheDir", PyUnicode_FromString(PlacesCache));
	PyModule_AddObject(Module, "StateDir", PyUnicode_FromString(PlacesState));

	return Module;
}
