/******************************************************************************\
* LBRY-GTK                                                                     *
* Copyright (C) 2021-2023 MorsMortium and Other Contributors                   *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU General Public License as published by         *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                *
* GNU General Public License for more details.                                 *
*                                                                              *
* You should have received a copy of the GNU General Public License            *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
\******************************************************************************/

// This file deals with various commenting interaction

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <jansson.h>
#include <Python.h>

#include "Header/Request.h"
#include "Header/Filter.h"
#include "Header/Url.h"
#include "Header/Json.h"
#include "Header/Comments.h"

// Format used by comment server
#define RPC_LENGTH 80
char *CommentsRPCFormat = "{\"id\" : 1, \"jsonrpc\" : \"2.0\", "
	"\"method\" : \"%s\", \"params\" : { %s }}";

static void CommentsSign(char *ChannelName, unsigned char *Data, char *Server,
	CommentsSignature *Signing) {
	// This function signs the data with channels signature

	// Make a hex string
	int Index = 0;
	char Hexdata[(strlen((char *) Data) * 2) + 1];
	for (int Loop = 0; Data[Loop] != '\0'; Loop++) {
		// Sending escape character may break some things
		if (Data[Loop] == '\\') {
			// \n is return and we need to check for that specifically
			// Others go with the good old "get the latter one"
			if (Data[Loop + 1] == 'n') {
				sprintf((char *) (Hexdata + Index), "0A");
			} else {
				sprintf((char *) (Hexdata + Index), "%02X", Data[Loop + 1]);
			}
			Loop++;
		} else {
			sprintf((char *) (Hexdata + Index), "%02X", Data[Loop]);
		}
		Index += 2;
	}

	// Insert NULL to the end
	Hexdata[Index++] = '\0';

	// Make the request
	char JsonRequest[strlen(ChannelName) + strlen(Hexdata) + 100];
	sprintf(JsonRequest, "{\"method\" : \"channel_sign\", \"params\" : "
		"{ \"channel_name\" : \"%s\", \"hexdata\" "
		":\"%s\"}}", ChannelName, Hexdata);

	// Get result
	json_t *Result = RequestJson(Server, JsonRequest);
	if (json_object_get(Result, "error") != NULL) {
		json_decref(Result);
		sprintf(Signing->Signature, "error");
		return;
	}

	// Get the meaningful items from signature json object
	const char *Signature =
		json_string_value(json_object_get(Result, "signature"));
	const char *SigningTS =
		json_string_value(json_object_get(Result, "signing_ts"));

	// Copy to struct to send off and so we can't lose reference
	// to the strings when using json_decref on result
	sprintf(Signing->Signature, "%s", Signature);
	sprintf(Signing->SigningTS, "%s", SigningTS);

	json_decref(Result);
}

json_t *CommentsSettingGet(char *ChannelID, char *CommentServer) {
	// This function gets the comment related settings

	// Make the params
	char Params[strlen(ChannelID) + 23];
	sprintf(Params, "\"channel_id\" : \"%s\"", ChannelID);

	// Request itself
	char JsonRequest[RPC_LENGTH + strlen(Params) + 12];
	sprintf(JsonRequest, CommentsRPCFormat, "setting.Get", Params);

	// Make the comment server url
	char CommentServerUrl[strlen(CommentServer) + 19];
	sprintf(CommentServerUrl, "%s?m=reaction.React", CommentServer);

	// Error handling is done in the other end
	return RequestJson(CommentServerUrl, JsonRequest);
}

json_t *CommentsReactionPost(char *ChannelName, char *ChannelID,
	char *CommentServer, char *CommentIDs, char *ReactionType, int Remove,
	char *Server) {
	// This function sets a reaction for comment

	json_error_t Error;
	json_t *Default = json_loads("{\"error\": \"not reacted\"}", 0, &Error);

	// Get signature in order to react
	CommentsSignature Signing;
	CommentsSign(ChannelName, (unsigned char *) ChannelName, Server, &Signing);
	if (strcmp(Signing.Signature, "error") == 0) {
		return Default;
	}

	// Compiling the json request string
	char *ExtraParams = "";
	if (Remove) {
		ExtraParams = ", \"remove\" : true";
	} else {
		// This clears the other from dislike/like
		if (ReactionType[0] == 'd') {
			ExtraParams = ", \"clear_types\" : \"like\"";
		} else if (ReactionType[0] == 'l') {
			ExtraParams = ", \"clear_types\" : \"dislike\"";
		}
	}

	// Make the params
	char Params[strlen(ChannelName) + strlen(ChannelID) +
		strlen(CommentIDs) + strlen(ReactionType) +
		sizeof(Signing) + strlen(ExtraParams) + 150];
	sprintf(Params, "\"channel_name\" : \"%s\", \"channel_id\" : \"%s\", "
		"\"comment_ids\" : \"%s\", \"type\" : \"%s\", "
		"\"signature\" : \"%s\", \"signing_ts\" : \"%s\" %s",
		ChannelName, ChannelID, CommentIDs, ReactionType,
		Signing.Signature, Signing.SigningTS, ExtraParams);

	// Request itself
	char JsonRequest[RPC_LENGTH + strlen(Params) + 15];
	sprintf(JsonRequest, CommentsRPCFormat, "reaction.React", Params);

	// Make the comment server url
	char CommentServerUrl[strlen(CommentServer) + 19];
	sprintf(CommentServerUrl, "%s?m=reaction.React", CommentServer);

	// Get result
	json_t *Result = RequestJson(CommentServerUrl, JsonRequest);

	// Error handling is done in the other end
	json_decref(Default);
	return Result;
}

json_t *CommentsReactionList(char *ChannelName, char *ChannelID,
	char *CommentServer, char *CommentIDs, char *Server) {
	// This lists reactions to given comment(s)

	json_error_t Error;
	json_t *Default = json_loads("{\"error\": \"not reacted\"}", 0, &Error);

	// Get signature in order to list reactions for user
	CommentsSignature Signing;
	char FirstParams[sizeof(Signing) + strlen(ChannelName) + strlen(ChannelID) +
		100];
	if (ChannelName && ChannelName[0] != '\0') {
		CommentsSign(ChannelName, (unsigned char *) ChannelName, Server,
			&Signing);
		if (strcmp(Signing.Signature, "error") == 0) {
			return Default;
		}

		sprintf(FirstParams,
			"\"channel_name\" : \"%s\", \"channel_id\" : \"%s\", "
			"\"signature\" : \"%s\", \"signing_ts\" : \"%s\",",
			ChannelName, ChannelID, Signing.Signature,
			Signing.SigningTS);
	} else {
		strcpy(FirstParams, "");
	}

	// Make the params
	char Params[strlen(FirstParams) + strlen(CommentIDs) + 30];
	sprintf(Params, "%s \"comment_ids\" : \"%s\"", FirstParams, CommentIDs);

	// Request itself
	char JsonRequest[RPC_LENGTH + strlen(Params) + 15];
	sprintf(JsonRequest, CommentsRPCFormat, "reaction.List", Params);

	// Make the comment server url
	char CommentServerUrl[strlen(CommentServer) + 19];
	sprintf(CommentServerUrl, "%s?m=reaction.List", CommentServer);

	json_t *Result = RequestJson(CommentServerUrl, JsonRequest);

	// Error handling is done in the other end
	json_decref(Default);
	return Result;
}

json_t *CommentsList(char *ChannelName, char *ChannelID, char *ParentID,
	char *AuthorClaimID, char *ClaimID, char *CommentServer, int SortBy,
	int PageSize, int Page, char *Server) {
	// This function lists comments

	// Compiling the json request string
	// TODO: Do all of this differently but without using json_t blocks
	char ParentIDExtra[strlen(ParentID) + 45];
	char ChannelNameExtra[strlen(ChannelName) + 25];
	char ChannelIDExtra[strlen(ChannelID) + 25];
	char AuthorClaimIDExtra[(strlen(AuthorClaimID) * 2) + strlen(ChannelName)
		+ 310];
	char ClaimIDExtra[strlen(ClaimID) + 25];

	// Check all strings that they are empty and if not empty add arguments
	// to be included with the request string
	if (ParentID[0] != '\0') {
		sprintf(ParentIDExtra, ",\"parent_id\" : \"%s\", \"top_level\" "
			": false ", ParentID);
	} else {
		strcpy(ParentIDExtra, ", \"top_level\" : true ");
	}
	if (ChannelName[0] != '\0') {
		sprintf(ChannelNameExtra, ",\"channel_name\" : \"%s\" ",
			ChannelName);
	} else {
		strcpy(ChannelNameExtra, "");
	}
	if (ChannelID[0] != '\0') {
		sprintf(ChannelIDExtra, ",\"channel_id\" : \"%s\" ", ChannelID);
	} else {
		strcpy(ChannelIDExtra, "");
	}
	if (AuthorClaimID[0] != '\0') {
		// AuthorClaimID requires us to sign the request
		// This is mainly used in comment history fetching
		CommentsSignature Signing;
		CommentsSign(ChannelName, (unsigned char *) ChannelName, Server,
			&Signing);
		sprintf(AuthorClaimIDExtra, ",\"author_claim_id\" : \"%s\", "
			"\"requestor_channel_id\" : \"%s\", "
			"\"requestor_channel_name\" : \"%s\", "
			"\"signature\" : \"%s\", \"signing_ts\" : \"%s\" ",
			AuthorClaimID, AuthorClaimID, ChannelName,
			Signing.Signature, Signing.SigningTS);
	} else {
		strcpy(AuthorClaimIDExtra, "");
	}
	if (ClaimID[0] != '\0') {
		sprintf(ClaimIDExtra, ",\"claim_id\" : \"%s\" ", ClaimID);
	} else {
		strcpy(ClaimIDExtra, "");
	}

	// TODO: SortBy (sorting) to be implemented in the future
	// Make the params
	char Params[strlen(ParentIDExtra) + strlen(AuthorClaimIDExtra) +
		strlen(ClaimIDExtra) + strlen(ChannelNameExtra) +
		strlen(ChannelIDExtra) + 100];
	sprintf(Params, "\"sort_by\" : %d, \"page\": %d, \"page_size\" : %d "
		"%s%s%s%s%s",
		SortBy, Page, PageSize, ParentIDExtra, ChannelNameExtra,
		ChannelIDExtra, AuthorClaimIDExtra, ClaimIDExtra);

	// Request itself
	char JsonRequest[RPC_LENGTH + strlen(Params) + 13];
	sprintf(JsonRequest, CommentsRPCFormat, "comment.List", Params);

	// Make the comment server url
	char CommentServerUrl[strlen(CommentServer) + 18];
	sprintf(CommentServerUrl, "%s?m=comment.List", CommentServer);

	// Get result
	json_t *Result = RequestJson(CommentServerUrl, JsonRequest);
	if (json_object_get(Result, "error") != NULL) {
		return Result;
	}

	// Get the item array out of result
	json_t *Items = json_object_get(Result, "items");

	// While loop here: break out of execution at failure
	while (1) {
		if (Items == NULL) {
			break;
		}

		// If array size is 0 theres no reason to continue
		size_t Length = json_array_size(Items);
		if (Length == 0) {
			break;
		}
		size_t Index;
		char *Querries[Length];
		json_t *Item, *Value;

		// Loop for each url
		json_array_foreach(Items, Index, Value) {
			Item = json_object_get(Value, "channel_url");
			if (Item == NULL) {
				break;
			}
			Querries[Index] = (char *) json_string_value(Item);
		}

		// Send the urls to be resolved
		json_t *Resolved = UrlGet(Querries, Index, Server);
		if (Resolved == NULL) {
			break;
		}
		json_array_foreach(Items, Index, Value) {
			Item = json_array_get(Resolved, Index);
			if (Item == NULL) {
				break;
			}
			json_object_set(Value, "resolved", Item);
		}

		json_decref(Resolved);
		break;
	}

	json_t *TotalPages = json_object_get(Result, "total_pages");
	json_incref(TotalPages);

	json_t *Parsed = FilterComments(Result);
	if (json_object_get(Parsed, "Error") != NULL) {
		json_decref(TotalPages);
		return Parsed;
	}

	// Create the array to be returned
	json_t *ReturnArray = json_array();
	json_array_append_new(ReturnArray, Parsed);
	json_array_append_new(ReturnArray, TotalPages);

	return ReturnArray;
}

json_t *CommentsPost(char *ChannelName, char *ChannelID, char *ParentID,
	char *ClaimID, char *CommentServer, unsigned char *Comment,
	char *SupportTXID, char *Server) {
	// This function posts a comment

	json_error_t Error;
	json_t *Default = json_loads("{\"error\": \"not posted\"}", 0, &Error);

	// Get signature in order to comment
	CommentsSignature Signing;
	CommentsSign(ChannelName, Comment, Server, &Signing);
	if (strcmp(Signing.Signature, "error") == 0) {
		return Default;
	}

	// Compiling the json request string
	char ParentIDExtra[strlen(ParentID) + 25];
	char SupportTXIDExtra[strlen(SupportTXID) + 30];
	if (ParentID[0] != '\0') {
		snprintf(ParentIDExtra, strlen(ParentID) + 25,
			",\"parent_id\" : \"%s\" ", ParentID);
	} else {
		strcpy(ParentIDExtra, "");
	}
	if (SupportTXID[0] != '\0') {
		snprintf(SupportTXIDExtra, strlen(SupportTXID) + 30,
			",\"support_tx_id\" : \"%s\" ", SupportTXID);
	} else {
		strcpy(SupportTXIDExtra, "");
	}

	// Make the params
	char Params[strlen(ChannelName) + strlen(ChannelID) +
		strlen((char *) Comment) + strlen(ClaimID) + sizeof(Signing) +
		strlen(ParentIDExtra) + strlen(SupportTXIDExtra) + 150];
	sprintf(Params, "\"channel_name\" : \"%s\", \"channel_id\" : \"%s\", "
		"\"comment\" : \"%s\", \"claim_id\" : \"%s\", "
		"\"signature\" : \"%s\", \"signing_ts\" : \"%s\" %s%s",
		ChannelName, ChannelID, Comment, ClaimID, Signing.Signature,
		Signing.SigningTS, ParentIDExtra, SupportTXIDExtra);

	// Request itself
	char JsonRequest[RPC_LENGTH + strlen(Params) + 15];
	sprintf(JsonRequest, CommentsRPCFormat, "comment.Create", Params);

	// Make the comment server url
	char CommentServerUrl[strlen(CommentServer) + 20];
	sprintf(CommentServerUrl, "%s?m=comment.Create", CommentServer);

	// Get result
	json_t *Result = RequestJson(CommentServerUrl, JsonRequest);

	// Error handling is done in the other end
	json_decref(Default);
	return Result;
}

json_t *CommentsUpdate(char *ChannelName, char *ChannelID, char *CommentServer,
	unsigned char *Comment, char *CommentID, char *Server) {
	// This function updates previously made comment

	json_error_t Error;
	json_t *Default = json_loads("{\"error\": \"not updated\"}", 0, &Error);

	// Get signature in order to update comment
	CommentsSignature Signing;
	CommentsSign(ChannelName, Comment, Server, &Signing);
	if (strcmp(Signing.Signature, "error") == 0) {
		return Default;
	}

	// Make the params
	char Params[strlen(ChannelName) + strlen(ChannelID) +
		strlen((char *) Comment) + strlen(CommentID) + sizeof(Signing) + 150];
	sprintf(Params, "\"channel_name\" : \"%s\", \"channel_id\" : \"%s\", "
		"\"comment\" : \"%s\", \"comment_id\" : \"%s\", "
		"\"signature\" : \"%s\", \"signing_ts\" : \"%s\"",
		ChannelName, ChannelID, Comment, CommentID, Signing.Signature,
		Signing.SigningTS);

	// Request itself
	char JsonRequest[RPC_LENGTH + strlen(Params) + 13];
	sprintf(JsonRequest, CommentsRPCFormat, "comment.Edit", Params);

	// Make the comment server url
	char CommentServerUrl[strlen(CommentServer) + 18];
	sprintf(CommentServerUrl, "%s?m=comment.Edit", CommentServer);

	// Get result
	json_t *Result = RequestJson(CommentServerUrl, JsonRequest);

	// Error handling is done in the other end
	json_decref(Default);
	return Result;
}

json_t *CommentsDelete(char *ChannelName, char *ChannelID, char *CommentServer,
	char *CommentID, char *Server) {
	// This function deletes made comment

	json_error_t Error;
	json_t *Default = json_loads("{\"error\": \"no deletion\"}", 0, &Error);

	// Get signature in order to update comment
	CommentsSignature Signing;
	CommentsSign(ChannelName, (unsigned char *) CommentID, Server, &Signing);
	if (strcmp(Signing.Signature, "error") == 0) {
		return Default;
	}

	// Make the params
	char Params[strlen(ChannelName) + strlen(ChannelID) +
		strlen(CommentID) + sizeof(Signing) + 130];
	sprintf(Params, "\"channel_name\" : \"%s\", \"channel_id\" : \"%s\", "
		"\"comment_id\" : \"%s\", \"signature\" : \"%s\","
		"\"signing_ts\" : \"%s\"",
		ChannelName, ChannelID, CommentID, Signing.Signature,
		Signing.SigningTS);

	// Request itself
	char JsonRequest[RPC_LENGTH + strlen(Params) + 16];
	sprintf(JsonRequest, CommentsRPCFormat, "comment.Abandon", Params);

	// Make the comment server url
	char CommentServerUrl[strlen(CommentServer) + 21];
	sprintf(CommentServerUrl, "%s?m=comment.Abandon", CommentServer);

	// Get result
	json_t *Result = RequestJson(CommentServerUrl, JsonRequest);

	// Error handling is done in the other end
	json_decref(Default);
	return Result;
}

// Everything under this line is removable after full C conversion
static PyObject *CommentsSettingGetPython(PyObject *Py_UNUSED(
		Self), PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Get arguments and pass it to the pythonless function
	char *ChannelID = NULL;
	char *CommentServer = NULL;
	if (!PyArg_ParseTuple(Args, "ss", &ChannelID, &CommentServer)) {
		return NULL;
	}

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	json_t *JsonData = CommentsSettingGet(ChannelID, CommentServer);

	// Acquire lock
	PyEval_RestoreThread(Save);

	return JsonToString(JsonData);
}

static PyObject *CommentsReactionPostPython(PyObject *Py_UNUSED(
		Self), PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Get arguments and pass it to the pythonless function
	char *ChannelName, *ChannelID, *CommentServer, *CommentIDs, *ReactionType;
	int Remove = 0;
	char *Server = NULL;
	if (!PyArg_ParseTuple(Args, "sssssis", &ChannelName, &ChannelID,
		&CommentServer, &CommentIDs, &ReactionType, &Remove, &Server)) {
		return NULL;
	}

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	json_t *JsonData =
		CommentsReactionPost(ChannelName, ChannelID, CommentServer,
			CommentIDs, ReactionType, Remove, Server);

	// Acquire lock
	PyEval_RestoreThread(Save);

	return JsonToString(JsonData);
}

static PyObject *CommentsReactionListPython(PyObject *Py_UNUSED(
		Self), PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Get arguments and pass it to the pythonless function
	char *ChannelName, *ChannelID, *CommentServer, *CommentIDs, *Server;
	if (!PyArg_ParseTuple(Args, "sssss", &ChannelName, &ChannelID,
		&CommentServer, &CommentIDs, &Server)) {
		return NULL;
	}

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	json_t *JsonData =
		CommentsReactionList(ChannelName, ChannelID, CommentServer, CommentIDs,
			Server);

	// Acquire lock
	PyEval_RestoreThread(Save);

	return JsonToString(JsonData);
}

static PyObject *CommentsListPython(PyObject *Py_UNUSED(Self), PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Get arguments and pass it to the pythonless function
	char *ChannelName, *ChannelID, *ParentID, *ClaimID, *CommentServer;
	int SortBy = 3;
	int PageSize = 5;
	int Page = 1;
	char *Server = NULL;
	if (!PyArg_ParseTuple(Args, "sssssiis", &ChannelName, &ChannelID,
		&ParentID, &ClaimID, &CommentServer, &PageSize, &Page, &Server)) {
		return NULL;
	}

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	json_t *JsonData = CommentsList(ChannelName, ChannelID, ParentID, "",
			ClaimID,
			CommentServer, SortBy, PageSize, Page, Server);

	// Acquire lock
	PyEval_RestoreThread(Save);

	return JsonToString(JsonData);
}

static PyObject *CommentsPostPython(PyObject *Py_UNUSED(Self), PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Get arguments and pass it to the pythonless function
	char *ChannelName, *ChannelID, *ParentID, *ClaimID, *CommentServer,
		*SupportTXID;
	char *Server = NULL;
	unsigned char *Comment;
	if (!PyArg_ParseTuple(Args, "ssssssss", &ChannelName, &ChannelID,
		&ParentID, &ClaimID, &CommentServer, &Comment,
		&SupportTXID, &Server)) {
		return NULL;
	}

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	json_t *JsonData = CommentsPost(ChannelName, ChannelID, ParentID,
			ClaimID, CommentServer, Comment, SupportTXID, Server);

	// Acquire lock
	PyEval_RestoreThread(Save);

	return JsonToString(JsonData);
}

static PyObject *CommentsUpdatePython(PyObject *Py_UNUSED(Self),
	PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Get arguments and pass it to the pythonless function
	char *ChannelName, *ChannelID, *CommentServer, *CommentID;
	char *Server = NULL;
	unsigned char *Comment;
	if (!PyArg_ParseTuple(Args, "ssssss", &ChannelName, &ChannelID,
		&CommentServer, &Comment, &CommentID, &Server)) {
		return NULL;
	}

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	json_t *JsonData = CommentsUpdate(ChannelName, ChannelID, CommentServer,
			Comment, CommentID, Server);

	// Acquire lock
	PyEval_RestoreThread(Save);

	return JsonToString(JsonData);
}

static PyObject *CommentsDeletePython(PyObject *Py_UNUSED(Self),
	PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Get arguments and pass it to the pythonless function
	char *ChannelName, *ChannelID, *CommentServer, *CommentID;
	char *Server = NULL;
	if (!PyArg_ParseTuple(Args, "sssss", &ChannelName, &ChannelID,
		&CommentServer, &CommentID, &Server)) {
		return NULL;
	}

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	json_t *JsonData = CommentsDelete(ChannelName, ChannelID, CommentServer,
			CommentID, Server);

	// Acquire lock
	PyEval_RestoreThread(Save);

	return JsonToString(JsonData);
}

// Function names and other python data
static PyMethodDef CommentsMethods[] = {
	{"SettingGet", CommentsSettingGetPython, METH_VARARGS, "Settings get"},
	{"ReactionReact", CommentsReactionPostPython, METH_VARARGS,
	 "Reaction react"},
	{"ReactionList", CommentsReactionListPython, METH_VARARGS,
	 "Reactions list"},
	{"List", CommentsListPython, METH_VARARGS, "List"},
	{"Post", CommentsPostPython, METH_VARARGS, "Post"},
	{"Update", CommentsUpdatePython, METH_VARARGS, "Update post"},
	{"Delete", CommentsDeletePython, METH_VARARGS, "Delete post"},
	{NULL, NULL, 0, NULL}
};

static struct PyModuleDef CommentsModule = {
	PyModuleDef_HEAD_INIT, "Comments", "Comments module", -1, CommentsMethods,
	0, 0, 0, 0
};

// The function name has to end with the module name
PyMODINIT_FUNC PyInit_Comments(void) {
	return PyModule_Create(&CommentsModule);
}
