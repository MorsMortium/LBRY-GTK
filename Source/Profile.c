/******************************************************************************\
* LBRY-GTK                                                                     *
* Copyright (C) 2021-2023 MorsMortium and Other Contributors                   *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU General Public License as published by         *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                *
* GNU General Public License for more details.                                 *
*                                                                              *
* You should have received a copy of the GNU General Public License            *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
\******************************************************************************/

// This file is responsible for managing the Profile widget

// POSIX.1-2008 compatibility
#define _XOPEN_SOURCE 700

#include <Python.h>
#include <gtk/gtk.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "Header/OS.h"
#include "Header/Global.h"
#include "Header/Places.h"
#include "Header/GTKExtra.h"
#include "Header/Icons.h"
#include "Header/Timestamp.h"
#include "Header/Thumbnail.h"
#include "Header/Column.h"
#include "Header/Popup.h"
#include "Header/Threads.h"
#include "Header/Profile.h"

ProfileData *ProfileCreate(char *ChannelUrl, char *Thumbnail, float ChannelLBC,
	float PostLBC, int UploadNumber, unsigned int Stamp, int Size,
	bool ProfileImage, PyObject *AddPage, char *Title, bool Circled,
	int MaxWidth, TabsData *TabData) {
	// This function is responsible for creating the Profile widget

	// Allocate memory for data, abort if NULL
	ProfileData *Data = malloc(sizeof(ProfileData));
	if (Data == NULL) {
		abort();
	}

	// Add tab data to struct
	Data->TabData = TabData;

	// Create thumbnail
	Data->Thumbnail = ThumbnailCreate(Size, Size, Circled);

	// Get glade file name, load it in builder
	char WidgetFile[strlen(PlacesGlade) + 15];
	sprintf(WidgetFile, "%sProfile.glade", PlacesGlade);
	gtk_builder_add_from_file(GlobalBuilder, WidgetFile, NULL);

	// Set got values
	Data->AddPage = AddPage;

	// Set channel url, if given and possible
	Data->ChannelUrl = NULL;
	if (ChannelUrl != NULL) {
		Data->ChannelUrl = malloc(strlen(ChannelUrl) + 1);
	}
	if (Data->ChannelUrl != NULL) {
		sprintf(Data->ChannelUrl, "%s", ChannelUrl);
	}

	// Set channel title, if given and possible
	Data->Title = NULL;
	if (Title != NULL) {
		Data->Title = malloc(strlen(Title) + 1);
	}
	if (Data->Title != NULL) {
		sprintf(Data->Title, "%s", Title);
	}

	// Get widgets used
	Data->Profile = (GtkWidget *) gtk_builder_get_object(GlobalBuilder,
			"Profile");
	Data->Image = (GtkWidget *) gtk_builder_get_object(GlobalBuilder, "Image");
	Data->Channel = (GtkWidget *) gtk_builder_get_object(GlobalBuilder,
			"Channel");
	Data->Publications =
		(GtkWidget *) gtk_builder_get_object(GlobalBuilder, "Publications");
	Data->ImageActive =
		(GtkWidget *) gtk_builder_get_object(GlobalBuilder, "ImageActive");

	// Set channel name
	int NameWidth = 100000;
	bool Force = MaxWidth != 0;
	if (Force) {
		// If max width is set, get width of button and remove it from max
		gtk_widget_get_preferred_width(Data->Channel, &NameWidth, NULL);

		// TODO: How to know 12
		NameWidth = MaxWidth - NameWidth * 2 - Size - 12;
	}
	GObject *NameWidget = gtk_builder_get_object(GlobalBuilder, "Name");

	// Channel name or placeholder
	if (ChannelUrl != NULL) {
		ColumnCreate(ChannelUrl + 7, (GtkWidget *) NameWidget, NameWidth, 1,
			true, true, Force, true);
	} else {
		ColumnCreate("No Channel", (GtkWidget *) NameWidget, NameWidth, 1,
			true, true, Force, true);
	}

	// Set amount of LBC the channel holds
	GObject *ChannelLBCWidget = gtk_builder_get_object(GlobalBuilder,
			"ChannelLBC");
	GTKExtraNumberLabel((GtkWidget *) ChannelLBCWidget, ChannelLBC);

	// Set amount of publications the channel made
	GObject *UploadLabel = gtk_builder_get_object(GlobalBuilder, "UploadLabel");
	GTKExtraNumberLabel((GtkWidget *) UploadLabel, UploadNumber);

	// Set LBC icon
	GtkImage *LBC1 = (GtkImage *) gtk_builder_get_object(GlobalBuilder, "LBC1");
	gtk_image_set_from_pixbuf(LBC1, IconsLBCLabel);

	GtkLabel *DateWidget = (GtkLabel *) gtk_builder_get_object(GlobalBuilder,
			"Date");

	// Set time passed since post (comment/publication) was made
	char *Passed = TimestampPassed(Stamp);
	gtk_label_set_text(DateWidget, Passed);
	free(Passed);

	// Set full time of posts (comment/publication) making
	char *Date = TimestampGet(Stamp);
	gtk_widget_set_tooltip_text((GtkWidget *) DateWidget, Date);
	free(Date);

	if (ProfileImage) {
		// If profile picture is enabled

		// Add thumbnail widget to holder, set extra data, redraw
		gtk_container_add((GtkContainer *) Data->Image, Data->Thumbnail->Frame);
		ThumbnailSetExtra(Data->Thumbnail, true, Thumbnail);
		gtk_widget_queue_draw(Data->Thumbnail->Frame);
	} else {
		// Remove thumbnail holder from widget
		GObject *ImageBox = gtk_builder_get_object(GlobalBuilder, "ImageBox");
		gtk_widget_hide((GtkWidget *) ImageBox);
	}

	GObject *PostLBCWidget = gtk_builder_get_object(GlobalBuilder, "PostLBC");
	GtkImage *LBC2 = (GtkImage *) gtk_builder_get_object(GlobalBuilder, "LBC2");
	if (PostLBC != 0) {
		// If post (comment/publication) has LBC
		// Set LBC amount/icon
		GTKExtraNumberLabel((GtkWidget *) PostLBCWidget, PostLBC);
		gtk_image_set_from_pixbuf(LBC2, IconsLBCLabel);
	} else {
		// If post (comment/publication) does not have LBC (in case of comments)

		// Hide LBC amount/icon
		gtk_widget_hide((GtkWidget *) PostLBCWidget);
		gtk_widget_hide((GtkWidget *) LBC2);
	}

	// Connect signals with data
	gtk_builder_connect_signals_full(GlobalBuilder, GTKExtraConnect, Data);

	return Data;
}

gboolean ProfileOnChannelButtonPressEvent(G_GNUC_UNUSED GtkWidget *Widget,
	GdkEventButton *Event, ProfileData *Data) {
	// This function is responsible for opening the channel page in current or
	// new page, depending on left or middle mouse button press

	// If url is empty, display message
	if (Data->ChannelUrl == NULL &&
		(Event->button == GDK_BUTTON_PRIMARY ||
		Event->button == GDK_BUTTON_MIDDLE)) {
		PopupMessage("This post does not have a channel.");
		return FALSE;
	}

	if (Event->button == GDK_BUTTON_PRIMARY) {
		// If left button is pressed

		ThreadsMakeExecute(Data->TabData, ThreadsGetPublication, NULL,
			Data->ChannelUrl, NULL, NULL);
	} else if (Event->button == GDK_BUTTON_MIDDLE) {
		// If middle button is pressed
		// Get GIL state
		PyGILState_STATE State = PyGILState_Ensure();

		// Create base arguments
		PyObject *BaseArguments = PyList_New(2);
		PyObject *None = Py_None;
		Py_INCREF(None);
		PyList_SetItem(BaseArguments, 0, None);
		PyList_SetItem(BaseArguments, 1,
			PyUnicode_FromString(Data->ChannelUrl));

		// Create list for function and data, fill it
		PyObject *FinalList = PyList_New(2);
		PyList_SetItem(FinalList, 0, PyUnicode_FromString("Publication"));
		PyList_SetItem(FinalList, 1, BaseArguments);

		// Create arguments with dummy GTK values, call function
		PyObject *Arguments = Py_BuildValue("ssO", ".", "", FinalList);
		Py_DECREF(FinalList);
		Py_DECREF(PyObject_Call(Data->AddPage, Arguments, NULL));

		// Free arguments, release GIL
		Py_DECREF(Arguments);
		PyGILState_Release(State);
	}
	return FALSE;
}

PyObject *ProfileBaseArguments(ProfileData *Data) {
	// This function is responsible for creating arguments for python search
	// functions
	// TODO: Remove this

	// Create search dict, add channel to it
	PyObject *ChannelObject = PyUnicode_FromString(Data->ChannelUrl);
	PyObject *Dict = PyDict_New();
	PyDict_SetItemString(Dict, "channel", ChannelObject);
	Py_DECREF(ChannelObject);

	// Create page title
	char NewTitle[strlen(Data->Title) + 15];
	sprintf(NewTitle, "Publications: %s", Data->Title);

	// Create list
	PyObject *BaseArguments = PyList_New(4);

	// Add new title
	PyObject *Object = PyUnicode_FromString(NewTitle);
	PyList_SetItem(BaseArguments, 0, Object);

	// Add None x2
	Object = Py_None;
	Py_INCREF(Object);
	PyList_SetItem(BaseArguments, 1, Object);
	Object = Py_None;
	Py_INCREF(Object);
	PyList_SetItem(BaseArguments, 2, Object);

	// Add dict containing tags
	PyList_SetItem(BaseArguments, 3, Dict);

	return BaseArguments;
}

gboolean ProfileOnPublicationsButtonPressEvent(G_GNUC_UNUSED GtkWidget *Widget,
	GdkEventButton *Event, ProfileData *Data) {
	// This function is responsible for opening the channel content page in
	// current or new page, depending on left or middle mouse button press

	// If url is empty, display message
	if (Data->ChannelUrl == NULL &&
		(Event->button == GDK_BUTTON_PRIMARY ||
		Event->button == GDK_BUTTON_MIDDLE)) {
		PopupMessage("This post does not have a channel.");
		return FALSE;
	}

	if (Event->button == GDK_BUTTON_PRIMARY) {
		// If left button is pressed
		char NewTitle[strlen(Data->Title) + 15];
		sprintf(NewTitle, "Publications: %s", Data->Title);
		ThreadsMakeExecute(Data->TabData, ThreadsButtonHelper, NewTitle, NULL,
			"channel", json_string(Data->ChannelUrl));
	} else if (Event->button == GDK_BUTTON_MIDDLE) {
		// If middle button is pressed
		// Get GIL state, create base arguments
		PyGILState_STATE State = PyGILState_Ensure();
		PyObject *BaseArguments = ProfileBaseArguments(Data);

		// Create list for function and data, fill it
		PyObject *FinalList = PyList_New(2);
		PyList_SetItem(FinalList, 0, PyUnicode_FromString("Advanced Search"));
		PyList_SetItem(FinalList, 1, BaseArguments);

		// Create arguments with dummy GTK values, call function
		PyObject *Arguments = Py_BuildValue("ssO", ".", "", FinalList);
		Py_DECREF(FinalList);
		PyObject *Call = PyObject_Call(Data->AddPage, Arguments, NULL);
		if (Call) {
			Py_DECREF(Call);
		} else {
			PyErr_Print();
		}

		// Free arguments, release GIL
		Py_DECREF(Arguments);
		PyGILState_Release(State);
	}
	return FALSE;
}

static void *ProfileFreeData(void *Args) {
	// Free profile data

	ProfileData *Data = (ProfileData *) Args;

	// Only free if needed
	if (Data->ChannelUrl != NULL) {
		free(Data->ChannelUrl);
	}
	if (Data->Title != NULL) {
		free(Data->Title);
	}
	free(Data);
	OSMemoryTrimmer();

	return NULL;
}

gboolean ProfileOnProfileDestroy(G_GNUC_UNUSED GtkWidget *Widget,
	ProfileData *Data) {
	// This function is responsible for freeing every data the widget uses

	pthread_t Thread;
	pthread_create(&Thread, NULL, ProfileFreeData, Data);
	pthread_detach(Thread);

	return FALSE;
}

// Everything under this line is removable after full C conversion
static PyObject *ProfileCreatePython(PyObject *Py_UNUSED(Self),
	PyObject *Args) {
	// This function is responsible for collecting python arguments, calling
	// pythonless version, then returning what it returns, or 0

	// Fill data
	uintptr_t TabPointer;
	char *ChannelUrl, *ThumbnailUrl, *Title;
	float ChannelLBC, PostLBC;
	int Rounded, UploadNumber, Size, ProfileImage;
	unsigned int Stamp;
	PyObject *AddPage;
	if (!PyArg_ParseTuple(Args, "ssffiIiiOsiL", &ChannelUrl, &ThumbnailUrl,
		&ChannelLBC, &PostLBC, &UploadNumber, &Stamp, &Size,
		&ProfileImage, &AddPage, &Title, &Rounded, &TabPointer)) {
		return NULL;
	}

	// Thread safe operation begins (release GIL)
	PyThreadState *Save = PyEval_SaveThread();

	if (strlen(ThumbnailUrl) == 0) {
		ThumbnailUrl = NULL;
	}

	ProfileData *Data = ProfileCreate(ChannelUrl, ThumbnailUrl, ChannelLBC,
			PostLBC, UploadNumber, Stamp, Size, (bool) ProfileImage,
			AddPage, Title, (bool) Rounded, 0, (TabsData *) TabPointer);

	// Acquire lock
	PyEval_RestoreThread(Save);

	PyObject *Dict = PyDict_New();

	PyObject *Object = PyLong_FromVoidPtr((void *) Data->Profile);
	PyDict_SetItemString(Dict, "Profile", Object);
	Py_DECREF(Object);

	Object = PyLong_FromVoidPtr((void *) Data->Channel);
	PyDict_SetItemString(Dict, "Channel", Object);
	Py_DECREF(Object);

	Object = PyLong_FromVoidPtr((void *) Data->Publications);
	PyDict_SetItemString(Dict, "Publications", Object);
	Py_DECREF(Object);

	Object = PyLong_FromVoidPtr((void *) Data->ImageActive);
	PyDict_SetItemString(Dict, "ImageActive", Object);
	Py_DECREF(Object);

	PyObject *DictThumbnail = PyDict_New();

	Object = PyLong_FromVoidPtr((void *) Data->Thumbnail->EventBox);
	PyDict_SetItemString(DictThumbnail, "EventBox", Object);
	Py_DECREF(Object);

	Object = PyLong_FromVoidPtr((void *) Data->Thumbnail);
	PyDict_SetItemString(DictThumbnail, "Pointer", Object);
	Py_DECREF(Object);

	PyDict_SetItemString(Dict, "Thumbnailer", DictThumbnail);
	Py_DECREF(DictThumbnail);

	Object = PyLong_FromVoidPtr((void *) Data);
	PyDict_SetItemString(Dict, "Pointer", Object);
	Py_DECREF(Object);

	return Dict;
}

// Function names and other python data
static PyMethodDef ProfileMethods[] = {
	{"Create", ProfileCreatePython, METH_VARARGS, ""},
	{NULL, NULL, 0, NULL}
};

static struct PyModuleDef ProfileModule = {
	PyModuleDef_HEAD_INIT, "Profile", "Profile module", -1, ProfileMethods, 0,
	0, 0, 0
};

// The function name has to end with the module name
PyMODINIT_FUNC PyInit_Profile(void) {
	return PyModule_Create(&ProfileModule);
}
