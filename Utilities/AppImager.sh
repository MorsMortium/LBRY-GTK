#!/usr/bin/env sh

################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2023 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

# LBRY-GTK AppImage build tool:
# Based on Inkscape build script (gtk3) but is HEAVILY modified!
#
# Somehow this is much better than anything I've managed to make
# so big applauds to Inkscape team!
#
# - Erwinjitsu

# Use the UbuntuInstall.sh script first to set working environment

if [[ $(git rev-parse --show-toplevel) != $PWD ]]; then
	echo "You need to run this script from LBRY-GTK repository root"
	exit 1
fi

PY_VER=3.8

########################################################################
# Build LBRY-GTK and install to appdir/
########################################################################

mkdir -p build && cd build
make -C .. DESTDIR=$PWD/appdir install

cat > appdir/AppRun <<EOF
#!/bin/bash

HERE="\$(dirname "\$(readlink -f "\${0}")")"

# Custom AppRun script for LBRY-GTK

export PATH="\${HERE}/usr/bin:\${PATH}"
PLATFORM=x86_64-linux-gnu

# Glib/Gtk environment
export GCONV_PATH="\$HERE/usr/lib/\$PLATFORM/gconv"
export FONTCONFIG_FILE="\$HERE/etc/fonts/fonts.conf"
export GTK_EXE_PREFIX="\$HERE/usr"
export GDK_PIXBUF_MODULEDIR=\$(readlink -f "\$HERE"/usr/lib/\$PLATFORM/gdk-pixbuf-*/*/loaders/ )
export GDK_PIXBUF_MODULE_FILE=\$(readlink -f "\$HERE"/usr/lib/\$PLATFORM/gdk-pixbuf-*/*/loaders.cache ) # Patched to contain no paths
export GI_TYPELIB_PATH="\$HERE/usr/lib/\$PLATFORM/girepository-1.0"

# For bundled themes
export XDG_DATA_DIRS="\$HERE/usr/share:\${XDG_DATA_DIRS:-/usr/local/share:/usr/share}"

# Shared library path
LIBRARY_PATH="\$HERE/lib/\$PLATFORM"
LIBRARY_PATH+=":\$HERE/usr/lib/\$PLATFORM"
LIBRARY_PATH+=":\$HERE/usr/lib"
LIBRARY_PATH+=":\$GDK_PIXBUF_MODULEDIR"

# If user has LD_PRELOAD set the images won't work again
# Workaround is to use the --preload flag for ld.so and unset LD_PRELOAD
LIBRARY_LOAD=\$LD_PRELOAD

# Otherwise getting "Unable to load image-loading module" when opening extension manager
export LD_LIBRARY_PATH="\$GDK_PIXBUF_MODULEDIR"
export LD_PRELOAD=

exec "\$HERE/lib/\$PLATFORM/ld-linux-x86-64.so.2" --inhibit-cache \\
		--library-path "\$LIBRARY_PATH" --preload "\$LIBRARY_LOAD" \\
		"\$HERE/usr/bin/lbry-gtk" "\$@"
EOF

chmod +x appdir/AppRun

# Get lbrynet
wget -nc "https://github.com/lbryio/lbry-sdk/releases/download/v0.113.0/lbrynet-linux.zip"
unzip lbrynet-linux.zip

mv lbrynet ./appdir/usr/bin
cp ./appdir/usr/share/icons/hicolor/scalable/apps/lbry-gtk.svg ./appdir/
cd appdir/

########################################################################
# Bundle everyhting
# to allow the AppImage to run on older systems as well
########################################################################

apt_bundle() {
    apt-get download "$@"
    find *.deb -exec dpkg-deb -x {} . \;
    find *.deb -delete
}

make_ld_launcher() {
    cat > "$2" <<EOF
#!/bin/sh
HERE="\$(dirname "\$(readlink -f "\${0}")")"
exec "\${HERE}/../../lib/x86_64-linux-gnu/ld-linux-x86-64.so.2" "\${HERE}/$1" "\$@"
EOF
    chmod a+x "$2"
}

# Rename lbry-gtk-convert so we can make ld launcher for it as well
mv usr/bin/lbry-gtk-convert usr/bin/lbry-gtk-convert-bin

# Bundle all of glibc; this should eventually be done by linuxdeployqt
apt_bundle libc6

# Make absolutely sure it will not load stuff from /lib or /usr
sed -i -e 's|/usr|/xxx|g' lib/x86_64-linux-gnu/ld-linux-x86-64.so.2

# Bundle Gdk pixbuf loaders without which the bundled Gtk does not work;
# this should eventually be done by linuxdeployqt
apt_bundle librsvg2-common libgdk-pixbuf2.0-0
cp /usr/lib/x86_64-linux-gnu/gdk-pixbuf-*/*/loaders/* usr/lib/x86_64-linux-gnu/gdk-pixbuf-*/*/loaders/
cp /usr/lib/x86_64-linux-gnu/gdk-pixbuf-*/*/loaders.cache usr/lib/x86_64-linux-gnu/gdk-pixbuf-*/*/
sed -i -e 's|/usr/lib/x86_64-linux-gnu/gdk-pixbuf-.*/.*/loaders/||g' usr/lib/x86_64-linux-gnu/gdk-pixbuf-*/*/loaders.cache

# Bundle some libs required by GTK_DEBUG=interactive
cp /usr/lib/x86_64-linux-gnu/libEGL.so.1 usr/lib/
cp /usr/lib/x86_64-linux-gnu/libGLdispatch.so.0 usr/lib/

# Bundle nocsd for those who are using nocsd
apt_bundle libgtk3-nocsd0

# Bundle fontconfig settings
mkdir -p etc/fonts/
cp /etc/fonts/fonts.conf etc/fonts/

# Bundle Python
apt_bundle \
    libpython${PY_VER}-stdlib \
    libpython${PY_VER}-minimal \
    python${PY_VER} \
    python${PY_VER}-minimal \
    python3-cairo \
    python3-gi \
    python3-gi-cairo \
    gir1.2-atk-1.0 \
    gir1.2-glib-2.0 \
    gir1.2-gtk-3.0 \
    gir1.2-gdkpixbuf-2.0 \
    gir1.2-freedesktop \
    gir1.2-pango-1.0
(
    cd usr/bin
    make_ld_launcher "python${PY_VER}" python3
    make_ld_launcher "python${PY_VER}" python
    make_ld_launcher lbry-gtk-convert-bin lbry-gtk-convert
)

# Compile GLib schemas if the subdirectory is present in the AppImage
# AppRun has to export GSETTINGS_SCHEMA_DIR for this to work
apt_bundle gnome-settings-daemon-common
mkdir -p usr/share/glib-2.0/schemas/
cp /usr/share/glib-2.0/schemas/*.gschema.xml usr/share/glib-2.0/schemas/
if [ -d usr/share/glib-2.0/schemas/ ] ; then
  ( cd usr/share/glib-2.0/schemas/ ; glib-compile-schemas . )
fi

cd -

########################################################################
# Generate AppImage
########################################################################

export ARCH=x86_64

wget -c -nv "https://github.com/probonopd/linuxdeployqt/releases/download/continuous/linuxdeployqt-continuous-x86_64.AppImage"
chmod a+x linuxdeployqt-continuous-x86_64.AppImage

linuxdeployqtargs=()

for so in $(find \
    appdir/usr/lib/x86_64-linux-gnu/gdk-pixbuf-*/*/loaders \
    appdir/usr/lib/python3 \
    appdir/usr/lib/python${PY_VER} \
    -name \*.so); do
    linuxdeployqtargs+=("-executable=$(readlink -f "$so")")
done

./linuxdeployqt-continuous-x86_64.AppImage --appimage-extract-and-run appdir/usr/share/applications/lbry-gtk.desktop \
  -appimage -unsupported-bundle-everything -executable=appdir/usr/bin/lbry-gtk \
  -executable=appdir/usr/bin/lbry-gtk-convert-bin \
  -executable=appdir/usr/bin/python${PY_VER} \
  "${linuxdeployqtargs[@]}"
