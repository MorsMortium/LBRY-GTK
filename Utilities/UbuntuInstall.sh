#!/usr/bin/env sh

################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2023 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

# Ubuntu 20.04 (or newer) setup script for LBRY-GTK development
# and to build the newest AppImage

# Update and upgrade for most up-to-date software
sudo apt update && sudo apt upgrade

# All required packages for build and such
sudo apt install \
	git \
	cmake \
	make \
	gcc \
	libgtk-3-dev \
	python3-dev \
	libjansson-dev \
	libsqlite3-dev \
	libcurl4-gnutls-dev \
	libjpeg-dev \
	libwebp-dev \
	python-gi-dev \
	build-essential

# All repositories cloned
git clone https://github.com/mity/md4c.git
git clone https://codeberg.org/MorsMortium/lbry-gtk.git
git clone https://github.com/ImageMagick/ImageMagick.git ImageMagick-7.1.1

( # MD4C build
	cd md4c
	mkdir build
	cd build
	cmake ..
	make
	sudo make install
)

( # ImageMagick build
	cd ImageMagick-7.1.1
	./configure --prefix=/usr --sysconfdir=/etc \
		--enable-shared \
		--disable-static \
		--enable-hdri \
		--with-jxl \
		--with-openjp2 \
		--with-rsvg \
		--with-webp \
		--with-xml \
		--without-autotrace \
		--without-djvu \
		--without-dps \
		--without-fftw \
		--without-fpx \
		--without-gcc-arch \
		--without-gslib \
		--without-gvc \
		--without-magick-plus-plus \
		--without-modules \
		--without-openexr \
		--without-perl \
		--without-tiff \
		--without-wmf
	make
	sudo make install
)

( # LBRY-GTK build
	cd lbry-gtk
	make
)

echo "All done!"
echo "You can now run LBRY-GTK or build the newest AppImage."

